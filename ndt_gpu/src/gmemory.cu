#include <ndt_gpu/gmemory.h>
#include <limits>

namespace gpu {

GMemoryAllocator::GMemoryAllocator()
{
	blocks_.reset(new std::list<GMemoryBlock>);
	blocks_->push_back(GMemoryBlock());
}

GMemoryAllocator::GMemoryAllocator(const GMemoryAllocator &other)
{
	blocks_ = other.blocks_;
}

GMemoryAllocator::GMemoryAllocator(GMemoryAllocator &&other)
{
	blocks_ = other.blocks_;
}

GMemoryAllocator &GMemoryAllocator::operator=(const GMemoryAllocator &other)
{
	release();

	blocks_ = other.blocks_;

	return *this;
}

GMemoryAllocator &GMemoryAllocator::operator=(GMemoryAllocator &&other)
{
	release();

	blocks_ = other.blocks_;

	return *this;
}

gmemErr_t GMemoryAllocator::gmemAlloc(void **ptr, uint64_t size)
{
	gmemErr_t retval = gmemSuccess;
	uint64_t min_free_size = std::numeric_limits<uint64_t>::max();
	auto candidate_it = blocks_->end();

	for (auto it = blocks_->begin(); it != blocks_->end(); ++it) {
		uint64_t candidate_size = it->getAvailableSize(size);

		if (size <= candidate_size && min_free_size > candidate_size) {
			candidate_it = it;
		}
	}

	if (candidate_it != blocks_->end()) {
		candidate_it->gmemAlloc(ptr, size);

		return gmemSuccess;
	}

	// If current blocks have no space, try to allocate a new one
	GMemoryBlock new_block;

	retval = new_block.gmemAlloc(ptr, size);

	if (retval != gmemSuccess) {
		return retval;
	}

	blocks_->push_back(new_block);

	return retval;
}

gmemErr_t GMemoryAllocator::gmemFree(void *ptr)
{
	if (ptr == nullptr) {
		return gmemSuccess;
	}

	for (auto it = blocks_->begin(); it != blocks_->end(); ++it) {
		if (it->gmemFree(ptr) == gmemSuccess) {
			if (it->isEmpty()) {
				blocks_->erase(it);
			}

			return gmemSuccess;
		}
	}

	return gmemErrInvalidPointer;
}

GMemoryAllocator::~GMemoryAllocator()
{
	release();
}

void GMemoryAllocator::release()
{
	blocks_.reset();
}


}
