#include <cmath>
#include <iostream>
#include <pcl/common/transforms.h>
#include <utility>
#include <fstream>

#include <ndt_gpu/debug.h>
#include <ndt_gpu/device_vector.h>
#include <ndt_gpu/utilities.h>
#include <ndt_gpu/NormalDistributionsTransform.h>

namespace gpu
{

template <typename PointSourceType, typename PointTargetType, typename Scalar>
NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
NormalDistributionsTransform(GMemoryAllocator *allocator) :
	BaseRegType(allocator),
	j_ang_(24, 1, __allocator),
	h_ang_(45, 1, __allocator),
	voxel_grid_(new GridType(__allocator)),
	tree_(new TreeType(__allocator))
{
	gauss_d1_ = gauss_d2_ = 0;
	outlier_ratio_ = 0.55;
	step_size_ = 0.1;
	resolution_ = 1.0f;
	trans_probability_ = 0;

	double gauss_c1, gauss_c2, gauss_d3;

	// Initializes the guassian fitting parameters (eq. 6.8) [Magnusson 2009]
	gauss_c1 = 10.0 * (1 - outlier_ratio_);
	gauss_c2 = outlier_ratio_ / pow(resolution_, 3);
	gauss_d3 = -log(gauss_c2);
	gauss_d1_ = -log(gauss_c1 + gauss_c2) - gauss_d3;
	gauss_d2_ = -2 * log((-log(gauss_c1 * exp(-0.5) + gauss_c2) - gauss_d3) / gauss_d1_);

	transformation_epsilon_ = 0.1;
	max_iterations_ = 35;

	real_iterations_ = 0;
	line_search_ = false;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
NormalDistributionsTransform(const NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>& other) :
	BaseRegType(other),
	j_ang_(other.j_ang_),
	h_ang_(other.h_ang_),
	voxel_grid_(other.voxel_grid_),
	tree_(other.tree_)
{
	gauss_d1_ = other.gauss_d1_;
	gauss_d2_ = other.gauss_d2_;

	outlier_ratio_ = other.outlier_ratio_;

	step_size_ = other.step_size_;
	resolution_ = other.resolution_;
	trans_probability_ = other.trans_probability_;
	real_iterations_ = other.real_iterations_;
	line_search_ = other.line_search_;
}

/* Set the input reference points.
 * Build a voxel grid and a search tree on top of the points.
 *
 * Input:
 * 		@input: pointer to the point cloud
 */
template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
setInputTarget(TargetHCPtr &input)
{
  // Copy input map data from the host memory to the GPU memory

	/* Added 2020.08.14 - Not use target_x_, target_y_, target_z_ anymore
	 * for saving space. Actually target points are not necessary to be
	 * retained in the GPU memory the whole time. They are used only for
	 * computing voxels' measurement when setInputTarget. Hence, we remove
	 * the class's member target_x_, target__y_, target_z_.
	 */
	TargetDCPtr target_cloud(new PointCloud<PointTargetType>(__allocator));

	target_cloud->setInputCloud(input);

	setInputTarget(target_cloud);
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
setInputTarget(TargetDCPtr &input)
{
  // Copy input map data from the host memory to the GPU memory

	/* Added 2020.08.14 - Not use target_x_, target_y_, target_z_ anymore
	 * for saving space. Actually target points are not necessary to be
	 * retained in the GPU memory the whole time. They are used only for
	 * computing voxels' measurement when setInputTarget. Hence, we remove
	 * the class's member target_x_, target__y_, target_z_.
	 */
	if (input->size() > 0) {
		voxel_grid_->setResolution(resolution_, resolution_, resolution_);

		// Build a voxel grid on the input target cloud
		voxel_grid_->template setInputCloud<PointTargetType>(input);

		// Build the search tree
		tree_->setInputCloud(input);
	}
}

/* Set the input reference points.
 * Build a voxel grid but NOT build a search tree on top of the points.
 *
 * Input:
 * 		@input: pointer to the point cloud
 */
template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
setInputTargetRaw(TargetHCPtr &input)
{
  // Copy input map data from the host memory to the GPU memory

	/* Added 2020.08.14 - Not use target_x_, target_y_, target_z_ anymore
	 * for saving space. Actually target points are not necessary to be
	 * retained in the GPU memory the whole time. They are used only for
	 * computing voxels' measurement when setInputTarget. Hence, we remove
	 * the class's member target_x_, target__y_, target_z_.
	 */
	TargetDCPtr target_cloud(new PointCloud<PointTargetType>(__allocator));

	target_cloud->setInputCloud(input);

	// Build the voxel grid
	setInputTargetRaw(target_cloud);
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
setInputTargetRaw(TargetDCPtr &input)
{
	// Build the voxel grid
	if (input->size() > 0) {
		voxel_grid_->setResolution(resolution_, resolution_, resolution_);

		voxel_grid_->template setInputCloud<PointTargetType>(input);
	}
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computeTransformation(const Eigen::Matrix<float, 4, 4>& guess)
{
	/* First, copy final_transformation_, transformation_, and previous_transformation_ to
	 * Eigen::Matrix<float, 4, 4> matrices.
	 */
	Eigen::Matrix<float, 4, 4> final_transformation, transformation, previous_transformation;

	this->copyToEigen(final_transformation, final_transformation_);
	this->copyToEigen(transformation, transformation_);
	this->copyToEigen(previous_transformation, previous_transformation_);

	j_ang_.reset();
	h_ang_.reset();

	nr_iterations_ = 0;
	converged_ = false;

	double gauss_c1, gauss_c2, gauss_d3;

	gauss_c1 = 10 * (1 - outlier_ratio_);
	gauss_c2 = outlier_ratio_ / pow(resolution_, 3);
	gauss_d3 = -log(gauss_c2);
	gauss_d1_ = -log(gauss_c1 + gauss_c2) - gauss_d3;
	gauss_d2_ = -2 * log((-log(gauss_c1 * exp(-0.5) + gauss_c2) - gauss_d3) / gauss_d1_);

	if (guess != Eigen::Matrix4f::Identity())
	{
		final_transformation = guess;

		transformPointCloud(*source_cloud_, *trans_cloud_, guess);
	}

	Eigen::Transform<float, 3, Eigen::Affine, Eigen::ColMajor> eig_transformation;
	eig_transformation.matrix() = final_transformation;

	Eigen::Matrix<double, 6, 1> p, delta_p, score_gradient;
	Eigen::Vector3f init_translation = eig_transformation.translation();
	Eigen::Vector3f init_rotation = eig_transformation.rotation().eulerAngles(0, 1, 2);

	p << init_translation(0), init_translation(1), init_translation(2), init_rotation(0), init_rotation(1),
			init_rotation(2);

	Eigen::Matrix<double, 6, 6> hessian;

	double score = 0;
	double delta_p_norm;

	score = computeDerivatives(score_gradient, hessian, trans_cloud_, p);

	int point_number = source_cloud_->size();

	while (!converged_) {
		previous_transformation = transformation;

		Eigen::JacobiSVD<Eigen::Matrix<double, 6, 6>> sv(hessian, Eigen::ComputeFullU | Eigen::ComputeFullV);

		delta_p = sv.solve(-score_gradient);

		delta_p_norm = delta_p.norm();

		if (delta_p_norm == 0 || delta_p_norm != delta_p_norm) {
			trans_probability_ = score / static_cast<double>(point_number);
			converged_ = delta_p_norm == delta_p_norm;

			// Copy temporal Eigen::Matrix<float, 4, 4> transformations to class members
			this->copyFromEigen(final_transformation_, final_transformation);
			this->copyFromEigen(transformation_, transformation);
			this->copyFromEigen(previous_transformation_, previous_transformation);

			return;
		}

		delta_p.normalize();

		delta_p_norm = computeStepLengthMT(p, delta_p, delta_p_norm, step_size_,
											transformation_epsilon_ / 2, score,
											score_gradient, hessian, trans_cloud_,
											final_transformation);
		delta_p *= delta_p_norm;

		Eigen::Translation<float, 3> translation(static_cast<float>(delta_p(0)), static_cast<float>(delta_p(1)),
													static_cast<float>(delta_p(2)));
		Eigen::AngleAxis<float> tmp1(static_cast<float>(delta_p(3)), Eigen::Vector3f::UnitX());
		Eigen::AngleAxis<float> tmp2(static_cast<float>(delta_p(4)), Eigen::Vector3f::UnitY());
		Eigen::AngleAxis<float> tmp3(static_cast<float>(delta_p(5)), Eigen::Vector3f::UnitZ());
		Eigen::AngleAxis<float> tmp4(tmp1 * tmp2 * tmp3);

		transformation = (translation * tmp4).matrix();

		p = p + delta_p;

		// Not update visualizer

		if (nr_iterations_ > max_iterations_ || (nr_iterations_ &&
				(std::fabs(delta_p_norm) < transformation_epsilon_))) {
			converged_ = true;
		}

		nr_iterations_++;
	}

	trans_probability_ = score / static_cast<double>(point_number);

	// Copy temporal Eigen::Matrix<float, 4, 4> transformations to class members
	this->copyFromEigen(final_transformation_, final_transformation);
	this->copyFromEigen(transformation_, transformation);
	this->copyFromEigen(previous_transformation_, previous_transformation);
}

template <typename Scalar>
__global__ void computePointGradient(CloudPtr<Scalar> scattered_source_cloud, int point_num,
                                       double* j_ang, double* pg00, double* pg11, double* pg22,
                                       double* pg13, double* pg23, double* pg04, double* pg14,
                                       double* pg24, double* pg05, double* pg15, double* pg25)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = id; i < point_num; i += stride) {
		// Orignal coordinates
		auto p = static_cast<Vector3d>(scattered_source_cloud[i]);

		// Set the 3x3 block start from (0, 0) to identity matrix
		pg00[i] = 1;
		pg11[i] = 1;
		pg22[i] = 1;

		// Compute point derivatives
		pg13[i] = p.x * j_ang[0] + p.y * j_ang[1] + p.z * j_ang[2];
		pg23[i] = p.x * j_ang[3] + p.y * j_ang[4] + p.z * j_ang[5];
		pg04[i] = p.x * j_ang[6] + p.y * j_ang[7] + p.z * j_ang[8];
		pg14[i] = p.x * j_ang[9] + p.y * j_ang[10] + p.z * j_ang[11];

		pg24[i] = p.x * j_ang[12] + p.y * j_ang[13] + p.z * j_ang[14];
		pg05[i] = p.x * j_ang[15] + p.y * j_ang[16] + p.z * j_ang[17];
		pg15[i] = p.x * j_ang[18] + p.y * j_ang[19] + p.z * j_ang[20];
		pg25[i] = p.x * j_ang[21] + p.y * j_ang[22] + p.z * j_ang[23];
  }
}

/* Computing point hessians */
template <typename Scalar>
__global__ void computePointHessian(CloudPtr<Scalar> scattered_source_cloud, int point_num,
                                     double* h_ang, double* ph93, double* ph103,
                                     double* ph113, double* ph123, double* ph94,
                                     double* ph133, double* ph104, double* ph143,
                                     double* ph114, double* ph153, double* ph95,
                                     double* ph163, double* ph105, double* ph173,
                                     double* ph115, double* ph124, double* ph134,
                                     double* ph144, double* ph154, double* ph125,
                                     double* ph164, double* ph135, double* ph174,
                                     double* ph145, double* ph155, double* ph165,
                                     double* ph175)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = id; i < point_num; i += stride) {
		// Orignal coordinates
		auto p = static_cast<Vector3d>(scattered_source_cloud[i]);

		ph93[i] = 0;
		ph103[i] = p.x * h_ang[0] + p.y * h_ang[1] + p.z * h_ang[2];
		ph113[i] = p.x * h_ang[3] + p.y * h_ang[4] + p.z * h_ang[5];

		ph123[i] = ph94[i] = 0;
		ph133[i] = ph104[i] = p.x * h_ang[6] + p.y * h_ang[7] + p.z * h_ang[8];
		ph143[i] = ph114[i] = p.x * h_ang[9] + p.y * h_ang[10] + p.z * h_ang[11];

		ph153[i] = ph95[i] = 0;
		ph163[i] = ph105[i] = p.x * h_ang[12] + p.y * h_ang[13] + p.z * h_ang[14];
		ph173[i] = ph115[i] = p.x * h_ang[15] + p.y * h_ang[16] + p.z * h_ang[17];

	    ph124[i] = p.x * h_ang[18] + p.y * h_ang[19] + p.z * h_ang[20];
	    ph134[i] = p.x * h_ang[21] + p.y * h_ang[22] + p.z * h_ang[23];
	    ph144[i] = p.x * h_ang[24] + p.y * h_ang[25] + p.z * h_ang[26];

	    ph154[i] = ph125[i] = p.x * h_ang[27] + p.y * h_ang[28] + p.z * h_ang[29];
	    ph164[i] = ph135[i] = p.x * h_ang[30] + p.y * h_ang[31] + p.z * h_ang[32];
	    ph174[i] = ph145[i] = p.x * h_ang[33] + p.y * h_ang[34] + p.z * h_ang[35];

	    ph155[i] = p.x * h_ang[36] + p.y * h_ang[37] + p.z * h_ang[38];
	    ph165[i] = p.x * h_ang[39] + p.y * h_ang[40] + p.z * h_ang[41];
	    ph175[i] = p.x * h_ang[42] + p.y * h_ang[43] + p.z * h_ang[44];
  }
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computePointDerivatives(PointCloud<PointSourceType, Scalar> &scattered_source_cloud,
						MatrixDD &dj_ang, MatrixDD &dh_ang,
						matrix_vector<double> &point_gradients,
						matrix_vector<double> &point_hessians,
						bool compute_hessian)
{
	int scattered_point_num = scattered_source_cloud.size();

	point_gradients.resize(scattered_point_num, 3, 6);

	// Initialize to zero
	fill(point_gradients, 0);

	checkCudaErrors(launchSync<BLOCK_SIZE_X2>(scattered_point_num, computePointGradient,
					scattered_source_cloud.data(), scattered_point_num, dj_ang.buffer(),
					point_gradients(0, 0), point_gradients(1, 1), point_gradients(2, 2),
					point_gradients(1, 3), point_gradients(2, 3), point_gradients(0, 4),
					point_gradients(1, 4), point_gradients(2, 4), point_gradients(0, 5),
					point_gradients(1, 5), point_gradients(2, 5)));

	if (compute_hessian) {
		point_hessians.resize(scattered_point_num, 18, 6);

		// Initialize to zero, too
		fill(point_hessians, 0);

		checkCudaErrors(launchSync<BLOCK_SIZE_X2>(scattered_point_num, computePointHessian,
						scattered_source_cloud.data(), scattered_point_num, dh_ang.buffer(),
						point_hessians(9, 3), point_hessians(10, 3), point_hessians(11, 3),
						point_hessians(12, 3), point_hessians(9, 4), point_hessians(13, 3),
						point_hessians(10, 4), point_hessians(14, 3), point_hessians(11, 4),
						point_hessians(15, 3), point_hessians(9, 5), point_hessians(16, 3),
						point_hessians(10, 5), point_hessians(17, 3), point_hessians(11, 5),
						point_hessians(12, 4), point_hessians(13, 4), point_hessians(14, 4),
						point_hessians(15, 4), point_hessians(12, 5), point_hessians(16, 4),
						point_hessians(13, 5), point_hessians(17, 4), point_hessians(14, 5),
						point_hessians(15, 5), point_hessians(16, 5), point_hessians(17, 5)));
	}
}

/* compute score_inc list for input points.
 * The final score_inc is calculated by a reduction sum
 * on this score_inc list. */
__global__ void computeScoreList(double* e_x_cov_x, int voxel_num, double gauss_d1, double gauss_d2, double* score)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = id; i < voxel_num; i += stride) {
		double score_inc = 0;
		double tmp_ex = e_x_cov_x[i];
		double tmp_inc = -gauss_d1 * tmp_ex;
		double tmp_ex2 = gauss_d2 * tmp_ex;

		if (!(tmp_ex2 > 1 || tmp_ex2 < 0 || tmp_ex2 != tmp_ex2)) {
			score_inc = tmp_inc;
		}

		score[i] = score_inc;

	}
}

/* Compute score gradient list for input points */
__global__ void computeScoreGradientList(CloudPtr<double> trans_cloud, int voxel_num,
											double* e_x_cov_x, double* cov_dxd_pi, double gauss_d1,
											double* score_gradients)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	int col = blockIdx.y;

	if (col < 6) {
		double* sg = score_gradients + col * voxel_num;
		double* cov_dxd_pi_mat0 = cov_dxd_pi + col * voxel_num;
		double* cov_dxd_pi_mat1 = cov_dxd_pi_mat0 + 6 * voxel_num;
		double* cov_dxd_pi_mat2 = cov_dxd_pi_mat1 + 6 * voxel_num;

		for (int i = id; i < voxel_num; i += stride) {
			double tmp_ex = e_x_cov_x[i];
			double tmp_sg = 0.0;

			auto d = trans_cloud[i];

			if (!(tmp_ex > 1 || tmp_ex < 0 || tmp_ex != tmp_ex)) {
				tmp_ex *= gauss_d1;

				tmp_sg = (d.x * cov_dxd_pi_mat0[i] + d.y * cov_dxd_pi_mat1[i] +
							d.z * cov_dxd_pi_mat2[i]) * tmp_ex;
			}

			sg[i] = tmp_sg;
		}
	}
}

/* Intermediate step to compute e_x_cov_x */
__global__ void computeExCovX(CloudPtr<double> trans_cloud, int voxel_num,
								double gauss_d1, double gauss_d2, double* e_x_cov_x,
								double* icov00, double* icov01, double* icov02,
								double* icov10, double* icov11, double* icov12,
								double* icov20, double* icov21, double* icov22)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = id; i < voxel_num; i += stride) {
		auto t = trans_cloud[i];

		e_x_cov_x[i] = exp(-gauss_d2 * ((t.x * icov00[i] + t.y * icov01[i] + t.z * icov02[i]) * t.x +
										((t.x * icov10[i] + t.y * icov11[i] + t.z * icov12[i]) * t.y) +
										((t.x * icov20[i] + t.y * icov21[i] + t.z * icov22[i]) * t.z)) / 2.0);
	}
}

/* update e_x_cov_x - Reusable portion of Equation 6.12 and 6.13 [Magnusson 2009] */
__global__ void updateExCovX(double* e_x_cov_x, double gauss_d2, int valid_voxel_num)
{
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  int stride = blockDim.x * gridDim.x;

  for (int i = id; i < valid_voxel_num; i += stride)
  {
    e_x_cov_x[i] *= gauss_d2;
  }
}

/* compute cov_dxd_pi as reusable portion of Equation 6.12 and 6.13 [Magnusson 2009]*/
__global__ void computeCovDxdPi(double* inverse_covariance, int voxel_num, double gauss_d1, double gauss_d2,
                                	double* point_gradients, double* cov_dxd_pi)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	int row = blockIdx.y;
	int col = blockIdx.z;

	if (row < 3 && col < 6) {
		double* icov0 = inverse_covariance + row * 3 * voxel_num;
		double* icov1 = icov0 + voxel_num;
		double* icov2 = icov1 + voxel_num;
		double* cov_dxd_pi_tmp = cov_dxd_pi + (row * 6 + col) * voxel_num;
		double* pg_tmp0 = point_gradients + col * voxel_num;
		double* pg_tmp1 = pg_tmp0 + 6 * voxel_num;
		double* pg_tmp2 = pg_tmp1 + 6 * voxel_num;

		for (int i = id; i < voxel_num; i += stride) {
			double pg0 = pg_tmp0[i];
			double pg1 = pg_tmp1[i];
			double pg2 = pg_tmp2[i];

			cov_dxd_pi_tmp[i] = icov0[i] * pg0 + icov1[i] * pg1 + icov2[i] * pg2;
		}
	}
}

/* Scatter point coordinates,
 * Output type is double for later subtraction with centroid.
 */
template <typename Scalar0, typename Scalar1>
__global__ void scatterPoints(CloudPtr<Scalar0> in_cloud, int *starting_voxel_id, int in_size,
								CloudPtr<Scalar1> out_cloud)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = id; i < in_size; i += stride) {
		auto p = in_cloud[i];
		int start = starting_voxel_id[i];
		int end = starting_voxel_id[i + 1];

		for (int j = start; j < end; ++j) {
			out_cloud.emplace(j, p);
		}
	}
}

/* First step to compute hessian list for input points */
__global__ void computeHessianListS0(CloudPtr<double> trans_cloud,
										double* icov00, double* icov01, double* icov02,
										double* icov10, double* icov11, double* icov12,
                                     	double* icov20, double* icov21, double* icov22,
                                     	double* point_gradients, double* tmp_hessian,
                                     	int voxel_num)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	int col = blockIdx.y;

	if (col < 6) {
		double* pg_row0 = point_gradients + col * voxel_num;
		double* pg_row1 = pg_row0 + 6 * voxel_num;
		double* pg_row2 = pg_row1 + 6 * voxel_num;
		double* tmp_h = tmp_hessian + col * voxel_num;

		for (int i = id; i < voxel_num; i += stride) {
			auto d = trans_cloud[i];

			// Get point gradients at column col
			double pg0 = pg_row0[i];
			double pg1 = pg_row1[i];
			double pg2 = pg_row2[i];

			tmp_h[i] = d.x * (icov00[i] * pg0 + icov01[i] * pg1 + icov02[i] * pg2) +
	                   d.y * (icov10[i] * pg0 + icov11[i] * pg1 + icov12[i] * pg2) +
	                   d.z * (icov20[i] * pg0 + icov21[i] * pg1 + icov22[i] * pg2);

		}
	}
}

/* Second step to compute hessian list */
__global__ void computeHessianListS1(CloudPtr<double> trans_cloud,
										double gauss_d1, double gauss_d2, double* e_x_cov_x,
										double* tmp_hessian, double* cov_dxd_pi, double* point_gradients,
										double* icov00, double* icov01, double* icov02,
										double* icov10, double* icov11, double* icov12,
										double* icov20, double* icov21, double* icov22,
										double* point_hessians, double* hessians, int voxel_num)
{

	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	int row = blockIdx.y;
	int col = blockIdx.z;

	if (row < 6 && col < 6) {
		double* cov_dxd_pi_row0 = cov_dxd_pi + row * voxel_num;
		double* cov_dxd_pi_row1 = cov_dxd_pi_row0 + 6 * voxel_num;
		double* cov_dxd_pi_row2 = cov_dxd_pi_row1 + 6 * voxel_num;

		double* tmp_h = tmp_hessian + col * voxel_num;
		double* h = hessians + (row * 6 + col) * voxel_num;

		double* pg_row0 = point_gradients + col * voxel_num;
		double* pg_row1 = pg_row0 + 6 * voxel_num;
		double* pg_row2 = pg_row1 + 6 * voxel_num;

		double* tmp_ph0 = point_hessians + ((3 * row) * 6 + col) * voxel_num;
		double* tmp_ph1 = tmp_ph0 + 6 * voxel_num;
		double* tmp_ph2 = tmp_ph1 + 6 * voxel_num;

		for (int i = id; i < voxel_num; i += stride) {
			auto d = trans_cloud[i];

			double ph0 = tmp_ph0[i];
			double ph1 = tmp_ph1[i];
			double ph2 = tmp_ph2[i];

			double tmp_ex = e_x_cov_x[i];

	        if (!(tmp_ex > 1 || tmp_ex < 0 || tmp_ex != tmp_ex)) {
	            double cov_dxd0 = cov_dxd_pi_row0[i];
	            double cov_dxd1 = cov_dxd_pi_row1[i];
	            double cov_dxd2 = cov_dxd_pi_row2[i];

	            tmp_ex *= gauss_d1;

	            double t0 = -gauss_d2 * (d.x * cov_dxd0 + d.y * cov_dxd1 + d.z * cov_dxd2) * tmp_h[i];
	            double t1 = (pg_row0[i] * cov_dxd0 + pg_row1[i] * cov_dxd1 + pg_row2[i] * cov_dxd2);

				double t2 = d.x * (icov00[i] * ph0 + icov01[i] * ph1 + icov02[i] * ph2);
				double t3 = d.y * (icov10[i] * ph0 + icov11[i] * ph1 + icov12[i] * ph2);
				double t4 = d.z * (icov20[i] * ph0 + icov21[i] * ph1 + icov22[i] * ph2);

	            h[i] = (t0 + t1 + t2 + t3 + t4) * tmp_ex;
	        }
		}
	}
}



/* Used to gather centroids and inverse covariances
 * of voxels returned from the radius search. This helps
 * eliminate non-coalesced access at later kernels.
 *
 * Simply apply output[i] = input[voxel_id[i]];
 *
 * The template row_num and col_num specify the shape of the
 * matrix and Scalar is the element type.
 *
 * The index of the element (row, col) is specified by blockIdx.y
 * and blockIdx.z, respectively. Hence, the grid size is
 * (total_block_number, row_num, col_num).
 *
 * Params:
 * 		@input[in]: all matrices of the voxel grid
 * 		@input_num[in]: number of the matrix in the input list
 * 		@voxel_id[in]: the indices of voxels returned from the radius search
 * 		@output_num[in]: number of voxels returned from the radius search
 * 		@output[out]: the output matrix list
 */
template <int row_num, int col_num, typename Scalar>
__global__ void gatherMatrix(Scalar *input, int input_num, int *voxel_id,
								int output_num, Scalar *output)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	int row = blockIdx.y, col = blockIdx.z;

	for (int i = index; i < output_num; i += stride) {
		MatrixDevice<Scalar> in(row_num, col_num, input_num, input + voxel_id[i]);
		MatrixDevice<Scalar> out(row_num, col_num, output_num, output + i);

		out(row, col) = in(row, col);
	}
}

/* Compute trans_x -= centroid */
template <typename Scalar>
__global__ void normalizeCoordinates(CloudPtr<Scalar> in_cloud,
										double *centr_x, double *centr_y, double *centr_z,
										int ele_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		auto p = in_cloud[i];

		p.x -= centr_x[i];
		p.y -= centr_y[i];
		p.z -= centr_z[i];

		in_cloud.emplace(i, p);
	}
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateDerivatives(PointCloud<PointSourceType, double> &scattered_trans_cloud,
					matrix_vector<double> &point_gradients, matrix_vector<double> &point_hessians,
					matrix_vector<double> &icov, Eigen::Matrix<double, 6, 1> &score_gradient,
					Eigen::Matrix<double, 6, 6> &hessian, bool compute_hessian)
{
	int voxel_num = scattered_trans_cloud.size();

	matrix_vector<double> tmp_hessian(voxel_num, 6, 1, __allocator);
	device_vector<double> e_x_cov_x(voxel_num, __allocator);
	matrix_vector<double> cov_dxd_pi(voxel_num, 3, 6, __allocator);

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeExCovX,
					scattered_trans_cloud.data(), voxel_num, gauss_d1_,
					gauss_d2_, e_x_cov_x.data(), icov(0, 0), icov(0, 1),
					icov(0, 2), icov(1, 0), icov(1, 1), icov(1, 2),
					icov(2, 0), icov(2, 1), icov(2, 2)));

	device_vector<double> score(voxel_num, __allocator);

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeScoreList,
					e_x_cov_x.data(), voxel_num, gauss_d1_, gauss_d2_, score.data()));

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, updateExCovX,
					e_x_cov_x.data(), gauss_d2_, voxel_num));

	matrix_vector<double> gradients(voxel_num, 6, 1, __allocator, true);

	checkCudaErrors((launchSync<BLOCK_SIZE_X, 3, 6>(voxel_num, computeCovDxdPi,
					icov.data(), voxel_num, gauss_d1_, gauss_d2_, point_gradients.data(),
					cov_dxd_pi.data())));

	checkCudaErrors((launchSync<BLOCK_SIZE_X, 6, 1>(voxel_num, computeScoreGradientList,
					scattered_trans_cloud.data(), voxel_num, e_x_cov_x.data(),
					cov_dxd_pi.data(), gauss_d1_, gradients.data())));

	if (compute_hessian) {
		matrix_vector<double> hessians(voxel_num, 6, 6, __allocator, true);

		checkCudaErrors((launchASync<BLOCK_SIZE_X, 6, 1>(voxel_num, computeHessianListS0,
						scattered_trans_cloud.data(), icov(0, 0), icov(0, 1), icov(0, 2),
						icov(1, 0), icov(1, 1), icov(1, 2), icov(2, 0), icov(2, 1), icov(2, 2),
						point_gradients.data(), tmp_hessian.data(), voxel_num)));

		checkCudaErrors((launchSync<BLOCK_SIZE_X, 6, 6>(voxel_num, computeHessianListS1,
						scattered_trans_cloud.data(), gauss_d1_, gauss_d2_, e_x_cov_x.data(),
						tmp_hessian.data(), cov_dxd_pi.data(), point_gradients.data(),
						icov(0, 0), icov(0, 1), icov(0, 2), icov(1, 0), icov(1, 1), icov(1, 2),
						icov(2, 0), icov(2, 1), icov(2, 2), point_hessians.data(), hessians.data(),
						voxel_num)));

		MatrixHost<double> hhess(6, 6, __allocator);

		reductionSum(hessians, hhess);

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++)	{
				hessian(i, j) = hhess(i, j);
			}
		}
	}

	checkCudaErrors(cudaDeviceSynchronize());

	MatrixHost<double> hgrad(gradients.rowNum(), gradients.colNum(), __allocator);

	reductionSum(gradients, hgrad);

	for (int i = 0; i < 6; i++) {
		score_gradient(i) = hgrad(i);
	}

	double score_inc;

	reductionSum(score, score_inc);

	return score_inc;
}


template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateHessians(PointCloud<PointSourceType, double> &scattered_trans_cloud,
					matrix_vector<double> &point_gradients, matrix_vector<double> &point_hessians,
					matrix_vector<double> &icov, Eigen::Matrix<double, 6, 6> &hessian)
{
	int voxel_num = scattered_trans_cloud.size();

	matrix_vector<double> tmp_hessian(voxel_num, 6, 1, __allocator);
	device_vector<double> e_x_cov_x(voxel_num, __allocator);
	matrix_vector<double> cov_dxd_pi(voxel_num, 3, 6, __allocator);

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeExCovX,
					scattered_trans_cloud.data(), voxel_num, gauss_d1_,
					gauss_d2_, e_x_cov_x.data(), icov(0, 0), icov(0, 1), icov(0, 2),
					icov(1, 0), icov(1, 1), icov(1, 2), icov(2, 0), icov(2, 1), icov(2, 2)));

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, updateExCovX,
					e_x_cov_x.data(), gauss_d2_, voxel_num));

	checkCudaErrors((launchASync<BLOCK_SIZE_X, 3, 6>(voxel_num, computeCovDxdPi,
					icov.data(), voxel_num, gauss_d1_, gauss_d2_, point_gradients.data(),
					cov_dxd_pi.data())));

	matrix_vector<double> hessians(voxel_num, 6, 6, __allocator, true);

	checkCudaErrors((launchASync<BLOCK_SIZE_X, 6, 1>(voxel_num, computeHessianListS0,
					scattered_trans_cloud.data(), icov(0, 0), icov(0, 1), icov(0, 2),
					icov(1, 0), icov(1, 1), icov(1, 2), icov(2, 0), icov(2, 1), icov(2, 2),
					point_gradients.data(), tmp_hessian.data(), voxel_num)));

	checkCudaErrors((launchASync<BLOCK_SIZE_X, 6, 6>(voxel_num, computeHessianListS1,
					scattered_trans_cloud.data(), gauss_d1_, gauss_d2_, e_x_cov_x.data(),
					tmp_hessian.data(), cov_dxd_pi.data(), point_gradients.data(),
					icov(0, 0), icov(0, 1), icov(0, 2), icov(1, 0), icov(1, 1), icov(1, 2),
					icov(2, 0), icov(2, 1), icov(2, 2), point_hessians.data(), hessians.data(),
					voxel_num)));

	checkCudaErrors(cudaDeviceSynchronize());


	MatrixHost<double> hhess(6, 6, __allocator);

	reductionSum(hessians, hhess);

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++)	{
			hessian(i, j) = hhess(i, j);
		}
	}
}


/* Given a transformed point cloud, compute derivatives and return the score.
 *
 * Input:
 * 		@trans_x, trans_y, trans_z: the transformed point cloud
 * 		@points_num: number of transformed points
 * 		@pose: pose
 * 		@compute_hessian: compute hessians or not
 *
 * Output:
 * 		@score_gradient, hessian: output derivatives
 *
 * Return: score
 *
 */
template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computeDerivatives(Eigen::Matrix<double, 6, 1>& score_gradient,
					Eigen::Matrix<double, 6, 6>& hessian, SourceDCPtr &trans_cloud,
                    Eigen::Matrix<double, 6, 1>& pose, bool compute_hessian)
{
	score_gradient.setZero();
	hessian.setZero();

	// Compute Angle Derivatives
	computeAngleDerivatives(pose);

	MatrixDD dj_ang(j_ang_);
	MatrixDD dh_ang(h_ang_);

	// Inverse covariances of neighbor voxels
	matrix_vector<double> icov(__allocator);

	// The transformed clouds shifted toward centroids of neighbor voxels
	PointCloud<PointSourceType, double> scattered_trans_cloud(__allocator);
	// The original source clouds
	PointCloud<PointSourceType, Scalar> scattered_source_cloud(__allocator);

	// Radius search and decompose results
	int voxel_num = preprocessing(trans_cloud->data(), scattered_trans_cloud,
									scattered_source_cloud, icov);

	if (voxel_num <= 0) {
		return 0;
	}

	// Compute score gradient and hessian matrix
	matrix_vector<double> point_gradients(__allocator);
	matrix_vector<double> point_hessians(__allocator);

	computePointDerivatives(scattered_source_cloud, dj_ang, dh_ang, point_gradients,
							point_hessians, compute_hessian);

	// TODO: check if voxel_num = tx.size()?
	double score_inc = updateDerivatives(scattered_trans_cloud, point_gradients, point_hessians,
											icov, score_gradient, hessian, compute_hessian);

	return score_inc;
}



template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computeAngleDerivatives(Eigen::Matrix<double, 6, 1>& pose, bool compute_hessian)
{
  double cx, cy, cz, sx, sy, sz;

  if (fabs(pose(3)) < 10e-5)
  {
    cx = 1.0;
    sx = 0.0;
  }
  else
  {
    cx = cos(pose(3));
    sx = sin(pose(3));
  }

  if (fabs(pose(4)) < 10e-5)
  {
    cy = 1.0;
    sy = 0.0;
  }
  else
  {
    cy = cos(pose(4));
    sy = sin(pose(4));
  }

  if (fabs(pose(5)) < 10e-5)
  {
    cz = 1.0;
    sz = 0.0;
  }
  else
  {
    cz = cos(pose(5));
    sz = sin(pose(5));
  }

  j_ang_(0) = -sx * sz + cx * sy * cz;
  j_ang_(1) = -sx * cz - cx * sy * sz;
  j_ang_(2) = -cx * cy;

  j_ang_(3) = cx * sz + sx * sy * cz;
  j_ang_(4) = cx * cz - sx * sy * sz;
  j_ang_(5) = -sx * cy;

  j_ang_(6) = -sy * cz;
  j_ang_(7) = sy * sz;
  j_ang_(8) = cy;

  j_ang_(9) = sx * cy * cz;
  j_ang_(10) = -sx * cy * sz;
  j_ang_(11) = sx * sy;

  j_ang_(12) = -cx * cy * cz;
  j_ang_(13) = cx * cy * sz;
  j_ang_(14) = -cx * sy;

  j_ang_(15) = -cy * sz;
  j_ang_(16) = -cy * cz;
  j_ang_(17) = 0;

  j_ang_(18) = cx * cz - sx * sy * sz;
  j_ang_(19) = -cx * sz - sx * sy * cz;
  j_ang_(20) = 0;

  j_ang_(21) = sx * cz + cx * sy * sz;
  j_ang_(22) = cx * sy * cz - sx * sz;
  j_ang_(23) = 0;

  j_ang_.syncGpu();

  if (compute_hessian)
  {
    h_ang_(0) = -cx * sz - sx * sy * cz;
    h_ang_(1) = -cx * cz + sx * sy * sz;
    h_ang_(2) = sx * cy;

    h_ang_(3) = -sx * sz + cx * sy * cz;
    h_ang_(4) = -cx * sy * sz - sx * cz;
    h_ang_(5) = -cx * cy;

    h_ang_(6) = cx * cy * cz;
    h_ang_(7) = -cx * cy * sz;
    h_ang_(8) = cx * sy;

    h_ang_(9) = sx * cy * cz;
    h_ang_(10) = -sx * cy * sz;
    h_ang_(11) = sx * sy;

    h_ang_(12) = -sx * cz - cx * sy * sz;
    h_ang_(13) = sx * sz - cx * sy * cz;
    h_ang_(14) = 0;

    h_ang_(15) = cx * cz - sx * sy * sz;
    h_ang_(16) = -sx * sy * cz - cx * sz;
    h_ang_(17) = 0;

    h_ang_(18) = -cy * cz;
    h_ang_(19) = cy * sz;
    h_ang_(20) = sy;

    h_ang_(21) = -sx * sy * cz;
    h_ang_(22) = sx * sy * sz;
    h_ang_(23) = sx * cy;

    h_ang_(24) = cx * sy * cz;
    h_ang_(25) = -cx * sy * sz;
    h_ang_(26) = -cx * cy;

    h_ang_(27) = sy * sz;
    h_ang_(28) = sy * cz;
    h_ang_(29) = 0;

    h_ang_(30) = -sx * cy * sz;
    h_ang_(31) = -sx * cy * cz;
    h_ang_(32) = 0;

    h_ang_(33) = cx * cy * sz;
    h_ang_(34) = cx * cy * cz;
    h_ang_(35) = 0;

    h_ang_(36) = -cy * cz;
    h_ang_(37) = cy * sz;
    h_ang_(38) = 0;

    h_ang_(39) = -cx * sz - sx * sy * cz;
    h_ang_(40) = -cx * cz + sx * sy * sz;
    h_ang_(41) = 0;

    h_ang_(42) = -sx * sz + cx * sy * cz;
    h_ang_(43) = -cx * sy * sz - sx * cz;
    h_ang_(44) = 0;

    h_ang_.syncGpu();
  }
}

template <typename Scalar>
__global__ void gpuTransform(CloudPtr<Scalar> in_cloud, CloudPtr<Scalar> out_cloud,
								int point_num, MatrixDD transform)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = idx; i < point_num; i += stride) {
		auto p = in_cloud[i];
		Vector3x<Scalar> out_p;

    	out_p.x = transform(0, 0) * p.x + transform(0, 1) * p.y + transform(0, 2) * p.z + transform(0, 3);
    	out_p.y = transform(1, 0) * p.x + transform(1, 1) * p.y + transform(1, 2) * p.z + transform(1, 3);
    	out_p.z = transform(2, 0) * p.x + transform(2, 1) * p.y + transform(2, 2) * p.z + transform(2, 3);

    	out_cloud.emplace(i, out_p);
	}
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
template <typename PointType>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
transformPointCloud(PointCloud<PointType> &in_cloud, PointCloud<PointType> &out_cloud,
					const Eigen::Matrix<float, 4, 4>& transform)
{
	if (in_cloud.size() <= 0) {
		return;
	}

	if (out_cloud.size() != in_cloud.size()) {
		out_cloud.resize(in_cloud.size());
	}

	Eigen::Transform<float, 3, Eigen::Affine> t(transform);
	MatrixHD htrans(3, 4, __allocator);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			htrans(i, j) = t(i, j);
		}
	}

	htrans.syncGpu();

	int point_number = in_cloud.size();

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_number, gpuTransform,
						in_cloud.data(), out_cloud.data(), point_number,
						MatrixDD(htrans)));
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computeStepLengthMT(const Eigen::Matrix<double, 6, 1>& x,
					Eigen::Matrix<double, 6, 1>& step_dir, double step_init,
					double step_max, double step_min, double& score,
					Eigen::Matrix<double, 6, 1>& score_gradient,
					Eigen::Matrix<double, 6, 6>& hessian, SourceDCPtr &trans_cloud,
					Eigen::Matrix<float, 4, 4> &final_transformation)
{
  double phi_0 = -score;
  double d_phi_0 = -(score_gradient.dot(step_dir));

  Eigen::Matrix<double, 6, 1> x_t;

  if (d_phi_0 >= 0)
  {
    if (d_phi_0 == 0)
      return 0;
    else
    {
      d_phi_0 *= -1;
      step_dir *= -1;
    }
  }

  int max_step_iterations = 10;
  int step_iterations = 0;

  double mu = 1.e-4;
  double nu = 0.9;
  double a_l = 0, a_u = 0;

  double f_l = auxilaryFunction_PsiMT(a_l, phi_0, phi_0, d_phi_0, mu);
  double g_l = auxilaryFunction_dPsiMT(d_phi_0, d_phi_0, mu);

  double f_u = auxilaryFunction_PsiMT(a_u, phi_0, phi_0, d_phi_0, mu);
  double g_u = auxilaryFunction_dPsiMT(d_phi_0, d_phi_0, mu);

  // When line search mode is off, always skip the loop
  bool interval_converged = ((step_max - step_min) < 0 && line_search_), open_interval = true;

  double a_t = step_init;

  a_t = std::min(a_t, step_max);
  a_t = std::max(a_t, step_min);

  x_t = x + step_dir * a_t;

  Eigen::Translation<float, 3> translation(static_cast<float>(x_t(0)), static_cast<float>(x_t(1)),
                                           static_cast<float>(x_t(2)));
  Eigen::AngleAxis<float> tmp1(static_cast<float>(x_t(3)), Eigen::Vector3f::UnitX());
  Eigen::AngleAxis<float> tmp2(static_cast<float>(x_t(4)), Eigen::Vector3f::UnitY());
  Eigen::AngleAxis<float> tmp3(static_cast<float>(x_t(5)), Eigen::Vector3f::UnitZ());
  Eigen::AngleAxis<float> tmp4(tmp1 * tmp2 * tmp3);

  final_transformation = (translation * tmp4).matrix();

  transformPointCloud(*source_cloud_, *trans_cloud, final_transformation);

  score = computeDerivatives(score_gradient, hessian, trans_cloud, x_t, true);

  double phi_t = -score;
  double d_phi_t = -(score_gradient.dot(step_dir));
  double psi_t = auxilaryFunction_PsiMT(a_t, phi_t, phi_0, d_phi_0, mu);
  double d_psi_t = auxilaryFunction_dPsiMT(d_phi_t, d_phi_0, mu);

  while (!interval_converged && step_iterations < max_step_iterations &&
		  !(psi_t <= 0 && d_phi_t <= -nu * d_phi_0))
  {
    if (open_interval)
    {
      a_t = trialValueSelectionMT(a_l, f_l, g_l, a_u, f_u, g_u, a_t, psi_t, d_psi_t);
    }
    else
    {
      a_t = trialValueSelectionMT(a_l, f_l, g_l, a_u, f_u, g_u, a_t, phi_t, d_phi_t);
    }

    a_t = (a_t < step_max) ? a_t : step_max;
    a_t = (a_t > step_min) ? a_t : step_min;

    x_t = x + step_dir * a_t;

    translation = Eigen::Translation<float, 3>(static_cast<float>(x_t(0)), static_cast<float>(x_t(1)),
                                               static_cast<float>(x_t(2)));
    tmp1 = Eigen::AngleAxis<float>(static_cast<float>(x_t(3)), Eigen::Vector3f::UnitX());
    tmp2 = Eigen::AngleAxis<float>(static_cast<float>(x_t(4)), Eigen::Vector3f::UnitY());
    tmp3 = Eigen::AngleAxis<float>(static_cast<float>(x_t(5)), Eigen::Vector3f::UnitZ());
    tmp4 = tmp1 * tmp2 * tmp3;

    final_transformation = (translation * tmp4).matrix();

    transformPointCloud(*source_cloud_, *trans_cloud, final_transformation);

    score = computeDerivatives(score_gradient, hessian, trans_cloud, x_t, false);

    phi_t -= score;
    d_phi_t -= (score_gradient.dot(step_dir));
    psi_t = auxilaryFunction_PsiMT(a_t, phi_t, phi_0, d_phi_0, mu);
    d_psi_t = auxilaryFunction_dPsiMT(d_phi_t, d_phi_0, mu);

    if (open_interval && (psi_t <= 0 && d_psi_t >= 0))
    {
      open_interval = false;

      f_l += phi_0 - mu * d_phi_0 * a_l;
      g_l += mu * d_phi_0;

      f_u += phi_0 - mu * d_phi_0 * a_u;
      g_u += mu * d_phi_0;
    }

    if (open_interval)
    {
      interval_converged = updateIntervalMT(a_l, f_l, g_l, a_u, f_u, g_u, a_t, psi_t, d_psi_t);
    }
    else
    {
      interval_converged = updateIntervalMT(a_l, f_l, g_l, a_u, f_u, g_u, a_t, phi_t, d_phi_t);
    }
    step_iterations++;
  }

  if (step_iterations)
  {
	  computeHessian(hessian, trans_cloud);
  }

  real_iterations_ += step_iterations;

  return a_t;
}

// Copied from ndt.hpp
template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
trialValueSelectionMT(double a_l, double f_l, double g_l, double a_u, double f_u,
						double g_u, double a_t, double f_t, double g_t)
{
  // Case 1 in Trial Value Selection [More, Thuente 1994]
  if (f_t > f_l)
  {
    // Calculate the minimizer of the cubic that interpolates f_l, f_t, g_l and g_t
    // Equation 2.4.52 [Sun, Yuan 2006]
    double z = 3 * (f_t - f_l) / (a_t - a_l) - g_t - g_l;
    double w = std::sqrt(z * z - g_t * g_l);
    // Equation 2.4.56 [Sun, Yuan 2006]
    double a_c = a_l + (a_t - a_l) * (w - g_l - z) / (g_t - g_l + 2 * w);

    // Calculate the minimizer of the quadratic that interpolates f_l, f_t and g_l
    // Equation 2.4.2 [Sun, Yuan 2006]
    double a_q = a_l - 0.5 * (a_l - a_t) * g_l / (g_l - (f_l - f_t) / (a_l - a_t));

    if (std::fabs(a_c - a_l) < std::fabs(a_q - a_l))
      return (a_c);
    else
      return (0.5 * (a_q + a_c));
  }
  // Case 2 in Trial Value Selection [More, Thuente 1994]
  else if (g_t * g_l < 0)
  {
    // Calculate the minimizer of the cubic that interpolates f_l, f_t, g_l and g_t
    // Equation 2.4.52 [Sun, Yuan 2006]
    double z = 3 * (f_t - f_l) / (a_t - a_l) - g_t - g_l;
    double w = std::sqrt(z * z - g_t * g_l);
    // Equation 2.4.56 [Sun, Yuan 2006]
    double a_c = a_l + (a_t - a_l) * (w - g_l - z) / (g_t - g_l + 2 * w);

    // Calculate the minimizer of the quadratic that interpolates f_l, g_l and g_t
    // Equation 2.4.5 [Sun, Yuan 2006]
    double a_s = a_l - (a_l - a_t) / (g_l - g_t) * g_l;

    if (std::fabs(a_c - a_t) >= std::fabs(a_s - a_t))
      return (a_c);
    else
      return (a_s);
  }
  // Case 3 in Trial Value Selection [More, Thuente 1994]
  else if (std::fabs(g_t) <= std::fabs(g_l))
  {
    // Calculate the minimizer of the cubic that interpolates f_l, f_t, g_l and g_t
    // Equation 2.4.52 [Sun, Yuan 2006]
    double z = 3 * (f_t - f_l) / (a_t - a_l) - g_t - g_l;
    double w = std::sqrt(z * z - g_t * g_l);
    double a_c = a_l + (a_t - a_l) * (w - g_l - z) / (g_t - g_l + 2 * w);

    // Calculate the minimizer of the quadratic that interpolates g_l and g_t
    // Equation 2.4.5 [Sun, Yuan 2006]
    double a_s = a_l - (a_l - a_t) / (g_l - g_t) * g_l;

    double a_t_next;

    if (std::fabs(a_c - a_t) < std::fabs(a_s - a_t))
      a_t_next = a_c;
    else
      a_t_next = a_s;

    if (a_t > a_l)
      return (std::min(a_t + 0.66 * (a_u - a_t), a_t_next));
    else
      return (std::max(a_t + 0.66 * (a_u - a_t), a_t_next));
  }
  // Case 4 in Trial Value Selection [More, Thuente 1994]
  else
  {
    // Calculate the minimizer of the cubic that interpolates f_u, f_t, g_u and g_t
    // Equation 2.4.52 [Sun, Yuan 2006]
    double z = 3 * (f_t - f_u) / (a_t - a_u) - g_t - g_u;
    double w = std::sqrt(z * z - g_t * g_u);
    // Equation 2.4.56 [Sun, Yuan 2006]
    return (a_u + (a_t - a_u) * (w - g_u - z) / (g_t - g_u + 2 * w));
  }
}

// Copied from ndt.hpp
template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateIntervalMT(double& a_l, double& f_l, double& g_l, double& a_u, double& f_u,
					double& g_u, double a_t, double f_t, double g_t)
{
  // Case U1 in Update Algorithm and Case a in Modified Update Algorithm [More, Thuente 1994]
  if (f_t > f_l)
  {
    a_u = a_t;
    f_u = f_t;
    g_u = g_t;
    return (false);
  }
  // Case U2 in Update Algorithm and Case b in Modified Update Algorithm [More, Thuente 1994]
  else if (g_t * (a_l - a_t) > 0)
  {
    a_l = a_t;
    f_l = f_t;
    g_l = g_t;
    return (false);
  }
  // Case U3 in Update Algorithm and Case c in Modified Update Algorithm [More, Thuente 1994]
  else if (g_t * (a_l - a_t) < 0)
  {
    a_u = a_l;
    f_u = f_l;
    g_u = g_l;

    a_l = a_t;
    f_l = f_t;
    g_l = g_t;
    return (false);
  }
  // Interval Converged
  else
    return (true);
}


template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
computeHessian(Eigen::Matrix<double, 6, 6>& hessian, SourceDCPtr &trans_cloud)
{
	j_ang_.syncGpu();
	h_ang_.syncGpu();

	MatrixDD dj_ang(j_ang_);
	MatrixDD dh_ang(h_ang_);

	PointCloud<PointSourceType, double> scattered_trans_cloud(__allocator);
	PointCloud<PointSourceType, Scalar> scattered_source_cloud(__allocator);
	matrix_vector<double> icov(__allocator);

	int voxel_num = preprocessing(trans_cloud->data(), scattered_trans_cloud,
									scattered_source_cloud, icov);

	if (voxel_num <= 0) {
		return;
	}

	/* Step 2: Update score gradient and hessian matrix */
	matrix_vector<double> point_gradients(__allocator);
	matrix_vector<double> point_hessians(__allocator);

	computePointDerivatives(scattered_source_cloud, dj_ang, dh_ang,
							point_gradients, point_hessians);

	updateHessians(scattered_trans_cloud, point_gradients, point_hessians,
					icov, hessian);
}

/* Verify if min distances are really smaller than or equal to max_range */
__global__ void verifyDistances(int* valid_distance, float* min_distance, double max_range, int point_num)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = idx; i < point_num; i += stride) {
		bool check = (min_distance[i] <= max_range);

		valid_distance[i] = (check) ? 1 : 0;

		if (!check) {
			min_distance[i] = 0;
		}
	}
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
getFitnessScore(double max_range)
{
	// Copy final_transformation_ to Eigen::Matrix<float, 4, 4>
	Eigen::Matrix<float, 4, 4> final_transformation;

	this->copyToEigen(final_transformation, final_transformation_);

	double fitness_score = 0.0;

	int point_number = source_cloud_->size();

	SourceDCPtr trans_cloud(new PointCloud<PointSourceType>(__allocator));

	transformPointCloud(*source_cloud_, *trans_cloud, final_transformation);

	device_vector<int> valid_distance(point_number, __allocator);
	device_vector<float> min_distance(point_number, __allocator);

	tree_->NNSearch(trans_cloud, min_distance);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_number, verifyDistances,
					valid_distance.data(), min_distance.data(), max_range, point_number));

	int nr;
	float f_fitness_score;

	reductionSum(min_distance, &f_fitness_score);
	reductionSum(valid_distance, &nr);

	fitness_score = f_fitness_score;

	if (nr > 0) {
		return (fitness_score / nr);
	}

	return DBL_MAX;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
double NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
getTransformationProbability2(SourceHCPtr &source_cloud, TargetHCPtr &trans_cloud)
{
	if (source_cloud->size() <= 0 || trans_cloud->size() <= 0) {
		return DBL_MAX;
	}

	SourceDCPtr scloud(new PointCloud<PointSourceType>(__allocator));
	TargetDCPtr tcloud(new PointCloud<PointTargetType>(__allocator));

	scloud->setInputCloud(source_cloud);
	tcloud->setInputCloud(trans_cloud);

	GridPtr source_grid(new GridType(__allocator));

	source_grid->setResolution(resolution_, resolution_, resolution_);
	source_grid->template setInputCloud<PointSourceType>(scloud);

	// Radius Search
	device_vector<int> starting_voxel_id(__allocator);
	PointCloud<PointSourceType, double> ncloud(__allocator);
	matrix_vector<double> icov(__allocator);

	int voxel_num = preprocessing(source_grid, tcloud->data(), ncloud,
									icov, starting_voxel_id);

	if (voxel_num <= 0) {
		return 0;
	}

	// Update score gradient and hessian matrix
	device_vector<double> score_list(voxel_num, __allocator);
	device_vector<double> e_x_cov_x(voxel_num, __allocator);

	// Need trans points & icov
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeExCovX,
					ncloud.data(), voxel_num, gauss_d1_, gauss_d2_, e_x_cov_x.data(),
					icov(0, 0), icov(0, 1), icov(0, 2),
					icov(1, 0), icov(1, 1), icov(1, 2),
					icov(2, 0), icov(2, 1), icov(2, 2)));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, computeScoreList,
					e_x_cov_x.data(), voxel_num, gauss_d1_, gauss_d2_,
					score_list.data()));

	// Compute score from score list using reduction
	double score;

	reductionSum(score_list, score);

	return score / static_cast<double>(trans_cloud->size());
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateVoxelGrid(SourceHCPtr &new_cloud)
{

	// If no point is added, do nothing
	if (new_cloud->size() <= 0) {
		return;
	}

	// Convert the new points to GPU-side xyz buffers
	TargetDCPtr cloud(new PointCloud<PointTargetType>(__allocator));

	cloud->setInputCloud(new_cloud);

	// Update voxel grid
	voxel_grid_->template update<PointTargetType>(cloud);

	// Update the search tree
	tree_->update(cloud);
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateVoxelGrid(SourceDCPtr &new_cloud)
{

	// If no point is added, do nothing
	if (new_cloud->size() <= 0) {
		return;
	}

	// Update voxel grid
	voxel_grid_->template update<PointSourceType>(new_cloud);

	// Update the search tree
	tree_->update(new_cloud);
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateVoxelGridRaw(SourceHCPtr &new_cloud)
{
	// If no point is added, do nothing
	if (new_cloud->size() <= 0) {
		return;
	}

	// Convert the new points to GPU-side xyz buffers
	TargetDCPtr cloud(new PointCloud<PointTargetType>(__allocator));

	cloud->setInputCloud(new_cloud);

	// Update voxel grid
	voxel_grid_->template update<PointTargetType>(cloud);
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
updateVoxelGridRaw(SourceDCPtr &new_cloud)
{
	// If no point is added, do nothing
	if (new_cloud->size() <= 0) {
		return;
	}

	// Update voxel grid
	voxel_grid_->template update<PointSourceType>(new_cloud);
}

/* Perform radius search to search for neighbor voxels of input points
 * within a specified radius.
 *
 * The points are later decomposed to maintain coalesce access in later
 * steps.
 *
 * Input:
 * 		@x, y, z: coordinates of query points
 * 		@radius: the radius to perform the query
 *
 * Output:
 * 		@tx, ty, tz: decomposed transformed points
 * 		@ox, oy, oz: decomposed original source points
 * 		@centroid: centroids of found voxels
 * 		@icov: inverse covariance of found voxels
 *
 * Return: the number of found voxels
 */
template <typename PointSourceType, typename PointTargetType, typename Scalar>
int NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
preprocessing(FCloudPtr trans_cloud, PointCloud<PointSourceType, double> &scattered_trans_cloud,
				PointCloud<PointSourceType, Scalar> &scattered_source_cloud, matrix_vector<double> &icov)
{
	device_vector<int> starting_voxel_id(__allocator);

	int voxel_num = preprocessing(voxel_grid_, trans_cloud, scattered_trans_cloud,
									icov, starting_voxel_id);

	if (voxel_num <= 0) {
		return 0;
	}

	// Scatter original source points
	scattered_source_cloud.resize(voxel_num);

	int point_num = source_cloud_->size();

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPoints,
					source_cloud_->data(), starting_voxel_id.data(), point_num,
					scattered_source_cloud.data()));

	return voxel_num;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
int NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar>::
preprocessing(GridPtr voxel_grid, FCloudPtr trans_cloud,
				PointCloud<PointSourceType, double> &scattered_trans_cloud,
				matrix_vector<double> &icov, device_vector<int> &starting_voxel_id)
{
	int point_num = trans_cloud.size();

	if (point_num <= 0) {
		return 0;
	}

	/* Radius Search.
	 * Results are returned to starting_voxel_id and voxel_id.
	 * To access indices of neighbor voxels of a point i, we just
	 * need to go to voxel_id from index starting_voxel_id[i] to
	 * starting_voxel_id[i + 1] - 1.
	 */
	device_vector<int> voxel_id(__allocator);

	voxel_grid->radiusSearch(trans_cloud, resolution_, INT_MAX,
								starting_voxel_id, voxel_id);

	int voxel_num = voxel_id.size();

	// If no voxel was found, return 0
	if (voxel_num <= 0) {
		return 0;
	}

	/* Gather inverse covariances and centroids of voxels returned
	 * from radiusSearch. This is to avoid non-coalesced accesses in
	 * later steps.
	 */
	// Gather inverse covariance
	int total_voxel_num = voxel_grid_->getVoxelNum();	// Number of input matrices
	matrix_vector<double> &all_icov = voxel_grid_->getInverseCovarianceList();	// Input

	icov.resize(voxel_num, 3, 3);	// Output

	// 3x3 inverse covariance matrices
	checkCudaErrors((launchSync<BLOCK_SIZE_X, 3, 3>(voxel_num, gatherMatrix<3, 3, double>,
					all_icov.data(), total_voxel_num, voxel_id.data(), voxel_num, icov.data())));

	// Gather centroids
	device_vector<double> &all_centroid = voxel_grid_->getCentroidList();	// Input
	matrix_vector<double> centroid(voxel_num, 3, 1, __allocator); 	// Output

	// 3x1 centroid matrices
	checkCudaErrors((launchSync<BLOCK_SIZE_X, 3, 1>(voxel_num, gatherMatrix<3, 1, double>,
					all_centroid.data(), total_voxel_num, voxel_id.data(), voxel_num, centroid.data())));

	// Decompose transformed point coordinates
	scattered_trans_cloud.resize(voxel_num);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPoints<float, double>,
					trans_cloud, starting_voxel_id.data(), point_num,
					scattered_trans_cloud.data()));

	// Compute trans_x -= centroid
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, normalizeCoordinates,
					scattered_trans_cloud.data(), centroid(0), centroid(1),
					centroid(2), voxel_num));

	return voxel_num;
}

}
