#include <cuda.h>
#include <vector>
#include <float.h>
#include <limits>
#include <utility>
#include <cmath>

#include <ndt_gpu/PointlessTree.h>
#include <ndt_gpu/utilities.h>

namespace gpu {

namespace pltree {

#define DEFAULT_RES_ (1.0)

#ifndef RES_X_
#define RES_X_ 	(4)
#define RES_SHIFT_X_	(2)
#endif

#ifndef RES_Y_
#define RES_Y_ (4)
#define RES_SHIFT_Y_	(2)
#endif

#ifndef RES_Z_
#define RES_Z_ (2)
#define RES_SHIFT_Z_	(1)
#endif

#ifndef GROUP_SIZE_
#define GROUP_SIZE_ 	(RES_X_ * RES_Y_ * RES_Z_)
#define GROUP_SHIFT_	(5)		// 2^5 = 32
#define GROUP_LID_CMP_	(31)
#endif

#ifndef FULL_MASK
#define FULL_MASK	(0xffffffff)
#endif

#ifndef TREE_MAX_HEIGHT_
#define TREE_MAX_HEIGHT_ (16)
#endif

/**
 * Default constructor. The tree is empty.
 */
template <typename PointType>
PointlessTree<PointType>::PointlessTree(GMemoryAllocator *allocator) :
	Base(allocator),
	status_(__allocator),
	level_id_(__allocator),
	level_dim_(__allocator),
	level_lb_(__allocator),
	level_ub_(__allocator),
	node_size_(__allocator),
	centroids_(new PointCloud<PointType>(__allocator))
{
	res_.x = res_.y = res_.z = DEFAULT_RES_;

	bot_lb_.x = bot_lb_.y = bot_lb_.z = std::numeric_limits<int>::max();
	bot_ub_.x = bot_ub_.y = bot_ub_.z = std::numeric_limits<int>::min();
}

// Added functions for incremental update
__host__ __device__ inline int roundUp(int input, int factor) {
	return (input < 0) ? (-((-input) / factor) * factor) : ((input  + factor - 1 ) / factor) * factor;
}

__host__ __device__ inline int roundDown(int input, int factor) {
	return (input < 0) ? (-(-input + factor - 1) / factor) * factor : (input / factor) * factor;
}

__host__ __device__ inline int div(int input, int divisor) {
	return (input < 0) ? (-(-input + divisor - 1) / divisor) : (input / divisor);
}

template <typename PointType>
void PointlessTree<PointType>::setInputCloud(typename PointCloud<PointType>::Ptr &cloud)
{
	// Reset parameters
	res_.x = res_.y = res_.z = DEFAULT_RES_;

	bot_lb_.x = bot_lb_.y = bot_lb_.z = std::numeric_limits<int>::max();
	bot_ub_.x = bot_ub_.y = bot_ub_.z = std::numeric_limits<int>::min();

	if (cloud->size() > 0) {
		buildTree(cloud);
	}
}

template <typename PointType>
PointlessTree<PointType>::PointlessTree(const PointlessTree<PointType> &other) :
	Base(other),
	status_(other.status_),
	level_id_(other.level_id_),
	level_dim_(other.level_dim_),
	level_lb_(other.level_lb_),
	level_ub_(other.level_ub_),
	node_size_(other.node_size_),
	centroids_(other.centroids_)
{
	res_ = other.res_;
	bot_lb_ = other.bot_lb_;
	bot_ub_ = other.bot_ub_;
}

template <typename PointType>
PointlessTree<PointType>::PointlessTree(PointlessTree<PointType> &&other) :
	Base(other),
	status_(std::move(other.status_)),
	level_id_(std::move(other.level_id_)),
	level_dim_(std::move(other.level_dim_)),
	level_lb_(std::move(other.level_lb_)),
	level_ub_(std::move(other.level_ub_)),
	node_size_(std::move(other.node_size_)),
	centroids_(std::move(other.centroids_))
{
	res_ = other.res_;
	bot_lb_ = other.bot_lb_;
	bot_ub_ = other.bot_ub_;

	other.res_.x = other.res_.y = other.res_.z = 0;

	other.bot_lb_.x = other.bot_lb_.y = other.bot_lb_.z = std::numeric_limits<int>::max();
	other.bot_ub_.x = other.bot_ub_.y = other.bot_ub_.z = std::numeric_limits<int>::lowest();
}

template <typename PointType>
PointlessTree<PointType> &PointlessTree<PointType>::operator=(const PointlessTree<PointType> &other)
{
	status_ = other.status_;

	level_id_ = other.level_id_;
	level_dim_ = other.level_dim_;
	level_lb_ = other.level_lb_;
	level_ub_ = other.level_ub_;

	node_size_ = other.node_size_;
	centroids_ = other.centroids_;

	res_ = other.res_;
	bot_lb_ = other.bot_lb_;
	bot_ub_ = other.bot_ub_;

	return *this;
}

template <typename PointType>
PointlessTree<PointType> &PointlessTree<PointType>::operator=(PointlessTree<PointType> &&other)
{
	status_ = std::move(other.status_);

	level_id_ = std::move(other.level_id_);
	level_dim_ = std::move(other.level_dim_);
	level_lb_ = std::move(other.level_lb_);
	level_ub_ = std::move(other.level_ub_);

	node_size_ = other.node_size_;
	centroids_ = other.centroids_;

	res_ = other.res_;
	bot_lb_ = other.bot_lb_;
	bot_ub_ = other.bot_ub_;

	other.res_.x = other.res_.y = other.res_.z = 0;

	other.bot_lb_.x = other.bot_lb_.y = other.bot_lb_.z = std::numeric_limits<int>::max();
	other.bot_ub_.x = other.bot_ub_.y = other.bot_ub_.z = std::numeric_limits<int>::lowest();

	return *this;
}

/**
 * Build the octree on top of a point cloud.
 * The GPU buffer for the tree is over-allocated and is
 * extended when the point cloud becomes larger than the
 * tree.
 */
template <typename PointType>
void PointlessTree<PointType>::buildTree(typename PointCloud<PointType>::Ptr &cloud)
{

	// Check if the input is a null pointer or an empty cloud
	if (!cloud || cloud->size() <= 0) {
		return;
	}

	// 1. Compute boundaries of the bottom level of the tree
	computeBoundaries(cloud->data(), bot_ub_, bot_lb_);

	/**
	 * 2. Compute the dimensions of each level and allocate the
	 *    GPU memory for holding parameters of tree nodes and
	 *    centroids of leaves.
	 */
	allocateTree(bot_lb_, bot_ub_, status_, level_dim_, level_lb_, level_ub_,
					node_size_, level_id_, centroids_);

	/**
	 * 3. Build the bottom level
	 * Distribute points to leaves, compute the number of points
	 * and the centroid of each leaf node.
	 */
	scatterPointsToLeaves(cloud, bot_ub_, bot_lb_);

	/**
	 * 4. Move from bottom to top of the tree and build upper levels
	 *   from lower levels.
	 */
	buildLevelMultiPass();
}

/* Compute the index of the leaf which the point belong to */
__host__ __device__ inline int leafId(Vector3f p, Vector3f res, Vector3i min_b, Vector3i leaf_size)
{
  int idx = static_cast<int>(floorf(p.x / res.x)) - min_b.x;
  int idy = static_cast<int>(floorf(p.y / res.y)) - min_b.y;
  int idz = static_cast<int>(floorf(p.z / res.z)) - min_b.z;

  return (idx + idy * leaf_size.x + idz * leaf_size.x * leaf_size.y);
}

/**
 * Count the number of points in each leaf node.
 * Compute the index of the leaf that a point belongs
 * to and use atomicAdd to update the number of points
 * in the leaf. Initially, all leaves' counts are 0.
 *
 * Params:
 * 		@cloud[in]		: Coordinates of points
 * 		@point_num[in]	: Number of points
 * 		@res[in]		: the resolution of leaf nodes
 * 		@bot_lb[in]		: the min boundaries of the bottom level grid
 * 		@bot_dim[in]	: size (in number of nodes) of the bottom level grid
 * 		@count[out]		: count[i] is the number of points in
 * 							the node i of the bottom grid
 */
__global__ void computeLeafSize(FCloudPtr cloud, int point_num, Vector3f res,
								Vector3i bot_lb, Vector3i bot_dim, int *count)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int leaf_id = leafId(cloud[i], res, bot_lb, bot_dim);

		atomicAdd(count + leaf_id, 1);
	}
}

/* Distribute points to leaf nodes
 *
 * Input:
 * 		@x, y, z				: the coordinates of the input points
 *		@point_num				: the number of points
 *		@wlocation				: the writing location, which indicate where to put
 *									points in the output point_id vector.
 *		@leaf_x, leaf_y, leaf_z	: the size (in number of nodes) of the bottom level
 *		@res_x, res_y, res_z	: the resolution of leaf nodes
 *		@min_b_x, min_b_y, min_b_z: the min boundaries of the bottom grid
 *		@offset: added to the output index
 * Output:
 * 		@point_id: the output vector where indices of points are re-organized
 */
__global__ void scatterPointToLeaf(FCloudPtr cloud, int point_num, int *wlocation,
									Vector3f res, Vector3i bot_lb, Vector3i bot_dim,
									int *point_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int leaf_id = leafId(cloud[i], res, bot_lb, bot_dim);

		int loc = atomicAdd(&wlocation[leaf_id], 1);

		point_id[loc] = i;
	}
}

__global__ void updateLeafCentroids(FCloudPtr cloud, int *starting_pid, int *point_id, int leaf_num,
									int *leaf_id, FCloudPtr centroids, uint32_t *points_per_leaf)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	const int warp_size = 32;
	int lane_id = threadIdx.x % warp_size;

	for (int i = index; i < leaf_num * warp_size; i += stride) {
		// 32 threads handle a leaf together+
		int lid = i / warp_size;	// Index of the new leaf node in the new bottom grid
		int start = starting_pid[lid], end = starting_pid[lid + 1];
		Vector3f tmp_centr(0, 0, 0);

		// Compute the sum of new points ONLY
		for (int j = lane_id + start; j < end; j += warp_size) {
			tmp_centr += cloud[point_id[j]];
		}

		for (int active_threads = warp_size >> 1; active_threads > 0; active_threads >>= 1) {
			tmp_centr.x += __shfl_xor_sync(FULL_MASK, tmp_centr.x, active_threads, warp_size);
			tmp_centr.y += __shfl_xor_sync(FULL_MASK, tmp_centr.y, active_threads, warp_size);
			tmp_centr.z += __shfl_xor_sync(FULL_MASK, tmp_centr.z, active_threads, warp_size);
		}

		// thread 0 in the wrap update the centroid of the leaf
		if (lane_id == 0) {
			int leaf_idx = leaf_id[lid];	// Index of the new leaf node in the old bottom grid
			float pnum = static_cast<float>(points_per_leaf[leaf_idx]);

			/* New centroid = (new_sum + (current_centroid * current_point_num)) /
			 * 					(new_point_num + current_point_num)
			 */
			tmp_centr += centroids[leaf_idx] * pnum;

			// Update the number of points as well
			pnum += (end - start);

			// Update the new center
			centroids.emplace(leaf_idx, tmp_centr / pnum);

			// TODO: update points per leaf
		}
	}
}

__global__ void updateLeafSize(int *starting_pid, int *leaf_id, int leaf_num,
								uint32_t *points_per_leaf)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < leaf_num; i += stride) {
		int lid = leaf_id[i];

		points_per_leaf[lid] += (starting_pid[i + 1] - starting_pid[i]);
	}
}


/* Mark the non-empty leaves of a bottom grid.
 *
 * Input:
 * 		@starting_point_ids: the starting indices of points that belong to leaves
 * 		@leaf_num: number of leaf nodes
 *
 * Output:
 * 		@mark: if leaf i is non-empty, mark[i] = 1. Otherwise, mark[i] = 0;
 */
__global__ void markNonEmptyLeaves(int *starting_point_ids, int leaf_num, int *mark)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < leaf_num; i += stride) {
		int val = (starting_point_ids[i] < starting_point_ids[i + 1]) ? 1 : 0;

		mark[i] = val;
	}
}


__global__ void collectNonEmptyLeaves(int *starting_pid, int *wlocation, int leaf_num,
										int *leaf_id, int *non_empty_starting_pid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < leaf_num; i += stride) {
		int loc = wlocation[i];
		int start = starting_pid[i];

		if (start < starting_pid[i + 1]) {
			leaf_id[loc] = i;
			non_empty_starting_pid[loc] = start;
		}
	}

	if (index == 0) {
		int loc = wlocation[leaf_num];

		non_empty_starting_pid[loc] = starting_pid[leaf_num];
	}
}

/* Convert the index of a leaf node from a source bottom grid
 * to a destination bottom grid.
 *
 * Input:
 * 		@leaf_id: indices of non-empty leaves
 * 		@non_empty_leaf_num: number of non-empty leaves
 * 		@src_bot_lb, dst_bot_lb: the lower boundaries of the source
 * 									and the destination bottom grid.
 * 		@src_bot_dim, dst_bot_dim: the dimensions of the source and
 * 		   							the destination voxel grid
 */
__global__ void indexConverter(int *leaf_id, int non_empty_leaf_num,
								Vector3i src_bot_lb, Vector3i src_bot_dim,
								Vector3i dst_bot_lb, Vector3i dst_bot_dim)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < non_empty_leaf_num; i += stride) {
		int src_id = leaf_id[i];

		Vector3i global_id = id1ToId3(src_id, src_bot_lb, src_bot_dim);

		int dst_id = id3ToId1(global_id, dst_bot_lb, dst_bot_dim);

		leaf_id[i] = dst_id;
	}
}

/* Update the number of points per leaf node.
 * Also update the centroids of leaf nodes.
 */
template <typename PointType>
void PointlessTree<PointType>::scatterPointsToLeaves(typename PointCloud<PointType>::Ptr &cloud,
														Vector3i bot_ub, Vector3i bot_lb)
{
	int point_num = cloud->size();

	Vector3i bot_dim = bot_ub - bot_lb + 1;

	int leaf_num = bot_dim.x * bot_dim.y * bot_dim.z;

	/***
	 * TODO: this should be done ONLY when the number of leaves
	 * are not so big. Otherwise the GPU memory may not be enough
	 * to hold @starting_pid. If the number of leaves is much
	 * larger than the number of points, another method should be
	 * used. I am working on it later.
	 *
	 * Besides, the current tree is a full tree. Both empty and
	 * non-empty nodes appear in the tree. Holding empty tree nodes
	 * is not a good idea since the GPU memory is very limited. A
	 * sparse tree should be considered.
	 */
	device_vector<int> starting_pid(leaf_num + 1, __allocator, true);

	// Count the number of points in each leaf node
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, computeLeafSize,
					cloud->data(), point_num, res_, bot_lb, bot_dim,
					starting_pid.data()));

	// Compute the writing location of the output
	ExclusiveScan(starting_pid);

	device_vector<int> wlocation(leaf_num, __allocator);

	gcopyDtoD(wlocation.data(), starting_pid.data(), sizeof(int) * leaf_num);

	// Sort the points to match their nodes.
	device_vector<int> point_id(point_num, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPointToLeaf,
					cloud->data(), point_num, wlocation.data(),
					res_, bot_lb, bot_dim, point_id.data()));

	/**
	 * The input nodes (that cover the input cloud) may (and most likely)
	 * not all non-empty. Lots of them are empty and we do not want to
	 * waste time to process them. Hence, we first extract non-empty nodes,
	 * then convert their indices to the indices in the bottom level of the
	 * tree, then update those nodes.
	 *
	 * TODO: There is another way to update the bottom level of the tree.
	 * Instead of filter out empty nodes, we just update all nodes in
	 * the input. So there is just 2 steps instead of 3. However, the later
	 * kernels (convert and update) will have to run on more nodes (since
	 * empty nodes also involve). I wonder if this method is faster than
	 * the 3-step update.
	 */
	// Stream compaction to collect non-empty voxels only
	device_vector<int> mark(leaf_num + 1, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num, markNonEmptyLeaves,
					starting_pid.data(), leaf_num, mark.data()));

	int non_empty_leaf_num = 0;

	ExclusiveScan(mark, non_empty_leaf_num);

	device_vector<int> non_empty_starting_pid(non_empty_leaf_num + 1, __allocator);
	device_vector<int> leaf_id(non_empty_leaf_num, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num, collectNonEmptyLeaves,
					starting_pid.data(), mark.data(), leaf_num,
					leaf_id.data(), non_empty_starting_pid.data()));

	/**
	 * Because the indices of the input leaves are in their system, they
	 * must be converted to the system of the tree's bottom level.
	 */
	Vector3i old_bot_dim = bot_ub_ - bot_lb_ + 1;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(non_empty_leaf_num, indexConverter,
					leaf_id.data(), non_empty_leaf_num, bot_lb, bot_dim, bot_lb_,
					old_bot_dim));

	// Update centroids of leaves that contain new points by warp-primitives
	// TODO: if the number of input leaves is too big, this may not be a good idea
	int thread_num = non_empty_leaf_num * 32;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(thread_num, updateLeafCentroids,
					cloud->data(), non_empty_starting_pid.data(), point_id.data(),
					non_empty_leaf_num, leaf_id.data(), centroids_->data(), status_.data()));

	// Update number of points per leaf
	// At the bottom level, status_ contains the number of points per leaf
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(non_empty_leaf_num, updateLeafSize,
					non_empty_starting_pid.data(), leaf_id.data(),
					non_empty_leaf_num, status_.data()));
}

/* Allocate GPU memory for the tree nodes */
template <typename PointType>
void PointlessTree<PointType>::allocateTree(Vector3i bot_lb, Vector3i bot_ub,
											device_vector<uint32_t> &status, device_vector<Vector3i> &level_dim,
											device_vector<Vector3i> &level_lb, device_vector<Vector3i> &level_ub,
											device_vector<Vector3f> &node_size, device_vector<int> &level_id,
											typename PointCloud<PointType>::Ptr &centroids)
{
	// host-side output buffers, will be copied to GPU buffers later
	std::vector<Vector3i> hlevel_dim;
	std::vector<int> hlevel_id;
	std::vector<Vector3i> hlevel_lb;
	std::vector<Vector3i> hlevel_ub;
	std::vector<Vector3f> hnode_size;

	// Clear the output buffers
	status.clear();
	level_dim.clear();
	level_id.clear();
	level_lb.clear();
	level_ub.clear();
	node_size.clear();

	centroids.reset(new PointCloud<PointType>(__allocator));

	int node_number = 0;	// Number of tree nodes in each level
	Vector3i ldim;			// Dimensions of a level (in number of nodes in the level)
	int alloc_size = 0;		// Total number of elements of @status, accumulated in each level

	// Compute size of the bottom first
	ldim = bot_ub - bot_lb + 1;

	hlevel_dim.push_back(ldim);

	alloc_size = node_number = ldim.x * ldim.y * ldim.z;

	// The @status of bottom nodes are from index 0 to (alloc_size - 1)
	hlevel_id.push_back(0);

	Vector3i lb, ub;	// Lower and upper boundaries of each level

	lb = bot_lb;
	ub = bot_ub;

	hlevel_lb.push_back(lb);

	hlevel_ub.push_back(ub);

	Vector3f nsize = res_;

	hnode_size.push_back(nsize);

	// Loop until all dimensions of a level are less than resolution of the tree
	while (ldim.x > RES_X_ || ldim.y > RES_Y_ || ldim.z > RES_Z_) {
		// The parameters of the parent level are computed from of the child level
		lb.x = div(lb.x, RES_X_);
		lb.y = div(lb.y, RES_Y_);
		lb.z = div(lb.z, RES_Z_);

		ub.x = div(ub.x, RES_X_);
		ub.y = div(ub.y, RES_Y_);
		ub.z = div(ub.z, RES_Z_);

		ldim = ub - lb + 1;

		node_number = ldim.x * ldim.y * ldim.z;

		nsize.x *= RES_X_;
		nsize.y *= RES_Y_;
		nsize.z *= RES_Z_;

		hlevel_dim.push_back(ldim);
		hlevel_id.push_back(alloc_size);
		hlevel_lb.push_back(lb);
		hlevel_ub.push_back(ub);
		hnode_size.push_back(nsize);

		// The total number of @status's elements is accumulated after each iteration
		alloc_size += node_number;
	}

	int height = hlevel_id.size();

	hlevel_id.push_back(alloc_size);	// Now the size of hlevel_id is height_ + 1

	// Copy level sizes to the device memory
	level_dim = hlevel_dim;

	// Setup level_id
	level_id = hlevel_id;

	// Now level_id_ specify the index where each tree level start from

	// Copy level boundaries to the GPU memory
	level_lb = hlevel_lb;
	level_ub = hlevel_ub;

	node_size = hnode_size;

	// Allocate memory for boundaries of tree nodes and empty them
	status.resize(alloc_size);

	gzero(status.data(), sizeof(uint32_t) * alloc_size);

	// Allocate memory for centroids of leaves
	int leaf_num = hlevel_dim[0].x * hlevel_dim[0].y * hlevel_dim[0].z;

	centroids->resize(leaf_num);

}

template <typename PointType>
void PointlessTree<PointType>::buildLevelMultiPass()
{
	/* The bottom level should have been built in scatterPointsToLeaves. */
	Vector3i cdim, pdim;	// Dimensions of a child and a parent level
	Vector3i clb, plb;		// Lower boundaries of a child and a parent level
	Vector3i cub, pub;		// Upper boundaries of a child and a parent level

	int height = level_dim_.size();

	/***
	 * Copy vectors of parameters to the host memory so we
	 * don't have to access the GPU memory from the host.
	 */
	auto hlevel_id = level_id_.host_vector();
	auto hlevel_dim = level_dim_.host_vector();
	auto hlevel_lb = level_lb_.host_vector();
	auto hlevel_ub = level_ub_.host_vector();

	/* Starting from the bottom + 1, going up the tree and build level (lv) from level (lv - 1) */
	for (int lv = 1; lv < height; lv++) {
		buildUpper(status_.data() + hlevel_id[lv], hlevel_dim[lv],
					hlevel_lb[lv], hlevel_ub[lv],
					status_.data() + hlevel_id[lv - 1], hlevel_dim[lv - 1],
					hlevel_lb[lv - 1], hlevel_ub[lv - 1]);
	}
}

inline __host__ __device__ Vector3i lid1ToLid3(int local_id)
{
	Vector3i lid;

	lid.z = local_id / (RES_X_ * RES_Y_);
	local_id = local_id % (RES_X_ * RES_Y_);
	lid.y = local_id / RES_Y_;
	lid.x = local_id - lid.y * RES_X_;

	return lid;
}

/**
 * Check if a tree node is inside a bounding box.
 *
 * Params:
 * 		@id[in]: the global 3D index of a tree node
 * 		@lb, ub[in]: the lower and upper boundaries of the box
 *
 * Returns: true if the node is inside the box, false otherwise
 */
inline __device__ bool isInside(Vector3i &id, Vector3i &lb, Vector3i &ub)
{
	return (id.x >= lb.x && id.y >= lb.y && id.z >= lb.z &&
			id.x <= ub.x && id.y <= ub.y && id.z <= ub.z);
}

/**
 * Build parent level from child level.
 * For each parent node, iterate through its children nodes
 * and update the status of the parent node accordingly.
 *
 * The status value of each tree node is a 32-bit unsigned integer.
 * Each bit in this value represents the empty/non-empty status of
 * a child node of the current node.
 *
 * Each child node has a local 3D index which indicates where it is
 * in the parent node. When convert this 3D index to a 1D index, we
 * obtain the location of the bit in the status value of the parent,
 * which represents the empty/non-empty status of the child node.
 *
 * For example, a parent node has a child node (1, 0, 1), the
 * resolution of the tree is 4x4x2, so the 1D index of the
 * bit that represent the emptiness of the child node in the
 * parent's 32-bit status is
 *
 * 1 + 0*4 + 1*4*4 = 17
 *
 * Hence, if the value (parent's status & (1 << 17)) is not 0,
 * then the child node (1, 0, 1) is not empty. Otherwise it is
 * empty.
 *
 * In the bottom level (leaves), the status is the number of
 * points per each leaf.
 *
 * Params:
 * 		@pstatus[out]: status vector of the  parent level
 * 		@pdim[in]: the dimensions of the parent level
 * 		@plb, pub[in]: the lower and upper boundaries of the parent level
 * 		@cstatus[in]: status vector of the child level
 * 		@cdim[in]: the dimensions of the child level
 * 		@clb, cub[in]: the lower and upper boundaries of the child level
 *
 */
__global__ void buildUpperLevel(uint32_t *pstatus, Vector3i pdim, Vector3i plb, Vector3i pub,
								uint32_t *cstatus, Vector3i cdim, Vector3i clb, Vector3i cub)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;	// Each thread handles one parent node
	int stride = blockDim.x * gridDim.x;
	const Vector3i res(RES_X_, RES_Y_, RES_Z_);	// Resolution of the tree
	const int cnum = RES_X_ * RES_Y_ * RES_Z_;	// The maximum number of children per a parent node
	const int pnum = pdim.x * pdim.y * pdim.z;	// The number of the parent nodes

	for (int pid = index; pid < pnum; pid += stride) {
		// Compute the 3d index of the parent node
		Vector3i pid3 = id1ToId3(pid, plb, pdim);

		// Compute the 3D index of the first child node
		Vector3i base_cid = pid3 * res;
		uint32_t pstat = 0;	// Initially assume all children are empty

		// Now check every child and update the pstat
		for (int local_cid = 0; local_cid < cnum; ++local_cid) {
			// Convert the child's 1D index to a global 3D index
			Vector3i cid3 = lid1ToLid3(local_cid) + base_cid;

			// Only care about children which are actually inside the child level
			if (isInside(cid3, clb, cub)) {
				// Compute the 1D index of the child in the child level
				int global_cid = id3ToId1(cid3, clb, cdim);

				// Check the emptiness of the child node and update pstat
				pstat += cstatus[global_cid];
			}
		}

		// Save the status of the parent node
		pstatus[pid] = pstat;
	}
}


/**
 * Build the status of a parent level from its child level.
 * This is just a wrapper around the GPU kernel that actually
 * do the job.
 *
 * Params:
 * 		@pstatus[out]: status of the parent level
 * 		@pdim[in]: the dimensions of the parent level
 * 		@plb, pub[in]: the lower and upper boundaries of the parent level
 * 		@cstatus[in]: status of the child level
 * 		@cdim[in]: the dimensions of the child level
 * 		@clb, cub[in]: the lower and uppepr boundaries of the child level
 */
template <typename PointType>
void PointlessTree<PointType>::buildUpper(uint32_t *pstatus, Vector3i pdim,
											Vector3i plb, Vector3i pub,
											uint32_t *cstatus, Vector3i cdim,
											Vector3i clb, Vector3i cub)
{
	int parent_num = pdim.x * pdim.y * pdim.z;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(parent_num, buildUpperLevel,
					pstatus, pdim, plb, pub, cstatus, cdim, clb, cub));
}

/* Compute the distance between a query point @p and a tree
 * node (ix, iy, iz). The distance in each dimension is
 * determined by comparing the coordinate of the query point
 * with the boundaries of the node. The final distance is
 * the norm of the distance from 3 dimensions.
 *
 * Params:
 * 		@q[in]: query point's coordinate
 * 		@ix, iy, iz[in]: the 3D index of the tree node
 * 		@node_size[in]: the dimensions in meter of the tree node
 *
 * Return: the distance between the query point and the node.
 *
 */
__device__ float dist(Vector3f p, int ix, int iy, int iz, Vector3f node_size)
{
	float lx, ly, lz, ux, uy, uz;

	// Compute the boundaries (in meters) of the node
	lx = ix * node_size.x;
	ly = iy * node_size.y;
	lz = iz * node_size.z;

	ux = lx + node_size.x;
	uy = ly + node_size.y;
	uz = lz + node_size.z;

	float tx = ((p.x > ux) ? p.x - ux : ((p.x < lx) ? lx - p.x : 0));
	float ty = ((p.y > uy) ? p.y - uy : ((p.y < ly) ? ly - p.y : 0));
	float tz = ((p.z > uz) ? p.z - uz : ((p.z < lz) ? lz - p.z : 0));

	return norm3df(tx, ty, tz);
}

/**
 * First pass of the Nearest Neighbor Search: going down the tree
 * and find the current nearest neighbor tree node and the distance
 * between its centroid and the query point.
 */
__global__ void nnSearchGoDown(FCloudPtr qcloud, int q_num, FCloudPtr centroids,
								uint32_t *status, Vector3f res, Vector3i *level_dim,
								Vector3i *level_lb, Vector3i *level_ub, Vector3f *node_size,
								int *level_id, int height, float *nn_dist, int *nn_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	float min_dist = FLT_MAX;
	int min_id = -1;	// Index of the current nearest node

	for (int i = index; i < q_num; i += stride) {
		// Each thread handles one query point
		auto q = qcloud[i];

		/* Go from the top to the bottom level. At each level, examine
		 * all possible tree node to find the nearest node to the query
		 * point.
		 */
		for (int level = height - 1, px = 0, py = 0, pz = 0; level >= 0;
				level--, px *= RES_X_, py *= RES_Y_, pz *= RES_Z_) {

			/* Previously, every time new points are added, the whole tree is
			 * re-built. We then use an index system in which all levels start
			 * at index (0, 0, 0). In that system, the tree has a single ROOT
			 * node at the top of the tree. Moving from top to bottom is done
			 * by a single policy: Starting from node (0, 0, 0), we check all
			 * possible children of a node, chose the nearest children and
			 * continue to move down.
			 *
			 * However, in the current version, the tree is updated rather than
			 * rebuilt when new points are added. Hence, we now use a 'global
			 * index' system - each level may start from negative indices. In
			 * this system, the tree may not have a single root node anymore.
			 * For instance, node (-1, -1, -1) and node (0, 0, 0) never belong
			 * to the same node, no matter how many times their indices are divided
			 * by RES_X_, RES_Y_, and RES_Z_. That is because div(-1, RES_X_)
			 * = -((1 + RES_X_ - 1) / RES_X_) = -1. The same thing happens to other
			 * dimensions.
			 *
			 * Instead, the tree now has a 'root level', in which the number
			 * of nodes is no more than RES_X_ * RES_Y_ * RES_Z_. Because there
			 * is no root node, we have to change the policy a bit. At the root
			 * level, we first have to check all nodes to find a nearest node,
			 * then we apply the go down policy to the node.
			 */
			auto cur_dim = level_dim[level];	// Dimensions of the level (in number of nodes)
			auto cur_lb = level_lb[level];		// Lower index boundaries of the level
			auto cur_ub = level_ub[level];		// Upper index boundaries of the level
			auto nsize = node_size[level];	// Resolution of nodes in the level
			int bid = level_id[level];			// Starting index of the level in the 1D node array

			min_dist = FLT_MAX;

			int sx, sy, sz;	// Starting indices of nodes to be examined at the current level
			int ex, ey, ez;	// End indices of nodes to be examined at the current level

			if (level == height - 1) {
				// At the root level, check all nodes
				sx = cur_lb.x;
				sy = cur_lb.y;
				sz = cur_lb.z;

				ex = cur_ub.x;
				ey = cur_ub.y;
				ez = cur_ub.z;
			} else {
				// At lower levels, check all children of the node examined in the previous level
				sx = (px > cur_lb.x) ? px : cur_lb.x;
				sy = (py > cur_lb.y) ? py : cur_lb.y;
				sz = (pz > cur_lb.z) ? pz : cur_lb.z;

				ex = (px + RES_X_ - 1 < cur_ub.x) ? px + RES_X_ - 1 : cur_ub.x;
				ey = (py + RES_Y_ - 1 < cur_ub.y) ? py + RES_Y_ - 1 : cur_ub.y;
				ez = (pz + RES_Z_ - 1 < cur_ub.z) ? pz + RES_Z_ - 1 : cur_ub.z;
			}


			for (int j = sx; j <= ex; j++) {
				for (int k = sy; k <= ey; k++) {
					for (int l = sz; l <= ez; l++) {
						int cur_id = bid + id3ToId1(j, k, l, cur_lb, cur_dim);

						// Only care about non-empty nodes
						if (status[cur_id] > 0) {
							float cur_dist = dist(q, j, k, l, nsize);

							if (min_dist > cur_dist) {
								min_id = cur_id - bid;
								min_dist = cur_dist;
								px = j;
								py = k;
								pz = l;
							}
						}
					}
				}
			}
		}

		/* At the end of the loop above, min_id is the index of
		 * the leaf (bottom-level node) that is currently the
		 * closest to the query point. The current nearest distance
		 * is between the query point and the centroid of the leaf.
		 */
		nn_id[i] = min_id;							// Current nearest leaf node
		nn_dist[i] = distance(q, centroids[min_id]);	// Current nearest distance
	}
}

/**
 * Convert 1D array index to 3D index in a group of (RES_X_ * RES_Y_ * RES_Z_).
 * Use to compute the 3D index of a thread in a group of threads.
 * The 3D indices are later used as offsets to compute the index of the
 * tree node that the thread is in charged of.
 *
 * TODO:
 * This function uses bitwise operators so it needs to be tested whether it
 * is actually faster than using multiplication.
 *
 * Input:
 * 		@local_id: the 1D array index in the group
 * Output:
 *  	@lidx, lidy, lidz: the 3D index in the group
 *
 */
inline __host__ __device__ void lid1ToLid3(int local_id, int &lidx, int &lidy, int &lidz)
{
	/* lidz = local_id / (RES_X_ * RES_Y_) */
	lidz = local_id >> 4;	// RES_SHIFT_X_ + RES_SHIFT_Y_ = 4

	/* The remainder of local_id / (RES_X_ * RES_Y_) */
	local_id = (local_id - (lidz << 4));

	/* lidy = local_id / RES_Y_ */
	lidy = local_id >> 2;

	/* lidx = local_id - lidy * RES_X_*/
	lidx = local_id - (lidy << 2);
}

/* Also going down, but comes with 2 major different:
 * - A number of groups of several threads with consecutive threadIdx.x
 *   are assigned to query points. Each group handles one query point.
 *   The number of threads in each group is equal to the number of children
 *   node in each parent node. While moving down, a group checks children
 *   nodes of a previous parent node. Each thread in the group is assigned
 *   one child node and computes the distance between the child node to
 *   the query point. A reduction is then applied to find the node
 *   with shortest distance.
 *
 * - Buffers on the shared memory are allocated to stores the distance and
 *   the node indices. Those buffers are used in reduction to share node
 *   information among threads in a group.
 *
 * Input:
 * 		@qx, qy, qz: arrays of query points' coordinates
 * 		@qnum: number of query points
 * 		@pnum: number of points in each tree node
 * 		@x, y, z: the coordinates of points in the tree
 * 		@level_dim: 3D dimensions of all tree levels (in number of nodes)
 * 		@level_lb, level_ub: lower and upper index boundaries of all tree levels
 * 		@node_size: resolution of nodes in each level
 * 		@level_id: starting indices of levels in the pnum arrays
 * 		@starting_point_id, point_id: to access points in the bottom level (leaves)
 *
 * Output:
 *  	@nn_dist: distance to the current nearest neighbors of query points
 *  	@nn_id: indices of the node that contains the current nearest neighbors of query points
 *  	@nn_pid: indices of the nearest points of query points
 */
__global__ void nnSearchGoDown2(FCloudPtr qcloud, int q_num, FCloudPtr centroids, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id, int height,
								float *nn_dist, int *nn_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = blockDim.x * gridDim.x;
	float cur_dist;
	int local_id = threadIdx.x % GROUP_SIZE_;	// Local 1D index of the thread in its group (lane_id)
	int lidx, lidy, lidz;	// Local 3D indices of the thread in the group
	int base_local_id = (threadIdx.x / GROUP_SIZE_) * GROUP_SIZE_;
	int cur_id;

	__shared__ float snn_dist[BLOCK_SIZE_X];	// For distances
	__shared__ int snn_id[BLOCK_SIZE_X];		// For indices of tree nodes

	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = index; i < GROUP_SIZE_ * q_num; i += offset) {
		// Each group handles one query point
		int qid = i >> GROUP_SHIFT_;	// qid = i / 32
		auto q = qcloud[qid];

		// Go to the bottom to find the current min distance and its nn point's id
		for (int level = height - 1, pidx = 0, pidy = 0, pidz = 0; level >= 0;
				level--, pidx *= RES_X_, pidy *= RES_Y_, pidz *= RES_Z_) {
			auto cur_dim = level_dim[level];	// Dimensions of the current level
			auto cur_lb = level_lb[level];		// Lower index boundaries of the current level
			auto cur_ub = level_ub[level];		// Upper index boundaries of the current level
			auto nsize = node_size[level];	// Resolution of a node in the current level

			int bid = level_id[level];			// Starting index (block idx) of the current
												// level in the 1D tree array
			Vector3i nid;		// 3D index of the node the thread is handling

			/* They are derived from the lower indices of the
			 * root level (at the root level) or the starting
			 * indices of the children of the previously examined
			 * node (at lower level).
			 */
			nid.x = (level == height - 1) ? cur_lb.x + lidx : pidx + lidx;
			nid.y = (level == height - 1) ? cur_lb.y + lidy : pidy + lidy;
			nid.z = (level == height - 1) ? cur_lb.z + lidz : pidz + lidz;

			if (nid.x >= cur_lb.x && nid.y >= cur_lb.y && nid.z >= cur_lb.z &&
					nid.x <= cur_ub.x && nid.y <= cur_ub.y && nid.z <= cur_ub.z) {
				cur_id = bid + id3ToId1(nid, cur_lb, cur_dim);

				// If the node is empty, set cur_dist to FLT_MAX
				cur_dist = (status[cur_id] > 0) ? dist(q, nid.x, nid.y, nid.z, nsize) : FLT_MAX;
			}

			snn_dist[threadIdx.x] = cur_dist;
			snn_id[threadIdx.x] = cur_id - bid;

			__syncwarp();

			// Use reduction to find the current nearest node
			for (int active_threads = GROUP_SIZE_ >> 1; active_threads > 0; active_threads >>= 1) {
				if (local_id < active_threads) {
					int hid = threadIdx.x + active_threads;

					if (snn_dist[threadIdx.x] > snn_dist[hid]) {
						snn_dist[threadIdx.x] = snn_dist[hid];
						snn_id[threadIdx.x] = snn_id[hid];
					}
				}
				__syncwarp();
			}

			/* The thread with local_id = 0 in the group now has the index
			 * of the node that has the min distance. We now need to broadcast
			 * that index to all threads in the group. Also convert it to
			 * 3D indices pidx, pidy, pidz.
			 */
			id1ToId3(snn_id[base_local_id], cur_lb, cur_dim, pidx, pidy, pidz);
			__syncwarp();
		}

		__syncwarp();

		if (local_id == 0) {
			int min_id = snn_id[0];		// At bottom level, bid = 0
			nn_dist[qid] = distance(q, centroids[min_id]);	// Current nearest distance
			nn_id[qid] = min_id;							// Current nearest leaf node
		}
		__syncwarp();
	}
}

/* Similar the nnSearchGoDown2, but use warp-level primitives
 * to exchange information between threads in a group. This
 * should be faster than using the shared memory.
 */
__global__ void nnSearchGoDown3(FCloudPtr qcloud, int q_num, FCloudPtr centroids, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id,	int height,
								float *nn_dist, int *nn_id)
{
	int offset = blockDim.x * gridDim.x;
	float cur_dist;
	/* local_id - index of thread in the group, also
	 * lane id when GROUP_SIZE_ == 32 (when group is
	 * same as a warp).
	 */
	int local_id = threadIdx.x & (GROUP_SIZE_ - 1);
	int lidx, lidy, lidz;
	int cur_id;

	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = threadIdx.x + blockIdx.x * blockDim.x; i < GROUP_SIZE_ * q_num; i += offset) {
		int qid = i >> GROUP_SHIFT_;
		auto q = qcloud[qid];

		// Go to the bottom to find the current min distance and its nn point's id
		for (int level = height - 1, pidx = 0, pidy = 0, pidz = 0; level >= 0;
				level--, pidx *= RES_X_, pidy *= RES_Y_, pidz *= RES_Z_) {
			auto cur_dim = level_dim[level];
			auto cur_lb = level_lb[level];
			auto cur_ub = level_ub[level];
			auto nsize = node_size[level];
			int bid = level_id[level];		// Starting index of a level in the 1D octree array

			/* The indices (nx, ny, nz) of the node to be examined
			 * are derived from the lower index boundaries of the level
			 * (at the root level) or from the 3D indices of the first
			 * sibling (computed by multiply the 3D indices of the parent
			 * node with 2)
			 */
			Vector3i nid;

			nid.x = (level == height - 1) ? cur_lb.x + lidx : pidx + lidx;
			nid.y = (level == height - 1) ? cur_lb.y + lidy : pidy + lidy;
			nid.z = (level == height - 1) ? cur_lb.z + lidz : pidz + lidz;

			cur_dist = FLT_MAX;
			cur_id = -1;

			/* Make sure that (nx, ny, nz) is inside the boundaries.
			 * Otherwise it may give an illegal memory access.
			 */
			if (nid.x >= cur_lb.x && nid.y >= cur_lb.y && nid.z >= cur_lb.z &&
					nid.x <= cur_ub.x && nid.y <= cur_ub.y && nid.z <= cur_ub.z) {
				cur_id = bid + id3ToId1(nid, cur_lb, cur_dim);

				cur_dist = (status[cur_id] > 0) ? dist(q, nid.x, nid.y, nid.z, nsize) : FLT_MAX;
			}

			cur_id -= bid;

			/* Use __shfl_xor_sync to perform reduction among threads
			 * in the same group to get the min distance and the corresponding
			 * node index.
			 */
			for (int lane_offset = GROUP_SIZE_ >> 1; lane_offset > 0; lane_offset >>= 1) {
				float other_dist = __shfl_xor_sync(FULL_MASK, cur_dist, lane_offset, GROUP_SIZE_);
				int other_id = __shfl_xor_sync(FULL_MASK, cur_id, lane_offset, GROUP_SIZE_);

				if (cur_dist > other_dist) {
					cur_dist = other_dist;
					cur_id = other_id;
				}
			}

			/* At the end of the reduction above, thread 0 in the group (not
			 * thread with threadIdx.x = 0 in the block) keeps the current
			 * nearest distance and the 1D index of the current nearest leaf.
			 *
			 * They must be broadcasted to other threads in the same group as well.
			 */
			cur_id = __shfl_sync(FULL_MASK, cur_id, 0, GROUP_SIZE_);

			// Convert the 1D index to 3D indices and continue to go down that node
			id1ToId3(cur_id, cur_lb, cur_dim, pidx, pidy, pidz);
		}

		if (local_id == 0) {
			nn_dist[qid] = distance(q, centroids[cur_id]); // Current nearest distance
			nn_id[qid] = cur_id;		// Current nearest leaf node
		}
	}
}

/* Second pass, go up from the bottom to the top of the tree.
 * Find nodes which has the distance to the query point less
 * than their current nearest distance and examine its sub-tree.
 *
 * Method 1: Each thread handles one query point.
 *
 * Params:
 * 		@qcloud[in]: coordinates of query points
 * 		@q_num[in]: number of query points
 * 		@centroids[in]: coordinates of points in the current tree node
 * 		@level_dim: dimensions (in number of nodes) of each tree level
 * 		@level_lb, level_ub: lower and upper 3D index boundaries of
 * 								each tree level
 * 		@node_size: resolution of nodes in each level
 * 		@level_id: the starting indices (index of the first node) of
 * 					each tree level in the whole tree node array
 * 		@height: height of the tree (number of level). Bottom at level 0,
 * 					root (top) at level (height - 1).
 * 		@starting_point_id, point_id: spare-representation of the indices
 * 										of points in the bottom level. To
 * 										examine points in a leaf i, we walk
 * 										through index starting_point_id[i]
 * 										to starting_point_id[i + 1] - 1 on the
 * 										array point_id.
 * 		@stack: the stack structures used to store tree nodes when go down
 * 				a sub-tree. Each stack element is a pair (node's level, node'fs
 * 				1D index). Depth of the stack is (height * RES_X_ * RES_Y_ * RES_Z_)
 * 				because every time we can only examine one path from top to
 * 				bottom at maximum. Each query point is given one stack, so
 * 				the number of stacks is q_num:
 *
 * Output:
 * 		@nn_dist: the nearest distance to query points
 * 		@nn_id: the index of the nearest tree node (TODO: remove later, now keep for debug)
 *		@nn_pid: the index of the nearest neighbor points
 */
__global__ void nnSearchGoUp(FCloudPtr qcloud, int q_num, FCloudPtr centroids, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id, int height,
								int2 *stack, float *nn_dist, int *nn_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = blockDim.x * gridDim.x;
	int stk_top;	// Top of the stack
	float min_dist;
	int min_id;
	float cur_dist;

	for (int i = index; i < q_num; i += offset) {
		// Each thread handles one query point
		auto q = qcloud[i];

		/* Get current nn distance, nn node, and nn pid of
		 * the query point, which were found in goDown previously
		 */
		min_dist = nn_dist[i];
		min_id = nn_id[i];

		// Now go up the tree to find the actual min dist
		//
		int idx, idy, idz;
		auto dim = level_dim[0];
		auto lb = level_lb[0];
		auto ub = level_ub[0];
		auto nsize = node_size[0];

		id1ToId3(min_id, lb, dim, idx, idy, idz);

		for (int level = 0; level < height;
				level++, idx = div(idx, RES_X_), idy = div(idy, RES_Y_), idz = div(idz, RES_Z_)) {
			// Erase the stack
			stk_top = -1;

			// Get the information of the current level
			dim = level_dim[level];
			lb = level_lb[level];
			ub = level_ub[level];
			nsize = node_size[level];

			// Start and end indices of nodes to be examine at the current level
			int sx, sy, sz, ex, ey, ez;

			if (level == height - 1) {
				// At the root level, examine all nodes
				sx = lb.x;
				sy = lb.y;
				sz = lb.z;

				ex = ub.x;
				ey = ub.y;
				ez = ub.z;
			} else {
				// At other levels, examine all siblings of the current node
				sx = roundDown(idx, RES_X_);
				sy = roundDown(idy, RES_Y_);
				sz = roundDown(idz, RES_Z_);

				/* The ending indices must not exceed the upper index
				 * boundaries of the current level.
				 */
				ex = (sx + RES_X_ - 1 < ub.x) ? sx + RES_X_ - 1 : ub.x;
				ey = (sy + RES_Y_ - 1 < ub.y) ? sy + RES_Y_ - 1 : ub.y;
				ez = (sz + RES_Z_ - 1 < ub.z) ? sz + RES_Z_ - 1 : ub.z;

				/* The starting indices must not less than the lower
				 * index boundaries of the current level. */
				sx = (sx > lb.x) ? sx : lb.x;
				sy = (sy > lb.y) ? sy : lb.y;
				sz = (sz > lb.z) ? sz : lb.z;
			}

			int bid = level_id[level];	// The starting 1D index of the current level's nodes

			/* The 1D index of the current nearest node (at
			 * the current level) is used to determine if
			 * a node is a sibling of this node or the node
			 * itself.
			 */
			min_id = bid + id3ToId1(idx, idy, idz, lb, dim);

			for (int ssx = sx; ssx <= ex; ssx++) {
				for (int ssy = sy; ssy <= ey; ssy++) {
					for (int ssz = sz; ssz <= ez; ssz++) {
						// Get the 1D index of the node
						int sid = bid + id3ToId1(ssx, ssy, ssz, lb, dim);

						cur_dist = dist(q, ssx, ssy, ssz, nsize);

						/* If a sibling is not empty and its distance to the query point
						 * is less than the current min_dist, push it to the stack.
						 */
						if (sid != min_id && status[sid] > 0 && min_dist > cur_dist) {
							++stk_top;

							// Each stack element is a pair of (node's level, node's index)
							stack[index + stk_top * offset] = make_int2(level, sid - bid);
						}
					}
				}
			}

			/* To examine the sub-trees that may contain the actual
			 * nearest neighbor, each thread loops until the stack is empty.
			 */
			while (stk_top >= 0) {
				/* In each iteration, pop the node at the top of the stack,
				 * and goDown the node. */
				int2 cur_node = stack[index + stk_top * offset];
				--stk_top;

				int cur_idx, cur_idy, cur_idz;

				dim = level_dim[cur_node.x];
				lb = level_lb[cur_node.x];

				// Convert 1D array index to 3D global index
				id1ToId3(cur_node.y, lb, dim, cur_idx, cur_idy, cur_idz);

				// If this node is a leaf, check its points and update the nearest neighbor if exist
				if (cur_node.x == 0) {
					cur_dist = distance(q, centroids[cur_node.y]);

					if (cur_dist < min_dist) {
						min_dist = cur_dist;
						min_id = cur_node.y;
					}
				} else {
					/* Otherwise, examine the node's children and push them
					 * to the stack if the children may contain the actual
					 * nearest neighbor.
					 */

					// Get the information of the lower level
					dim = level_dim[cur_node.x - 1];
					ub = level_ub[cur_node.x - 1];
					lb = level_lb[cur_node.x - 1];
					nsize = node_size[cur_node.x - 1];

					/* Compute the range (start, end) of indices of the children
					 * of the current node. The range must be inside the lower
					 * and upper index boundaries of the lower level (lb and ub).
					 */
					sx = cur_idx * RES_X_;
					sy = cur_idy * RES_Y_;
					sz = cur_idz * RES_Z_;

					ex = (sx + RES_X_ - 1 < ub.x) ? sx + RES_X_ - 1 : ub.x;
					ey = (sy + RES_Y_ - 1 < ub.y) ? sy + RES_Y_ - 1 : ub.y;
					ez = (sz + RES_Z_ - 1 < ub.z) ? sz + RES_Z_ - 1 : ub.z;

					sx = (sx > lb.x) ? sx : lb.x;
					sy = (sy > lb.y) ? sy : lb.y;
					sz = (sz > lb.z) ? sz : lb.z;

					// The starting 1D index of the lower level's node
					bid = level_id[cur_node.x - 1];

					for (int j = sx; j <= ex; j++) {
						for (int k = sy; k <= ey; k++) {
							for (int l = sz; l <= ez; l++) {
								int id = bid + id3ToId1(j, k, l, lb, dim);

								cur_dist = dist(q, j, k, l, nsize);

								/* If a child is not empty and its distance to the query point
								 * is less than current min_dist, push its pair (level, id) to
								 * the stack.
								 */
								if (status[id] > 0 && min_dist >= cur_dist) {
									++stk_top;
									stack[index + stk_top * offset] = make_int2(cur_node.x - 1, id - bid);
								}
							}
						}
					}
				}
			}
		}

		nn_id[i] = min_id;
		nn_dist[i] = min_dist;
	}
}

/* Method 3: Multiple threads handles one point. Use warp-primitives
 * __shfl_xor_sync and __shfl_sync to exchange status of a stack between
 * threads in a group.
 *
 * Similar to method 1, threads go from bottom to top of the tree to
 * compare the distance between nodes and the query point with the current
 * min_dist of the query point. However, the major difference is:
 *
 * - In the method 1, each thread handles one query point. When it meets
 * a node, it checks ALL siblings of that node to find candidate nodes,
 * and pushes all candidate nodes to A SINGLE stack of that thread. It then
 * pop elements from the stack one by one. For each popped node, the thread
 * examines children of that node to find candidate nodes. Candidate nodes
 * are then pushed to the same stack and so on.
 *
 * - In the method 3, a group of (RES_X_ * RES_Y_ * RES_Z_) threads handles
 * one query point (currently a group is equal to a warp). When they meets
 * a node (while moving to top), each thread in the group checks ONE sibling
 * of that node. Threads in a group cannot share a SINGLE stack because
 * conflict may occur if multiple threads try to push their candidate nodes
 * to the stack. Instead, each thread is given one stack (depth is equal
 * to the height of the tree).
 *
 * However, those stacks cannot work individually (aka use individual stack
 * pointer) because threads in a group always need to work on children of a
 * same parent node. To that end, we use a bit mask and a stack pointer
 * (stk_level) which are shared among all threads in a group to represent
 * the status of all nodes in a level examined by that group. If a node
 * becomes candidate, the corresponding bit is set. If the mask is not zero
 * (means at least one node in the group is candidate), candidate nodes
 * are pushed to stacks by their threads, and the stk_level increase by 1.
 * When all nodes in the same level are examined by the group, they are removed
 * from stacks and stk_level decrease by 1.
 *
 * The function uses 2 stacks:
 * - Data stack: stores the pair (node's level, node's index). Each thread is
 * given one data stack.
 * - Status stack: stores the bit mask of the children belong to the same
 * parent, which are handled by a group. When all bit are zero, which means all
 * children are examined, the bit mask is removed from the status stack.
 *
 * Input:
 * 		@qx, qy, qz: the coordinates of query points
 * 		@q_num: number of query points
 * 		@pnum: number of points per tree node
 * 		@x, y, z: coordinates of the points in the tree
 * 		@level_dim: 3D dimensions (in number of nodes) of each tree level
 * 		@level_lb, level_ub: lower and upper 3D index boundaries of each tree level
 * 		@node_size: resolutions of nodes of each tree level
 * 		@level_id: starting index (index of node 0) of each level in the whole
 * 					1D array nodes of the tree.
 * 		@height: height aka number of level of the tree
 * 		@starting_point_id, point_id: used to access points belong to each voxel
 * 		@stack: used to perform depth-first traverse sub-trees
 *
 * Output:
 * 		@nn_dist: the actual nearest distance
 * 		@nn_id: the index of the tree node that contains the nearest neighbor point
 * 		@nn_pid: the index of the nearest neighbor point
 */
__global__ void nnSearchGoUp3(FCloudPtr qcloud, int q_num, FCloudPtr centroids, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id, int height,
								int2 *stack, float *nn_dist, int *nn_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = blockDim.x * gridDim.x;

	int stk_level = 0;		// Top level of the stack
	float min_dist;
	int min_id;
	float cur_dist;
	int bid;			// Starting 1D index of a level
	int local_id = threadIdx.x & (GROUP_SIZE_ - 1);		// The local index in a group of a thread (lane_id)
	int group_id = (index / GROUP_SIZE_) * GROUP_SIZE_;	// The global index of the group the thread belongs to
	int local_group_id = threadIdx.x / GROUP_SIZE_;		// Index of the group in the block
	int local_group_num = blockDim.x / GROUP_SIZE_;		// Number of groups in a block

	/* 3D local index of the thread in its group.
	 * They are used to compute the index of the tree
	 * node handled by the thread.
	 */
	int lidx, lidy, lidz;

	/* A shared memory buffer to record the status of each level in the stack.
	 * If a bit is set, then a tree node is pushed to that bit location.
	 */
	__shared__ uint32_t stack_status[BLOCK_SIZE_X * 8];		// Assume the maximum height of a tree is 8

	/* We use a bit mask to represent the status of children of a
	 * tree node. If a child node has distance to a query point less
	 * than the current min_dist of that point, the bit corresponding
	 * to the node is set, and the child node is pushed to a stack.
	 *
	 * The level_mask is shared between threads in the group, so all
	 * threads are aware of the status of all nodes they are handling.
	 */
	uint32_t level_mask;

	// Convert 1D lane id to 3D local id
	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = index; i < GROUP_SIZE_ * q_num; i += offset) {
		int qid = i / GROUP_SIZE_;	// index of the query point, GROUP_SIZE_ (32) threads handle one point
		auto q = qcloud[qid];

		// Get current nn distance, nn node, and nn pid
		min_dist = nn_dist[qid];
		min_id = nn_id[qid];

		int idx, idy, idz;
		auto dim = level_dim[0];
		auto lb = level_lb[0];
		auto ub = level_ub[0];
		auto nsize = node_size[0];

		id1ToId3(min_id, lb, dim, idx, idy, idz);

		// Now go up the tree to find the actual min dist
		for (int level = 0; level < height;
				level++, idx = div(idx, RES_X_), idy = div(idy, RES_Y_), idz = div(idz, RES_Z_)) {

			/* At each level, examine all siblings oif the current node. */

			// Get the information of the current level
			dim = level_dim[level];
			lb = level_lb[level];
			ub = level_ub[level];
			nsize = node_size[level];

			// 3D index of the sibling
			int sbx, sby, sbz;

			if (level == height - 1) {
				// At the root level, examine all nodes
				sbx = lb.x + lidx;
				sby = lb.y + lidy;
				sbz = lb.z + lidz;
			} else {
				// At other levels, examine all siblings of the current node
				sbx = roundDown(idx, RES_X_) + lidx;
				sby = roundDown(idy, RES_Y_) + lidy;
				sbz = roundDown(idz, RES_Z_) + lidz;
			}

			bid = level_id[level];

			// The 1D index of the sibling node handled by the thread
			min_id = bid + id3ToId1(idx, idy, idz, lb, dim);

			// Clear the mask initially
			level_mask = 0x0000;

			// Empty the stack
			stk_level = 0;

			/* The sibling's indices must  be inside the lower and upper
			 * index boundaries of the current level.
			 */
			if (sbx >= lb.x && sby >= lb.y && sbz >= lb.z &&
					sbx <= ub.x && sby <= ub.y && sbz <= ub.z) {
				int sid = bid + id3ToId1(sbx, sby, sbz, lb, dim);

				cur_dist = dist(q, sbx, sby, sbz, nsize);

				/* If a sibling is not empty and its distance to the query point
				 * is less than the current min_dist, push it to the stack */
				if (sid != min_id && status[sid] > 0 && min_dist >= cur_dist) {
					stack[index + stk_level * offset] = make_int2(level, sid - bid);	// index = node_id + group_id
					level_mask |= (1 << local_id);
				}
			}

			// Compute the status of the current level of the stack
			for (int group_offset = GROUP_SIZE_ >> 1; group_offset > 0; group_offset >>= 1) {
				level_mask |= __shfl_xor_sync(FULL_MASK, level_mask, group_offset, GROUP_SIZE_);
			}

			if (level_mask)
				++stk_level;

			if (local_id == 0 && stk_level > 0 && level_mask > 0)
				stack_status[local_group_id  + (stk_level - 1) * local_group_num] = level_mask;

			// Loop until the stack is empty
			while (stk_level > 0) {
				// Get the status mask at the top of the status stack
				level_mask = stack_status[local_group_id  + (stk_level - 1) * local_group_num];

				// Get the index of a candidate node
				int node_id = __ffs(level_mask) - 1;

				/* Get the location of the first set bit of the current level.
				 * It is also the location of the set */

				stack_status[local_group_id  + (stk_level - 1) * local_group_num] &= (~(1 << node_id));

				int2 cur_node = stack[node_id + group_id + (stk_level - 1) * offset];

				/* If this is the last bit 1 in level_mask, that means all
				 * children nodes of this level are now examined. Lower
				 * the stack pointer.
				 */
				if (level_mask == (1 << node_id)) {
					--stk_level;
				}

				// Now examine the current node
				int cur_idx, cur_idy, cur_idz;

				dim = level_dim[cur_node.x];
				lb = level_lb[cur_node.x];

				id1ToId3(cur_node.y, lb, dim, cur_idx, cur_idy, cur_idz);

				if (cur_node.x == 0) {
					/* If the current node is a leaf, compare the min_dist
					 * with the distance from query point to the leaf's centroid.
					 */
					cur_dist = distance(q, centroids[cur_node.y]);

					if (min_dist > cur_dist) {
						min_dist = cur_dist;
						min_id = cur_node.y;
					}

				} else {

					/* If this is an inner node (has children node), examine
					 * its children. If the distance from the query point to
					 * a child node is less than the current nearest distance,
					 * push the child to the stack.
					 */
					dim = level_dim[cur_node.x - 1];
					lb = level_lb[cur_node.x - 1];
					ub = level_ub[cur_node.x - 1];
					nsize = node_size[cur_node.x - 1];

					// Determine the 3D index of the child node to be examined
					int cbx = cur_idx * RES_X_ + lidx;
					int cby = cur_idy * RES_Y_ + lidy;
					int cbz = cur_idz * RES_Z_ + lidz;

					// Get the starting 1D index of children nodes
					bid = level_id[cur_node.x - 1];

					// Clear the bit mask
					level_mask = 0;

					/* Always check this condition to make sure
					 * the node is stay in the range of the level
					 */
					if (cbx >= lb.x && cby >= lb.y && cbz >= lb.z &&
							cbx <= ub.x && cby <= ub.y && cbz <= ub.z) {
						int id = bid + id3ToId1(cbx, cby, cbz, lb, dim);

						cur_dist = dist(q, cbx, cby, cbz, nsize);

						/* If a child is not empty and its distance to the query point
						 * is less than current min dist, push it to the stack
						 */
						if (status[id] > 0 && min_dist >= cur_dist) {
							stack[index + stk_level * offset] = make_int2(cur_node.x - 1, id - bid);
							level_mask |= (1 << local_id);
						}
					}

					/* Exchange information between threads in the group to make
					 * sure all of them are aware of the status of all children nodes.
					 */
					for (int group_offset = GROUP_SIZE_ >> 1; group_offset > 0; group_offset >>= 1) {
						level_mask |= __shfl_xor_sync(FULL_MASK, level_mask, group_offset, GROUP_SIZE_);
					}

					/* If the level_mask is not zero, there exist candidate nodes.
					 * Increase the stack pointer by 1.
					 */
					if (level_mask)
						stk_level++;

					/* Push the bit mask to the status stack if it is not zero. */
					if (local_id == 0 && stk_level > 0 && level_mask > 0)
						stack_status[local_group_id + (stk_level - 1) * local_group_num] = level_mask;
					__syncwarp();
				}
			}
		}

		if (local_id == 0) {
			nn_id[qid] = min_id;
			nn_dist[qid] = min_dist;
		}
	}
}

float distance(float qx, float qy, float qz,
				int idx, int idy, int idz,
				Vector3f nsize)
{
	float lx, ly, lz, ux, uy, uz;

	lx = idx * nsize.x;
	ly = idy * nsize.y;
	lz = idz * nsize.z;

	ux = lx + nsize.x;
	uy = ly + nsize.y;
	uz = lz + nsize.z;

	float tx = ((qx > ux) ? qx - ux : ((qx < lx) ? lx - qx : 0));
	float ty = ((qy > uy) ? qy - uy : ((qy < ly) ? ly - qy : 0));
	float tz = ((qz > uz) ? qz - uz : ((qz < lz) ? lz - qz : 0));

	return sqrt(tx * tx + ty * ty + tz * tz);
}




/* Test the goDown GPU kernels (method 1, 2, 3). Used for
 * debugging.
 *
 * For each point, this function go down using the same
 * policy as the GPU kernels. It then compare the final
 * result with the result from the GPU kernels. If the
 * cpu's result and the gpu's result at a query point is
 * different, the function prints out the path from bottom
 * to top of the cpu and the gpu. This makes the debugging
 * process easier.
 *
 * Notice: the cpu's path and gpu's path may not be the same,
 * because for each query point, there may be multiple nodes
 * at the same level has the same distance to the query point.
 *
 * Input:
 * 		@qx, qy, qz: coordinates of the query points
 * 		@nearest_node: indices of the nodes which are the result
 * 						of the GPU's goDown
 * 		@gpu_min_dist: distance from query points to their nearest
 * 						nodes found by the GPU's goDown
 */
template <typename PointType>
void PointlessTree<PointType>::testGoDown(typename PointCloud<PointType>::Ptr &qcloud,
											const device_vector<int> &nearest_node,
											const device_vector<float> &gpu_min_dist)
{
	HostPLTree<PointType> host_tree(*this);
	typename pcl::PointCloud<PointType>::Ptr hc = qcloud->host_cloud();
	std::vector<int> hnn_id = nearest_node.host_vector();
	std::vector<float> hnn_dist = gpu_min_dist.host_vector();

	for (size_t i = 0; i < hc->size(); ++i) {
		PointType p = hc->at(i);
		int test_nn_id;
		float test_nn_dist;

		host_tree.goDown(p, test_nn_id, test_nn_dist);

		if (test_nn_id != hnn_id[i]) {
			printf("[PointlessTree.cu, %d] %s::Test goDown failed at point %d\n", __LINE__, __func__, i);
			printf("[PointlessTree.cu, %d] %s::nn test leafId = %d, nn gpu leafId = %d\n",
					__LINE__, __func__, test_nn_id, hnn_id[i]);
			printf("[PointlessTree.cu, %d] %s::Test nn_dist = %f, gpu_dist = %f\n",
					__LINE__, __func__, test_nn_dist, hnn_dist[i]);

			std::vector<Vector3i> test_path;
			std::vector<Vector3i> gpu_path;

			host_tree.traceToTop(test_nn_id, test_path);
			host_tree.traceToTop(hnn_id[i], gpu_path);

			printf("[PointlessTree.cu, %d] %s::Test path:\n", __LINE__, __func__);

			for (auto nid : test_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			printf("[PointlessTree.cu, %d] %s::GPU path:\n", __LINE__, __func__);

			for (auto nid : gpu_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			exit(1);
		}
	}

	printf("[PointlessTree.cu, %d] %s::Test goDown PASSED!\n", __LINE__, __func__);
}





/* Testing function for the nnSearchGoUp.
 *
 * For each query point, find its nearest neighbor using brute force (BF),
 * then compare the nearest distance found by BF with the nearest distance
 * found by nnSearchGoUp. If there is significant difference (absolute value
 * of the subtraction is larger than 0.0000001), the test fails.
 *
 * In the case the test fails, it prints the index of the first mismatch
 * query point, and its debugging information: paths from bottom to top of
 * the CPU and GPU NN Search.
 *
 * Input:
 * 		@qx, qy, qz	: GPU-side coordinates of query points
 * 		@nn_pid		: GPU-side indices of the nearest neighbors of query points
 * 		@nn_id		: GPU-side indices of leaves that contain the nearest neighbors of query points
 * 		@nn_dist	: GPU-side the nearest distances between the query points and their NNs
 */
template <typename PointType>
void PointlessTree<PointType>::testGoUp(typename PointCloud<PointType>::Ptr &qcloud,
										device_vector<int> &nn_id, device_vector<float> &nn_dist)
{
	HostPLTree<PointType> host_tree(*this);
	typename pcl::PointCloud<PointType>::Ptr hc = qcloud->host_cloud();
	std::vector<int> hnn_id = nn_id.host_vector();
	std::vector<float> hnn_dist = nn_dist.host_vector();

	for (size_t i = 0; i < hc->size(); ++i) {
		PointType p = hc->at(i);
		int tnn_id;
		float tnn_dist;

		host_tree.BFNNSearch(p, tnn_id, tnn_dist);

		// Compare test values and print out the path if error occurs
		if (tnn_id != hnn_id[i] && std::abs(tnn_dist - hnn_dist[i]) > 0.000001) {
			printf("[PointlessTree.cu, %d] %s::Error: Mismatched at query point %d\n", __LINE__, __func__, i);
			printf("[PointlessTree.cu, %d] %s::Test nn leafId = %d, gpu nn leafId = %d\n",
					__LINE__, __func__, tnn_id, hnn_id[i]);
			printf("[PointlessTree.cu, %d] %s::Test nn_dist = %f, gpu_dist = %f\n",
					__LINE__, __func__, tnn_dist, hnn_dist[i]);

			std::vector<Vector3i> test_path, gpu_path;

			host_tree.traceToTop(tnn_id, test_path);
			host_tree.traceToTop(hnn_id[i], gpu_path);

			printf("[PointlessTree.cu, %d] %s::Test path\n", __LINE__, __func__);
			for (auto nid : test_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			printf("\nTrace-up gpu node: \n");
			for (auto nid : gpu_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			exit(1);
		}
	}

	printf("[PointlessTree.cu, %d] %s::Test goUp PASSED\n", __LINE__, __func__);
}

//#define PROF_TREE_NNS_	// For profiling execution time of NN Search

/* 2-pass parallel nearest neighbor search */
template <typename PointType>
void PointlessTree<PointType>::NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
										device_vector<int> &nn_id,
										device_vector<float> &nn_dist)
{
	int qpnum = qcloud->size();

	if (qpnum <= 0) {
		return;
	}

	nn_id.resize(qpnum);
	nn_dist.resize(qpnum);

	int height = level_lb_.size();
	int thread_num = qpnum * GROUP_SIZE_;
	int block_x = (thread_num > BLOCK_SIZE_X2) ? BLOCK_SIZE_X : thread_num;
	int grid_x = (thread_num + block_x - 1) / block_x;

	nnSearchGoDown3<<<grid_x, block_x>>>(qcloud->data(), qpnum, centroids_->data(), status_.data(),
											level_dim_.data(), level_lb_.data(), level_ub_.data(),
											node_size_.data(), level_id_.data(), height,
											nn_dist.data(), nn_id.data());
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	// TODO: stack size = height * block_x * grid_x
	device_vector<int2> stack(height * block_x * grid_x, __allocator);

	nnSearchGoUp3<<<grid_x, block_x>>>(qcloud->data(), qpnum, centroids_->data(), status_.data(),
										level_dim_.data(), level_lb_.data(), level_ub_.data(),
										node_size_.data(), level_id_.data(), height,
										stack.data(), nn_dist.data(), nn_id.data());
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());
}

template <typename PointType>
void PointlessTree<PointType>::NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
										device_vector<float> &nn_dist)
{
	if (qcloud->size() <= 0) {
		return;
	}

	device_vector<int> nn_id(__allocator);

	NNSearch(qcloud, nn_id, nn_dist);
}

template <typename PointType>
void PointlessTree<PointType>::update(typename PointCloud<PointType>::Ptr &new_cloud)
{
	int new_point_num = new_cloud->size();

	if (new_point_num <= 0) {
		return;
	}

	/* Step 1: Compute new boundaries of the newly added points */
	Vector3i new_bot_ub, new_bot_lb;

	computeBoundaries(new_cloud->data(), new_bot_ub, new_bot_lb);

	/* Step 2: Extend current buffers to hold new points */
	extendMemory(new_bot_ub, new_bot_lb);

	/* Step 3: Re-calculate the centroids of tree nodes */
	updateTreeContent(new_cloud, new_bot_ub, new_bot_lb);
}

template <typename PointType>
void PointlessTree<PointType>::computeBoundaries(FCloudPtr cloud, Vector3i &bot_ub, Vector3i &bot_lb)
{
	float max_x, max_y, max_z, min_x, min_y, min_z;

	minMax(cloud.X(), min_x, max_x, cloud.size(), __allocator);
	minMax(cloud.Y(), min_y, max_y, cloud.size(), __allocator);
	minMax(cloud.Z(), min_z, max_z, cloud.size(), __allocator);

	bot_lb.x = static_cast<int>(floor(min_x / res_.x));
	bot_lb.y = static_cast<int>(floor(min_y / res_.y));
	bot_lb.z = static_cast<int>(floor(min_z / res_.z));

	bot_ub.x = static_cast<int>(floor(max_x / res_.x));
	bot_ub.y = static_cast<int>(floor(max_y / res_.y));
	bot_ub.z = static_cast<int>(floor(max_z / res_.z));

	/**
	 * The tree may expand as new points are added. In such cases,
	 * a new larger tree is created and data are copied from the
	 * old tree to the new one. The copy process double the GPU
	 * memory needed, so it should be avoid as much as possible.
	 *
	 * To that end, a tree is over-allocated. Its actual size is
	 * larger than the size needed to cover a point cloud, so
	 * when new points enter, the tree may still be large enough
	 * to hold both the old and new centroids of points.
	 *
	 * The boundaries of the tree, for that reason, are larger
	 * than the one computed above.
	 */
	bot_lb.x = roundDown(bot_lb.x, MAX_BX_);
	bot_lb.y = roundDown(bot_lb.y, MAX_BY_);
	bot_lb.z = roundDown(bot_lb.z, MAX_BZ_);

	bot_ub.x = roundUp(bot_ub.x, MAX_BX_);
	bot_ub.y = roundUp(bot_ub.y, MAX_BY_);
	bot_ub.z = roundUp(bot_ub.z, MAX_BZ_);
}

/* Copy data from the source level to the destination level.
 *
 * Input:
 * 		@src_pnum: the source number of points per node array
 * 		@src_upper_x, src_upper_y, src_upper_z:	the source upper coordinates array
 * 		@src_dim: the dimensions of the source level
 * 		@src_lb: the lower boundaries of the source level
 * 		@src_node_num: the number of nodes of the source level
 * 		@dst_dim: the dimensions of the destination level
 * 		@dst_lb: the lower boundaries of the destination level
 *
 * Output:
 * 		@dst_pnum: the destination number of points per node array
 */
__global__ void copyTreeNode(uint32_t *src_status, Vector3i src_dim, Vector3i src_lb, int src_node_num,
								uint32_t *dst_status, Vector3i dst_dim, Vector3i dst_lb)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_node_num; i += stride) {
		// Here i is the 1D index of the current source node in the source level
		// Convert source node's 1D array index to 3D global index
		auto src_id3 = id1ToId3(i, src_lb, src_dim);

		// Compute the 1D index of the source node in the destination level
		int dst_id1 = id3ToId1(src_id3, dst_lb, dst_dim);

		// Copy data from the source to the destination
		// Data here is only the number of points in the node
		dst_status[dst_id1] = src_status[i];
	}
}



/* Copy the centroids from the source bottom level to the
 * destination bottom level.
 *
 * Input:
 * 		@src_cx, src_cy, src_cz: centroids of the source bottom level
 * 		@src_bottom_dim: dimensions of the source bottom level
 * 		@src_bottom_lb: lower boundaries of the source bottom level
 * 		@dst_bottom_dim: dimensions of the destination bottom level
 * 		@dst_bottom_lb: lower boundaries of the destination bottom level
 *
 * Output:
 * 		@dst_cx, dst_cy, dst_cz: centroids of the destination bottom level
 */
__global__ void copyLeafCentroids(FCloudPtr src_centroids, int src_leaf_num,
									Vector3i src_bottom_dim, Vector3i src_bottom_lb,
									Vector3i dst_bottom_dim, Vector3i dst_bottom_lb,
									FCloudPtr dst_centroids)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_leaf_num; i += stride) {
		// Convert i to 3D global index
		auto src_id3 = id1ToId3(i, src_bottom_lb, src_bottom_dim);

		// Compute the 1D array index of the source leaf in the destination grid
		int dst_id = id3ToId1(src_id3, dst_bottom_lb, dst_bottom_dim);

		dst_centroids.emplace(dst_id, src_centroids[i]);
	}
}

/* Allocate a new buffer and copy the data from the old tree
 * to the new buffer. */
template <typename PointType>
void PointlessTree<PointType>::extendMemory(Vector3i new_bot_ub, Vector3i new_bot_lb)
{
	/* Extend buffers for the tree only if there exist new
	 * boundary that is greater than the corresponding old one.
	 */
	if (new_bot_ub.x > bot_ub_.x || new_bot_ub.y > bot_ub_.y || new_bot_ub.z > bot_ub_.z ||
			new_bot_lb.x < bot_lb_.x || new_bot_lb.y < bot_lb_.y || new_bot_lb.z < bot_lb_.z) {

		// Compute new boundaries at the bottom level
		if (new_bot_ub.x < bot_ub_.x) {
			new_bot_ub.x = bot_ub_.x;
		}

		if (new_bot_ub.y < bot_ub_.y) {
			new_bot_ub.y = bot_ub_.y;
		}

		if (new_bot_ub.z < bot_ub_.z) {
			new_bot_ub.z = bot_ub_.z;
		}

		if (new_bot_lb.x > bot_lb_.x) {
			new_bot_lb.x = bot_lb_.x;
		}

		if (new_bot_lb.y > bot_lb_.y) {
			new_bot_lb.y = bot_lb_.y;
		}

		if (new_bot_lb.z > bot_lb_.z) {
			new_bot_lb.z = bot_lb_.z;
		}


		// Allocate new buffers
		device_vector<uint32_t> new_status(__allocator);
		device_vector<Vector3i> new_level_dim(__allocator), new_level_lb(__allocator),
								new_level_ub(__allocator);
		device_vector<int> new_level_id(__allocator);
		device_vector<Vector3f> new_node_size(__allocator);

		typename PointCloud<PointType>::Ptr new_centroids(new PointCloud<PointType>(__allocator));

		allocateTree(new_bot_lb, new_bot_ub, new_status,
						new_level_dim, new_level_lb, new_level_ub,
						new_node_size, new_level_id, new_centroids);

		// Copy bottom only. Higher levels are re-built later.
		auto src_dim = level_dim_[0];
		auto src_lb = level_lb_[0];

		auto dst_dim = new_level_dim[0];
		auto dst_lb = new_level_lb[0];

		int leaf_num = centroids_->size();

		if (leaf_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num, copyTreeNode,
							status_.data(), src_dim, src_lb, leaf_num,
							new_status.data(), dst_dim, dst_lb));

			/* Copy the centroids of the bottom level from the current
			 * tree to the newly allocated tree.
			 */
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num, copyLeafCentroids,
							centroids_->data(), leaf_num, src_dim, src_lb,
							dst_dim, dst_lb, new_centroids->data()));

			// Replace the centroid list with the new one
			centroids_ = std::move(new_centroids);
		}

		// Replace the old buffers by the new ones
		status_ = std::move(new_status);

		level_dim_ = std::move(new_level_dim);
		level_lb_ = std::move(new_level_lb);
		level_ub_ = std::move(new_level_ub);
		level_id_ = std::move(new_level_id);
		node_size_ = std::move(new_node_size);

		bot_lb_ = new_bot_lb;
		bot_ub_ = new_bot_ub;
	}
}


/* Update the tree's content as new points are added.
 * 2 steps:
 *
 * 1. Update the bottom level's starting_pid_ and point_id_
 * 2. Update the number of points per node in all levels
 *
 * Input:
 * 		@x, y, z: coordinates of new points to be added
 * 		@new_point_num: number of new points to be added
 *
 */
template <typename PointType>
void PointlessTree<PointType>::updateTreeContent(typename PointCloud<PointType>::Ptr &new_cloud,
													Vector3i new_bot_ub, Vector3i new_bot_lb)
{
	int new_point_num = new_cloud->size();

	if (new_point_num <= 0) {
		return;
	}

	// Put the points to leaf nodes
	scatterPointsToLeaves(new_cloud, new_bot_ub, new_bot_lb);

	// Re-build the tree
	buildLevelMultiPass();
}

template <typename PointType>
HostPLTree<PointType>::HostPLTree(const PointlessTree<PointType> &gpu_tree)
{
	status_ = gpu_tree.status_.host_vector();
	level_id_ = gpu_tree.level_id_.host_vector();
	level_dim_ = gpu_tree.level_dim_.host_vector();
	level_lb_ = gpu_tree.level_lb_.host_vector();
	level_ub_ = gpu_tree.level_ub_.host_vector();

	node_size_ = gpu_tree.node_size_.host_vector();
	res_ =  gpu_tree.res_;

	bot_lb_ = gpu_tree.bot_lb_;
	bot_ub_ = gpu_tree.bot_ub_;
	bot_dim_ = bot_ub_ - bot_lb_ + 1;

	cx_ = (gpu_tree.centroids_->X()).host_vector();
	cy_ = (gpu_tree.centroids_->Y()).host_vector();
	cz_ = (gpu_tree.centroids_->Z()).host_vector();
}

/* Given a query point and 3D index of a parent node,
 * compute the index of a child node that is the nearest
 * node to the query point.
 *
 * Used to debug the GPU nnSearchGoDown.
 *
 * Input:
 * 		@level: the level of the parent node
 * 		@idx, idy, idz: the 3D index of the parent node
 * 		@qx, qy, qz: the coordinate of the query point
 * 		@pnum: host-side tree nodes (copied from pnum_)
 *
 * Output:
 * 		@idx, idy, idz: 3D index of the child node that is
 *	 					the nearest node to the query point (by goDown)
 * 		@id: 1D index of the output node
 */
template <typename PointType>
void HostPLTree<PointType>::goDown(PointType p, int level, Vector3i &node_id)
{
	int cbase = level_id_[level - 1];

	auto cdim = level_dim_[level - 1];
	auto clb = level_lb_[level - 1];
	auto cub = level_ub_[level - 1];
	auto cnsize = node_size_[level - 1];

	int idx, idy, idz;

	int sx, sy, sz, ex, ey, ez;

	int height = level_dim_.size();

	if (level == height) {
		sx = clb.x;
		sy = clb.y;
		sz = clb.z;

		ex = cub.x;
		ey = cub.y;
		ez = cub.z;
	} else {
		sx = node_id.x * RES_X_;
		sy = node_id.y * RES_Y_;
		sz = node_id.z * RES_Z_;

		ex = (sx + RES_X_ - 1 < cub.x) ? sx + RES_X_ - 1 : cub.x;
		ey = (sy + RES_Y_ - 1 < cub.y) ? sy + RES_Y_ - 1 : cub.y;
		ez = (sz + RES_Z_ - 1 < cub.z) ? sz + RES_Z_ - 1 : cub.z;
	}


	float min_dist = FLT_MAX;

	idx = idy = idz = -1;

	for (int i = sx; i <= ex; i++) {
		for (int j = sy; j <= ey; j++) {
			for (int k = sz; k <= ez; k++) {
				int cid = cbase + id3ToId1(i, j, k, clb, cdim);
				float cur_dist = dist(p, i, j, k, cnsize);

				if (status_[cid] > 0 && min_dist > cur_dist) {
					min_dist = cur_dist;
					idx = i;
					idy = j;
					idz = k;
				}
			}
		}
	}

	node_id.x = idx;
	node_id.y = idy;
	node_id.z = idz;
}

/* Go down for one query point (qx, qy, qz).
 *
 * Go from top to bottom  to find the node that is
 * the nearest to the query point. At the bottom level,
 * find the point that is the nearest to the query point.
 *
 * This function is used to debug the GPU goDown.
 *
 * Params:
 * 		@p[in]: coordinates of the query point
 * 		@nn_id[out]: index of the current nearest leaf
 * 		@nn_dist[out]: distance to the centroid of the nearest leaf
 */
template <typename PointType>
void HostPLTree<PointType>::goDown(PointType p, int &nn_id, float &nn_dist)
{
	// Indices of candidate nodes while going down (3D + 1D)
	Vector3i node_id;
	int height = level_dim_.size();

	for (int level = height; level > 0; level--) {
		goDown(p, level, node_id);
	}

	nn_id = id3ToId1(node_id, level_lb_[0], level_dim_[0]);

	nn_dist = std::sqrt(pow(p.x - cx_[nn_id], 2.0) + pow(p.y - cy_[nn_id], 2.0) +
						pow(p.z - cz_[nn_id], 2.0));
}

/* Go from bottom to height and print out the path
 * that lead from top to the specified node. Usually
 * height is set by height_ (tree height).
 *
 * Input:
 * 		@x, y, z: the 3D indices of the node
 * 		@height: the level to which we want to print
 * 					out the path from bottom
 */
template <typename PointType>
void HostPLTree<PointType>::traceToTop(int leaf_id, std::vector<Vector3i> &path)
{
	int height = level_lb_.size();

	path.resize(height);

	auto id = id1ToId3(leaf_id, level_lb_[0], level_dim_[0]);

	path[0] = id;

	for (int level = 1; level < height; ++level) {
		id.x = div(id.x, RES_X_);
		id.y = div(id.y, RES_Y_);
		id.z = div(id.z, RES_Z_);

		path[level] = id;
	}
}

/* The brute-force nearest neighbor search.
 * Search for the nearest leaf of the input point by
 * checking every leaf.
 * Use to test the result of the nearest neighbor search.
 *
 * TODO: the trace_up part is wrong. Need to fix later.
 *
 * Input:
 * 		@qx, qy, qz: the coordinate of the query point to be searched
 * 		@centr_x, centr_y, centr_z: the host-side coordinates of points in the tree
 *
 * Output:
 * 		@nn_pid: index of the point which is the nearest to the query point
 * 		@nn_id: index of the leaf containing the nearest neighbor
 * 		@nn_dist: distance between the nearest point and the query point
 */
template <typename PointType>
void HostPLTree<PointType>::BFNNSearch(PointType p, int &nn_id, float &nn_dist)
{
	nn_dist = std::numeric_limits<float>::max();

	for (size_t i = 0; i < cx_.size(); ++i) {
		float cx = cx_[i] - p.x;
		float cy = cy_[i] - p.y;
		float cz = cz_[i] - p.z;
		float cur_dist = sqrt(cx * cx + cy * cy + cz * cz);

		if (cur_dist < nn_dist) {
			nn_dist = cur_dist;
			nn_id = i;
		}
	}
}

template class PointlessTree<pcl::PointXYZ>;
template class PointlessTree<pcl::PointXYZI>;

}

}
