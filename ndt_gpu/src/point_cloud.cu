#include <iostream>
#include <utility>
#include <vector>

#include <ndt_gpu/common.h>
#include <ndt_gpu/point_cloud.h>
#include <ndt_gpu/utilities.h>

#include <ndt_gpu/gmemory.h>

namespace gpu {


template <typename PointType, typename Scalar>
__global__ void convertInput(PointType* input, Scalar* out_x, Scalar* out_y,
								Scalar* out_z, int point_num)
{
  int idx = threadIdx.x + blockIdx.x * blockDim.x;
  int stride = blockDim.x * gridDim.x;

  for (int i = idx; i < point_num; i += stride)
  {
	  PointType tmp = input[i];

	  out_x[i] = tmp.x;
	  out_y[i] = tmp.y;
	  out_z[i] = tmp.z;
  }
}

template <typename PointType, typename Scalar>
void PointCloud<PointType, Scalar>::setInputCloud(typename pcl::PointCloud<PointType>::Ptr &input)
{
	if (input->size() > 0) {
		point_num_ = input->size();

		device_vector<PointType> tmp(point_num_, __allocator);

		gcopyHtoD(tmp.data(), input->points.data(), sizeof(PointType) * point_num_);

		x_ = device_vector<Scalar>(point_num_, __allocator);
		y_ = device_vector<Scalar>(point_num_, __allocator);
		z_ = device_vector<Scalar>(point_num_, __allocator);

		checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num_, convertInput<PointType>,
						tmp.data(), x_.data(), y_.data(), z_.data(), point_num_));
	}
}

template <typename PointType, typename Scalar>
void PointCloud<PointType, Scalar>::
setInputCloud(typename std::list<typename pcl::PointCloud<PointType>::Ptr> &input_clouds)
{
	point_num_ = 0;

	for (auto ptr : input_clouds) {
		point_num_ += ptr->size();
	}

	if (point_num_ > 0) {
		x_ = device_vector<Scalar>(point_num_, __allocator);
		y_ = device_vector<Scalar>(point_num_, __allocator);
		z_ = device_vector<Scalar>(point_num_, __allocator);

		device_vector<PointType> tmp(point_num_, __allocator);

		// Overlap memory copy and kernel execution
		std::vector<cudaStream_t> streams(input_clouds.size());

		for (int i = 0; i < streams.size(); ++i) {
			checkCudaErrors(cudaStreamCreate(&streams[i]));
		}

		int i = 0;
		int accumulated_size = 0;

		for (auto ptr : input_clouds) {
			int current_size = ptr->size();

			gcopyHtoDAsync(tmp.data() + accumulated_size, ptr->points.data(),
							sizeof(PointType) * current_size, streams[i]);

			checkCudaErrors(launchASync<BLOCK_SIZE_X>(current_size, streams[i], convertInput<PointType>,
							tmp.data() + accumulated_size, x_.data() + accumulated_size,
							y_.data() + accumulated_size, z_.data() + accumulated_size,
							current_size));

			accumulated_size += current_size;
			++i;
		}

		checkCudaErrors(cudaDeviceSynchronize());

		for (int i = 0; i < streams.size(); ++i) {
			checkCudaErrors(cudaStreamDestroy(streams[i]));
		}
	}
}

template <typename PointType, typename Scalar>
void PointCloud<PointType, Scalar>::clear()
{
	x_.clear();
	y_.clear();
	z_.clear();

	point_num_ = 0;
}

/**
 * Resize the number of points in the point cloud.
 * 2 cases:
 * - If the number of new points is less than or equal to the current
 *   size of the point cloud, simply change the point_num_ value.
 *
 * - If the new number is greater than the reserved size, allocate
 *   new vectors and copy data from the old one to the new one.
 */
template <typename PointType, typename Scalar>
void PointCloud<PointType, Scalar>::resize(int new_point_num)
{
	int reserved_size = x_.size();

	if (new_point_num > reserved_size) {
		reserve(new_point_num);
	}

	point_num_ = new_point_num;
}

/**
 * Reserve the memory to contain at least new_size points.
 * 2 cases:
 * - If the current size of vectors is greater than or equal
 *   to the new_size, do nothing.
 * - If the current size of vectors is less than the new size,
 *   reserve new_size, then copy the data from the old vectors
 *   to the new ones.
 * In both cases, point_num_ does not change.
 */
template <typename PointType, typename Scalar>
void PointCloud<PointType, Scalar>::reserve(int new_size)
{
	int reserved_size = x_.size();

	if (reserved_size < new_size) {
		device_vector<Scalar> new_x(new_size, __allocator);
		device_vector<Scalar> new_y(new_size, __allocator);
		device_vector<Scalar> new_z(new_size, __allocator);

		if (point_num_ > 0) {
			gcopyDtoD(new_x.data(), x_.data(), sizeof(Scalar) * point_num_);
			gcopyDtoD(new_y.data(), y_.data(), sizeof(Scalar) * point_num_);
			gcopyDtoD(new_z.data(), z_.data(), sizeof(Scalar) * point_num_);
		}

		x_ = std::move(new_x);
		y_ = std::move(new_y);
		z_ = std::move(new_z);
	}
}

template class PointCloud<pcl::PointXYZ, float>;
template class PointCloud<pcl::PointXYZI, float>;
template class PointCloud<pcl::PointXYZ, double>;
template class PointCloud<pcl::PointXYZI, double>;
}

