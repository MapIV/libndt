#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cassert>

#include <ndt_gpu/MatrixDevice.h>
#include <ndt_gpu/MatrixHost.h>
#include <ndt_gpu/debug.h>

namespace gpu
{

template <typename Scalar>
__global__ void copyMatrixDevToDev(MatrixDevice<Scalar> input, MatrixDevice<Scalar> output)
{
  int row = threadIdx.x;
  int col = threadIdx.y;
  int rows_num = input.rows();
  int cols_num = input.cols();

  if (row < rows_num && col < cols_num)
    output(row, col) = input(row, col);
}

template <typename Scalar>
MatrixHost<Scalar>& MatrixHost<Scalar>::operator=(const MatrixDevice<Scalar> &input)
{
	assert((rows_ == input.rows() && cols_ == input.cols()));

	dim3 block(rows_, cols_, 1);
	dim3 grid(1, 1, 1);

	copyMatrixDevToDev<<<grid, block>>>(input, MatrixDevice<Scalar>(*this));
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	gcopyDtoH(host_buffer_.data(), device_buffer_.data(), sizeof(Scalar) * rows_ * cols_);

	return *this;
}

template class MatrixHost<float>;
template class MatrixHost<double>;

}
