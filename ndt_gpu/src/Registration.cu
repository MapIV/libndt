#include "ndt_gpu/Registration.h"
#include "ndt_gpu/debug.h"
#include <iostream>
#include "ndt_gpu/utilities.h"

namespace gpu
{

template <typename PointSourceType, typename PointTargetType, typename Scalar>
Registration<PointSourceType, PointTargetType, Scalar>::
Registration(GMemoryAllocator *allocator) :
	Base(allocator),
	final_transformation_(4, 4, __allocator),
	transformation_(4, 4, __allocator),
	previous_transformation_(4, 4, __allocator)
{
	max_iterations_ = 0;

	converged_ = false;
	nr_iterations_ = 0;

	transformation_epsilon_ = 0;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
Registration<PointSourceType, PointTargetType, Scalar>::
Registration(const Registration<PointSourceType, PointTargetType, Scalar>& other) :
	Base(other),
	final_transformation_(other.final_transformation_),
	transformation_(other.transformation_),
	previous_transformation_(other.previous_transformation_)
{
	transformation_epsilon_ = other.transformation_epsilon_;
	max_iterations_ = other.max_iterations_;

	// Original scanned point clouds
	source_cloud_ = other.source_cloud_;
	trans_cloud_ = other.trans_cloud_;

	converged_ = other.converged_;

	nr_iterations_ = other.nr_iterations_;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
Eigen::Matrix<float, 4, 4> Registration<PointSourceType, PointTargetType, Scalar>::
getFinalTransformation() const
{
	Eigen::Matrix<float, 4, 4> output;

	for (int r = 0; r < 4; ++r) {
		for (int c = 0; c < 4; ++c) {
			output(r, c) = final_transformation_.at(r, c);
		}
	}

	return output;
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void Registration<PointSourceType, PointTargetType, Scalar>::
align(const Eigen::Matrix<float, 4, 4>& guess)
{
  converged_ = false;

  final_transformation_ = transformation_ = previous_transformation_ =
		  MatrixHost<float>::Identity(4, 4, __allocator);

  computeTransformation(guess);
}

// Set input scan data
template <typename PointSourceType, typename PointTargetType, typename Scalar>
void Registration<PointSourceType, PointTargetType, Scalar>::
setInputSource(typename pcl::PointCloud<PointSourceType>::Ptr &input)
{
  if (input->size() > 0)
  {
    source_cloud_.reset(new PointCloud<PointSourceType>(__allocator));
    source_cloud_->setInputCloud(input);

    trans_cloud_.reset(new PointCloud<PointSourceType>(__allocator));
    *trans_cloud_ = *source_cloud_;
  }
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void Registration<PointSourceType, PointTargetType, Scalar>::
setInputSource(typename PointCloud<PointSourceType>::Ptr &input)
{
	if (input->size() > 0) {
		source_cloud_ = input;
		trans_cloud_.reset(new PointCloud<PointSourceType>(__allocator));
		*trans_cloud_ = *source_cloud_;
	}
}

template <typename PointSourceType, typename PointTargetType, typename Scalar>
void Registration<PointSourceType, PointTargetType, Scalar>::
computeTransformation(const Eigen::Matrix<float, 4, 4>& guess)
{
  printf("Unsupported by Registration\n");
}
}
