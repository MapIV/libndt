#include <cuda.h>
#include <cuda_runtime.h>

#include <vector>
#include <float.h>
#include <limits>
#include <utility>

#include <ndt_gpu/Octree.h>
#include <ndt_gpu/utilities.h>

namespace gpu {

namespace octree {

#define DEFAULT_RES_ (1.0)

#ifndef RES_X_
#define RES_X_ 	(4)
#define RES_SHIFT_X_	(2)
#endif

#ifndef RES_Y_
#define RES_Y_ (4)
#define RES_SHIFT_Y_	(2)
#endif

#ifndef RES_Z_
#define RES_Z_ (2)
#define RES_SHIFT_Z_	(1)
#endif

#ifndef GROUP_SIZE_
#define GROUP_SIZE_ 	(RES_X_ * RES_Y_ * RES_Z_)
#define GROUP_SHIFT_	(5)		// 2^5 = 32
#define GROUP_LID_CMP_	(31)
#endif

#ifndef FULL_MASK
#define FULL_MASK	(0xffffffff)
#endif

#ifndef TREE_MAX_HEIGHT_
#define TREE_MAX_HEIGHT_ (16)
#endif

/**
 * Default constructor. The tree is empty.
 */
template <typename PointType>
Octree<PointType>::Octree(GMemoryAllocator *allocator) :
	Base(allocator),
	status_(__allocator),
	level_id_(__allocator),
	level_dim_(__allocator),
	level_ub_(__allocator),
	level_lb_(__allocator),
	node_size_(__allocator),
	starting_pid_(__allocator),
	point_id_(__allocator)
{
	height_ = 0;

	leaf_num_ = 0;
	leaf_size_ = Vector3i(0, 0, 0);
	point_num_ = 0;
	res_ = Vector3f(0, 0, 0);

	min_b_.x = min_b_.y = min_b_.z = std::numeric_limits<int>::max();
	max_b_.x = max_b_.y = max_b_.z = std::numeric_limits<int>::lowest();

	node_num_ = 0;
	alloc_point_num_ = 0;

	cloud_.reset(new PointCloud<PointType>(__allocator));
}

// Added functions for incremental update
__host__ __device__ inline int roundUp(int input, int factor) {
	return (input < 0) ? (-((-input) / factor) * factor) : ((input  + factor - 1 ) / factor) * factor;
}

__host__ __device__ inline int roundDown(int input, int factor) {
	return (input < 0) ? (-(-input + factor - 1) / factor) * factor : (input / factor) * factor;
}

__host__ __device__ inline int div(int input, int divisor) {
	return (input < 0) ? (-(-input + divisor - 1) / divisor) : (input / divisor);
}

/**
 * Convert 1D array index to 3D index in a group of (RES_X_ * RES_Y_ * RES_Z_).
 * Use to compute the 3D index of a thread in a group of threads.
 * The 3D indices are later used as offsets to compute the index of the
 * tree node that the thread is in charged of.
 *
 * TODO:
 * This function uses bitwise operators so it needs to be tested whether it
 * is actually faster than using multiplication.
 *
 * Input:
 * 		@local_id: the 1D array index in the group
 * Output:
 *  	@lidx, lidy, lidz: the 3D index in the group
 *
 */
inline __host__ __device__ void lid1ToLid3(int local_id, int &lidx, int &lidy, int &lidz)
{
	/* lidz = local_id / (RES_X_ * RES_Y_) */
	lidz = local_id >> 4;	// RES_SHIFT_X_ + RES_SHIFT_Y_ = 4

	/* The remainder of local_id / (RES_X_ * RES_Y_) */
	local_id = (local_id - (lidz << 4));

	/* lidy = local_id / RES_Y_ */
	lidy = local_id >> 2;

	/* lidx = local_id - lidy * RES_X_*/
	lidx = local_id - (lidy << 2);
}

inline __host__ __device__ Vector3i lid1ToLid3(int local_id)
{
	Vector3i lid;

	lid.z = local_id / (RES_X_ * RES_Y_);
	local_id = local_id % (RES_X_ * RES_Y_);
	lid.y = local_id / RES_Y_;
	lid.x = local_id - lid.y * RES_X_;

	return lid;
}

template <typename PointType>
void Octree<PointType>::setInputCloud(typename PointCloud<PointType>::Ptr &input)
{
	// Reset parameters
	point_num_ = input->size();

	height_ = 0;
	leaf_num_ = 0;
	leaf_size_.x = leaf_size_.y = leaf_size_.z = 0;
	res_.x = res_.y = res_.z = DEFAULT_RES_;	// Default resolution is 1m

	min_b_.x = min_b_.y = min_b_.z = std::numeric_limits<int>::max();
	max_b_.x = max_b_.y = max_b_.z = std::numeric_limits<int>::min();

	node_num_ = 0;

	cloud_.reset(new PointCloud<PointType>(__allocator));

	if (point_num_ > 0) {
		// Buffers are over-allocated
		alloc_point_num_ = roundUp(point_num_, POINT_BLOCK_);

		cloud_->reserve(alloc_point_num_);
		*cloud_ = *input;

		buildOctree();
	}
}

template <typename PointType>
Octree<PointType>::Octree(const Octree<PointType> &other) :
	Base(other),
	cloud_(other.cloud_),
	status_(other.status_),

	level_id_(other.level_id_),
	level_dim_(other.level_dim_),

	level_lb_(other.level_lb_),
	level_ub_(other.level_ub_),

	node_size_(other.node_size_),
	starting_pid_(other.starting_pid_),
	point_id_(other.point_id_)
{

	height_ = other.height_;
	node_num_ = other.node_num_;

	leaf_num_ = other.leaf_num_;
	leaf_size_ = other.leaf_size_;

	point_num_ = other.point_num_;

	res_ = other.res_;
	min_b_ = other.min_b_;
	max_b_ = other.max_b_;

	alloc_point_num_ = other.alloc_point_num_;
}

template <typename PointType>
Octree<PointType>::Octree(Octree<PointType> &&other) :
	Base(other),
	cloud_(std::move(other.cloud_)),
	status_(std::move(other.status_)),
	level_id_(std::move(other.level_id_)),
	level_dim_(std::move(other.level_dim_)),
	level_lb_(std::move(other.level_lb_)),
	level_ub_(std::move(other.level_ub_)),
	node_size_(std::move(other.node_size_)),
	starting_pid_(std::move(other.starting_pid_)),
	point_id_(std::move(other.point_id_))
{
	height_ = other.height_;
	node_num_ = other.node_num_;

	leaf_num_ = other.leaf_num_;
	leaf_size_ = other.leaf_size_;
	point_num_ = other.point_num_;

	res_ = other.res_;
	min_b_ = other.min_b_;
	max_b_ = other.max_b_;

	alloc_point_num_ = other.alloc_point_num_;

	other.leaf_num_ = 0;
	other.leaf_size_ = Vector3i(0, 0, 0);
	other.point_num_ = 0;
	other.res_ = Vector3f(0, 0, 0);

	other.min_b_.x = other.min_b_.y = other.min_b_.z = std::numeric_limits<int>::max();
	other.max_b_.x = other.max_b_.y = other.max_b_.z = std::numeric_limits<int>::lowest();
}

template <typename PointType>
Octree<PointType> &Octree<PointType>::operator=(const Octree<PointType> &other)
{
	cloud_ = other.cloud_;
	status_ = other.status_;

	level_id_ = other.level_id_;
	level_dim_ = other.level_dim_;

	level_lb_ = other.level_lb_;
	level_ub_ = other.level_ub_;

	node_size_ = other.node_size_;

	height_ = other.height_;
	node_num_ = other.node_num_;

	starting_pid_ = other.starting_pid_;
	point_id_ = other.point_id_;

	leaf_num_ = other.leaf_num_;
	leaf_size_ = other.leaf_size_;
	point_num_ = other.point_num_;
	res_ = other.res_;
	min_b_ = other.min_b_;
	max_b_ = other.max_b_;

	alloc_point_num_ = other.alloc_point_num_;

	return *this;
}

template <typename PointType>
Octree<PointType> &Octree<PointType>::operator=(Octree<PointType> &&other)
{
	cloud_ = std::move(other.cloud_);
	status_ = std::move(other.status_);

	level_id_ = std::move(other.level_id_);
	level_dim_ = std::move(other.level_dim_);

	level_lb_ = std::move(other.level_lb_);
	level_ub_ = std::move(other.level_ub_);

	height_ = other.height_;
	node_num_ = other.node_num_;

	starting_pid_ = std::move(other.starting_pid_);
	point_id_ = std::move(other.point_id_);

	leaf_num_ = other.leaf_num_;
	leaf_size_ = other.leaf_size_;
	point_num_ = other.point_num_;
	res_ = other.res_;
	min_b_ = other.min_b_;
	max_b_ = other.max_b_;
	alloc_point_num_ = other.alloc_point_num_;

	other.leaf_num_ = 0;
	other.leaf_size_ = Vector3i(0, 0, 0);
	other.point_num_ = 0;
	other.res_ = Vector3f(0, 0, 0);

	other.min_b_.x = other.min_b_.y = other.min_b_.z = std::numeric_limits<int>::max();
	other.max_b_.x = other.max_b_.y = other.max_b_.z = std::numeric_limits<int>::lowest();

	other.alloc_point_num_ = 0;

	return *this;
}

/**
 * Build the octree on top of a point cloud.
 * The GPU buffer for the tree is over-allocated and is
 * extended when the point cloud becomes larger than the
 * tree.
 */
template <typename PointType>
void Octree<PointType>::buildOctree()
{
	/* 1. Compute boundaries of the tree */
	findBoundaries();

	/* 2. Determine the index of points that belong to each leaf */
	scatterPointsToLeaves();

	/* 3. Compute the size of each Octree level and allocate the
	 *    GPU memory for holding octree nodes.
	 */
	allocateOctree();

	/* 4. Move from bottom to top of the tree and build upper levels
	 *    from lower levels.
	 */
	buildLevelMultiPass();
}

template <typename PointType>
void Octree<PointType>::findBoundaries()
{
	computeBoundaries(cloud_->data(), max_b_, min_b_, leaf_size_, leaf_num_);
}

/* Compute the index of the leaf which the point belong to */
__host__ __device__ inline int leafId(Vector3f p, Vector3i leaf_size, Vector3f res, Vector3i min_b)
{
  int idx = static_cast<int>(floorf(p.x / res.x)) - min_b.x;
  int idy = static_cast<int>(floorf(p.y / res.y)) - min_b.y;
  int idz = static_cast<int>(floorf(p.z / res.z)) - min_b.z;

  return (idx + idy * leaf_size.x + idz * leaf_size.x * leaf_size.y);
}

__host__ __device__ inline int leafId(Vector3f p, Vector3f res, Vector3i bottom_dim, Vector3i bottom_lb)
{
	int id_x = static_cast<int>(floorf(p.x / res.x)) - bottom_lb.x;
	int id_y = static_cast<int>(floorf(p.y / res.y)) - bottom_lb.y;
	int id_z = static_cast<int>(floorf(p.z / res.z)) - bottom_lb.z;

	return (id_x + id_y * bottom_dim.x + id_z * bottom_dim.x * bottom_dim.y);
}

/**
 * Count the number of points in each leaf node
 *
 * Params:
 * 		@x, y, z				: Coordinates of points
 * 		@point_num				: Number of points
 * 		@leaf_x, leaf_y, leaf_z	: size (in number of nodes) of the bottom level grid
 * 		@res_x, res_y, res_z	: the resolution of leaf nodes
 * 		@min_b_x, min_b_y, min_b_z: the min boundaries of the bottom level grid
 * Output:
 * 		@count: count[i] is the number of points in node i of the bottom grid
 */
__global__ void computeLeafSize(FCloudPtr cloud, int point_num, Vector3i leaf_size,
								Vector3f res, Vector3i min_b, int *count)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int leaf_id = leafId(cloud[i], leaf_size, res, min_b);

		atomicAdd(count + leaf_id, 1);
	}
}

/* Distribute points to leaf nodes
 *
 * Params:
 * 		@cloud[in]		: the coordinates of the input points
 *		@point_num[in]	: the number of points
 *		@wlocation[in]	: the writing location, which indicate where to put
 *									points in the output point_id vector.
 *		@leaf_size[in]	: the size (in number of nodes) of the bottom level
 *		@res_[in]		: the resolution of leaf nodes
 *		@min_b[in]		: the min boundaries of the bottom grid
 *		@point_id[out]	: the output vector where indices of points are re-organized
 *		@offset[in]: added to the output index
 *
 */
__global__ void scatterPointToLeaf(FCloudPtr cloud, int point_num, int *wlocation,
									Vector3i leaf_size, Vector3f res, Vector3i min_b,
									int *point_id, int offset = 0)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int leaf_id = leafId(cloud[i], leaf_size, res, min_b);

		int loc = atomicAdd(&wlocation[leaf_id], 1);

		point_id[loc] = i + offset;
	}
}

/* Put indices of points to their corresponding leaf nodes.
 *
 * After this step is finished, starting_pid_[i] will
 * point to the head index of the list of points in the leaf i.
 * The indices of points are re-organized in point_id_.
 */
template <typename PointType>
void Octree<PointType>::scatterPointsToLeaves()
{
	if (point_num_ <= 0) {
		return;
	}

	starting_pid_ = device_vector<int>(leaf_num_ + 1, __allocator, true);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num_, computeLeafSize,
					cloud_->data(), point_num_, leaf_size_, res_, min_b_,
					starting_pid_.data()));

	ExclusiveScan(starting_pid_);

	device_vector<int> wlocation(leaf_num_, __allocator);

	gcopyDtoD(wlocation.data(), starting_pid_.data(), sizeof(int) * leaf_num_);

	point_id_ = device_vector<int>(point_num_, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num_, scatterPointToLeaf,
					cloud_->data(), point_num_, wlocation.data(), leaf_size_,
					res_, min_b_, point_id_.data(), 0));
}

template <typename PointType>
void Octree<PointType>::allocateOctree()
{
	allocateOctree(leaf_size_, min_b_, max_b_,
					status_, level_dim_, level_lb_, level_ub_,
					node_size_, level_id_, node_num_, height_);
}

/* Allocate GPU memory for the tree nodes */
template <typename PointType>
void Octree<PointType>::allocateOctree(Vector3i leaf_size, Vector3i min_b, Vector3i max_b,
										device_vector<uint32_t> &status, device_vector<Vector3i> &level_dim,
										device_vector<Vector3i> &level_lb, device_vector<Vector3i> &level_ub,
										device_vector<Vector3f> &node_size, device_vector<int> &level_id,
										int &node_num, int &height)
{
	// Host-side buffers for manipulating dimensions of the tree
	std::vector<Vector3i> hlevel_dim;
	std::vector<int> hlevel_id;
	std::vector<Vector3i> hlevel_lb;
	std::vector<Vector3i> hlevel_ub;
	std::vector<Vector3f> hnode_size;

	// Re-calculate information of the tree
	int node_number = 0;	// Number of tree nodes in each level
	Vector3i ldim = leaf_size;				// Dimensions of a level
	int alloc_size = 0;		// Size of GPU allocation

	hlevel_dim.push_back(ldim);

	alloc_size = node_number = ldim.x * ldim.y * ldim.z;

	hlevel_id.push_back(0);

	Vector3i lb, ub;	// Lower and upper boundaries of each level

	lb = min_b;
	ub = max_b;

	hlevel_lb.push_back(lb);

	hlevel_ub.push_back(ub);

	Vector3f nsize = res_;

	hnode_size.push_back(nsize);

	while (ldim.x > RES_X_ || ldim.y > RES_Y_ || ldim.z > RES_Z_) {
		lb.x = div(lb.x, RES_X_);
		lb.y = div(lb.y, RES_Y_);
		lb.z = div(lb.z, RES_Z_);

		ub.x = div(ub.x, RES_X_);
		ub.y = div(ub.y, RES_Y_);
		ub.z = div(ub.z, RES_Z_);

		ldim = ub - lb + 1;

		node_number = ldim.x * ldim.y * ldim.z;

		nsize.x *= RES_X_;
		nsize.y *= RES_Y_;
		nsize.z *= RES_Z_;

		hlevel_dim.push_back(ldim);
		hlevel_id.push_back(alloc_size);
		hlevel_lb.push_back(lb);
		hlevel_ub.push_back(ub);
		hnode_size.push_back(nsize);

		alloc_size += node_number;
	}

	node_num = alloc_size;

	height = hlevel_id.size();

	hlevel_id.push_back(alloc_size);	// Now the size of hlevel_id is height_ + 1

	// Copy level sizes to the device memory
	level_dim.resize(height);

	gcopyHtoD(level_dim.data(), hlevel_dim.data(), sizeof(Vector3i) * height);

	// Setup level_id
	level_id.resize(height + 1);

	gcopyHtoD(level_id.data(), hlevel_id.data(), sizeof(int) * (height + 1));
	// Now level_id_ specify the index where each tree level start from

	// Copy level boundaries to the GPU memory
	level_lb.resize(height);
	level_ub.resize(height);

	gcopyHtoD(level_lb.data(), hlevel_lb.data(), sizeof(Vector3i) * height);
	gcopyHtoD(level_ub.data(), hlevel_ub.data(), sizeof(Vector3i) * height);

	node_size.resize(height);

	gcopyHtoD(node_size.data(), hnode_size.data(), sizeof(Vector3f) * height);

	// Allocate memory for boundaries of tree nodes and empty them
	status.resize(alloc_size);

	fill(status, 0);

}

template <typename PointType>
void Octree<PointType>::buildLevelMultiPass()
{
	/* 1. Build the bottom level */
	buildLeaves(status_);

	/* 2. Going up the tree and build level (lv) from level (lv - 1) */
	std::vector<int> hlevel_id = level_id_.host_vector();
	std::vector<Vector3i> hlevel_dim = level_dim_.host_vector();
	std::vector<Vector3i> hlevel_lb = level_lb_.host_vector();
	std::vector<Vector3i> hlevel_ub = level_ub_.host_vector();

	for (int lv = 1; lv < height_; lv++) {
		buildUpper(status_.data() + hlevel_id[lv], hlevel_dim[lv],
					hlevel_lb[lv], hlevel_ub[lv],
					status_.data() + hlevel_id[lv - 1], hlevel_dim[lv - 1],
					hlevel_lb[lv - 1], hlevel_ub[lv - 1]);
	}
}


/**
 * Build the bottom level of the octree.
 *
 * In each leaf, we iterate through its points and find
 * the max/min coordinate values of points.
 *
 * Input:
 * 		@x, y, z					: the coordinates of input points
 *		@leaves_num					: number of leaves in the bottom level
 *										of the tree
 *		@starting_point_id, point_id: vectors to access indices of points
 *										in each tree node
 * Output:
 * 		@status: the number of points in each tree node
 *
 */
__global__ void buildLeavesLevel(int leaves_num, int *starting_point_id, uint32_t *status)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < leaves_num; i += stride) {
		status[i] = starting_point_id[i + 1] - starting_point_id[i];
	}
}

/* Build the bottom level of the Octree.
 * Compute the maximum and minimum coordinates
 * of points in each leaf node.
 */
template <typename PointType>
void Octree<PointType>::buildLeaves(device_vector<uint32_t> &status)
{
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num_, buildLeavesLevel,
					leaf_num_, starting_pid_.data(), status.data()));
}

/**
 * Build parent level from child level.
 * For each parent node, iterate through its children node
 * and update the parent node's lower and upper coordinates
 * accordingly.
 *
 * Params:
 * 		@pstatus[out]: status of parent nodes
 * 		@pdim[in]: dimensions of the upper level
 * 		@plb, pub[in]: the lower and upper boundaries of the upper level
 * 		@cstatus[in]: status of child nodes
 * 		@cdim[in]: dimensions of the lower level
 * 		@clb, cub[in]: the lower and upper boundaries of the lower level
 *
 */
__global__ void buildUpperLevel(uint32_t *pstatus, Vector3i pdim, Vector3i plb, Vector3i pub,
								uint32_t *cstatus, Vector3i cdim, Vector3i clb, Vector3i cub)
{
	int pid1 = threadIdx.x + blockIdx.x * blockDim.x;	// Each thread handles one parent node

	Vector3i pid3;
	const Vector3i res(RES_X_, RES_Y_, RES_Z_);
	const int cnum = RES_X_ * RES_Y_ * RES_Z_;

	if (pid1 < pdim.x * pdim.y * pdim.z) {
		pid3 = id1ToId3(pid1, plb, pdim);	// Compute the 3d index of the parent node

		// The smallest index of children of this parent node
		Vector3i base_cid = pid3 * res;

		uint32_t pstat = 0;		// Status of the parent node

		for (int i = 0; i < cnum; ++i) {
			// Compute the 3D index of a child node
			Vector3i cid = lid1ToLid3(i) + base_cid;

			// If the node is in the range of the lower level
			if (cid.x >= clb.x && cid.y >= clb.y && cid.z >= clb.z &&
				cid.x <= cub.x && cid.y <= cub.y && cid.z <= cub.z) {
				// Compute the 1D index of the child node
				int id = id3ToId1(cid, clb, cdim);

				// If the child node is inside the range of the lower level and is not empty
				pstat |= (cstatus[id] > 0) ? (1 << i) : 0;
			}
		}

		// Update the parent nodes
		pstatus[pid1] = pstat;
	}
}


/**
 * Build parent level from child level.
 *
 * Input:
 * 	@cstatus: number of points of child's nodes
 * 	@cdim: dimensions of the child level
 * 	@clb, cub: lower and upper boundaries of the child level
 * 	@pdim: dimensions of the parent level
 * 	@plb, pub: lower and upper boundaries of the parent level
 * Output:
 * 	@pstatus: number of points of parent's nodes
 */
template <typename PointType>
void Octree<PointType>::buildUpper(uint32_t *pstatus, Vector3i pdim, Vector3i plb, Vector3i pub,
									uint32_t *cstatus, Vector3i cdim, Vector3i clb, Vector3i cub)
{
	int parent_num = pdim.x * pdim.y * pdim.z;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(parent_num, buildUpperLevel,
					pstatus, pdim, plb, pub, cstatus, cdim, clb, cub));
}

/* Compute the distance between the query point (qx, qy, qz)
 * and the octree node (ix, iy, iz). The distance in each
 * dimension is determined by comparing the coordinate of
 * the query point with the boundaries of the node. The final
 * distance is the norm of the distance from 3 dimensions.
 *
 * Input:
 * 		@qx, qy, qz: query point's coordinate
 * 		@ix, iy, iz: 3D index of the tree node
 * 		@node_size: dimensions of the tree node (size in each
 * 						dim of the node)
 *
 * Return: the distance between the query point and the node.
 *
 */
__device__ float dist(Vector3f q, int ix, int iy, int iz, Vector3f node_size)
{
	float lx, ly, lz, ux, uy, uz;

	lx = ix * node_size.x;
	ly = iy * node_size.y;
	lz = iz * node_size.z;

	ux = lx + node_size.x;
	uy = ly + node_size.y;
	uz = lz + node_size.z;

	float tx = ((q.x > ux) ? q.x - ux : ((q.x < lx) ? lx - q.x : 0));
	float ty = ((q.y > uy) ? q.y - uy : ((q.y < ly) ? ly - q.y : 0));
	float tz = ((q.z > uz) ? q.z - uz : ((q.z < lz) ? lz - q.z : 0));

	return norm3df(tx, ty, tz);
}

/* First pass, going down the tree and find the current
 * nearest neighbor (nn) distance, index of the nn leaf,
 * and index of the nn point of each query point.
 *
 * Params:
 * 		@qcloud[in]: the query cloud
 * 		@q_num[in]: the number of points in the query cloud
 * 		@ref_cloud[in]: the reference cloud
 * 		@status[in]: the status of the reference cloud
 * 		@level_dim[in]: the dimensions of each tree level
 * 		@level_lb[in], level_ub[in]: the lower and upper bound of levels
 * 		@node_size[in]: size of nodes in each level
 * 		@level_id[in]: starting index of each level
 * 		@height[in]: number of tree levels
 * 		@starting_point_id[in]: the starting indices of points in leaf nodes
 * 		@point_id[in]: sorted indces of points in the bottom level
 * 		@nn_dist[out]: current nearest distances of query points
 * 		@nn_id[out]: indices of the current nearest leaf of query points
 * 		@nn_pid[out]: indices of the current nearest neighbor of query points
 */
__global__ void nnSearchGoDown(FCloudPtr qcloud, int q_num, FCloudPtr ref_cloud,
								uint32_t *status, Vector3i *level_dim,
								Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id,	int height,
								int *starting_point_id, int *point_id,
								float *nn_dist, int *nn_id, int *nn_pid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	float min_dist = FLT_MAX;
	int min_id = -1;	// Index of the current nearest node
	int min_pid = -1;	// Index of the current nearest neighbor point

	for (int i = index; i < q_num; i += stride) {
		// Each thread handles one query point
		Vector3f q = qcloud[i];

		/* Go from the top to the bottom level. At each level, examine
		 * all possible tree node to find the nearest node to the query
		 * point.
		 */
		for (int level = height - 1, px = 0, py = 0, pz = 0; level >= 0;
				level--, px *= RES_X_, py *= RES_Y_, pz *= RES_Z_) {

			/* Previously, every time new points are added, the whole tree is
			 * re-built. We then use an index system in which all levels start
			 * at index (0, 0, 0). In that system, the tree has a single ROOT
			 * node at the top of the tree. Moving from top to bottom is done
			 * by a single policy: Starting from node (0, 0, 0), we check all
			 * possible children of a node, chose the nearest children and
			 * continue to move down.
			 *
			 * However, in the current version, the tree is updated rather than
			 * rebuilt when new points are added. Hence, we now use a 'global
			 * index' system - each level may start from negative indices. In
			 * this system, the tree may not have a single root node anymore.
			 * For instance, node (-1, -1, -1) and node (0, 0, 0) never belong
			 * to the same node, no matter how many times their indices are divided
			 * by RES_X_, RES_Y_, and RES_Z_. That is because div(-1, RES_X_)
			 * = -((1 + RES_X_ - 1) / RES_X_) = -1. The same thing happens to other
			 * dimensions.
			 *
			 * Instead, the tree now has a 'root level', in which the number
			 * of nodes is no more than RES_X_ * RES_Y_ * RES_Z_. Because there
			 * is no root node, we have to change the policy a bit. At the root
			 * level, we first have to check all nodes to find a nearest node,
			 * then we apply the go down policy to the node.
			 */
			Vector3i cur_dim = level_dim[level];	// Dimensions of the level (in number of nodes)
			Vector3i cur_lb = level_lb[level];		// Lower index boundaries of the level
			Vector3i cur_ub = level_ub[level];		// Upper index boundaries of the level
			Vector3f nsize = node_size[level];	// Resolution of nodes in the level
			int bid = level_id[level];			// Starting index of the level in the 1D node array

			min_dist = FLT_MAX;

			int sx, sy, sz;	// Starting indices of nodes to be examined at the current level
			int ex, ey, ez;	// End indices of nodes to be examined at the current level

			if (level == height - 1) {
				// At the root level, check all nodes
				sx = cur_lb.x;
				sy = cur_lb.y;
				sz = cur_lb.z;

				ex = cur_ub.x;
				ey = cur_ub.y;
				ez = cur_ub.z;
			} else {
				// At lower levels, check all children of the node examined in the previous level
				sx = (px > cur_lb.x) ? px : cur_lb.x;
				sy = (py > cur_lb.y) ? py : cur_lb.y;
				sz = (pz > cur_lb.z) ? pz : cur_lb.z;

				ex = (px + RES_X_ - 1 < cur_ub.x) ? px + RES_X_ - 1 : cur_ub.x;
				ey = (py + RES_Y_ - 1 < cur_ub.y) ? py + RES_Y_ - 1 : cur_ub.y;
				ez = (pz + RES_Z_ - 1 < cur_ub.z) ? pz + RES_Z_ - 1 : cur_ub.z;
			}


			for (int j = sx; j <= ex; j++) {
				for (int k = sy; k <= ey; k++) {
					for (int l = sz; l <= ez; l++) {
						int cur_id = bid + id3ToId1(j, k, l, cur_lb, cur_dim);

						// Only care about non-empty nodes
						if (status[cur_id] > 0) {
							float cur_dist = dist(q, j, k, l, nsize);

							if (min_dist > cur_dist) {
								min_id = cur_id - bid;
								min_dist = cur_dist;
								px = j;
								py = k;
								pz = l;
							}
						}
					}
				}
			}
		}

		/* At the end of the loop above, min_id is the index of
		 * the leaf (bottom-level node) that is currently the
		 * closest to the query point.
		 */

		/* At the bottom level (leaves), from the list of points
		 * belonging to a leaf, find the point that is the closest
		 * to the query point.
		 */
		min_dist = FLT_MAX;

		for (int j = starting_point_id[min_id]; j < starting_point_id[min_id + 1]; j++) {
			int pid = point_id[j];

			float cur_dist = distance(ref_cloud[pid], q);

			if (min_dist > cur_dist) {
				min_pid = pid;
				min_dist = cur_dist;
			}
		}

		nn_dist[i] = min_dist;	// Current nearest distance
		nn_id[i] = min_id;		// Current nearest leaf node
		nn_pid[i] = min_pid;	// Current nearest point
	}
}



/* Also going down, but comes with 2 major different:
 * - A number of groups of several threads with consecutive threadIdx.x
 *   are assigned to query points. Each group handles one query point.
 *   The number of threads in each group is equal to the number of children
 *   node in each parent node. While moving down, a group checks children
 *   nodes of a previous parent node. Each thread in the group is assigned
 *   one child node and computes the distance between the child node to
 *   the query point. A reduction is then applied to find the node
 *   with shortest distance.
 *
 * - Buffers on the shared memory are allocated to stores the distance and
 *   the node indices. Those buffers are used in reduction to share node
 *   information among threads in a group.
 *
 * Params:
 * 		@qcloud[in]: the input query cloud
 * 		@qnum[in]: number of query points
 * 		@ref_cloud[in]: the reference cloud
 * 		@status[in]: status of the tree nodes
 * 		@level_dim[in]: 3D dimensions of all tree levels (in number of nodes)
 * 		@level_lb, level_ub[in]: lower and upper index boundaries of all tree levels
 * 		@node_size[in]: resolution of nodes in each level
 * 		@level_id[in]: starting indices of levels in the status arrays
 * 		@starting_point_id, point_id[in]: to access points in the bottom level (leaves)
 *  	@nn_dist[out]: distance to the current nearest neighbors of query points
 *  	@nn_id[out]: indices of the node that contains the current nearest neighbors of query points
 *  	@nn_pid[out]: indices of the nearest points of query points
 */
__global__ void nnSearchGoDown2(FCloudPtr qcloud, int q_num, FCloudPtr ref_cloud, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id, int height,
								int *starting_point_id, int *point_id,
								float *nn_dist, int *nn_id, int *nn_pid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = blockDim.x * gridDim.x;
	float min_dist = FLT_MAX;
	int min_id = -1;
	int min_pid = -1;
	float cur_dist;
	int local_id = threadIdx.x % GROUP_SIZE_;	// Local 1D index of the thread in its group (lane_id)
	int lidx, lidy, lidz;	// Local 3D indices of the thread in the group
	int base_local_id = (threadIdx.x / GROUP_SIZE_) * GROUP_SIZE_;
	int cur_id;

	__shared__ float snn_dist[BLOCK_SIZE_X];	// For distances
	__shared__ int snn_id[BLOCK_SIZE_X];		// For indices of tree nodes

	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = index; i < GROUP_SIZE_ * q_num; i += offset) {
		// Each group handles one query point
		int qid = i >> GROUP_SHIFT_;	// qid = i / 32
		Vector3f q = qcloud[qid];

		// Go to the bottom to find the current min distance and its nn point's id
		for (int level = height - 1, pidx = 0, pidy = 0, pidz = 0; level >= 0;
				level--, pidx *= RES_X_, pidy *= RES_Y_, pidz *= RES_Z_) {
			Vector3i cur_dim = level_dim[level];	// Dimensions of the current level
			Vector3i cur_lb = level_lb[level];		// Lower index boundaries of the current level
			Vector3i cur_ub = level_ub[level];		// Upper index boundaries of the current level
			Vector3f nsize = node_size[level];		// Resolution of a node in the current level

			int bid = level_id[level];			// Starting index of the current
												// level in the 1D tree array
			int nx, ny, nz;		// 3D index of the node the thread is handling

			/* They are derived from the lower indices of the
			 * root level (at the root level) or the starting
			 * indices of the children of the previously examined
			 * node (at lower level).
			 */
			nx = (level == height - 1) ? cur_lb.x + lidx : pidx + lidx;
			ny = (level == height - 1) ? cur_lb.y + lidy : pidy + lidy;
			nz = (level == height - 1) ? cur_lb.z + lidz : pidz + lidz;

			if (nx >= cur_lb.x && ny >= cur_lb.y && nz >= cur_lb.z &&
					nx <= cur_ub.x && ny <= cur_ub.y && nz <= cur_ub.z) {
				cur_id = bid + id3ToId1(nx, ny, nz, cur_lb, cur_dim);

				// If the node is empty, set cur_dist to FLT_MAX
				cur_dist = (status[cur_id] > 0) ? dist(q, nx, ny, nz, nsize) : FLT_MAX;
			}

			snn_dist[threadIdx.x] = cur_dist;
			snn_id[threadIdx.x] = cur_id - bid;

//			__syncthreads();	// A group is also a warp, so may be this is not necessary?

			// Use reduction to find the current nearest node
			for (int active_threads = GROUP_SIZE_ >> 1; active_threads > 0; active_threads >>= 1) {
				if (local_id < active_threads) {
					int hid = threadIdx.x + active_threads;

					if (snn_dist[threadIdx.x] > snn_dist[hid]) {
						snn_dist[threadIdx.x] = snn_dist[hid];
						snn_id[threadIdx.x] = snn_id[hid];
					}
				}
			}

			/* The thread with local_id = 0 in the group now has the index
			 * of the node that has the min distance. We now need to broadcast
			 * that index to all threads in the group. Also convert it to
			 * 3D indices pidx, pidy, pidz.
			 */
			id1ToId3(snn_id[base_local_id], cur_lb, cur_dim, pidx, pidy, pidz);
			__syncwarp();
		}

		/* At the end of the loop above, snn_id[base_local_id] is the index of
		 * the nearest leaf to the query point.
		 */
		min_id = snn_id[base_local_id];
		min_dist = FLT_MAX;
		__syncwarp();

		/* Each group iterates through points in the leaf node and find the
		 * point having the min distance to the query point.
		 */
		for (int j = starting_point_id[min_id] + local_id; j < starting_point_id[min_id + 1]; j += GROUP_SIZE_) {
			int pid = point_id[j];

			cur_dist = distance(ref_cloud[pid], q);

			if (min_dist > cur_dist) {
				min_pid = pid;
				min_dist = cur_dist;
			}
		}

		snn_dist[threadIdx.x] = min_dist;
		snn_id[threadIdx.x] = min_pid;
		__syncwarp();

		// Reduction to find the current nearest neighbor
		for (int active_threads = GROUP_SIZE_ >> 1; active_threads > 0; active_threads >>= 1) {
			if (local_id < active_threads) {
				int hid = threadIdx.x + active_threads;

				if (snn_dist[threadIdx.x] > snn_dist[hid]) {
					snn_dist[threadIdx.x] = snn_dist[hid];
					snn_id[threadIdx.x] = snn_id[hid];
				}
			}

			__syncwarp();
		}

		if (local_id == 0) {
			nn_dist[qid] = snn_dist[base_local_id];	// Current nearest distance
			nn_id[qid] = min_id;					// Current nearest leaf node
			nn_pid[qid] = snn_id[base_local_id];	// Current nearest point]
		}
		__syncwarp();
	}
}

/* Similar the nnSearchGoDown2, but use warp-level primitives
 * to exchange information between threads in a group. This
 * should be faster than using the shared memory.
 */
__global__ void nnSearchGoDown3(FCloudPtr qcloud, int q_num, FCloudPtr ref_cloud, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id,	int height,
								int *starting_point_id, int *point_id,
								float *nn_dist, int *nn_id, int *nn_pid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = blockDim.x * gridDim.x;
	float min_dist = FLT_MAX;
	int min_id = -1;
	int min_pid = -1;
	float cur_dist;
	/* local_id - index of thread in the group, also
	 * lane id when GROUP_SIZE_ == 32 (when group is
	 * same as a warp).
	 */
	int local_id = threadIdx.x & (GROUP_SIZE_ - 1);
	int lidx, lidy, lidz;
	int cur_id;

	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = index; i < GROUP_SIZE_ * q_num; i += offset) {
		int qid = i >> GROUP_SHIFT_;
		Vector3f q = qcloud[qid];

		// Go to the bottom to find the current min distance and its nn point's id
		for (int level = height - 1, pidx = 0, pidy = 0, pidz = 0;
				level >= 0; level--, pidx *= RES_X_, pidy *= RES_Y_, pidz *= RES_Z_) {
			Vector3i cur_dim = level_dim[level];
			Vector3i cur_lb = level_lb[level];
			Vector3i cur_ub = level_ub[level];
			Vector3f nsize = node_size[level];
			int bid = level_id[level];		// Starting index of a level in the 1D octree array

			/* The indices (nx, ny, nz) of the node to be examined
			 * are derived from the lower index boundaries of the level
			 * (at the root level) or from the 3D indices of the first
			 * sibling (computed by multiply the 3D indices of the parent
			 * node with 2)
			 */
			int nx = (level == height - 1) ? cur_lb.x + lidx : pidx + lidx;
			int ny = (level == height - 1) ? cur_lb.y + lidy : pidy + lidy;
			int nz = (level == height - 1) ? cur_lb.z + lidz : pidz + lidz;

			cur_dist = FLT_MAX;
			cur_id = -1;

			/* Make sure that (nx, ny, nz) is inside the boundaries.
			 * Otherwise it may give an illegal memory access.
			 */
			if (nx >= cur_lb.x && ny >= cur_lb.y && nz >= cur_lb.z &&
					nx <= cur_ub.x && ny <= cur_ub.y && nz <= cur_ub.z) {
				cur_id = bid + id3ToId1(nx, ny, nz, cur_lb, cur_dim);

				cur_dist = (status[cur_id] > 0) ? dist(q, nx, ny, nz, nsize) : FLT_MAX;
			}

			cur_id -= bid;

			/* Use __shfl_xor_sync to perform reduction among threads
			 * in the same group to get the min distance and the corresponding
			 * node index.
			 */
			for (int lane_offset = GROUP_SIZE_ >> 1; lane_offset > 0; lane_offset >>= 1) {
				float other_dist = __shfl_xor_sync(FULL_MASK, cur_dist, lane_offset, GROUP_SIZE_);
				int other_id = __shfl_xor_sync(FULL_MASK, cur_id, lane_offset, GROUP_SIZE_);

				if (cur_dist > other_dist) {
					cur_dist = other_dist;
					cur_id = other_id;
				}
			}

			/* At the end of the reduction above, thread 0 in the group (not
			 * thread with threadIdx.x = 0 in the block) keeps the current
			 * nearest distance and the 1D index of the current nearest leaf.
			 *
			 * They must be broadcasted to other threads in the same group as well.
			 */
			cur_id = __shfl_sync(FULL_MASK, cur_id, 0, GROUP_SIZE_);

			// Convert the 1D index to 3D indices and continue to go down that node
			id1ToId3(cur_id, cur_lb, cur_dim, pidx, pidy, pidz);
		}

		/* Reach the bottom level, look from the points in the current
		 * nearest node to find the point with the smallest distance
		 * to the query point.
		 */
		min_id = cur_id;
		min_dist = FLT_MAX;

		for (int j = starting_point_id[min_id] + local_id; j < starting_point_id[min_id + 1]; j += GROUP_SIZE_) {
			int pid = point_id[j];

			cur_dist = distance(ref_cloud[pid], q);

			if (min_dist > cur_dist) {
				min_pid = pid;
				min_dist = cur_dist;
			}
		}

		for (int lane_offset = GROUP_SIZE_ >> 1; lane_offset > 0; lane_offset >>= 1) {
			float other_dist = __shfl_xor_sync(FULL_MASK, min_dist, lane_offset, GROUP_SIZE_);
			int other_pid = __shfl_xor_sync(FULL_MASK, min_pid, lane_offset, GROUP_SIZE_);

			if (min_dist > other_dist) {
				min_dist = other_dist;
				min_pid = other_pid;
			}
		}

		if (local_id == 0) {
			nn_dist[qid] = min_dist;	// Current nearest distance
			nn_id[qid] = min_id;		// Current nearest leaf node
			nn_pid[qid] = min_pid;		// Current nearest point
		}
	}
}

/* Second pass, go up from the bottom to the top of the tree.
 * Find nodes which has the distance to the query point less
 * than their current nearest distance and examine its sub-tree.
 *
 * Method 1: Each thread handles one query point.
 *
 * Params:
 * 		@qcloud[in]: coordinates of query points
 * 		@q_num[in]: number of query points
 * 		@ref_cloud[in]: coordinates of points in the current tree node
 * 		@status[in]: number of points in each tree node
 * 		@level_dim[in]: dimensions (in number of nodes) of each tree level
 * 		@level_lb, level_ub[in]: lower and upper 3D index boundaries of
 * 									each tree level
 * 		@node_size[in]: resolution of nodes in each level
 * 		@level_id[in]: the starting indices (index of the first node) of
 * 					each tree level in the whole tree node array
 * 		@height[in]: height of the tree (number of level). Bottom at level 0,
 * 					root (top) at level (height - 1).
 * 		@starting_point_id, point_id[in]: spare-representation of the indices
 * 										of points in the bottom level. To
 * 										examine points in a leaf i, we walk
 * 										through index starting_point_id[i]
 * 										to starting_point_id[i + 1] - 1 on the
 * 										array point_id.
 * 		@stack: the stack structures used to store tree nodes when go down
 * 				a sub-tree. Each stack element is a pair (node's level, node's
 * 				1D index). Depth of the stack is (height * RES_X_ * RES_Y_ * RES_Z_)
 * 				because every time we can only examine one path from top to
 * 				bottom at maximum. Each query point is given one stack, so
 * 				the number of stacks is q_num:
 *
 * Output:
 * 		@nn_dist: the nearest distance to query points
 * 		@nn_id: the index of the nearest tree node (TODO: remove later, now keep for debug)
 *		@nn_pid: the index of the nearest neighbor points
 */
__global__ void nnSearchGoUp(FCloudPtr qcloud, int q_num, FCloudPtr ref_cloud, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id,	int height,
								int *starting_point_id, int *point_id, int2 *stack,
								float *nn_dist, int *nn_id, int *nn_pid)
{
	int offset = blockDim.x * gridDim.x;
	int stk_top;	// Top of the stack
	float min_dist;
	int min_id;
	int min_pid;
	float cur_dist;
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	for (int i = index; i < q_num; i += offset) {
		// Each thread handles one query point
		Vector3f q = qcloud[i];

		/* Get current nn distance, nn node, and nn pid of
		 * the query point, which were found in goDown previously
		 */
		min_dist = nn_dist[i];
		min_id = nn_id[i];
		min_pid = nn_pid[i];

		// Now go up the tree to find the actual min dist
		//
		int idx, idy, idz;
		Vector3i dim = level_dim[0];
		Vector3i lb = level_lb[0];
		Vector3i ub = level_ub[0];
		Vector3f nsize = node_size[0];

		id1ToId3(min_id, lb, dim, idx, idy, idz);

		for (int level = 0; level < height; level++, idx = div(idx, RES_X_),
				idy = div(idy, RES_Y_), idz = div(idz, RES_Z_)) {
			// Erase the stack
			stk_top = -1;

			// Get the information of the current level
			dim = level_dim[level];
			lb = level_lb[level];
			ub = level_ub[level];
			nsize = node_size[level];

			// Start and end indices of nodes to be examine at the current level
			int sx, sy, sz, ex, ey, ez;

			if (level == height - 1) {
				// At the root level, examine all nodes
				sx = lb.x;
				sy = lb.y;
				sz = lb.z;

				ex = ub.x;
				ey = ub.y;
				ez = ub.z;
			} else {
				// At other levels, examine all siblings of the current node
				sx = roundDown(idx, RES_X_);
				sy = roundDown(idy, RES_Y_);
				sz = roundDown(idz, RES_Z_);

				/* The ending indices must not exceed the upper index
				 * boundaries of the current level.
				 */
				ex = (sx + RES_X_ < ub.x) ? sx + RES_X_ : ub.x;
				ey = (sy + RES_Y_ < ub.y) ? sy + RES_Y_ : ub.y;
				ez = (sz + RES_Z_ < ub.z) ? sz + RES_Z_ : ub.z;

				/* The starting indices must not less than the lower
				 * index boundaries of the current level. */
				sx = (sx > lb.x) ? sx : lb.x;
				sy = (sy > lb.y) ? sy : lb.y;
				sz = (sz > lb.z) ? sz : lb.z;
			}

			int bid = level_id[level];	// The starting 1D index of the current level's nodes

			/* The 1D index of the current nearest node (at
			 * the current level) is used to determine if
			 * a node is a sibling of this node or the node
			 * itself.
			 */
			min_id = bid + id3ToId1(idx, idy, idz, lb, dim);

			for (int ssx = sx; ssx <= ex; ssx++) {
				for (int ssy = sy; ssy <= ey; ssy++) {
					for (int ssz = sz; ssz <= ez; ssz++) {
						// Get the 1D index of the node
						int sid = bid + id3ToId1(ssx, ssy, ssz, lb, dim);

						cur_dist = dist(q, ssx, ssy, ssz, nsize);

						/* If a sibling is not empty and its distance to the query point
						 * is less than the current min_dist, push it to the stack.
						 */
						if (sid != min_id && status[sid] > 0 && min_dist > cur_dist) {
							++stk_top;

							// Each stack element is a pair of (node's level, node's index)
							stack[index + stk_top * offset] = make_int2(level, sid - bid);
						}
					}
				}
			}

			/* To examine the sub-trees that may contain the actual
			 * nearest neighbor, each thread loops until the stack is empty.
			 */
			while (stk_top >= 0) {
				/* In each iteration, pop the node at the top of the stack,
				 * and goDown the node. */
				int2 cur_node = stack[index + stk_top * offset];
				--stk_top;

				int cur_idx, cur_idy, cur_idz;

				dim = level_dim[cur_node.x];
				lb = level_lb[cur_node.x];

				// Convert 1D array index to 3D global index
				id1ToId3(cur_node.y, lb, dim, cur_idx, cur_idy, cur_idz);

				// If this node is a leaf, check its points and update the nearest neighbor if exist
				if (cur_node.x == 0) {
					for (int j = starting_point_id[cur_node.y]; j < starting_point_id[cur_node.y + 1]; j++) {
						int pid = point_id[j];

						cur_dist = distance(ref_cloud[pid], q);

						if (min_dist > cur_dist) {
							min_pid = pid;
							min_dist = cur_dist;
						}
					}
				} else {
					/* Otherwise, examine the node's children and push them
					 * to the stack if the children may contain the actual
					 * nearest neighbor.
					 */

					// Get the information of the lower level
					dim = level_dim[cur_node.x - 1];
					ub = level_ub[cur_node.x - 1];
					lb = level_lb[cur_node.x - 1];
					nsize = node_size[cur_node.x - 1];

					/* Compute the range (start, end) of indices of the children
					 * of the current node. The range must be inside the lower
					 * and upper index boundaries of the lower level (lb and ub).
					 */
					sx = cur_idx * RES_X_;
					sy = cur_idy * RES_Y_;
					sz = cur_idz * RES_Z_;

					ex = (sx + RES_X_ - 1 < ub.x) ? sx + RES_X_ - 1 : ub.x;
					ey = (sy + RES_Y_ - 1 < ub.y) ? sy + RES_Y_ - 1 : ub.y;
					ez = (sz + RES_Z_ - 1 < ub.z) ? sz + RES_Z_ - 1 : ub.z;

					sx = (sx > lb.x) ? sx : lb.x;
					sy = (sy > lb.y) ? sy : lb.y;
					sz = (sz > lb.z) ? sz : lb.z;

					// The starting 1D index of the lower level's node
					bid = level_id[cur_node.x - 1];

					for (int j = sx; j <= ex; j++) {
						for (int k = sy; k <= ey; k++) {
							for (int l = sz; l <= ez; l++) {
								int id = bid + id3ToId1(j, k, l, dim, lb);

								cur_dist = dist(q, j, k, l, nsize);

								/* If a child is not empty and its distance to the query point
								 * is less than current min_dist, push its pair (level, id) to
								 * the stack.
								 */
								if (status[id] > 0 && min_dist >= cur_dist) {
									++stk_top;
									stack[index + stk_top * offset] = make_int2(cur_node.x - 1, id - bid);
								}
							}
						}
					}
				}
			}
		}

		nn_pid[i] = min_pid;
		nn_dist[i] = min_dist;
	}
}

/* Method 3: Multiple threads handles one point. Use warp-primitives
 * __shfl_xor_sync and __shfl_sync to exchange status of a stack between
 * threads in a group.
 *
 * Similar to method 1, threads go from bottom to top of the tree to
 * compare the distance between nodes and the query point with the current
 * min_dist of the query point. However, the major difference is:
 *
 * - In the method 1, each thread handles one query point. When it meets
 * a node, it checks ALL siblings of that node to find candidate nodes,
 * and pushes all candidate nodes to A SINGLE stack of that thread. It then
 * pop elements from the stack one by one. For each popped node, the thread
 * examines children of that node to find candidate nodes. Candidate nodes
 * are then pushed to the same stack and so on.
 *
 * - In the method 3, a group of (RES_X_ * RES_Y_ * RES_Z_) threads handles
 * one query point (currently a group is equal to a warp). When they meets
 * a node (while moving to top), each thread in the group checks ONE sibling
 * of that node. Threads in a group cannot share a SINGLE stack because
 * conflict may occur if multiple threads try to push their candidate nodes
 * to the stack. Instead, each thread is given one stack (depth is equal
 * to the height of the tree).
 *
 * However, those stacks cannot work individually (aka use individual stack
 * pointer) because threads in a group always need to work on children of a
 * same parent node. To that end, we use a bit mask and a stack pointer
 * (stk_level) which are shared among all threads in a group to represent
 * the status of all nodes in a level examined by that group. If a node
 * becomes candidate, the corresponding bit is set. If the mask is not zero
 * (means at least one node in the group is candidate), candidate nodes
 * are pushed to stacks by their threads, and the stk_level increase by 1.
 * When all nodes in the same level are examined by the group, they are removed
 * from stacks and stk_level decrease by 1.
 *
 * The function uses 2 stacks:
 * - Data stack: stores the pair (node's level, node's index). Each thread is
 * given one data stack.
 * - Status stack: stores the bit mask of the children belong to the same
 * parent, which are handled by a group. When all bit are zero, which means all
 * children are examined, the bit mask is removed from the status stack.
 *
 * Input:
 * 		@qx, qy, qz: the coordinates of query points
 * 		@q_num: number of query points
 * 		@status: number of points per tree node
 * 		@x, y, z: coordinates of the points in the tree
 * 		@level_dim: 3D dimensions (in number of nodes) of each tree level
 * 		@level_lb, level_ub: lower and upper 3D index boundaries of each tree level
 * 		@node_size: resolutions of nodes of each tree level
 * 		@level_id: starting index (index of node 0) of each level in the whole
 * 					1D array nodes of the tree.
 * 		@height: height aka number of level of the tree
 * 		@starting_point_id, point_id: used to access points belong to each voxel
 * 		@stack: used to perform depth-first traverse sub-trees
 *
 * Output:
 * 		@nn_dist: the actual nearest distance
 * 		@nn_id: the index of the tree node that contains the nearest neighbor point
 * 		@nn_pid: the index of the nearest neighbor point
 */
__global__ void nnSearchGoUp3(FCloudPtr qcloud, int q_num, FCloudPtr ref_cloud, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb, Vector3i *level_ub,
								Vector3f *node_size, int *level_id,	int height,
								int *starting_point_id, int *point_id, int2 *stack,
								float *nn_dist, int *nn_id, int *nn_pid)
{
	int offset = blockDim.x * gridDim.x;
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stk_level = 0;		// Top level of the stack
	float min_dist;
	int min_id;
	int min_pid;
	float cur_dist;
	int bid;			// Starting 1D index of a level
	int local_id = threadIdx.x & (GROUP_SIZE_ - 1);		// The local index in a group of a thread (lane_id)
	int group_id = (index / GROUP_SIZE_) * GROUP_SIZE_;	// The global index of the group the thread belongs to
	int local_group_id = threadIdx.x / GROUP_SIZE_;		// Index of the group in the block
	int local_group_num = blockDim.x / GROUP_SIZE_;		// Number of groups in a block

	/* 3D local index of the thread in its group.
	 * They are used to compute the index of the tree
	 * node handled by the thread.
	 */
	int lidx, lidy, lidz;

	/* A shared memory buffer to record the status of each level in the stack.
	 * If a bit is set, then a tree node is pushed to that bit location.
	 */
	__shared__ uint32_t stack_status[BLOCK_SIZE_X * 8];		// Assume the maximum height of a tree is 8

	/* We use a bit mask to represent the status of children of a
	 * tree node. If a child node has distance to a query point less
	 * than the current min_dist of that point, the bit corresponding
	 * to the node is set, and the child node is pushed to a stack.
	 *
	 * The level_mask is shared between threads in the group, so all
	 * threads are aware of the status of all nodes they are handling.
	 */
	uint32_t level_mask;

	// Convert 1D lane id to 3D local id
	lid1ToLid3(local_id, lidx, lidy, lidz);

	for (int i = index; i < GROUP_SIZE_ * q_num; i += offset) {
		int qid = i / GROUP_SIZE_;	// index of the query point, GROUP_SIZE_ (32) threads handle one point
		Vector3f q = qcloud[qid];

		// Get current nn distance, nn node, and nn pid
		min_dist = nn_dist[qid];
		min_id = nn_id[qid];
		min_pid = nn_pid[qid];

		int idx, idy, idz;
		Vector3i dim = level_dim[0];
		Vector3i lb = level_lb[0];
		Vector3i ub = level_ub[0];
		Vector3f nsize = node_size[0];

		id1ToId3(min_id, lb, dim, idx, idy, idz);

		// Now go up the tree to find the actual min dist
		for (int level = 0; level < height;
				level++, idx = div(idx, RES_X_), idy = div(idy, RES_Y_), idz = div(idz, RES_Z_)) {

			/* At each level, examine all siblings oif the current node. */

			// Get the information of the current level
			dim = level_dim[level];
			lb = level_lb[level];
			ub = level_ub[level];
			nsize = node_size[level];

			// 3D index of the sibling
			int sbx, sby, sbz;

			if (level == height - 1) {
				// At the root level, examine all nodes
				sbx = lb.x + lidx;
				sby = lb.y + lidy;
				sbz = lb.z + lidz;
			} else {
				// At other levels, examine all siblings of the current node
				sbx = roundDown(idx, RES_X_) + lidx;
				sby = roundDown(idy, RES_Y_) + lidy;
				sbz = roundDown(idz, RES_Z_) + lidz;
			}

			bid = level_id[level];

			// The 1D index of the sibling node handled by the thread
			min_id = bid + id3ToId1(idx, idy, idz, lb, dim);

			// Clear the mask initially
			level_mask = 0x0000;

			// Empty the stack
			stk_level = 0;

			/* The sibling's indices must  be inside the lower and upper
			 * index boundaries of the current level.
			 */
			if (sbx >= lb.x && sby >= lb.y && sbz >= lb.z &&
					sbx <= ub.x && sby <= ub.y && sbz <= ub.z) {
				int sid = bid + id3ToId1(sbx, sby, sbz, lb, dim);

				cur_dist = dist(q, sbx, sby, sbz, nsize);

				/* If a sibling is not empty and its distance to the query point
				 * is less than the current min_dist, push it to the stack */
				if (sid != min_id && status[sid] > 0 && min_dist >= cur_dist) {
					stack[index + stk_level * offset] = make_int2(level, sid - bid);	// index = node_id + group_id
					level_mask |= (1 << local_id);
				}
			}

			// Compute the status of the current level of the stack
			for (int group_offset = GROUP_SIZE_ >> 1; group_offset > 0; group_offset >>= 1) {
				level_mask |= __shfl_xor_sync(FULL_MASK, level_mask, group_offset, GROUP_SIZE_);
			}

			if (level_mask)
				++stk_level;

			if (local_id == 0 && stk_level > 0 && level_mask > 0)
				stack_status[local_group_id  + (stk_level - 1) * local_group_num] = level_mask;

			// Loop until the stack is empty
			while (stk_level > 0) {
				// Get the status mask at the top of the status stack
				level_mask = stack_status[local_group_id  + (stk_level - 1) * local_group_num];

				// Get the index of a candidate node
				int node_id = __ffs(level_mask) - 1;

				/* Get the location of the first set bit of the current level.
				 * It is also the location of the set */

				stack_status[local_group_id  + (stk_level - 1) * local_group_num] &= (~(1 << node_id));

				int2 cur_node = stack[node_id + group_id + (stk_level - 1) * offset];

				/* If this is the last bit 1 in level_mask, that means all
				 * children nodes of this level are now examined. Lower
				 * the stack pointer.
				 */
				if (level_mask == (1 << node_id)) {
					--stk_level;
				}

				// Now examine the current node
				int cur_idx, cur_idy, cur_idz;

				dim = level_dim[cur_node.x];
				lb = level_lb[cur_node.x];

				id1ToId3(cur_node.y, lb, dim, cur_idx, cur_idy, cur_idz);

				if (cur_node.x == 0) {
					/* If the current node is a leaf, check its points.
					 * Use reduction to find the point which is the nearest
					 * to the query point.
					 */
					for (int j = starting_point_id[cur_node.y] + local_id; j < starting_point_id[cur_node.y + 1]; j += GROUP_SIZE_) {
						int pid = point_id[j];

						cur_dist = distance(ref_cloud[pid], q);

						if (min_dist > cur_dist) {
							min_pid = pid;
							min_dist = cur_dist;
						}
					}

					for (int loff = GROUP_SIZE_ >> 1; loff > 0; loff >>= 1) {
						float other_dist = __shfl_xor_sync(FULL_MASK, min_dist, loff, GROUP_SIZE_);
						int other_pid = __shfl_xor_sync(FULL_MASK, min_pid, loff, GROUP_SIZE_);

						if (min_dist > other_dist) {
							min_dist = other_dist;
							min_pid = other_pid;
						}
					}

					// Broadcast the nearest distance to all
					min_dist = __shfl_sync(FULL_MASK, min_dist, 0, GROUP_SIZE_);
					min_pid = __shfl_sync(FULL_MASK, min_pid, 0, GROUP_SIZE_);

				} else {

					/* If this is an inner node (has children node), examine
					 * its children. If the distance from the query point to
					 * a child node is less than the current nearest distance,
					 * push the child to the stack.
					 */
					dim = level_dim[cur_node.x - 1];
					lb = level_lb[cur_node.x - 1];
					ub = level_ub[cur_node.x - 1];
					nsize = node_size[cur_node.x - 1];

					int cbx = cur_idx * RES_X_ + lidx;
					int cby = cur_idy * RES_Y_ + lidy;
					int cbz = cur_idz * RES_Z_ + lidz;

					bid = level_id[cur_node.x - 1];

					// Clear the bit mask
					level_mask = 0;

					/* Always check this condition to make sure
					 * the node is stay in the range of the level
					 */
					if (cbx >= lb.x && cby >= lb.y && cbz >= lb.z &&
							cbx <= ub.x && cby <= ub.y && cbz <= ub.z) {
						int id = bid + id3ToId1(cbx, cby, cbz, lb, dim);

						cur_dist = dist(q, cbx, cby, cbz, nsize);

						/* If a child is not empty and its distance to the query point
						 * is less than current min dist, push it to the stack
						 */
						if (status[id] > 0 && min_dist >= cur_dist) {
							stack[index + stk_level * offset] = make_int2(cur_node.x - 1, id - bid);
							level_mask |= (1 << local_id);
						}
					}

					/* Exchange information between threads in the group to make
					 * sure all of them are aware of the status of all children nodes.
					 */
					for (int group_offset = GROUP_SIZE_ >> 1; group_offset > 0; group_offset >>= 1) {
						level_mask |= __shfl_xor_sync(FULL_MASK, level_mask, group_offset, GROUP_SIZE_);
					}

					/* If the level_mask is not zero, there exist candidate nodes.
					 * Increase the stack pointer by 1.
					 */
					if (level_mask)
						stk_level++;

					/* Push the bit mask to the status stack if it is not zero. */
					if (local_id == 0 && stk_level > 0 && level_mask > 0)
						stack_status[local_group_id + (stk_level - 1) * local_group_num] = level_mask;
				}
			}
		}

		if (local_id == 0) {
			nn_pid[qid] = min_pid;
			nn_dist[qid] = min_dist;
		}
	}
}

template <typename PointType>
void Octree<PointType>::testGoDown(typename PointCloud<PointType>::Ptr &qcloud,
									const device_vector<int> &nearest_node,
									const device_vector<float> &gpu_min_dist)
{
	HostOctree<PointType> host_tree(*this);
	typename pcl::PointCloud<PointType>::Ptr hc = qcloud->host_cloud();
	std::vector<int> hnn_id = nearest_node.host_vector();
	std::vector<float> hnn_dist = gpu_min_dist.host_vector();

	for (size_t i = 0; i < hc->size(); ++i) {
		PointType p = hc->at(i);
		int test_nn_id;
		float test_nn_dist;

		host_tree.goDown(p, test_nn_id, test_nn_dist);

		if (test_nn_id != hnn_id[i]) {
			printf("[Octree.cu, %d] %s::Test goDown failed at point %d\n", __LINE__, __func__, i);
			printf("[Octree.cu, %d] %s::nn test leafId = %d, nn gpu leafId = %d\n",
					__LINE__, __func__, test_nn_id, hnn_id[i]);
			printf("[Octree.cu, %d] %s::Test nn_dist = %f, gpu_dist = %f\n",
					__LINE__, __func__, test_nn_dist, hnn_dist[i]);


			std::vector<Vector3i> test_path;
			std::vector<Vector3i> gpu_path;

			host_tree.traceToTop(test_nn_id, test_path);
			host_tree.traceToTop(hnn_id[i], gpu_path);

			printf("[Octree.cu, %d] %s::Test path:\n", __LINE__, __func__);

			for (auto nid : test_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			printf("[Octree.cu, %d] %s::GPU path:\n", __LINE__, __func__);

			for (auto nid : gpu_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			exit(1);
		}
	}

	printf("[Octree.cu, %d] %s::Test goDown PASSED!\n", __LINE__, __func__);
}



/* Testing function for the nnSearchGoUp.
 *
 * For each query point, find its nearest neighbor using brute force (BF),
 * then compare the nearest distance found by BF with the nearest distance
 * found by nnSearchGoUp. If there is significant difference (absolute value
 * of the subtraction is larger than 0.0000001), the test fails.
 *
 * In the case the test fails, it prints the index of the first mismatch
 * query point, and its debugging information: paths from bottom to top of
 * the CPU and GPU NN Search.
 *
 * Input:
 * 		@qx, qy, qz	: GPU-side coordinates of query points
 * 		@nn_pid		: GPU-side indices of the nearest neighbors of query points
 * 		@nn_id		: GPU-side indices of leaves that contain the nearest neighbors of query points
 * 		@nn_dist	: GPU-side the nearest distances between the query points and their NNs
 */
template <typename PointType>
void Octree<PointType>::testGoUp(typename PointCloud<PointType>::Ptr  &qcloud,
									const device_vector<int> &nn_id,
									const device_vector<int> &nn_pid,
									const device_vector<float> &nn_dist)
{
	HostOctree<PointType> host_tree(*this);
	typename pcl::PointCloud<PointType>::Ptr hc = qcloud->host_cloud();
	std::vector<int> gpu_nn_id = nn_id.host_vector();
	std::vector<int> gpu_nn_pid = nn_pid.host_vector();
	std::vector<float> gpu_nn_dist = nn_dist.host_vector();

	for (size_t i = 0; i < hc->size(); ++i) {
		PointType p = hc->at(i);
		int tnn_id, tnn_pid;
		float tnn_dist;

		host_tree.BFNNSearch(p, tnn_id, tnn_pid, tnn_dist);

		// Compare test values and print out the path if error occurs
		if (tnn_pid != gpu_nn_pid[i] && std::abs(tnn_dist - gpu_nn_dist[i]) > 0.000001) {
			printf("[Octree.cu, %d] %s::Error: Mismatched at query point %d\n", __LINE__, __func__, i);
			printf("[Octree.cu, %d] %s::Test nn pid = %d, gpu nn pid = %d\n",
					__LINE__, __func__, tnn_pid, gpu_nn_pid[i]);
			printf("[Octree.cu, %d] %s::Test nn leafId = %d, gpu nn leafId = %d\n",
					__LINE__, __func__, tnn_id, gpu_nn_id[i]);
			printf("[Octree.cu, %d] %s::Test nn_dist = %f, gpu_dist = %f\n",
					__LINE__, __func__, tnn_dist, gpu_nn_dist[i]);

			std::vector<Vector3i> test_path, gpu_path;

			host_tree.traceToTop(tnn_id, test_path);
			host_tree.traceToTop(gpu_nn_id[i], gpu_path);

			printf("[Octree.cu, %d] %s::Test path\n", __LINE__, __func__);
			for (auto nid : test_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			printf("\nTrace-up gpu node: \n");
			for (auto nid : gpu_path) {
				printf("\t(%d, %d, %d)\n", nid.x, nid.y, nid.z);
			}

			exit(1);
		}
	}

	printf("[Octree.cu, %d] %s::Test goUp PASSED\n", __LINE__, __func__);
}

//#define PROF_TREE_NNS_	// For profiling execution time of NN Search

/* 2-pass parallel nearest neighbor search */
template <typename PointType>
void Octree<PointType>::NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
									device_vector<int> &nn_pid, device_vector<float> &nn_dist)
{
	int qnum = qcloud->size();

	// Indices of leaves that contain nearest neighbor of query points
	device_vector<int> nn_id(qnum, __allocator);
	int thread_num = qnum * GROUP_SIZE_;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(thread_num, nnSearchGoDown3,
					qcloud->data(), qnum, cloud_->data(), status_.data(),
					level_dim_.data(), level_lb_.data(), level_ub_.data(),
					node_size_.data(), level_id_.data(), height_,
					starting_pid_.data(), point_id_.data(),
					nn_dist.data(), nn_id.data(), nn_pid.data()));

	device_vector<int2> stack(height_ * thread_num, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(thread_num, nnSearchGoUp3,
					qcloud->data(), qnum, cloud_->data(), status_.data(),
					level_dim_.data(), level_lb_.data(), level_ub_.data(),
					node_size_.data(), level_id_.data(),	height_,
					starting_pid_.data(), point_id_.data(), stack.data(),
					nn_dist.data(), nn_id.data(), nn_pid.data()));
}

template <typename PointType>
void Octree<PointType>::NNSearch(typename PointCloud<PointType>::Ptr &qcloud, device_vector<float> &nn_dist)
{
	device_vector<int> nn_pid(qcloud->size(), __allocator);

	NNSearch(qcloud, nn_pid, nn_dist);
}

template <typename PointType>
void Octree<PointType>::update(typename PointCloud<PointType>::Ptr &new_cloud, int new_point_num)
{
	if (new_point_num <= 0 && new_point_num < new_cloud->size()) {
		return;
	}

	// Compute new boundaries
	Vector3i new_max_b, new_min_b, new_leaf_size;
	int new_leaf_num;

	computeBoundaries(new_cloud->data(), new_max_b, new_min_b, new_leaf_size, new_leaf_num);

	// Extend current buffers to hold new points
	extendMemory(new_point_num, new_max_b, new_min_b, new_leaf_size, new_leaf_num);

	// Add new points to the new tree
	updateTreeContent(new_cloud, new_point_num);
}

template <typename PointType>
void Octree<PointType>::computeBoundaries(FCloudPtr cloud, Vector3i &new_max_b, Vector3i &new_min_b,
											Vector3i &new_leaf_size, int &new_leaf_num)
{
	int point_num = cloud.size();
	float max_x, max_y, max_z, min_x, min_y, min_z;

	minMax(cloud.X(), min_x, max_x, point_num, __allocator);
	minMax(cloud.Y(), min_y, max_y, point_num, __allocator);
	minMax(cloud.Z(), min_z, max_z, point_num, __allocator);

	new_min_b.x = static_cast<int>(floor(min_x / res_.x));
	new_min_b.y = static_cast<int>(floor(min_y / res_.y));
	new_min_b.z = static_cast<int>(floor(min_z / res_.z));

	new_max_b.x = static_cast<int>(floor(max_x / res_.x));
	new_max_b.y = static_cast<int>(floor(max_y / res_.y));
	new_max_b.z = static_cast<int>(floor(max_z / res_.z));

	new_min_b.x = roundDown(new_min_b.x, MAX_BX_);
	new_min_b.y = roundDown(new_min_b.y, MAX_BY_);
	new_min_b.z = roundDown(new_min_b.z, MAX_BZ_);

	new_max_b.x = roundUp(new_max_b.x, MAX_BX_);
	new_max_b.y = roundUp(new_max_b.y, MAX_BY_);
	new_max_b.z = roundUp(new_max_b.z, MAX_BZ_);

	new_leaf_size = new_max_b - new_min_b + 1;

	new_leaf_num = new_leaf_size.x * new_leaf_size.y * new_leaf_size.z;
}

/* Copy data from the source level to the destination level.
 *
 * Input:
 * 		@src_status: the source number of points per node array
 * 		@src_upper_x, src_upper_y, src_upper_z:	the source upper coordinates array
 * 		@src_dim: the dimensions of the source level
 * 		@src_lb: the lower boundaries of the source level
 * 		@src_node_num: the number of nodes of the source level
 * 		@dst_dim: the dimensions of the destination level
 * 		@dst_lb: the lower boundaries of the destination level
 *
 * Output:
 * 		@dst_status: the destination number of points per node array
 */
__global__ void copyTreeNode(uint32_t *src_status, Vector3i src_dim, Vector3i src_lb, int src_node_num,
								uint32_t *dst_status, Vector3i dst_dim, Vector3i dst_lb)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_node_num; i += stride) {
		// Here i is the 1D index of the current source node in the source level
		// Convert source node's 1D array index to 3D global index
		Vector3i src_id3 = id1ToId3(i, src_dim, src_lb);

		// Compute the 1D index of the source node in the destination level
		int dst_id1 = id3ToId1(src_id3, dst_dim, dst_lb);

		// Copy data from the source to the destination
		// Data here is only the number of points in the node
		dst_status[dst_id1] = src_status[i];
	}
}

/* To copy the information in starting_pid and point_id from
 * the bottom level of the source tree to the bottom level of
 * the destination tree, we fist need to compute the number of
 * points each destination node has, based on the starting_pid
 * of the source leaves.
 *
 * Input:
 * 		@src_starting_pid: the starting_pid of the source bottom level
 * 		@src_leaf_num: number of leaves of the source tree
 * 		@src_bottom_dim: dimensions of the source bottom level
 * 		@src_bottom_lb: the lower index boundaries of the source bottom level
 * 		@dst_bottom_dim: dimensions of the destination bottom level
 * 		@dst_bottom_lb: the lower index boundaries of the destination bottom level
 *
 * Output:
 * 		@dst_points_per_node: number of points each destination node has
 */
__global__ void computePointsPerLeaf(int *src_starting_pid, int src_leaf_num,
										Vector3i src_bottom_dim, Vector3i src_bottom_lb,
										Vector3i dst_bottom_dim, Vector3i dst_bottom_lb,
										int *dst_points_per_node)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_leaf_num; i += stride) {
		// The number of points the current source leaf i has
		int point_num = src_starting_pid[i + 1] - src_starting_pid[i];

		// Compute the 3D global indices of the current source leaf i
		Vector3i src_id3 = id1ToId3(i, src_bottom_dim, src_bottom_lb);

		// Compute the 1D array index of the leaf in the destination tree
		int new_id1 = id3ToId1(src_id3, dst_bottom_dim, dst_bottom_lb);

		dst_points_per_node[new_id1] = point_num;
	}
}

/* Copy the points' indices from the source bottom level to the
 * destination bottom level.
 *
 * Input:
 * 		@src_starting_pid: the starting_pid of the source bottom level
 * 		@src_point_id: the points' indices of the source bottom level
 * 		@src_leaf_num: number of leaves of the source tree
 * 		@src_bottom_dim: dimensions of the source bottom level
 * 		@src_bottom_lb: the lower index boundaries of the source bottom level
 * 		@dst_bottom_dim: dimensions of the destination bottom level
 * 		@dst_bottom_lb: the lower index boundaries of the destination bottom level
 * 		@dst_starting_pid: the starting_pid of the destination bottom level
 *
 * Output:
 * 		@dst_point_id: the points' indices of the destination bottom level
 * */
__global__ void copyBottom(FCloudPtr cloud, int point_num, Vector3f res,
							int *src_starting_pid, int *src_point_id,
							Vector3i src_bottom_dim, Vector3i src_bottom_lb,
							Vector3i dst_bottom_dim, Vector3i dst_bottom_lb,
							int *dst_starting_pid, int *dst_point_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		// Index of the current point
		int pid = src_point_id[i];
		// Coordinates of the point
		Vector3f p = cloud[pid];

		// Compute the index of the source leaf where the point belongs to
		int leaf_id = leafId(p, res, src_bottom_lb, src_bottom_dim);

		/* Compute the relative index of the point id in the list
		 * of points belonging to this leaf.
		 */
		int relative_id = i - src_starting_pid[leaf_id];

		// Compute the 3D global index of the source leaf
		Vector3i src_id3 = id1ToId3(leaf_id, src_bottom_lb, src_bottom_dim);

		// Compute the 1D array index of the source leaf in the destination grid
		int dst_id1 = id3ToId1(src_id3, dst_bottom_lb, dst_bottom_dim);

		int new_id = relative_id + dst_starting_pid[dst_id1];

		dst_point_id[new_id] = pid;
	}
}

/* Allocate a new buffer and copy the data from the old tree
 * to the new buffer. */
template <typename PointType>
void Octree<PointType>::extendMemory(int new_point_num, Vector3i new_max_b, Vector3i new_min_b,
										Vector3i new_leaf_size, int new_leaf_num)
{

	/* If there is not enough free space for new points,
	 * extend buffers for the point cloud.
	 */
	if (point_num_ + new_point_num > alloc_point_num_) {

		// Allocate new buffers
		alloc_point_num_ = roundUp(point_num_ + new_point_num, POINT_BLOCK_);

		cloud_->reserve(alloc_point_num_);

		// point_num_ is currently still the same because we haven't added new points yet
	}

	/* Extend buffers for the tree only if there exist new
	 * boundary that is greater than the corresponding old one.
	 */
	if (new_max_b.x > max_b_.x || new_max_b.y > max_b_.y || new_max_b.z > max_b_.z ||
			new_min_b.x < min_b_.x || new_min_b.y < min_b_.y || new_min_b.z < min_b_.z) {

		// Compute new boundaries at the bottom level
		if (new_max_b.x < max_b_.x) {
			new_max_b.x = max_b_.x;
		}

		if (new_max_b.y < max_b_.y) {
			new_max_b.y = max_b_.y;
		}

		if (new_max_b.z < max_b_.z) {
			new_max_b.z = max_b_.z;
		}

		if (new_min_b.x > min_b_.x) {
			new_min_b.x = min_b_.x;
		}

		if (new_min_b.y > min_b_.y) {
			new_min_b.y = min_b_.y;
		}

		if (new_min_b.z > min_b_.z) {
			new_min_b.z = min_b_.z;
		}

		// Update new_leaf_x, new_leaf_y, new_leaf_z and new_leaf_num
		new_leaf_size = new_max_b - new_min_b + 1;
		new_leaf_num = new_leaf_size.x * new_leaf_size.y * new_leaf_size.z;

		// Allocate new buffers
		device_vector<uint32_t> new_status(__allocator);
		device_vector<Vector3i> new_level_dim(__allocator), new_level_lb(__allocator),
								new_level_ub(__allocator);
		device_vector<int> new_level_id(__allocator);
		device_vector<Vector3f> new_node_size(__allocator);

		int new_node_num, new_height;

		allocateOctree(new_leaf_size, new_min_b, new_max_b, new_status,
						new_level_dim, new_level_lb, new_level_ub,
						new_node_size, new_level_id, new_node_num, new_height);

		// Copy data from old buffers to new ones
		auto hlevel_dim = level_dim_.host_vector();
		auto hlevel_lb = level_lb_.host_vector();
		auto hlevel_ub = level_ub_.host_vector();
		auto hlevel_id = level_id_.host_vector();

		auto new_hlevel_dim = new_level_dim.host_vector();
		auto new_hlevel_lb = new_level_lb.host_vector();
		auto new_hlevel_ub = new_level_ub.host_vector();
		auto new_hlevel_id = new_level_id.host_vector();

		for (int level = 0; level < height_; ++level) {
			Vector3i src_dim = hlevel_dim[level];
			Vector3i src_lb = hlevel_lb[level];

			Vector3i dst_dim = new_hlevel_dim[level];
			Vector3i dst_lb = new_hlevel_lb[level];

			int src_lv_id = hlevel_id[level];
			int dst_lv_id = new_hlevel_id[level];

			int src_node_num = src_dim.x * src_dim.y * src_dim.z;

			if (src_node_num <= 0) {
				break;
			}

			checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_node_num, copyTreeNode,
							status_.data() + src_lv_id, src_dim, src_lb, src_node_num,
							new_status.data() + dst_lv_id, dst_dim, dst_lb));
		}

		/* The new height may be higher than the current height_.
		 * In such cases, we have to build the levels higher than
		 * current height_.
		 */
		for (int lv = height_; lv < new_height; lv++) {
			buildUpper(new_status.data() + new_hlevel_id[lv], new_hlevel_dim[lv],
						new_hlevel_lb[lv], new_hlevel_ub[lv],
						new_status.data() + new_hlevel_id[lv - 1], new_hlevel_dim[lv - 1],
						new_hlevel_lb[lv - 1], new_hlevel_ub[lv - 1]);
		}

		status_ = std::move(new_status);

		level_dim_ = std::move(new_level_dim);
		level_lb_ = std::move(new_level_lb);
		level_ub_ = std::move(new_level_ub);
		level_id_ = std::move(new_level_id);
		node_size_ = std::move(new_node_size);

		/* Copy the starting_pid_ and point_id_ of the bottom level
		 * from the current tree to the newly allocated tree.
		 */
		if (leaf_num_ > 0) {
			// First, allocate new starting_pid vector with new_leaf_num
			device_vector<int> new_starting_pid(new_leaf_num + 1, __allocator, true);

			if (point_num_ > 0) {
				checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num_, computePointsPerLeaf,
								starting_pid_.data(), leaf_num_, hlevel_dim[0], hlevel_lb[0],
								new_hlevel_dim[0], new_hlevel_lb[0], new_starting_pid.data()));

				/* Now new_starting_pid[i] contains the number of points
				 * in the voxel [i] of the bottom level of the new tree.
				 */

				// Compute the new starting_pid
				ExclusiveScan(new_starting_pid);

				device_vector<int> new_point_id(point_id_.size(), __allocator);

				// Copy points' indices from old bottom level to new bottom level
				checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num_, copyBottom,
								cloud_->data(), point_num_, res_, starting_pid_.data(), point_id_.data(),
								hlevel_dim[0], hlevel_lb[0], new_hlevel_dim[0], new_hlevel_lb[0],
								new_starting_pid.data(), new_point_id.data()));

				point_id_ = std::move(new_point_id);
			}

			// Replace the old buffers by the new ones
			starting_pid_ = std::move(new_starting_pid);
		}

		// Replace private variables by the new buffers and new variables

		leaf_num_ = new_leaf_num;
		node_num_ = new_node_num;
		height_ = new_height;

		leaf_size_ = new_leaf_size;
		min_b_ = new_min_b;
		max_b_ = new_max_b;
	}

	/* Notice that currently point_num_ is still the same
	 * because we haven't added new points to the tree yet.
	 */
}

/* Copy point_id_ from source bottom level to destination bottom level.
 * Used when updating content of the octree's bottom level.
 *
 * Params:
 * 		@cloud[in]: coordinates of points
 * 		@point_num: number of points
 * 		@res[in]: resolution of the voxel grid
 * 		@bottom_dim[in]: the dimensions of the source level
 * 		@bottom_lb[in]: the lower index boundaries of the source level
 * 		@src_point_id[in]: the point id of the source level
 * 		@src_starting_pid[in]: the starting indices of list of points belonging
 * 								to each source leaf
 * 		@dst_starting_pid[in]: the starting indices of list of points belonging
 *	 							to each destination leaf
 * 		@dst_point_id[out]: the point id of the destination level
 *
 */
__global__ void copyPointId(FCloudPtr cloud, int point_num, Vector3f res,
							Vector3i bottom_dim, Vector3i bottom_lb,
							int *src_point_id, int *src_starting_pid,
							int *dst_point_id, int *dst_starting_pid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	// Iterate through every element of the src_point_id
	for (int i = index; i < point_num; i += stride) {
		// The index of the current point in the point cloud (x, y, z)
		int pid = src_point_id[i];
		Vector3f p = cloud[pid];

		// Compute the index of the source leaf where the point belongs to
		int leaf_id = leafId(p, res, bottom_lb, bottom_dim);

		/* Compute the relative index of the point id in the list
		 * of points belonging to this leaf.
		 */
		int relative_id = i - src_starting_pid[leaf_id];

		/* The new index of the point id in the list of points
		 * belonging to the leaf in the destination grid is
		 * the sum of its relative index and the new starting index.
		 */
		int new_id = relative_id + dst_starting_pid[leaf_id];

		dst_point_id[new_id] = pid;
	}
}

/* After indices of old points are copied to the new
 * point_ids, the wlocation must be updated as well.
 * new_wlocation[i] = wlocation[i] + old_points_per_node[i]
 */
__global__ void updateWritingLocation(int *wlocation, uint32_t *old_points_per_node, int leaf_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < leaf_num; i += stride) {
		wlocation[i] += old_points_per_node[i];
	}
}

/* Update the tree node's number of points.
 * Each thread picks a point and goes from bottom to top
 * of the tree and use atomicAdd to increase the point number
 * of every node it meets.
 *
 * TODO:
 * If atomicAdd is too slow, consider building another
 * tree using the new points then fuse new tree and old tree.
 *
 * Input:
 * 		@x, y, z: coordinates of newly added points
 * 		@point_num: number of newly added points
 * 		@leaf_x, leaf_y, leaf_z: the dimensions of the bottom level
 * 		@res_x, res_y, res_z: the resolution of the leaf
 * 		@min_b_x, min_b_y, min_b_z:
 * */
__global__ void updateTreeNode(FCloudPtr cloud, int point_num, Vector3i leaf_size,
								Vector3f res, Vector3i min_b, uint32_t *status,
								Vector3i *level_dim, Vector3i *level_lb,
								int *level_id, int height)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int leaf_id = leafId(cloud[i], leaf_size, res, min_b);
		Vector3i id3 = id1ToId3(leaf_id, level_lb[0], level_dim[0]);

		for (int level = 0; level < height; ++level, id3.x = div(id3.x, RES_X_),
				id3.y = div(id3.y, RES_Y_), id3.z = div(id3.z, RES_Z_)) {
			int node_id = level_id[level] + id3ToId1(id3, level_lb[level], level_dim[level]);

			atomicAdd(&status[node_id], 1);
		}
	}
}


/* Update the tree's content as new points are added.
 * 2 steps:
 *
 * 1. Update the bottom level's starting_pid_ and point_id_
 * 2. Update the number of points per node in all levels
 *
 * Input:
 * 		@x, y, z: coordinates of new points to be added
 * 		@new_point_num: number of new points to be added
 *
 */
template <typename PointType>
void Octree<PointType>::updateTreeContent(typename PointCloud<PointType>::Ptr &new_cloud, int new_point_num)
{
	if (new_point_num <= 0)
		return;

	// Append new points to the inner buffers
	cloud_->resize(point_num_ + new_point_num);
	copy(cloud_->data() + point_num_, new_cloud->data(), new_point_num);

	/* Update bottom level starting_pid and point_ids
	 * TODO: Actually so far we have to copy this bottom level's
	 * starting_pid_ and point_id_ twice: once in extend memory,
	 * once here. Can we only copy it once here?
	 */
	device_vector<int> points_per_node(leaf_num_ + 1, __allocator);

	// Initially, points_per_node is just a copy of the status_ at the lowest level
	gcopyDtoD(points_per_node.data(), status_.data(), sizeof(int) * leaf_num_);

	// Use atomicAdd to update the number of points in a leaf when a new point is added
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_point_num, computeLeafSize,
					new_cloud->data(), new_point_num, leaf_size_, res_,
					min_b_, points_per_node.data()));

	// Now the number of points in each leaf is the sum of old points and new points

	// Compute the new writing location
	ExclusiveScan(points_per_node);

	// Now points_per_node contains the writing locations of points in leaf nodes
	device_vector<int> wlocation = std::move(points_per_node);

	// Copy old point indices to the new point_ids
	device_vector<int> new_starting_pid(leaf_num_ + 1, __allocator);
	device_vector<int> new_point_id(point_num_ + new_point_num, __allocator);

	// Backup new starting_pid
	gcopyDtoD(new_starting_pid.data(), wlocation.data(), sizeof(int) * (leaf_num_ + 1));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num_, copyPointId,
					cloud_->data(), point_num_, res_, level_dim_[0], level_lb_[0],
					point_id_.data(), starting_pid_.data(),
					new_point_id.data(), wlocation.data()));

	/* Because old point indices are just written to new_point_id,
	 * the writing location must be updated as well.
	 */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(leaf_num_, updateWritingLocation,
					wlocation.data(), status_.data(), leaf_num_));

	// Insert new points to the new bottom's new_point_id
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_point_num, scatterPointToLeaf,
					new_cloud->data(), new_point_num, wlocation.data(), leaf_size_,
					res_, min_b_, new_point_id.data(), point_num_));

	// Update tree node's number of points
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_point_num, updateTreeNode,
					new_cloud->data(), new_point_num, leaf_size_, res_, min_b_,
			 	 	status_.data(), level_dim_.data(), level_lb_.data(),
			 	 	level_id_.data(), height_));

	 // Replace old buffers by the new one and update point_num_
	 starting_pid_ = std::move(new_starting_pid);
	 point_id_ = std::move(new_point_id);
	 point_num_ += new_point_num;
}

/* Compute the total number of points in each level
 * and test if it is equal to the total number of points.
 * If not, errors occured while the tree is being built.
 *
 * Input:
 * 		@level: the level to be tested
 * 		@line_no: line number, usually __LINE__
 *
 */
template <typename PointType>
void Octree<PointType>::testLevel(int level, int line_no)
{
	int level_size = level_id_[level + 1] - level_id_[level];

	std::vector<int> test_level(level_size);

	gcopyDtoH(test_level.data(), status_.data() + level_id_[level], sizeof(int) * level_size);

	int test_point_num = 0;

	for (int i = 0; i < level_size; ++i) {
		test_point_num += test_level[i];
	}

	printf("\t[Octree.cu, %d] At level = %d, test_point_num = %d, point_num = %d\n\n", line_no, level, test_point_num, point_num_);

	if (test_point_num != point_num_)
		exit(1);
}



// Class for debugging the GPU Octree
template <typename PointType>
HostOctree<PointType>::HostOctree(const Octree<PointType> &gpu_tree)
{
	cloud_ = (gpu_tree.cloud_)->host_cloud();
	status_ = (gpu_tree.status_).host_vector();
	level_id_ = (gpu_tree.level_id_).host_vector();
	level_dim_ = (gpu_tree.level_dim_).host_vector();
	level_lb_ = (gpu_tree.level_lb_).host_vector();
	level_ub_ = (gpu_tree.level_ub_).host_vector();
	node_size_ = (gpu_tree.node_size_).host_vector();
	starting_pid_ = (gpu_tree.starting_pid_).host_vector();
	point_id_ = (gpu_tree.point_id_).host_vector();

	leaf_size_ = gpu_tree.leaf_size_;
	res_ = gpu_tree.res_;
	min_b_ = gpu_tree.min_b_;
	max_b_ = gpu_tree.max_b_;

	height_ = gpu_tree.height_;
}

template <typename PointType>
void HostOctree<PointType>::goDown(PointType p, int &nn_id, float &nn_dist)
{
	Vector3i nn_node_id;	// Indices of the nearest node in each level

	// Go down to the bottom level
	for (int lv = height_; lv > 0; --lv) {
		goDown(p, lv, nn_node_id);
	}

	// At the bottom level, find the nearest point
	int leaf_id = id3ToId1(nn_node_id, min_b_, leaf_size_);

	goDown(p, leaf_id, nn_id, nn_dist);
}

/* Given a query point and 3D index of a parent node,
 * compute the index of a child node that is the nearest
 * node to the query point.
 *
 * Used to debug the GPU nnSearchGoDown.
 *
 * Params:
 * 		@p[in]: the coordinates of the query point
 * 		@level: the level of the parent node
 * 		@node_id[in/out]: indices of the parent node and output
 * 							are indices of the nearest children
 */
template <typename PointType>
void HostOctree<PointType>::goDown(PointType p, int level, Vector3i &node_id)
{
	Vector3f q(p.x, p.y, p.z);
	int cbase = level_id_[level - 1];

	Vector3i cdim = level_dim_[level - 1];
	Vector3i clb = level_lb_[level - 1];
	Vector3i cub = level_ub_[level - 1];
	Vector3f nsize = node_size_[level - 1];

	int sx, sy, sz, ex, ey, ez;

	if (level == height_) {
		sx = clb.x;
		sy = clb.y;
		sz = clb.z;

		ex = cub.x;
		ey = cub.y;
		ez = cub.z;
	} else {
		sx = node_id.x * RES_X_;
		sy = node_id.y * RES_Y_;
		sz = node_id.z * RES_Z_;

		ex = (sx + RES_X_ - 1 < cub.x) ? sx + RES_X_ - 1 : cub.x;
		ey = (sy + RES_Y_ - 1 < cub.y) ? sy + RES_Y_ - 1 : cub.y;
		ez = (sz + RES_Z_ - 1 < cub.z) ? sz + RES_Z_ - 1 : cub.z;
	}


	float min_dist = FLT_MAX;

	for (int i = sx; i <= ex; i++) {
		for (int j = sy; j <= ey; j++) {
			for (int k = sz; k <= ez; k++) {
				int cid = cbase + id3ToId1(i, j, k, clb, cdim);
				float cur_dist = dist(q, i, j, k, nsize);

				if (status_[cid] > 0 && min_dist > cur_dist) {
					min_dist = cur_dist;
					node_id = Vector3i(i, j, k);
				}
			}
		}
	}
}

/* Among the points in a leaf, find the one
 * that is the nearest to the query point.
 *
 * This function is used to debug the nnSearchGoDown.
 *
 * Input:
 * 		@nid: 1D index of the leaf
 * 		@qx, qy, qz: coordinates of the query point
 * 		@hpid: host-side indices of reference points (copied from point_id_)
 * 		@hx, hy, hz: host-side coordinates of the reference points
 * 					(copied from x_, y_, z_)
 *
 * Output:
 * 		@nn_dist: distance to the nearest neighbor (found by goDown,
 * 					may not be the actual nearest neighbor)
 *
 */
template <typename PointType>
void HostOctree<PointType>::goDown(PointType p, int lid, int &nn_id, float &nn_dist)
{
	int start = starting_pid_[lid];
	int end = starting_pid_[lid + 1];

	nn_dist = FLT_MAX;

	for (int i = start; i < end; i++) {
		PointType ref_p = cloud_->at(i);
		float cur_dist = dist(ref_p, p);

		if (nn_dist > cur_dist) {
			nn_dist = cur_dist;
			nn_id = i;
		}
	}
}

/* Go from bottom to height and print out the path
 * that lead from top to the specified node. Usually
 * height is set by height_ (tree height).
 *
 * Input:
 * 		@x, y, z: the 3D indices of the node
 * 		@height: the level to which we want to print
 * 					out the path from bottom
 */
template <typename PointType>
void HostOctree<PointType>::traceToTop(int leaf_id, std::vector<Vector3i> &path)
{
	path.resize(height_);

	Vector3i id = id1ToId3(leaf_id, level_lb_[0], level_dim_[0]);

	path[0] = id;

	for (int level = 1; level < height_; ++level) {
		id.x = div(id.x, RES_X_);
		id.y = div(id.y, RES_Y_);
		id.z = div(id.z, RES_Z_);

		path[level] = id;
	}
}

/* The brute-force nearest neighbor search.
 * Use to test the result of the nearest neighbor search.
 *
 * TODO: the trace_up part is wrong. Need to fix later.
 *
 * Input:
 * 		@qx, qy, qz: the coordinate of the query point to be searched
 * 		@hx, hy, hz: the host-side coordinates of points in the tree
 *
 * Output:
 * 		@nn_pid: index of the point which is the nearest to the query point
 * 		@nn_id: index of the leaf containing the nearest neighbor
 * 		@nn_dist: distance between the nearest point and the query point
 */
template <typename PointType>
void HostOctree<PointType>::BFNNSearch(PointType p, int &nn_id, int &nn_pid, float &nn_dist)
{
	nn_dist = FLT_MAX;

	for (size_t i = 0; i < cloud_->size(); ++i) {
		PointType ref_p = cloud_->at(i);
		float cur_dist = dist(p, ref_p);

		if (nn_dist > cur_dist) {
			nn_dist = cur_dist;
			nn_pid = i;
		}
	}

	nn_id = leafId(p);
}

template class Octree<pcl::PointXYZ>;
template class Octree<pcl::PointXYZI>;
template class HostOctree<pcl::PointXYZ>;
template class HostOctree<pcl::PointXYZI>;

}

}
