#include <iostream>
#include <math.h>
#include <limits>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/scan.h>
#include <thrust/fill.h>
#include <inttypes.h>

#include <vector>
#include <cmath>

#include <stdio.h>
#include <sys/time.h>

#include <ndt_gpu/VoxelGrid.h>
#include <ndt_gpu/debug.h>
#include <ndt_gpu/common.h>
#include <ndt_gpu/utilities.h>
#include <ndt_gpu/SymmetricEigenSolver.h>

#ifndef FULL_MASK
#define FULL_MASK	(0xffffffff)
#endif

namespace gpu
{

namespace vgrid
{

VoxelGrid::VoxelGrid(GMemoryAllocator *allocator) :
	Base(allocator),
	centroid_(__allocator),
	icov_(__allocator),
	points_per_voxel_(__allocator),
	tmp_centroid_(__allocator),
	tmp_cov_(__allocator)
{
	voxel_num_ = 0;
	res_ = Vector3f(0, 0, 0);
	max_b_.x = max_b_.y = max_b_.z = INT_MIN;
	min_b_.x = min_b_.y = min_b_.z = INT_MAX;
	vgdim_ = Vector3i(0, 0, 0);
	min_points_per_voxel_ = 6;
	real_max_b_.x = real_max_b_.y = real_max_b_.z = INT_MIN;
	real_min_b_.x = real_min_b_.y = real_min_b_.z = INT_MAX;
}

VoxelGrid::VoxelGrid(const VoxelGrid& other) :
	Base(other),
	centroid_(other.centroid_),
	icov_(other.icov_),
	points_per_voxel_(other.points_per_voxel_),
	tmp_centroid_(other.tmp_centroid_),
	tmp_cov_(other.tmp_cov_)
{
  voxel_num_ = other.voxel_num_;

  res_ = other.res_;
  max_b_ = other.max_b_;
  min_b_ = other.min_b_;
  vgdim_ = other.vgdim_;

  min_points_per_voxel_ = other.min_points_per_voxel_;

  real_max_b_ = other.real_max_b_;
  real_min_b_ = other.real_min_b_;
}

VoxelGrid::VoxelGrid(VoxelGrid &&other) :
	Base(other),
	centroid_(std::move(other.centroid_)),
	icov_(std::move(other.icov_)),
	points_per_voxel_(std::move(other.points_per_voxel_)),
	tmp_centroid_(std::move(other.tmp_centroid_)),
	tmp_cov_(std::move(other.tmp_cov_))
{
  voxel_num_ = other.voxel_num_;

  res_ = other.res_;
  max_b_ = other.max_b_;
  min_b_ = other.min_b_;
  vgdim_ = other.vgdim_;

  min_points_per_voxel_ = other.min_points_per_voxel_;

  real_max_b_ = other.real_max_b_;
  real_min_b_ = other.real_min_b_;
}

VoxelGrid& VoxelGrid::operator=(const VoxelGrid& other)
{
  centroid_ = other.centroid_;
  icov_ = other.icov_;
  points_per_voxel_ = other.points_per_voxel_;

  tmp_centroid_ = other.tmp_centroid_;
  tmp_cov_ = other.tmp_cov_;

  voxel_num_ = other.voxel_num_;

  res_ = other.res_;
  max_b_ = other.max_b_;
  min_b_ = other.min_b_;
  vgdim_ = other.vgdim_;

  min_points_per_voxel_ = other.min_points_per_voxel_;

  real_max_b_ = other.real_max_b_;
  real_min_b_ = other.real_min_b_;

  return *this;
}

VoxelGrid& VoxelGrid::operator=(VoxelGrid &&other)
{
  centroid_ = std::move(other.centroid_);
  icov_ = std::move(other.icov_);
  points_per_voxel_ = std::move(other.points_per_voxel_);

  tmp_centroid_ = std::move(other.tmp_centroid_);
  tmp_cov_ = std::move(other.tmp_cov_);

  voxel_num_ = other.voxel_num_;

  res_ = other.res_;
  max_b_ = other.max_b_;
  min_b_ = other.min_b_;
  vgdim_ = other.vgdim_;

  min_points_per_voxel_ = other.min_points_per_voxel_;

  real_max_b_ = other.real_max_b_;
  real_min_b_ = other.real_min_b_;

  return *this;
}

void VoxelGrid::allocBuffers()
{
	centroid_.resize(voxel_num_ * 3);

	icov_.resize(voxel_num_ * 9);
	fill(icov_, 0);

	points_per_voxel_.resize(voxel_num_);
	fill(points_per_voxel_, 0);

	tmp_centroid_.resize(voxel_num_ * 3);
	fill(tmp_centroid_, 0);

	tmp_cov_.resize(voxel_num_ * 9);
	fill(tmp_cov_, 0);
}

// Added for incremental ndt, used in over-allocation
__host__ __device__ int roundUp(int input, int factor)
{
	return (input < 0) ? -((-input) / factor) * factor : ((input + factor - 1) / factor) * factor;
}

__host__ __device__ int roundDown(int input, int factor)
{
	return (input < 0) ? -((-input + factor - 1) / factor) * factor : (input / factor) * factor;
}

/**
 * Compute the index of the voxel that contains a point @p
 *
 * Params:
 * 		@p[in]: coordinate of the point p
 * 		@res[in]: resolution of the voxel grid
 * 		@min_b[in]: the lower bound of the voxel grid
 * 		@vgdim[in]: the dimensions of the voxel grid
 *
 * Returns: the 1D index of the voxel that contains @p
 */
inline __device__ int voxelId(Vector3f p, Vector3f res, Vector3i min_b, Vector3i vgdim)
{
  int idx = static_cast<int>(floorf(p.x / res.x)) - min_b.x;
  int idy = static_cast<int>(floorf(p.y / res.y)) - min_b.y;
  int idz = static_cast<int>(floorf(p.z / res.z)) - min_b.z;

  return (idx + idy * vgdim.x + idz * vgdim.x * vgdim.y);
}

/* Initialize tmp centroids and covariance matrices.
 * Centroids are set to (0, 0, 0) and covariances are set
 * to identity matrix initially.
 *
 * Input:
 * 		@voxel_num: number of voxels in the grid
 *
 * Output:
 * 		@tmp_centroids: the tmp_centroids of voxels
 * 		@tmp_covariance: the tmp_covariance of voxels
 */
__global__ void initTmpCentroidAndCovariance(double *tmp_centroids, double *tmp_covariances, int voxel_num)
{
	  int index = threadIdx.x + blockIdx.x * blockDim.x;
	  int stride = blockDim.x * gridDim.x;

	  for (int i = index; i < voxel_num; i += stride) {
		  MatrixDevice<> tmp_centr(3, 1, voxel_num, tmp_centroids + i);
		  MatrixDevice<> tmp_cov(3, 3, voxel_num, tmp_covariances + i);

		  tmp_centr(0) = tmp_centr(1) = tmp_centr(2) = 0.0;

		  tmp_cov(0, 0) = tmp_cov(1, 1) = tmp_cov(2, 2) = 1.0;

		  tmp_cov(0, 1) = tmp_cov(0, 2) = tmp_cov(1, 2) = 0.0;
	  }
}

/* Compute the tmp centroids and tmp covariances of voxels, given
 * the input point cloud x, y, z.
 *
 * Input:
 * 		@x, y, z: the coordinates of the input points
 * 		@starting_point_ids: the starting indices of the points that belong
 * 								to each voxel.
 * 		@point_ids: the sorted indices of points to match the order of voxels.
 * 		@voxel_num: number of voxels in the grid
 *
 * Output:
 * 		@tmp_centroids: the tmp_centroids of all voxels
 * 		@tmp_covariances: the tmp_covariances of all voxels
 */
__global__ void computeTmpCentroidAndCovariance(FCloudPtr cloud, int* starting_point_ids, int* point_ids,
												int voxel_num, double* tmp_centroids,
												double* tmp_covariances)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = idx; i < voxel_num; i += stride) {
		// The loop range to access the points belonging to this voxel
		int start = starting_point_ids[i], end = starting_point_ids[i + 1];

		// If the voxel is empty, skip it
		if (start == end)
			continue;

		MatrixDD tmp_centr(3, 1, voxel_num, tmp_centroids + i);
		MatrixDD tmp_cov(3, 3, voxel_num, tmp_covariances + i);

		double centr0, centr1, centr2;
		double cov00, cov01, cov02, cov11, cov12, cov22;

		centr0 = centr1 = centr2 = 0.0;
		cov00 = cov11 = cov22 = 0.0;
		cov01 = cov02 = cov12 = 0.0;

		for (int j = start; j < end; j++) {
			int pid = point_ids[j];
			Vector3f p = cloud[pid];
			double t_x = static_cast<double>(p.x);
			double t_y = static_cast<double>(p.y);
			double t_z = static_cast<double>(p.z);

			centr0 += t_x;
			centr1 += t_y;
			centr2 += t_z;

			cov00 += t_x * t_x;
			cov01 += t_x * t_y;
			cov02 += t_x * t_z;
			cov11 += t_y * t_y;
			cov12 += t_y * t_z;
			cov22 += t_z * t_z;
		}

		tmp_centr(0) += centr0;
		tmp_centr(1) += centr1;
		tmp_centr(2) += centr2;

		tmp_cov(0, 0) += cov00;
		tmp_cov(0, 1) += cov01;
		tmp_cov(0, 2) += cov02;
		tmp_cov(1, 1) += cov11;
		tmp_cov(1, 2) += cov12;
		tmp_cov(2, 2) += cov22;
	}
}


/* Compute centroids of voxels from tmp_centroid.
 * centroid = tmp_centroid / point_num
 *
 * Input:
 * 		@tmp_centroid: tmp_centroids of voxels
 * 		@points_per_voxel: number of points per voxel
 * 		@voxel_num: number of voxels
 * Output:
 * 		@centroid: centroids of voxels*/
__global__ void computeVoxelCentroid(double* tmp_centroid, int* points_per_voxel, int voxel_num, double *centroid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		MatrixDD centr(3, 1, voxel_num, centroid + vid);
		MatrixDD tmp_centr(3, 1, voxel_num, tmp_centroid + vid);
		int tmp_pnum = points_per_voxel[vid];

		/* If the voxel is not empty, then its centroid = tmp_centroid / point_num.
		 * Otherwise, its centroid is a copy of tmp_centroid
		 * (aka centroid = tmp_centroid / 1)
		 */
		double point_num = (tmp_pnum > 0) ? static_cast<double>(tmp_pnum) : 1;

		centr(0) = tmp_centr(0) / point_num;
		centr(1) = tmp_centr(1) / point_num;
		centr(2) = tmp_centr(2) / point_num;
	}
}

/* Compute voxels' covariances.
 *
 * Input:
 * 		@centroid: voxels' centroids
 * 		@tmp_centroid: voxels' tmp centroids
 * 		@tmp_covariance: voxels' tmp covariances
 * 		@points_per_voxel: number of points belong to each voxel
 * 		@voxel_num: number of voxels in the grid
 * 		@min_points_per_voxel: minimum number of points that a voxel must
 * 		 						have to be involved in later computation
 *
 * Output:
 * 		@covariance: voxels' covariances
 */
__global__ void computeVoxelCovariance(double* centroid, double* tmp_centroid, double* tmp_covariance,
                                        int* points_per_voxel, int voxel_num, int min_points_per_voxel,
                                        double *covariance)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		MatrixDD cov(3, 3, voxel_num, covariance + vid);
		MatrixDD tcov(3, 3, voxel_num, tmp_covariance + vid);
		double point_num = static_cast<double>(points_per_voxel[vid]);

		double c0 = centroid[vid];
		double c1 = centroid[voxel_num + vid];
		double c2 = centroid[voxel_num * 2 + vid];
		double p0 = tmp_centroid[vid];
		double p1 = tmp_centroid[voxel_num + vid];
		double p2 = tmp_centroid[voxel_num * 2 + vid];

		if (point_num >= min_points_per_voxel) {
			double mult = (point_num - 1.0) / point_num;

			cov(0, 0) = ((tcov(0, 0) - 2.0 * p0 * c0) / point_num + c0 * c0) * mult;
			cov(0, 1) = ((tcov(0, 1) - 2.0 * p0 * c1) / point_num + c0 * c1) * mult;
			cov(0, 2) = ((tcov(0, 2) - 2.0 * p0 * c2) / point_num + c0 * c2) * mult;
			cov(1, 1) = ((tcov(1, 1) - 2.0 * p1 * c1) / point_num + c1 * c1) * mult;
			cov(1, 2) = ((tcov(1, 2) - 2.0 * p1 * c2) / point_num + c1 * c2) * mult;
			cov(2, 2) = ((tcov(2, 2) - 2.0 * p2 * c2) / point_num + c2 * c2) * mult;
		} else {
			cov(0, 0) = tcov(0, 0);
			cov(0, 1) = tcov(0, 1);
			cov(0, 2) = tcov(0, 2);
			cov(1, 1) = tcov(1, 1);
			cov(1, 2) = tcov(1, 2);
			cov(2, 2) = tcov(2, 2);
		}

		cov(1, 0) = cov(0, 1);
		cov(2, 0) = cov(0, 2);
		cov(2, 1) = cov(1, 2);
	}
}


/* Compute inverse eigenvectors of voxels.
 *
 * Input:
 * 		@eigenvectors: eigenvectors computed from previous steps
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 *
 * Output:
 * 		@inverse_eigenvectors: the output inverse eigenvectors
 */
__global__ void computeInverseEigenvectors(double* eigenvectors, int* points_per_voxel, int voxel_num,
											int min_points_per_voxel, double* inverse_eigenvectors)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD ievec(3, 3, voxel_num, inverse_eigenvectors + vid);
			MatrixDD evec(3, 3, voxel_num, eigenvectors + vid);

			evec.inverse(ievec);
		}
	}
}

/* Compute the product eigen_vecs = eigen_vecs * eigen_val.
 *
 * Input:
 * 		@eigenvectors: eigenvectors of the covariance matrices of voxels
 * 		@eigenvalues: eigenvalues of the covariance matrices of voxels
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 *
 * Output:
 * 		@eigenvectors: to save GPU memory space, store the product
 * 						directlyto the eigenvectors buffer.
 */
__global__ void updateCovarianceS0(double* eigenvectors, double* eigenvalues, int* points_per_voxel,
									int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD eigen_vectors(3, 3, voxel_num, eigenvectors + vid);

			double eig_val0 = eigenvalues[vid];
			double eig_val1 = eigenvalues[vid + voxel_num];
			double eig_val2 = eigenvalues[vid + 2 * voxel_num];

			eigen_vectors(0, 0) *= eig_val0;
			eigen_vectors(1, 0) *= eig_val0;
			eigen_vectors(2, 0) *= eig_val0;

			eigen_vectors(0, 1) *= eig_val1;
			eigen_vectors(1, 1) *= eig_val1;
			eigen_vectors(2, 1) *= eig_val1;

			eigen_vectors(0, 2) *= eig_val2;
			eigen_vectors(1, 2) *= eig_val2;
			eigen_vectors(2, 2) *= eig_val2;
		}
	}
}

/* Compute new covariance cov = new eigen_vecs * inverse eigen_vecs
 *
 * Input:
 * 		@new_eigenvectors: the product eigen_vecs * eigen_val of all voxels
 * 		@inverse_eigenvectors: the inverse eigenvectors of all voxels
 * 		@point_per_voxel: the number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 * 		@col: the column index to be computed
 *
 * Output:
 * 		@covariance: the output covariance matrices
 */
__global__ void updateCovarianceS1(double* new_eigenvectors, double* inverse_eigenvectors, int* points_per_voxel,
									int voxel_num, int min_points_per_voxel, int col, double* covariance)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD cov(3, 3, voxel_num, covariance + vid);
			MatrixDD ievec(3, 3, voxel_num, inverse_eigenvectors + vid);
			MatrixDD nevec(3, 3, voxel_num, new_eigenvectors + vid);

			double tmp0 = ievec(0, col);
			double tmp1 = ievec(1, col);
			double tmp2 = ievec(2, col);

			cov(0, col) = nevec(0, 0) * tmp0 + nevec(0, 1) * tmp1 + nevec(0, 2) * tmp2;
			cov(1, col) = nevec(1, 0) * tmp0 + nevec(1, 1) * tmp1 + nevec(1, 2) * tmp2;
			cov(2, col) = nevec(2, 0) * tmp0 + nevec(2, 1) * tmp1 + nevec(2, 2) * tmp2;
		}
	}
}

/* Compupte the inverse covariance matrices.
 *
 * Input:
 * 		@covariance: the input covariance matrices
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels in the grid
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 *
 * Output:
 * 		@inverse_covariance: the inverse covariance matrices
 *
 */
__global__ void computeInverseCovariance(double* covariance, double* inverse_covariance,
                                          int* points_per_voxel, int voxel_num,
										  int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD cov(3, 3, voxel_num, covariance + vid);
			MatrixDD icov(3, 3, voxel_num, inverse_covariance + vid);

			cov.inverse(icov);
		}
	}
}

/* Normalize input matrices to avoid overflow.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 *
 */
__global__ void normalize(SymmetricEigensolver3x3 sv, int* points_per_voxel,
							int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		// Ignore empty voxels
		if (points_per_voxel[id] > 0) {
			sv.normalizeInput(id);
		}
	}
}

/* Compute eigenvalues. Eigenvalues are arranged in increasing order.
 * (eigen(0) <= eigen(1) <= eigen(2).
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEigenvalues(SymmetricEigensolver3x3 sv, int* points_per_voxel,
									int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvalues(id);
	}
}

/* First step to compute the eigenvector 0 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 * */
__global__ void computeEvec00(SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector00(id);
	}
}

/* Second step to compute the eigenvector 0 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec01(SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector01(id);
	}
}

/* First step to compute the eigenvector 1 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec10(SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector10(id);
	}
}

/* Second step to compute the eigenvector 1 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec11(SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector11(id);
	}
}

/* Compute the eigenvector 2 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec2(SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector2(id);
	}
}

/* Final step to compute eigenvalues of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void updateEval(SymmetricEigensolver3x3 sv, int* points_per_voxel,
							int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.updateEigenvalues(id);
	}
}

/* Update eigenvalues in the case covariance matrix is nearly singular.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void updateEval2(double* eigenvalues, int* points_per_voxel,
							int voxel_num, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		if (points_per_voxel[id] >= min_points_per_voxel) {
			MatrixDD eigen_val(3, 1, voxel_num, eigenvalues + id);
			double ev0 = eigen_val(0);
			double ev1 = eigen_val(1);
			double ev2 = eigen_val(2);

			if (ev0 < 0 || ev1 < 0 || ev2 <= 0) {
				continue;
			}

			double min_cov_eigvalue = ev2 * 0.01;

			if (ev0 < min_cov_eigvalue) {
				ev0 = min_cov_eigvalue;

				if (ev1 < min_cov_eigvalue) {
					ev1 = min_cov_eigvalue;
				}
			}

			eigen_val(0) = ev0;
			eigen_val(1) = ev1;
			eigen_val(2) = ev2;
		}
	}
}

/* Compute new measurements of a voxel grid given an input point cloud.
 *
 * Input:
 * 		@x, y, z: the coordinates of the input point clouds
 * 		@starting_point_ids: the starting indices of list of points
 * 								belong to each voxel.
 * 		@point_ids: the indices of input points, sorted to matched
 * 					the order of voxels they blong to.
 */
void VoxelGrid::computeCentroidAndCovariance(FCloudPtr input, device_vector<int> &starting_point_ids,
												device_vector<int> &point_ids)
{
	if (voxel_num_ <= 0) {
		return;
	}

	// Initialize tmp centroids and tmp covariance
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, initTmpCentroidAndCovariance,
					tmp_centroid_.data(), tmp_cov_.data(), voxel_num_));

	// Compute tmp centroids and tmp covariances using new points
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeTmpCentroidAndCovariance,
					input, starting_point_ids.data(), point_ids.data(), voxel_num_,
					tmp_centroid_.data(), tmp_cov_.data()));

	/* TODO:
	 * Here should we copy tmp_centroid to centroid_ and
	 * tmp_cov_ to covariance_?
	 */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeVoxelCentroid,
					tmp_centroid_.data(), points_per_voxel_.data(), voxel_num_,
					centroid_.data()));

	device_vector<double> covariance(voxel_num_ * 9, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeVoxelCovariance,
					centroid_.data(), tmp_centroid_.data(), tmp_cov_.data(),
					points_per_voxel_.data(), voxel_num_, min_points_per_voxel_,
					covariance.data()));

	device_vector<double> eigenvalues_dev(voxel_num_ * 3, __allocator);
	device_vector<double> eigenvectors_dev(voxel_num_ * 9, __allocator);

	/* Deal with singular covariance matrices */

	// Solving eigenvalues and eigenvectors problem by the GPU.
	HSymmetricEigensolver3x3 hsv(voxel_num_, __allocator);

	// TODO: Here should use shared_ptr instead of raw pointer
	hsv.setInputMatrices(covariance.data());
	hsv.setEigenvalues(eigenvalues_dev.data());
	hsv.setEigenvectors(eigenvectors_dev.data());

	SymmetricEigensolver3x3 sv(hsv);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, normalize,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEigenvalues,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEvec00,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEvec01,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEvec10,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEvec11,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeEvec2,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, updateEval,
					sv, points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, updateEval2,
					eigenvalues_dev.data(), points_per_voxel_.data(), voxel_num_,
					min_points_per_voxel_));

	/* Compute inverse eigenvectors. To save GPU memory, use inverse_covariance
	 * to store the output of this step (inverse eigenvectors of voxels).
	 */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeInverseEigenvectors,
					eigenvectors_dev.data(), points_per_voxel_.data(), voxel_num_,
					min_points_per_voxel_, icov_.data()));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, updateCovarianceS0,
					eigenvectors_dev.data(), eigenvalues_dev.data(), points_per_voxel_.data(),
					voxel_num_, min_points_per_voxel_));

	// Final step, compute the new covariance matrices
	for (int i = 0; i < 3; ++i) {
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, updateCovarianceS1,
						eigenvectors_dev.data(), icov_.data(), points_per_voxel_.data(),
						voxel_num_,  min_points_per_voxel_, i, covariance.data()));
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeInverseCovariance,
					covariance.data(), icov_.data(), points_per_voxel_.data(),
		  	  	  	voxel_num_, min_points_per_voxel_));
}

// Input are supposed to be in device memory
void VoxelGrid::setInputCloud(FCloudPtr input)
{
	findBoundaries(input.X(), input.Y(), input.Z(), input.size(),
					min_b_, max_b_, real_min_b_, real_max_b_,
					vgdim_, voxel_num_);

	allocBuffers();

	device_vector<int> starting_point_ids(__allocator), point_ids(__allocator);

	scatterPointsToVoxelGrid(input, starting_point_ids, point_ids);

	computeCentroidAndCovariance(input, starting_point_ids, point_ids);
}

/* Find the indices range (min_bound, max_bound) of voxels
 * which are close to a coordinate value.
 *
 * Input:
 * 		@coordinate: the input coordinate value
 * 		@radius: the range to determine the vicinity
 * 		@resolution: size of a voxel dimension
 * 		@min_bound, max_bound: the lowest and highest boundary values
 * Output:
 * 		@min_vid, max_vid: the lower and upper boundaries of voxels
 * 							in the coordinate's vicinity
 */
__device__ void findVoxelBoundary(float coordinate, float radius, float resolution,
									int min_bound, int max_bound,
									int &min_vid, int &max_vid)
{
	max_vid = __float2int_rd((coordinate + radius) / resolution);
	min_vid = __float2int_rd((coordinate - radius) / resolution);

	max_vid = (max_vid > max_bound) ? max_bound : max_vid;
	min_vid = (min_vid < min_bound) ? min_bound : min_vid;
}

__global__ void findNeighborVoxel(FCloudPtr qcloud, int cvoxel_num, float radius, Vector3i bb_dim,
									double *centroids, int *points_per_voxel, int voxel_num,
									Vector3i real_min_b, Vector3i real_max_b, Vector3i min_b,
									Vector3i vgdim, Vector3f res, int cvoxel_per_point,
									int *cvoxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < cvoxel_num; i += stride) {
		int pid = i / cvoxel_per_point;
		int local_id = i % cvoxel_per_point;
		Vector3f q = qcloud[pid];
		Vector3i min_id, max_id;

		// Find the boundaries of candidiate voxels (inclusive)
		findVoxelBoundary(q.x, radius, res.x, real_min_b.x, real_max_b.x, min_id.x, max_id.x);
		findVoxelBoundary(q.y, radius, res.y, real_min_b.y, real_max_b.y, min_id.y, max_id.y);
		findVoxelBoundary(q.z, radius, res.z, real_min_b.z, real_max_b.z, min_id.z, max_id.z);

		Vector3i vid = id1ToId3(local_id, bb_dim) + min_id;

		if (vid.x <= max_id.x && vid.y <= max_id.y && vid.z <= max_id.z) {
			// Compute the 1D index of the candidate voxel
			int cvid = id3ToId1(vid, min_b, vgdim);
			MatrixDD centr(3, 1, voxel_num, centroids + cvid);
			double dx = static_cast<double>(q.x) - centr(0);
			double dy = static_cast<double>(q.y) - centr(1);
			double dz = static_cast<double>(q.z) - centr(2);

			if (norm3d(dx, dy, dz) <= radius) {
				cvoxel_id[i] = cvid;
			}
		}
	}
}

struct isValid {
	inline __device__ bool operator()(int val) {
		return (val >= 0);
	}
};

__global__ void computeStartingVid(int *psum, int point_num, int cvoxel_per_point,
									int *starting_voxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	// The last element of starting_voxel_id is the total number of neighbor voxels
	for (int i = index; i <= point_num; i += stride) {
		starting_voxel_id[i] = psum[i * cvoxel_per_point];
	}
}

void VoxelGrid::radiusSearch(FCloudPtr qcloud, float radius, int max_nn,
								device_vector<int> &starting_voxel_id,
								device_vector<int> &nn_voxel_id)
{
	// Clear the output
	starting_voxel_id.clear();
	nn_voxel_id.clear();

	int point_num = qcloud.size();

	if (point_num <= 0 || voxel_num_ <= 0) {
		return;
	}

	/**
	 * Assume the number of qcloud is quite small (less than 100,000 points),
	 * pre-allocate buffer to store indices of all candidate voxels of query
	 * points.
	 */
	Vector3i bb_dim;	// Dimensions of the boundding box that covers the sphere around each query point

	bb_dim.x = static_cast<int>(floor((radius + radius) / res_.x)) + 1;
	bb_dim.y = static_cast<int>(floor((radius + radius) / res_.y)) + 1;
	bb_dim.z = static_cast<int>(floor((radius + radius) / res_.z)) + 1;

	int cvoxel_per_point = bb_dim.x * bb_dim.y * bb_dim.z;
	int cvoxel_num = point_num * cvoxel_per_point;

	if (cvoxel_num <= 0) {
		return;
	}

	// Compute candidate voxel
	device_vector<int> cvoxel_id(cvoxel_num, __allocator);

	fill(cvoxel_id, -1);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(cvoxel_num, findNeighborVoxel,
					qcloud, cvoxel_num, radius, bb_dim, centroid_.data(),
					points_per_voxel_.data(), voxel_num_, real_min_b_,
					real_max_b_, min_b_, vgdim_, res_, cvoxel_per_point,
					cvoxel_id.data()));

	// Get non-negative indices
	device_vector<int> psum(__allocator);

	streamCompaction<int, isValid>(cvoxel_id, nn_voxel_id, psum);

	// Compute starting_voxel_id from psum
	starting_voxel_id.resize(point_num + 1);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, computeStartingVid,
					psum.data(), point_num, cvoxel_per_point, starting_voxel_id.data()));
}

/* Compute the voxel id and the number of points per voxel using atomicAdd */
__global__ void countPointsPerVoxel(FCloudPtr cloud, int point_num, Vector3f res,
									Vector3i min_b, Vector3i vgdim, int *voxel_id,
									int *points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int vid = voxelId(cloud[i], res, min_b, vgdim);

		voxel_id[i] = vid;
		atomicAdd(points_per_voxel + vid, 1);
	}
}


/* Rearrange points to locations corresponding to voxels */
__global__ void scatterPointsToVoxels(int *voxel_id, int point_num, int* wloc, int* point_ids)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = idx; i < point_num; i += stride) {
		int vid = voxel_id[i];
		int loc = atomicAdd(wloc + vid, 1);

		point_ids[loc] = i;
	}
}

void VoxelGrid::scatterPointsToVoxelGrid(FCloudPtr input, device_vector<int> &starting_point_ids,
											device_vector<int> &point_ids)
{
	int point_num = input.size();

	if (point_num <= 0) {
		return;
	}

	device_vector<int> voxel_id(point_num, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, countPointsPerVoxel,
					input, point_num, res_, min_b_, vgdim_, voxel_id.data(),
					points_per_voxel_.data()));

	starting_point_ids.resize(voxel_num_ + 1);

	device_vector<int> wloc(voxel_num_, __allocator);

	gcopyDtoD(starting_point_ids.data(), points_per_voxel_.data(), sizeof(int) * voxel_num_);

	ExclusiveScan(starting_point_ids);

	gcopyDtoD(wloc.data(), starting_point_ids.data(), sizeof(int) * voxel_num_);

	point_ids.resize(point_num);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPointsToVoxels,
					voxel_id.data(), point_num, wloc.data(), point_ids.data()));
}


template <typename PointType>
void VoxelGrid::update(typename PointCloud<PointType>::Ptr &new_cloud)
{
	// New boundaries and new size of the new point cloud
	Vector3i min_b, max_b, real_min_b, real_max_b, vgdim;
	int voxel_num;

	// Step 1: Find boundaries of the voxel grid that covers the points to be added
	findBoundaries(new_cloud->X(), new_cloud->Y(), new_cloud->Z(), new_cloud->size(),
					min_b, max_b, real_min_b, real_max_b, vgdim, voxel_num);

	// Update real_max_bxyz_, real_min_bxyz_
	real_max_b_.x = std::max(real_max_b_.x, real_max_b.x);
	real_max_b_.y = std::max(real_max_b_.y, real_max_b.y);
	real_max_b_.z = std::max(real_max_b_.z, real_max_b.z);

	real_min_b_.x = std::min(real_min_b_.x, real_min_b.x);
	real_min_b_.y = std::min(real_min_b_.y, real_min_b.y);
	real_min_b_.z = std::min(real_min_b_.z, real_min_b.z);

	// Step 2: Extend buffers so the voxel grid can cover the new points
	extendMemory(min_b, max_b);

	// Step 3.1: Build a small voxel grid that cover the new points
	device_vector<int> points_per_voxel(__allocator);
	device_vector<int> starting_point_ids(__allocator);
	device_vector<int> point_ids(__allocator);

	scatterPointsToVoxelGrid(new_cloud->data(), min_b, vgdim, points_per_voxel,
								starting_point_ids, point_ids);

	// Step 3.2: Remove empty voxels since they contribute nothing to the final voxel grid
	device_vector<int> voxel_id(__allocator);

	filterOutEmptyVoxels(points_per_voxel, starting_point_ids, voxel_id);

	// Step 4: Update the destination grid's measurement with new points
	updateCentroidAndCovariance(new_cloud->data(), voxel_id, points_per_voxel,
								starting_point_ids, point_ids, min_b, vgdim);
}

void VoxelGrid::findBoundaries(float *x, float *y, float *z, int point_num,
								Vector3i &min_b, Vector3i &max_b,
								Vector3i &real_min_b, Vector3i &real_max_b,
								Vector3i &vgdim, int &voxel_num)
{
	float min_x, min_y, min_z, max_x, max_y, max_z;

	minMax(x, min_x, max_x, point_num, __allocator);
	minMax(y, min_y, max_y, point_num, __allocator);
	minMax(z, min_z, max_z, point_num, __allocator);

	real_max_b.x = static_cast<int>(floor(max_x / res_.x));
	real_max_b.y = static_cast<int>(floor(max_y / res_.y));
	real_max_b.z = static_cast<int>(floor(max_z / res_.z));

	real_min_b.x = static_cast<int>(floor(min_x / res_.x));
	real_min_b.y = static_cast<int>(floor(min_y / res_.y));
	real_min_b.z = static_cast<int>(floor(min_z / res_.z));

	max_b.x = roundUp(real_max_b.x, BLOCK_X_);
	max_b.y = roundUp(real_max_b.y, BLOCK_Y_);
	max_b.z = roundUp(real_max_b.z, BLOCK_Z_);

	min_b.x = roundDown(real_min_b.x, BLOCK_X_);
	min_b.y = roundDown(real_min_b.y, BLOCK_Y_);
	min_b.z = roundDown(real_min_b.z, BLOCK_Z_);

	vgdim = max_b - min_b + 1;

	voxel_num = vgdim.x * vgdim.y * vgdim.z;
}


template <int row_num, int col_num, typename Scalar = double>
__global__ void copyMatrix(Scalar *src, int src_voxel_num, Vector3i src_min_b, Vector3i src_vgdim,
							Scalar *dst, int dst_voxel_num, Vector3i dst_min_b, Vector3i dst_vgdim)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		// Convert 1D array index to 3D global index
		Vector3i src_id = id1ToId3(i, src_min_b, src_vgdim);

		// Convert 3D global index to dst 1D array index
		int dst_id = id3ToId1(src_id, dst_min_b, dst_vgdim);

		MatrixDevice<Scalar> src_mat(row_num, col_num, src_voxel_num, src + i);
		MatrixDevice<Scalar> dst_mat(row_num, col_num, dst_voxel_num, dst + dst_id);

		// Copy src to dst
		src_mat.copy(dst_mat);
	}
}


/* Extend the buffers for voxel grid.
 * The input are the voxel boundaries of the grid that cover the new point cloud.
 * If the current voxel grid cannot cover the new grid, new larger buffers are
 * allocated and the data of the current voxel grid are copied to the new buffers.
 *
 *
 * */
void VoxelGrid::extendMemory(Vector3i min_b, Vector3i max_b)
{
	memInfo();

	if (min_b.x < min_b_.x || min_b.y < min_b_.y || min_b.z < min_b_.z ||
			max_b.x > max_b_.x || max_b.y > max_b_.y || max_b.z > max_b_.z) {
		min_b.x = std::min(min_b.x, min_b_.x);
		min_b.y = std::min(min_b.y, min_b_.y);
		min_b.z = std::min(min_b.z, min_b_.z);

		max_b.x = std::max(max_b.x, max_b_.x);
		max_b.y = std::max(max_b.y, max_b_.y);
		max_b.z = std::max(max_b.z, max_b_.z);

		// Dimensions of the new voxel grid
		Vector3i vgdim = max_b - min_b + 1;

		int voxel_num = vgdim.x * vgdim.y * vgdim.z;

		// Allocate new buffers

		printf("Allocate tmp_centroid size = %d (double)\n", voxel_num * 3);
		device_vector<double> tmp_centroid(voxel_num * 3, __allocator, true);

		printf("Allocate tmp_cov size = %d (double)\n", voxel_num * 9);
		device_vector<double> tmp_cov(voxel_num * 9, __allocator, true);

		// Initialize tmp centroid to (0, 0, 0) and tmp covariance to identity
		// Initialize tmp_centroid and tmp_cov
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, initTmpCentroidAndCovariance,
						tmp_centroid.data(), tmp_cov.data(), voxel_num));

		// Copy data from old buffers to new buffers
		printf("Allocate centroid size = %d (double)\n", voxel_num * 3);
		device_vector<double> centroid(voxel_num * 3, __allocator);

		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, copyMatrix<3, 1>,
						centroid_.data(), voxel_num_, min_b_, vgdim_,
						centroid.data(), voxel_num, min_b, vgdim));

		centroid_ = std::move(centroid);

		printf("Allocate inverse_covariance size = %d (double)\n", voxel_num * 9);
		device_vector<double> icov(voxel_num * 9, __allocator, true);

		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, copyMatrix<3, 3>,
						icov_.data(), voxel_num_, min_b_, vgdim_,
						icov.data(), voxel_num, min_b, vgdim));

		icov_ = std::move(icov);


		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, copyMatrix<3, 1>,
						tmp_centroid_.data(), voxel_num_, min_b_, vgdim_,
						tmp_centroid.data(), voxel_num, min_b, vgdim));

		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, copyMatrix<3, 3>,
						tmp_cov_.data(), voxel_num_, min_b_, vgdim_,
						tmp_cov.data(), voxel_num, min_b, vgdim));

		printf("Allocate points_per_voxel size = %d (int)\n", voxel_num);
		device_vector<int> points_per_voxel(voxel_num, __allocator, true);

		checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, copyMatrix<1, 1, int>,
						points_per_voxel_.data(), voxel_num_, min_b_, vgdim_,
						points_per_voxel.data(), voxel_num, min_b, vgdim));

		points_per_voxel_ = std::move(points_per_voxel);

		// Replace old buffers by the new one
		tmp_centroid_ = std::move(tmp_centroid);
		tmp_cov_ = std::move(tmp_cov);


		// Update boundaries
		min_b_ = min_b;
		max_b_ = max_b;
		vgdim_ = vgdim;

		voxel_num_ = voxel_num;
	}
}


/* Build a voxel grid that best fits an input point cloud.
 *
 * TODO:
 * This code is somewhat similar to other scatterPointsToVoxelGrid.
 * One of them should be removed.
 *
 * Input:
 * 		@x, y, z: coordinates of input points
 * 		@point_num: number of input points
 * 		@min_b_x, min_b_y, min_b_z: the lower boundaries of the voxel grid
 * 		@vgrid_x, vgrid_y, vgrid_z: the dimensions of the voxel grid
 *
 * Output:
 * 		@points_per_voxel: number of points per each voxel
 * 		@starting_point_ids: the starting indices of the list of points of each voxel
 * 		@point_ids: the indices of the input points
 */
void VoxelGrid::scatterPointsToVoxelGrid(FCloudPtr input, Vector3i min_b, Vector3i vgdim,
											device_vector<int> &points_per_voxel,
											device_vector<int> &starting_point_ids,
											device_vector<int> &point_ids)
{
	int point_num = input.size();
	int voxel_num = vgdim.x * vgdim.y * vgdim.z;

	points_per_voxel.resize(voxel_num);

	fill(points_per_voxel, 0);

	device_vector<int> voxel_id(point_num, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, countPointsPerVoxel,
					input, point_num, res_, min_b, vgdim, voxel_id.data(), points_per_voxel.data()));

	starting_point_ids.resize(voxel_num + 1);

	device_vector<int> wlocation(voxel_num, __allocator);

	gcopyDtoD(starting_point_ids.data(), points_per_voxel.data(), sizeof(int) * voxel_num);

	ExclusiveScan(starting_point_ids);

	gcopyDtoD(wlocation.data(), starting_point_ids.data(), sizeof(int) * voxel_num);

	point_ids.resize(point_num);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPointsToVoxels,
					voxel_id.data(), point_num, wlocation.data(), point_ids.data()));
}

/* Mark non-empty voxels */
__global__ void markNonEmptyVoxels(int *points_per_voxel, int *mark, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		mark[i] = (points_per_voxel[i] > 0) ? 1 : 0;
	}
}

/* Write the information of non-empty voxels to output buffers.
 *
 * Input:
 * 		@in_points_per_voxel: the number of points per voxel (including
 * 								empty voxels)
 * 		@in_starting_point_ids: the starting indices of list of points
 * 								in each original voxel (including empty ones)
 * 		@wlocation: the writing location of non-empty voxels to output buffers
 * 		@voxel_num: number of original voxels
 *
 * Output:
 * 		@out_points_per_voxel: the number of points per non-empty voxel
 * 		@out_starting_point_ids: the starting indices of list of points
 * 									in non-empty voxels
 * 		@out_voxel_id: indices of non-empty voxels
 * 	*/
__global__ void getNonEmptyVoxels(int *in_points_per_voxel, int *in_starting_point_ids, int *wlocation,
									int voxel_num, int *out_points_per_voxel, int *out_starting_point_ids,
									int *out_voxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		int pnum = in_points_per_voxel[i];

		// Write information of non-empty voxels to the output buffers
		if (pnum > 0) {
			int wloc = wlocation[i];

			out_points_per_voxel[wloc] = pnum;
			out_starting_point_ids[wloc] = pnum;
			out_voxel_id[wloc] = i;
		}
	}
}

/* Used in updating the voxel grid when a new point cloud is added.
 * A voxel grid (the source grid) is built on top of the new point
 * cloud, then is fused to the current voxel grid (the destination
 * grid). Because the source grid may contain empty voxels, and
 * those voxels contribute nothing to the measurements of the
 * destination grid, they should be removed from the source grid.
 *
 * Input:
 * 		@points_per_voxel: the number of points per source voxel
 * 		@starting_point_ids: the starting indices of list of points
 * 								that belong to each source voxel
 * Output:
 * 		@points_per_voxel: the number of points per non-empty source voxel
 * 		@starting_point_ids: the starting indices of list of points
 * 								that belong to each non-empty source voxel
 * 		@voxel_id: indices of non-empty voxels
 */
void VoxelGrid::filterOutEmptyVoxels(device_vector<int> &points_per_voxel,
										device_vector<int> &starting_point_ids,
										device_vector<int> &voxel_id)
{
	int voxel_num = points_per_voxel.size();

	if (voxel_num <= 0) {
		return;
	}

	device_vector<int> mark(voxel_num + 1, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, markNonEmptyVoxels,
					points_per_voxel.data(), mark.data(), voxel_num));

	int non_empty_voxel_num = 0;

	ExclusiveScan(mark, non_empty_voxel_num);

	device_vector<int> out_points_per_voxel(non_empty_voxel_num, __allocator);
	device_vector<int> out_starting_point_ids(non_empty_voxel_num + 1, __allocator);

	voxel_id.resize(non_empty_voxel_num);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(non_empty_voxel_num, getNonEmptyVoxels,
					points_per_voxel.data(), starting_point_ids.data(), mark.data(),
					voxel_num, out_points_per_voxel.data(), out_starting_point_ids.data(),
					voxel_id.data()));

	ExclusiveScan(out_starting_point_ids);

	points_per_voxel = std::move(out_points_per_voxel);
	starting_point_ids = std::move(out_starting_point_ids);
}

/* Compute the indices of voxels */
__global__ void computeVoxelId(int *in_voxel_id, int src_voxel_num,
								Vector3i src_min_b, Vector3i src_vgdim,
								Vector3i dst_min_b, Vector3i dst_vgdim,
								int *voxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		Vector3i src_id = id1ToId3(in_voxel_id[i], src_min_b, src_vgdim);

		voxel_id[i] = id3ToId1(src_id, dst_min_b, dst_vgdim);
	}
}


/* Update tmp centroids and tmp covariance of the destination grid
 * with new points from a source grid.
 *
 * Input:
 * 		@x, y, z: new points added to the voxel grid
 * 		@starting_point_ids: the starting index of list of points in each voxel
 * 		@point_ids: the indices of points belong to each voxel
 * 		@voxel_id: the 1D indices of source voxels in the destination grid
 * 		@src_voxel_num: number of source voxels
 * 		@dst_voxel_num: number of destination voxels
 *
 * Output:
 * 		@tmp_centroids: the tmp centroids of the destination grid
 * 		@tmp_covariances: the tmp covariance of the destination grid
 */
__global__ void updateTmpCAndC(FCloudPtr cloud, int* starting_point_ids, int* point_ids,
								int *voxel_id, int src_voxel_num, int dst_voxel_num,
								double* tmp_centroids, double* tmp_covariances)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int start = starting_point_ids[i], end = starting_point_ids[i + 1];
		int dst_vid = voxel_id[i];

		MatrixDD tmp_centr(3, 1, dst_voxel_num, tmp_centroids + dst_vid);
		MatrixDD tmp_cov(3, 3, dst_voxel_num, tmp_covariances + dst_vid);

		double centr0, centr1, centr2;
		double cov00, cov01, cov02, cov11, cov12, cov22;

		centr0 = centr1 = centr2 = 0.0;
		cov00 = cov11 = cov22 = 0.0;
		cov01 = cov02 = cov12 = 0.0;

		for (int j = start; j < end; j++) {
			int pid = point_ids[j];
			Vector3d t = static_cast<Vector3d>(cloud[pid]);

			centr0 += t.x;
			centr1 += t.y;
			centr2 += t.z;

			cov00 += t.x * t.x;
			cov01 += t.x * t.y;
			cov02 += t.x * t.z;
			cov11 += t.y * t.y;
			cov12 += t.y * t.z;
			cov22 += t.z * t.z;
	  }

	  tmp_centr(0) += centr0;
	  tmp_centr(1) += centr1;
	  tmp_centr(2) += centr2;

	  tmp_cov(0, 0) += cov00;
	  tmp_cov(0, 1) += cov01;
	  tmp_cov(0, 2) += cov02;
	  tmp_cov(1, 1) += cov11;
	  tmp_cov(1, 2) += cov12;
	  tmp_cov(2, 2) += cov22;
  }
}

/* Update the number of points per voxel in the destination grid,
 * given the number of points from the source grid.
 *
 * Input:
 * 		@voxel_id: the indices of source voxel in the destination grid
 * 		@src_voxel_num: the number of source voxels
 * 		@src_point_per_voxel: the number of points per source voxel
 *
 * Output:
 * 		@dst_point_per_voxel: the number of points per destination voxel
 */
__global__ void updatePointsPerVoxel(int *voxel_id, int src_voxel_num,
										int *src_point_per_voxel,
										int *dst_point_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int dst_id = voxel_id[i];

		dst_point_per_voxel[dst_id] += src_point_per_voxel[i];
	}
}

/* Update centroids of voxels.
 * If a voxel contains new points, its centroid
 * is re-calculated using tmp_centroid.
 *
 * Input:
 * 		@voxel_id: the indices of the source voxel in the destination grid
 * 		@src_voxel_num: the number of source voxels
 * 		@dst_tmp_centroid: the destination tmp_centroid, compute from the previous
 * 							step (updateTmpCAndC).
 * 		@dst_points_per_voxel: the number of points per destination voxel, used to
 * 								compute the new centroids.
 *		@dst_voxel_num: number of destination voxels
 * Output:
 * 		@dst_centroid: centroids of the destination voxels
 *  */
__global__ void updateVoxelCentroid(int *voxel_id, int src_voxel_num,
									double *dst_tmp_centroid, int *dst_points_per_voxel,
									int dst_voxel_num, double* dst_centroid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int dst_id = voxel_id[i];
		MatrixDD centr(3, 1, dst_voxel_num, dst_centroid + dst_id);
		MatrixDD tmp_centr(3, 1, dst_voxel_num, dst_tmp_centroid + dst_id);
		double point_num = static_cast<double>(dst_points_per_voxel[dst_id]);

		centr(0) = tmp_centr(0) / point_num;
		centr(1) = tmp_centr(1) / point_num;
		centr(2) = tmp_centr(2) / point_num;
	}
}

/* Update covariance of destination voxels from non-empty source voxels
 *
 * Input:
 * 		@voxel_id				: the indices of source voxels in the destination grid
 * 		@src_points_per_voxel	: number of points per source voxel
 * 		@src_voxel_num			: number of source voxel
 * 		@dst_centroid			: centroids of the destination voxels
 * 		@dst_tmp_centroid		: tmp centroids of the destination voxels
 * 		@dst_tmp_cov			: tmp covariance of the destination voxels
 * 		@dst_points_per_voxel	: number of points per destination voxel
 * 		@dst_voxel_num			: number of destination voxel
 * 		@min_points_per_voxel	: the minimum number of points that a voxel
 * 									needs to have to be qualified to join
 * 									the computation. A voxel having less points
 * 									than this number is ignored in further
 * 									computation.
 *
 * Output:
 * 		@dst_covariance: covariance of the destination voxels
 *  */
__global__ void updateVoxelCovariance(int *voxel_id, int src_voxel_num,
										double* dst_centroid, double* dst_tmp_centroid,
										double *dst_tmp_cov, int* dst_points_per_voxel,
										int dst_voxel_num, int min_points_per_voxel,
										double* dst_covariance)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int vid = voxel_id[i];

		MatrixDD centr(3, 1, dst_voxel_num, dst_centroid + vid);
		MatrixDD cov(3, 3, dst_voxel_num, dst_covariance + vid);
		MatrixDD pts(3, 1, dst_voxel_num, dst_tmp_centroid + vid);
		MatrixDD tmp_cov(3, 3, dst_voxel_num, dst_tmp_cov + vid);
		double point_num = static_cast<double>(dst_points_per_voxel[vid]);

		double c0 = centr(0);
		double c1 = centr(1);
		double c2 = centr(2);
		double p0 = pts(0);
		double p1 = pts(1);
		double p2 = pts(2);

		//points_per_voxel[vid] = (point_num < min_points_per_voxel) ? 0 : point_num;

		if (point_num >= min_points_per_voxel) {
			double mult = (point_num - 1.0) / point_num;

			cov(0, 0) = ((tmp_cov(0, 0) - 2.0 * p0 * c0) / point_num + c0 * c0) * mult;
			cov(0, 1) = ((tmp_cov(0, 1) - 2.0 * p0 * c1) / point_num + c0 * c1) * mult;
			cov(0, 2) = ((tmp_cov(0, 2) - 2.0 * p0 * c2) / point_num + c0 * c2) * mult;
			cov(1, 0) = cov(0, 1);
			cov(1, 1) = ((tmp_cov(1, 1) - 2.0 * p1 * c1) / point_num + c1 * c1) * mult;
			cov(1, 2) = ((tmp_cov(1, 2) - 2.0 * p1 * c2) / point_num + c1 * c2) * mult;
			cov(2, 0) = cov(0, 2);
			cov(2, 1) = cov(1, 2);
			cov(2, 2) = ((tmp_cov(2, 2) - 2.0 * p2 * c2) / point_num + c2 * c2) * mult;
		}
	}
}

/* Normalize the covariance matrices of destination voxels that contain
 * new points to avoid overflow in later computations.
 *
 * Input:
 * 		@voxel_id: the 1D indices of non-empty source voxel in the destination grid.
 * 		@src_voxel_num: number of source voxels
 * 		@sv: the symmetric eigen solve object
 * 		@min_point_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 */
__global__ void normalize(int *voxel_id, int src_voxel_num,
							SymmetricEigensolver3x3 sv,
							int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		sv.normalizeInput(voxel_id[i]);
	}
}

/* Compute eigenvalues of covariance matrices of destination voxels that changed.
 * Eigenvalues are arranged in ascending order. (eigen(0) <= eigen(1) <= eigen(2).
 * Eigenvalues are stored inside sv.
 *
 * Input:
 * 		@voxel_id: the 1D indices of non-empty source voxel in the destination grid.
 * 		@src_voxel_num: number of source voxels
 * 		@sv: the symmetric eigen solve object
 * 		@points_per_voxel: number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 */
__global__ void computeEigenvalues(int *voxel_id, int src_voxel_num,
									SymmetricEigensolver3x3 sv, int *points_per_voxel,
									int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		// Only update voxels that contain new points
		if (points_per_voxel[id] >= min_points_per_voxel) {
			sv.computeEigenvalues(id);
		}
	}
}

/* First step to compute eigenvector 0 of covariance matrices
 * of changed destination voxels.
 *
 * Input:
 *		@voxel_id: the 1D indices of source voxel in the destination grid.
 * 		@src_voxel_num: number of source voxels
 * 		@sv: the symmetric eigen solve object
 * 		@points_per_voxel: number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 *
 */
__global__ void computeEvec00(int *voxel_id, int src_voxel_num,
								SymmetricEigensolver3x3 sv, int* points_per_voxel,
								int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel) {
			sv.computeEigenvector00(id);
		}
	}
}

/* Second step to compute eigenvector 0 of covariance matrices
 * of changed destination voxels.
 *
 * Input:
 * 		@voxel_id: indices of non-empty source voxel in the destination grid
 * 		@src_voxel_num: number of non-empty source voxels
 * 		@sv: the symmetric eigen solver class object
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 *
 * */
__global__ void computeEvec01(int *voxel_id, int src_voxel_num, SymmetricEigensolver3x3 sv,
								int* points_per_voxel, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel) {
			sv.computeEigenvector01(id);
		}
	}
}

/* First step to update eigenvector 1 of covariance matrices
 * of changed destination voxels.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@sv: the symmetric eigen solver class object
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 */
__global__ void computeEvec10(int *voxel_id, int src_voxel_num, SymmetricEigensolver3x3 sv,
								int* points_per_voxel, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector10(id);
	}
}

/* Second step to compute eigenvector 1 of covariance matrices
 * of changed destination voxels.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@sv: the symmetric eigen solver class object
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 */
__global__ void computeEvec11(int *voxel_id, int src_voxel_num, SymmetricEigensolver3x3 sv,
								int* points_per_voxel, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector11(id);
	}
}


/* Compute eigenvector 2 of covariance matrices of changed destination voxels.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@sv: the symmetric eigen solver class object
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.*/
__global__ void computeEvec2(int *voxel_id, int src_voxel_num, SymmetricEigensolver3x3 sv,
								int* points_per_voxel, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.computeEigenvector2(id);
	}
}


/* Final step to compute eigenvalues of matrices of changed destination voxels.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@sv: the symmetric eigen solver class object
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 */
__global__ void updateEval(int *voxel_id, int src_voxel_num, SymmetricEigensolver3x3 sv,
							int* points_per_voxel, int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel)
			sv.updateEigenvalues(id);
	}
}

/* Update eigenvalues in the case covariance matrix is nearly singular.
 * Only performed on changed destination matrix. Used to update the
 * voxel grid when new points are added.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@eigenvalues: the vector of 3x1 eigenvalue vectors
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@dst_voxel_num: the number of destination voxels
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 * Output:
 * 		@eigenvalues: the updated eigenvalue vectors
 *
 */
__global__ void updateEval2(int *voxel_id, int src_voxel_num, double* eigenvalues,
								int* points_per_voxel, int dst_voxel_num,
								int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int id = voxel_id[i];

		if (points_per_voxel[id] >= min_points_per_voxel) {
			MatrixDD eigen_val(3, 1, dst_voxel_num, eigenvalues + id);
			double ev0 = eigen_val(0);
			double ev1 = eigen_val(1);
			double ev2 = eigen_val(2);

			if (ev0 < 0 || ev1 < 0 || ev2 <= 0) {
				continue;
			}

			double min_cov_eigvalue = ev2 * 0.01;

			if (ev0 < min_cov_eigvalue) {
				ev0 = min_cov_eigvalue;

				if (ev1 < min_cov_eigvalue) {
					ev1 = min_cov_eigvalue;
				}
			}

			eigen_val(0) = ev0;
			eigen_val(1) = ev1;
			eigen_val(2) = ev2;
		}
	}
}

/* Compute the inverse eigenvectors of destination voxels that changed.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@dst_voxel_num: the number of destination voxels
 * 		@eigenvectors: the list of eigenvectors of the destination grid
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 * Output:
 * 		@output: the inverse eigenvectors of the destination grid
 */
__global__ void computeInverseEigenvectors(int *voxel_id, int src_voxel_num,
											int* points_per_voxel, int dst_voxel_num,
											double* eigenvectors, int min_points_per_voxel,
											double* output)
{
	int stride = blockDim.x * gridDim.x;
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int vid = voxel_id[i];

		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD ievec(3, 3, dst_voxel_num, output + vid);
			MatrixDD evec(3, 3, dst_voxel_num, eigenvectors + vid);

			evec.inverse(ievec);
		}
	}
}

/* Step 0: new_eigen_vecs = eigen_vecs * eigen_val
 * Only applied for destination voxels that contain new points.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@eigenvalues: the eigenvalue vector of the destination grid
 * 		@eigenvectors: the eigenvector values of the destination grid
 * 		@dst_voxel_num: the number of destination voxels
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 *
 * Output:
 * 		@eigenvectors: eigen_vector = eigen_vector * eigen_value
 */
__global__ void updateCovarianceS0(int *voxel_id, int src_voxel_num,
									int* points_per_voxel, double* eigenvalues,
									double* eigenvectors, int dst_voxel_num,
									int min_points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int vid = voxel_id[i];

		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD eigen_vectors(3, 3, dst_voxel_num, eigenvectors + vid);

			double eig_val0 = eigenvalues[vid];
			double eig_val1 = eigenvalues[vid + dst_voxel_num];
			double eig_val2 = eigenvalues[vid + 2 * dst_voxel_num];

			eigen_vectors(0, 0) *= eig_val0;
			eigen_vectors(1, 0) *= eig_val0;
			eigen_vectors(2, 0) *= eig_val0;

			eigen_vectors(0, 1) *= eig_val1;
			eigen_vectors(1, 1) *= eig_val1;
			eigen_vectors(2, 1) *= eig_val1;

			eigen_vectors(0, 2) *= eig_val2;
			eigen_vectors(1, 2) *= eig_val2;
			eigen_vectors(2, 2) *= eig_val2;
		}
	}
}

/* Step 1: cov = new eigen_vecs * eigen_vecs inverse
 * 			   = eigen_vecs * eigen_value * eigen_vecs.inverse
 * Only applied for destination voxels that contains new points.
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@inverse_covariance: contains the new eigenvectors from step 0
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@dst_voxel_num: the number of destination voxels
 * 		@eigenvectors: the eigenvector values of the destination grid
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 * 		@col: targeted column of the covariance matrices. We cannot compute
 * 				the whole covariance matrix at once because there is not enough
 * 				registers. Instead, we compute their columns one by one.
 *
 * Output:
 * 		@covariance: the buffers containing the output covariance matrices of
 * 						the destination grid.
 */

__global__ void updateCovarianceS1(int *voxel_id, int src_voxel_num,
									double* inverse_eigenvector, double* eigenvectors,
									int* points_per_voxel, int dst_voxel_num,
									int min_points_per_voxel, int col, double* covariance)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int vid = voxel_id[i];

		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD cov(3, 3, dst_voxel_num, covariance + vid);
			MatrixDD ievec(3, 3, dst_voxel_num, inverse_eigenvector + vid);
			MatrixDD evecs(3, 3, dst_voxel_num, eigenvectors + vid);	// new eigenvectors

			double tmp0 = ievec(0, col);
			double tmp1 = ievec(1, col);
			double tmp2 = ievec(2, col);

			cov(0, col) = evecs(0, 0) * tmp0 + evecs(0, 1) * tmp1 + evecs(0, 2) * tmp2;
			cov(1, col) = evecs(1, 0) * tmp0 + evecs(1, 1) * tmp1 + evecs(1, 2) * tmp2;
			cov(2, col) = evecs(2, 0) * tmp0 + evecs(2, 1) * tmp1 + evecs(2, 2) * tmp2;
		}
	}
}

/* Final step: compute inverse covariance
 *
 * Input:
 * 		@voxel_id: 1D indices of the non-empty voxels in the destination grid
 * 		@src_voxel_num: the number of non-empty source voxels
 * 		@covariance: contains the new eigenvectors from step 0
 * 		@points_per_voxel: the number of points per destination voxel
 * 		@dst_voxel_num: the number of destination voxels
 * 		@min_points_per_voxel: minimum number of points that a voxel must have
 * 								to be considered in computation. Voxels having
 * 								less points than this number are ignored.
 *
 * Output:
 * 		@inverse_covariance: the inverse covariance of changed destination matrices
 */
__global__ void computeInverseCovariance(int *voxel_id, int src_voxel_num, double* covariance,
											int* points_per_voxel, int dst_voxel_num,
											int min_points_per_voxel, double *inverse_covariance)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		int vid = voxel_id[i];

		if (points_per_voxel[vid] >= min_points_per_voxel) {
			MatrixDD cov(3, 3, dst_voxel_num, covariance + vid);
			MatrixDD icov(3, 3, dst_voxel_num, inverse_covariance + vid);

			cov.inverse(icov);
		}
	}
}


/* Add new points' measurement to the current voxel grid.
 * Update centroids, covariances, inverse covariances, tmp centroids, and
 * tmp cov of voxels that contains new points.
 *
 * Input:
 * 		@x, y, z: coordinates of new points
 * 		@point_num: number of new points
 * 		@points_per_voxel: number of new points per new voxel
 * 		@starting_point_ids: starting indices of new points in new voxels
 * 		@point_ids: indices of new points
 * 		@min_b_x, min_b_y, min_b_z: the lower voxel boundaries of the voxel grid
 * 									that fits the new points
 * 		@vgrid_x, vgrid_y, vgrid_z: the dimensions of the voxel grid that fits
 * 									the new points
 */
void VoxelGrid::updateCentroidAndCovariance(FCloudPtr input,
												device_vector<int> &src_voxel_id,
												device_vector<int> &points_per_voxel,
												device_vector<int> &starting_point_ids,
												device_vector<int> &point_ids,
												Vector3i min_b, Vector3i vgdim)
{
	int point_num = input.size();

	/**
	 * Source grid: the voxel gird that covers the new points
	 * Source voxels: the voxels that belong to the source grid
	 * Destination grid: the current voxel grid of the VoxelGrid class object
	 * Destination voxels: the voxels that belong to the destination grid
	 */
	int src_voxel_num = src_voxel_id.size();

	if (src_voxel_num <= 0) {
		return;
	}

	// Compute the 1D indices of source voxels in the destination grid
	device_vector<int> voxel_id(src_voxel_num, __allocator);

	// Compute the 1D index of the source voxels in  the destination grid
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeVoxelId,
					src_voxel_id.data(), src_voxel_num, min_b, vgdim,
					min_b_, vgdim_, voxel_id.data()));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateTmpCAndC,
					input, starting_point_ids.data(), point_ids.data(),
					voxel_id.data(), src_voxel_num, voxel_num_,
					tmp_centroid_.data(), tmp_cov_.data()));

	// Update number of points per voxel
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updatePointsPerVoxel,
					voxel_id.data(), src_voxel_num, points_per_voxel.data(),
					points_per_voxel_.data()));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateVoxelCentroid,
					voxel_id.data(), src_voxel_num, tmp_centroid_.data(),
					points_per_voxel_.data(), voxel_num_, centroid_.data()));

	device_vector<double> covariance(voxel_num_ * 9, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateVoxelCovariance,
					voxel_id.data(), src_voxel_num, centroid_.data(), tmp_centroid_.data(),
					tmp_cov_.data(), points_per_voxel_.data(), voxel_num_,
					min_points_per_voxel_, covariance.data()));

	device_vector<double> eigenvalues_dev(voxel_num_ * 3, __allocator);
	device_vector<double> eigenvectors_dev(voxel_num_ * 9, __allocator);

	HSymmetricEigensolver3x3 hsv(voxel_num_, __allocator);

	// TODO: use shared ptr instead of raw ptr
	hsv.setInputMatrices(covariance.data());
	hsv.setEigenvalues(eigenvalues_dev.data());
	hsv.setEigenvectors(eigenvectors_dev.data());

	SymmetricEigensolver3x3 sv(hsv);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, normalize,
					voxel_id.data(), src_voxel_num, sv, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEigenvalues,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEvec00,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEvec01,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEvec10,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEvec11,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeEvec2,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateEval,
					voxel_id.data(), src_voxel_num, sv, points_per_voxel_.data(),
					min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateEval2,
					voxel_id.data(), src_voxel_num, eigenvalues_dev.data(),
					points_per_voxel_.data(), voxel_num_, min_points_per_voxel_));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeInverseEigenvectors,
					voxel_id.data(), src_voxel_num, points_per_voxel_.data(),
					voxel_num_, eigenvectors_dev.data(), min_points_per_voxel_, icov_.data()));

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateCovarianceS0,
					voxel_id.data(), src_voxel_num, points_per_voxel_.data(),
					eigenvalues_dev.data(), eigenvectors_dev.data(),
		  	  	  	voxel_num_, min_points_per_voxel_));

	// Compute columns of the 3x3 covariance matrix one by one
	for (int i = 0; i < 3; i++) {
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, updateCovarianceS1,
						voxel_id.data(), src_voxel_num, icov_.data(), eigenvectors_dev.data(),
						points_per_voxel_.data(), voxel_num_, min_points_per_voxel_, i,
						covariance.data()));
	}

	// Finally, compute the inverse covariance
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(src_voxel_num, computeInverseCovariance,
					voxel_id.data(), src_voxel_num, covariance.data(), points_per_voxel_.data(),
					voxel_num_, min_points_per_voxel_, icov_.data()));
}

void VoxelGrid::testVoxel(int vid)
{
	// Print centroid
	MatrixHD mat31(3, 1, __allocator), mat33(3, 3, __allocator);

	mat31 = MatrixDD(3, 1, voxel_num_, centroid_.data() + vid);

	std::cout << std::endl << "GPU At voxel " << vid << std::endl;
	std::cout << "Number of points = " << points_per_voxel_[vid] << std::endl;
	std::cout << "Centroid = " << std::endl << mat31 << std::endl;

	mat33 = MatrixDD(3, 3, voxel_num_, icov_.data() + vid);

	std::cout << "InverseCovariance = " << std::endl << mat33 << std::endl;

	mat31 = MatrixDD(3, 1, voxel_num_, tmp_centroid_.data() + vid);

	std::cout << "TmpCentroid = " << std::endl << mat31 << std::endl;

	mat33 = MatrixDD(3, 3, voxel_num_, tmp_cov_.data() + vid);

	std::cout << "TmpCov = " << std::endl << mat33 << std::endl << std::endl;
}

}

}
