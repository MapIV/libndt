#include <math.h>
#include <limits>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/scan.h>
#include <thrust/fill.h>
#include <inttypes.h>

#include <vector>
#include <cmath>

#include <stdio.h>
#include <sys/time.h>

#include <ndt_gpu/SymmetricEigenSolver.h>
#include <ndt_gpu/debug.h>
#include <ndt_gpu/common.h>
#include <ndt_gpu/utilities.h>
#include <ndt_gpu/VoxelGrid2.h>


namespace gpu
{

namespace vgrid2
{

VoxelGrid2::VoxelGrid2(GMemoryAllocator *allocator) :
	Base(allocator),
	voxel_id_(__allocator),
	centroid_(__allocator),
	inverse_covariance_(__allocator),
	points_per_voxel_(__allocator),
	tmp_centroid_(__allocator),
	tmp_cov_(__allocator)
{
	voxel_num_ = 0;

	res_.x = res_.y = res_.z = 0;

	max_b_.x = max_b_.y = max_b_.z = std::numeric_limits<int>::min();
	min_b_.x = min_b_.y = min_b_.z = std::numeric_limits<int>::max();

	vgdim_.x = vgdim_.y = vgdim_.z = 0;

	min_points_per_voxel_ = 6;

	rmax_b_.x = rmax_b_.y = rmax_b_.z = std::numeric_limits<int>::min();
	rmin_b_.x = rmin_b_.y = rmin_b_.z = std::numeric_limits<int>::max();
}

VoxelGrid2::VoxelGrid2(const VoxelGrid2& other) :
	Base(other),
	voxel_id_(other.voxel_id_),
	centroid_(other.centroid_),
	inverse_covariance_(other.inverse_covariance_),
	points_per_voxel_(other.points_per_voxel_),
	tmp_centroid_(other.tmp_centroid_),
	tmp_cov_(other.tmp_cov_)
{
	voxel_num_ = other.voxel_num_;

	res_ = other.res_;

	max_b_ = other.max_b_;
	min_b_ = other.min_b_;
	vgdim_ = other.vgdim_;

	min_points_per_voxel_ = other.min_points_per_voxel_;

	rmax_b_ = other.rmax_b_;
	rmin_b_ = other.rmin_b_;
}

VoxelGrid2::VoxelGrid2(VoxelGrid2&& other) :
	Base(other),
	voxel_id_(std::move(other.voxel_id_)),
	centroid_(std::move(other.centroid_)),
	inverse_covariance_(std::move(other.inverse_covariance_)),
	points_per_voxel_(std::move(other.points_per_voxel_)),
	tmp_centroid_(std::move(other.tmp_centroid_)),
	tmp_cov_(std::move(other.tmp_cov_))
{
	voxel_num_ = other.voxel_num_;

	res_ = other.res_;

	max_b_ = other.max_b_;
	min_b_ = other.min_b_;
	vgdim_ = other.vgdim_;

	min_points_per_voxel_ = other.min_points_per_voxel_;

	rmax_b_ = other.rmax_b_;
	rmin_b_ = other.rmin_b_;
}

VoxelGrid2& VoxelGrid2::operator=(const VoxelGrid2& other)
{
	voxel_id_ = other.voxel_id_;

	centroid_ = other.centroid_;
	inverse_covariance_ = other.inverse_covariance_;
	points_per_voxel_ = other.points_per_voxel_;

	tmp_centroid_ = other.tmp_centroid_;
	tmp_cov_ = other.tmp_cov_;

	voxel_num_ = other.voxel_num_;

	res_ = other.res_;

	max_b_ = other.max_b_;
	min_b_ = other.min_b_;
	vgdim_ = other.vgdim_;

  	min_points_per_voxel_ = other.min_points_per_voxel_;

  	rmax_b_ = other.rmax_b_;
  	rmin_b_ = other.rmin_b_;

  	return *this;
}

VoxelGrid2& VoxelGrid2::operator=(VoxelGrid2&& other)
{
	voxel_id_ = std::move(other.voxel_id_);

	centroid_ = std::move(other.centroid_);
	inverse_covariance_ = std::move(other.inverse_covariance_);
	points_per_voxel_ = std::move(other.points_per_voxel_);

	tmp_centroid_ = std::move(other.tmp_centroid_);
	tmp_cov_ = std::move(other.tmp_cov_);

	voxel_num_ = other.voxel_num_;

	res_ = other.res_;

	max_b_ = other.max_b_;
	min_b_ = other.min_b_;

	vgdim_ = other.vgdim_;

	min_points_per_voxel_ = other.min_points_per_voxel_;

	rmax_b_ = other.rmax_b_;
	rmin_b_ = other.rmin_b_;

	return *this;
}


// Added for incremental ndt, used in over-allocation
__host__ __device__ int roundUp(int input, int factor)
{
	return (input < 0) ? -((-input) / factor) * factor : ((input + factor - 1) / factor) * factor;
}

__host__ __device__ int roundDown(int input, int factor)
{
	return (input < 0) ? -((-input + factor - 1) / factor) * factor : (input / factor) * factor;
}

/**
 * Compute the voxel index from point coordinates.
 *
 * Params:
 * 		@p[in]: coordinates of the point
 * 		@res[in]: resolution of the voxel grid
 * 		@min_b[in]: min boundaries of the voxel grid
 * 		@vgdim[in]: dimensions of the voxel grid (in number of voxels)
 *
 * Return: 1D index of the voxel that contains the point.
 *
 */
inline __device__ int voxelId(Vector3f p, Vector3f res, Vector3i min_b, Vector3i vgdim)
{
  int idx = static_cast<int>(floorf(p.x / res.x)) - min_b.x;
  int idy = static_cast<int>(floorf(p.y / res.y)) - min_b.y;
  int idz = static_cast<int>(floorf(p.z / res.z)) - min_b.z;

  return (idx + idy * vgdim.x + idz * vgdim.x * vgdim.y);
}

__global__ void markNonEmptyVoxels(FCloudPtr cloud, int point_num, Vector3f res,
									Vector3i min_b, Vector3i vgdim, int *mark)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		int vid = voxelId(cloud[i], res, min_b, vgdim);

		mark[vid] = 1;
	}
}

int VoxelGrid2::computeVoxelId(FCloudPtr cloud, Vector3i min_b, Vector3i vgdim,
								device_vector<int> &voxel_id)
{
	// Compute the number of non-empty voxels
	int total_voxel_num = vgdim.x * vgdim.y * vgdim.z;

	voxel_id.resize(total_voxel_num + 1);

	// Initially, all voxels are empty
	fill(voxel_id, 0);

	int point_num = cloud.size();

	if (point_num <= 0) {
		return 0;
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, markNonEmptyVoxels,
					cloud, point_num, res_, min_b, vgdim, voxel_id.data()));

	int non_empty_voxel_num = 0;

	ExclusiveScan(voxel_id, non_empty_voxel_num);

	return non_empty_voxel_num;
}


/**
 * Allocate buffers of the voxel grid.
 * Initialize them to 0.
 *
 * Input:
 * 		@voxel_num: number of non-empty voxels
 *
 */
void VoxelGrid2::allocBuffers(int voxel_num)
{
	centroid_.resize(voxel_num * 3);

	inverse_covariance_.resize(voxel_num, 3, 3);
	fill(inverse_covariance_, 0);

	points_per_voxel_.resize(voxel_num);
	fill(points_per_voxel_, 0);

	tmp_centroid_.resize(voxel_num, 3, 1);
	fill(tmp_centroid_, 0);

	tmp_cov_.resize(voxel_num, 3, 3);
	fill(tmp_cov_, 0);
}

/* Initialize the upper half of covariance matrices to identity.
 * Centroids are initialized to zero by cudaMemset so no need to
 * make a kernel to initialize them.
 *
 * Input:
 * 		@voxel_num: number of voxels in the grid
 *
 * Output:
 * 		@tmp_covariance: the tmp_covariance of voxels
 */
__global__ void initTmpCovariance(double *tmp_covariances, int voxel_num)
{
	  int index = threadIdx.x + blockIdx.x * blockDim.x;
	  int stride = blockDim.x * gridDim.x;

	  for (int i = index; i < voxel_num; i += stride) {
		  MatrixDD tmp_cov(3, 3, voxel_num, tmp_covariances + i);

		  tmp_cov(0, 0) = tmp_cov(1, 1) = tmp_cov(2, 2) = 1.0;

		  tmp_cov(0, 1) = tmp_cov(0, 2) = tmp_cov(1, 2) = 0.0;
	  }
}


/* Compute the tmp centroids and tmp covariances of voxels.
 * If the updated_vid is a nullptr, all voxels in the grid
 * are updated. Otherwise, only voxels specified by updated_vid
 * are updated.
 *
 * Params:
 * 		@cloud[in]: the coordinates of the input points
 * 		@starting_point_ids[in]: the starting indices of the points that belong
 * 									to each voxel to be updated.
 * 		@point_ids[in]: the sorted indices of points to match the order of voxels.
 * 		@voxel_num[in]: number of voxels to be updated
 * 		@tmp_centrX[out]: the tmp_centroids of updated voxels
 * 		@tmp_covX[out]: the tmp_covariances of updated voxels
 */
#ifndef FULL_MASK
#define FULL_MASK	(0xffffffff)
#endif

__global__ void computeTmpCentroidAndCovariance(FCloudPtr cloud, int* starting_point_ids,
												int* point_ids, int voxel_num,
												double *tmp_centr0, double *tmp_centr1, double *tmp_centr2,
												double *tmp_cov00, double *tmp_cov01, double *tmp_cov02,
												double *tmp_cov11, double *tmp_cov12, double *tmp_cov22,
												int *updated_vid = nullptr)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;
	const int warp_size = 32;
	int lane_id = threadIdx.x % warp_size;

	for (int i = idx; i < voxel_num * warp_size; i += stride) {
		int vid = i / warp_size;
		// The loop range to access the points belonging to this voxel
		int start = starting_point_ids[vid], end = starting_point_ids[vid + 1];
		/***
		 * True index of the voxels to be updated. If updated_vid
		 * is null, all non-empty voxels in the grid are updated.
		 * Otherwise, only voxels indicated by updated_vid are updated.
		 */
		int dst_vid = (updated_vid == nullptr) ? vid : updated_vid[vid];

		double centr0, centr1, centr2;
		double cov00, cov01, cov02, cov11, cov12, cov22;

		centr0 = centr1 = centr2 = 0.0;
		cov00 = cov11 = cov22 = 0.0;
		cov01 = cov02 = cov12 = 0.0;

		for (int j = start + lane_id; j < end; j += warp_size) {
			int pid = point_ids[j];
			auto t = static_cast<Vector3d>(cloud[pid]);

			centr0 += t.x;
			centr1 += t.y;
			centr2 += t.z;

			cov00 += t.x * t.x;
			cov01 += t.x * t.y;
			cov02 += t.x * t.z;
			cov11 += t.y * t.y;
			cov12 += t.y * t.z;
			cov22 += t.z * t.z;
		}

		for (int active_threads = warp_size >> 1; active_threads > 0; active_threads >>= 1) {
			centr0 += __shfl_xor_sync(FULL_MASK, centr0, active_threads, warp_size);
			centr1 += __shfl_xor_sync(FULL_MASK, centr1, active_threads, warp_size);
			centr2 += __shfl_xor_sync(FULL_MASK, centr2, active_threads, warp_size);

			cov00 += __shfl_xor_sync(FULL_MASK, cov00, active_threads, warp_size);
			cov01 += __shfl_xor_sync(FULL_MASK, cov01, active_threads, warp_size);
			cov02 += __shfl_xor_sync(FULL_MASK, cov02, active_threads, warp_size);
			cov11 += __shfl_xor_sync(FULL_MASK, cov11, active_threads, warp_size);
			cov12 += __shfl_xor_sync(FULL_MASK, cov12, active_threads, warp_size);
			cov22 += __shfl_xor_sync(FULL_MASK, cov22, active_threads, warp_size);

		}

		if (lane_id == 0) {
			tmp_centr0[dst_vid] += centr0;
			tmp_centr1[dst_vid] += centr1;
			tmp_centr2[dst_vid] += centr2;

			tmp_cov00[dst_vid] += cov00;
			tmp_cov01[dst_vid] += cov01;
			tmp_cov02[dst_vid] += cov02;
			tmp_cov11[dst_vid] += cov11;
			tmp_cov12[dst_vid] += cov12;
			tmp_cov22[dst_vid] += cov22;
		}
	}
}

/* Compute centroids of voxels from tmp_centroid.
 * centroid = tmp_centroid / point_num
 *
 * Input:
 * 		@tmp_centroid: tmp_centroids of voxels
 * 		@points_per_voxel: number of points per voxel
 * 		@voxel_num: number of voxels
 * Output:
 * 		@centroid: centroids of voxels*/
__global__ void computeVoxelCentroid(double* tmp_centroid, int* points_per_voxel,
										int voxel_num, double *centroid, int all_voxel_num,
										int *updated_vid = nullptr)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		int vid = (updated_vid == nullptr) ? i : updated_vid[i];
		MatrixDD centr(3, 1, all_voxel_num, centroid + vid);
		MatrixDD tmp_centr(3, 1, all_voxel_num, tmp_centroid + vid);
		int tmp_pnum = points_per_voxel[vid];

		/* If the voxel is not empty, then its centroid = tmp_centroid / point_num.
		 * Otherwise, its centroid is a copy of tmp_centroid
		 * (i.e. centroid = tmp_centroid / 1)
		 */
		double point_num = (tmp_pnum > 0) ? static_cast<double>(tmp_pnum) : 1;

		centr(0) = tmp_centr(0) / point_num;
		centr(1) = tmp_centr(1) / point_num;
		centr(2) = tmp_centr(2) / point_num;
	}
}

/* Compute voxels' covariances.
 * Only voxels that have more than min_points_per_voxel
 * are considered. For those that do not fulfill that condition,
 * their covariance matrices are ignored .
 *
 * Params:
 * 		@centroid[in]: all voxels' centroids
 * 		@tmp_centroid[in]: all voxels' tmp centroids
 * 		@tmp_covariance[in]: all voxels' tmp covariances
 * 		@points_per_voxel[in]: number of points belong to each voxel
 * 		@voxel_num[in]: number of voxels to be updated
 * 		@all_voxel_num[in]: number of all voxels in the grid
 * 		@updated_vid[in]: indices of voxels to be updated
 * 		@covariance[out]: covariances of voxels to be updated
 */
__global__ void computeVoxelCovariance(double* centroid, double* tmp_centroid, double* tmp_covariance,
                                        int* points_per_voxel, int voxel_num, int all_voxel_num,
                                        double *covariance, int *updated_vid = nullptr)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int out_vid = index; out_vid < voxel_num; out_vid += stride) {
		int in_vid = (updated_vid == nullptr) ? out_vid : updated_vid[out_vid];
		MatrixDD tcov(3, 3, all_voxel_num, tmp_covariance + in_vid);	// In matrix
		double point_num = static_cast<double>(points_per_voxel[in_vid]);
		MatrixDD cov(3, 3, voxel_num, covariance + out_vid);	// Out matrix

		double c0 = centroid[in_vid];
		double c1 = centroid[all_voxel_num + in_vid];
		double c2 = centroid[all_voxel_num * 2 + in_vid];
		double p0 = tmp_centroid[in_vid];
		double p1 = tmp_centroid[all_voxel_num + in_vid];
		double p2 = tmp_centroid[all_voxel_num * 2 + in_vid];

		double mult = (point_num - 1.0) / point_num;

		cov(0, 0) = ((tcov(0, 0) - 2.0 * p0 * c0) / point_num + c0 * c0) * mult;
		cov(0, 1) = ((tcov(0, 1) - 2.0 * p0 * c1) / point_num + c0 * c1) * mult;
		cov(0, 2) = ((tcov(0, 2) - 2.0 * p0 * c2) / point_num + c0 * c2) * mult;
		cov(1, 1) = ((tcov(1, 1) - 2.0 * p1 * c1) / point_num + c1 * c1) * mult;
		cov(1, 2) = ((tcov(1, 2) - 2.0 * p1 * c2) / point_num + c1 * c2) * mult;
		cov(2, 2) = ((tcov(2, 2) - 2.0 * p2 * c2) / point_num + c2 * c2) * mult;

		// The covariances are symmetric, so no need to compute the lower triangle
		cov(1, 0) = cov(0, 1);
		cov(2, 0) = cov(0, 2);
		cov(2, 1) = cov(1, 2);
	}
}


/* Compute inverse eigenvectors of voxels.
 * To save memory, we leverage the class's inverse_covariances_
 * to stores the inverse eigenvectors.
 *
 * Params:
 * 		@eigenvectors[in]: eigenvectors computed from previous steps
 * 		@voxel_num[in]: number of voxels involved to the computation
 * 		@inverse_evecs[out]: output inverse eigenvectors
 * 		@all_voxel_num[in]: number of non-empty voxels in the grid
 * 		@updated_vid[in]: specify where the inverse eigenvectors located
 */
__global__ void computeInverseEigenvectors(double* eigenvectors, double* inverse_evecs,
											int voxel_num, int all_voxel_num,
											int *updated_vid = nullptr)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		int out_vid = (updated_vid == nullptr) ? i : updated_vid[i];
		MatrixDD evec(3, 3, voxel_num, eigenvectors + i);	// Input matrix
		MatrixDD ievec(3, 3, all_voxel_num, inverse_evecs + out_vid);	// Output matrix

		evec.inverse(ievec);
	}
}

/**
 * First step of updating covariance matrices:
 * Compute the product eigen_vecs = eigen_vecs * eigen_val.
 *
 * Params:
 * 		@eigenvectors[in/out]: eigenvectors of the covariance matrices of voxels.
 * 								Also stores the product eigen_vecs * eigen_val to
 * 								save the GPU memory.
 * 		@eigenvalues[in]: eigenvalues of the covariance matrices of voxels
 * 		@voxel_num: number of voxels involved to the computation
 *
 */
__global__ void updateCovarianceS0(double* eigenvectors, double* eigenvalues, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		MatrixDD eigen_vectors(3, 3, voxel_num, eigenvectors + i);

		double eig_val0 = eigenvalues[i];
		double eig_val1 = eigenvalues[i + voxel_num];
		double eig_val2 = eigenvalues[i + 2 * voxel_num];

		eigen_vectors(0, 0) *= eig_val0;
		eigen_vectors(1, 0) *= eig_val0;
		eigen_vectors(2, 0) *= eig_val0;

		eigen_vectors(0, 1) *= eig_val1;
		eigen_vectors(1, 1) *= eig_val1;
		eigen_vectors(2, 1) *= eig_val1;

		eigen_vectors(0, 2) *= eig_val2;
		eigen_vectors(1, 2) *= eig_val2;
		eigen_vectors(2, 2) *= eig_val2;
	}
}

/**
 * Second step of updating covariance matrices.
 * Compute covariance cov = new eigen_vecs * inverse eigen_vecs
 *
 * Input:
 * 		@new_eigenvectors: the product eigen_vecs * eigen_val of all voxels
 * 		@inverse_eigenvectors: the inverse eigenvectors of all voxels
 * 		@point_per_voxel: the number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 * 		@col: the column index to be computed
 *
 * Output:
 * 		@covariance: the output covariance matrices
 */
__global__ void updateCovarianceS1(double* new_eigenvectors, double* inverse_eigenvectors,
									int voxel_num, int all_voxel_num, int col, double* covariance,
									int *updated_vid = nullptr)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		int inverse_vid = (updated_vid == nullptr) ? vid : updated_vid[vid];
		MatrixDD cov(3, 3, voxel_num, covariance + vid);	// Output
		MatrixDD ievec(3, 3, all_voxel_num, inverse_eigenvectors + inverse_vid);	// inverse eigenvectors
		MatrixDD nevec(3, 3, voxel_num, new_eigenvectors + vid);	// The product eigenvectors * eigenvalues

		double tmp0 = ievec(0, col);
		double tmp1 = ievec(1, col);
		double tmp2 = ievec(2, col);

		cov(0, col) = nevec(0, 0) * tmp0 + nevec(0, 1) * tmp1 + nevec(0, 2) * tmp2;
		cov(1, col) = nevec(1, 0) * tmp0 + nevec(1, 1) * tmp1 + nevec(1, 2) * tmp2;
		cov(2, col) = nevec(2, 0) * tmp0 + nevec(2, 1) * tmp1 + nevec(2, 2) * tmp2;
	}
}

/* Compupte the inverse covariance matrices.
 *
 * Params:
 * 		@covariance[in]: the input new covariance matrices
 * 		@inverse_covariance[out]: the output inverse covariance matrices
 * 		@voxel_num[in]: number of voxels involved to the computation
 * 		@all_voxel_num[in]: number of all voxels in the voxel grid
 * 		@update_vid[in]: indices of voxels involved to the computation
 */
__global__ void computeInverseCovariance(double* covariance, double* inverse_covariance,
                                           int voxel_num, int all_voxel_num, int *updated_vid = nullptr)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int vid = index; vid < voxel_num; vid += stride) {
		int out_vid = (updated_vid == nullptr) ? vid : updated_vid[vid];
		MatrixDD cov(3, 3, voxel_num, covariance + vid);
		MatrixDD icov(3, 3, all_voxel_num, inverse_covariance + out_vid);

		cov.inverse(icov);
	}
}

/* Normalize input matrices to avoid overflow.
 *
 * Params:
 * 		@sv[in/out]: object of class SymmetricEigensolve3x3
 * 		@voxel_num[in]: number of voxels
 */
__global__ void normalize(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		sv.normalizeInput(i);
	}
}

/* Compute eigenvalues. Eigenvalues are arranged in increasing order.
 * (eigen(0) <= eigen(1) <= eigen(2).
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEigenvalues(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		sv.computeEigenvalues(i);
	}
}

/* Compute the eigenvector 0 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec0(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		sv.computeEigenvector00(i);
		sv.computeEigenvector01(i);
	}
}
/* Compute the eigenvector 1 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec1(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int id = index; id < voxel_num; id += stride) {
		sv.computeEigenvector10(id);
		sv.computeEigenvector11(id);
	}
}


/* Compute the eigenvector 2 of covariance matrices of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void computeEvec2(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		sv.computeEigenvector2(i);
	}
}

/* Final step to compute eigenvalues of voxels.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void updateEval(SymmetricEigensolver3x3 sv, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		sv.updateEigenvalues(i);
	}
}

/* Update eigenvalues in the case covariance matrix is nearly singular.
 *
 * Input:
 * 		@sv: object of class SymmetricEigensolve3x3
 * 		@points_per_voxel: number of points belonging to each voxel
 * 		@voxel_num: number of voxels
 * 		@min_points_per_voxel: the minimum number of points that a
 * 								voxel must have to be considered in
 * 								the process of computing measurement values.
 * 								Shortly, if a voxel has less points than
 * 								this number, it is totally ignored.
 */
__global__ void updateEval2(double* eigenvalues, int voxel_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		MatrixDD eigen_val(3, 1, voxel_num, eigenvalues + i);
		double ev0 = eigen_val(0);
		double ev1 = eigen_val(1);
		double ev2 = eigen_val(2);

		if (ev0 < 0 || ev1 < 0 || ev2 <= 0) {
			continue;
		}

		double min_cov_eigvalue = ev2 * 0.01;

		if (ev0 < min_cov_eigvalue) {
			ev0 = min_cov_eigvalue;

			if (ev1 < min_cov_eigvalue) {
				ev1 = min_cov_eigvalue;
			}
		}

		eigen_val(0) = ev0;
		eigen_val(1) = ev1;
		eigen_val(2) = ev2;
	}
}

/**
 * Examine the input voxels and mark the voxels that have
 * at least min_points_per_voxel.
 *
 * TODO: the @input_vid must be the real indices of voxels,
 * not their 1D indices. This must be confirmed later.
 *
 * Params:
 * 		@input_vid[in]: real indices of the input voxels
 * 		@points_per_voxel[in]: the VoxelGrid's points_per_voxel,
 * 						contains number of points per each non-empty
 * 						voxel
 * 		@voxel_num[in]: size of @input_vid
 * 		@min_points_per_voxel[in]: minimum number of points for
 * 						a voxel to be considered valid
 * 		@mark[out]: mark[i] = 0 if the corresponding voxel has
 * 						less than @min_points_per_voxel and 1 otherwise
 */
__global__ void markValidVoxels(int *input_vid, int *points_per_voxel, int voxel_num,
								int min_points_per_voxel, int *mark)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		int vid = (input_vid == nullptr) ? i : input_vid[i];

		mark[i] = (points_per_voxel[vid] < min_points_per_voxel) ? 0 : 1;
	}
}

/**
 * Collect valid entries (marked above).
 *
 * Params:
 * 		@input_vid[in]: the real indices of input voxels
 * 		@wlocation[in]: indicates where to write the valid entries to
 * 		@voxel_num[in]: length ofo @input_vid
 * 		@valid_vid[out]: valid entries
 */
__global__ void collectValidVoxels(int *input_vid, int *wlocation, int voxel_num, int *valid_vid)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < voxel_num; i += stride) {
		int vid = (input_vid == nullptr) ? i : input_vid[i];
		int wloc = wlocation[i];

		if (wloc < wlocation[i + 1]) {
			valid_vid[wloc] = vid;
		}
	}
}

/**
 * Only voxels that contains more than min_points_per_voxel_
 * are involved to the NDT process. Hence, this function
 * examines the input voxels, and extracts voxels that have
 * more than min_points_per_voxel_.
 */
void VoxelGrid2::extractValidVoxels(device_vector<int> &input_vid, device_vector<int> &valid_vid)
{
	// Mark valid voxels
	int voxel_num = (input_vid.size() <= 0) ? voxel_num_ : input_vid.size();

	device_vector<int> mark(voxel_num + 1, __allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, markValidVoxels,
					input_vid.data(), points_per_voxel_.data(), voxel_num,
					min_points_per_voxel_, mark.data()));

	// Exclusive scan to comput output writing idx
	int valid_vid_num = 0;

	ExclusiveScan(mark, valid_vid_num);

	// Reserve space for the output
	valid_vid.resize(valid_vid_num);

	if (valid_vid_num <= 0) {
		return;
	}

	// Collect the valid voxels
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, collectValidVoxels,
					input_vid.data(), mark.data(), voxel_num, valid_vid.data()));
}


/* Compute PDF measurements of a voxel grid given an input point cloud.
 * The input are point coordinates and
 *
 * Input:
 * 		@x, y, z: the coordinates of the input point clouds
 * 		@starting_point_ids: the starting indices of list of points
 * 								belong to each voxel.
 * 		@point_ids: the indices of input points, sorted to matched
 * 					the order of voxels they blong to.
 */
void VoxelGrid2::computeCentroidAndCovariance(FCloudPtr cloud, device_vector<int> &starting_point_ids,
												device_vector<int> &point_ids)
{
	if (voxel_num_ <= 0) {
		return;
	}

	/* Initialize tmp covariance to identity matrices.
	 * No need to initialize tmp centroid by this way because
	 * it is already initialized to all zeros by cudaMemset when
	 * being allocated in allocBuffers().
	 */
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num_, initTmpCovariance,
					tmp_cov_.data(), voxel_num_));

	// Update tmp centroids and tmp covariances using new points
	// Just to tell initialize() to initialize all voxels in the grid
	device_vector<int> dummy_updated_vid(__allocator);

	initialize(cloud, starting_point_ids, point_ids, dummy_updated_vid);

	// Compute centroids
	updateCentroids(dummy_updated_vid);

	// Extract indices of voxels which have at least min_points_per_voxel_ points
	device_vector<int> updated_vid(__allocator);	// Indices of voxels whose covariances are going to be updated
	device_vector<int> dummy_input_vid(__allocator);	// Just an empty input_vid

	extractValidVoxels(dummy_input_vid, updated_vid);

	// Compute the inverse covariances of all valid voxels
	updateInverseCovariances(updated_vid);
}

void VoxelGrid2::updateInverseCovariances(device_vector<int> &updated_vid)
{
	// The number of voxels to be updated
	/***
	 * If the updated_vid is not empty, voxels specified by updated_vid
	 * are updated. Otherwise, all voxels in the grid are updated.
	 */
	int voxel_num = (updated_vid.size() > 0) ? updated_vid.size() : voxel_num_;

	if (voxel_num <= 0) {
		return;
	}

	matrix_vector<double> covariance(voxel_num, 3, 3, __allocator);

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeVoxelCovariance,
					centroid_.data(), tmp_centroid_.data(), tmp_cov_.data(),
	                points_per_voxel_.data(), voxel_num, voxel_num_,
	                covariance.data(), updated_vid.data()));

	matrix_vector<double> eigenvalues_dev(voxel_num, 3, 1, __allocator);
	matrix_vector<double> eigenvectors_dev(voxel_num, 3, 3, __allocator, true);	// Testing, zero-out

	/* Deal with singular covariance matrices. This is the most time-consuming
	 * process. Because there are not enough registers to launch a big kernel
	 * that do everything in one single launch, I have to break it into multiple
	 * smaller kernels. The cost is we have to access global memory multiple times,
	 * which is super slow.
	 */

	// Solving eigenvalues and eigenvectors problem by the GPU.
	HSymmetricEigensolver3x3 hsv(voxel_num, __allocator);

	hsv.setInputMatrices(covariance.data());
	hsv.setEigenvalues(eigenvalues_dev.data());
	hsv.setEigenvectors(eigenvectors_dev.data());

	SymmetricEigensolver3x3 sv(hsv);

	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, normalize, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeEigenvalues, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeEvec0, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X2>(voxel_num, computeEvec1, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeEvec2, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, updateEval, sv, voxel_num));
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, updateEval2,
					eigenvalues_dev.data(), voxel_num));

	/* Compute inverse eigenvectors. To save GPU memory, use inverse_covariance_
	 * to store the output of this step (inverse eigenvectors of voxels).
	 */
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num, computeInverseEigenvectors,
					eigenvectors_dev.data(), inverse_covariance_.data(), voxel_num,
					voxel_num_, updated_vid.data()));

	// Compute product eigenvectors * eigenvalues
	checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num_, updateCovarianceS0,
					eigenvectors_dev.data(), eigenvalues_dev.data(), voxel_num));

	// Compute the new covariance = (eigenvectors * eigenvalues) * inverse eigenvectors
	for (int i = 0; i < 3; ++i) {
		checkCudaErrors(launchASync<BLOCK_SIZE_X>(voxel_num_, updateCovarianceS1,
						eigenvectors_dev.data(), inverse_covariance_.data(), voxel_num,
						voxel_num_,  i, covariance.data(), updated_vid.data()));
	}

	// Final step, compute inverse covariance
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num_, computeInverseCovariance,
					covariance.data(), inverse_covariance_.data(),
		  	  	  	voxel_num, voxel_num_, updated_vid.data()));
}

/* Set input point cloud and compute PDF measurements in each voxel.
 * Input are supposed to be in device memory.
 * Basically 3 steps: compute boundary values, re-arrange indices of
 * points, and compute centroid and covariance values of voxels.
 *
 * Params:
 * 		@cloud[in]: coordinates of the input points
 *
 */
//#define UPDATE

template <typename PointType>
void VoxelGrid2::setInputCloud(typename PointCloud<PointType>::Ptr &cloud)
{
	// null pointers should be ignored
	if (!cloud) {
		return;
	}

	// Pointers to the cloud
	FCloudPtr cptr = cloud->data();

	findBoundaries(cptr, rmin_b_, rmax_b_, min_b_, max_b_, vgdim_);

	voxel_num_ = computeVoxelId(cptr, min_b_, vgdim_, voxel_id_);

	allocBuffers(voxel_num_);

	// Sort the input points to match the voxels' order
	device_vector<int> starting_point_ids(__allocator), point_ids(__allocator);

	scatterPointsToVoxelGrid(cptr, min_b_, vgdim_, voxel_num_, voxel_id_,
								points_per_voxel_, starting_point_ids, point_ids);

	computeCentroidAndCovariance(cptr, starting_point_ids, point_ids);
}

template <typename PointType>
void VoxelGrid2::radiusSearch(typename PointCloud<PointType>::Ptr &qcloud,
								float radius, int max_nn,
								device_vector<int> &starting_voxel_id,
								device_vector<int> &nn_voxel_id)
{
	// Null pointers are ignored
	if (!qcloud) {
		return;
	}

	radiusSearch(qcloud->data(), radius, max_nn, starting_voxel_id, nn_voxel_id);
}

void VoxelGrid2::radiusSearch(FCloudPtr qcloud, float radius, int max_nn,
								device_vector<int> &starting_voxel_id,
								device_vector<int> &nn_voxel_id)
{
	if (qcloud.size() <= BATCH_SIZE_) {
		radiusSearchSmall(qcloud, radius, max_nn, starting_voxel_id, nn_voxel_id);
	} else {
		radiusSearchLarge(qcloud, radius, max_nn, starting_voxel_id, nn_voxel_id);
	}
}

void VoxelGrid2::radiusSearchLarge(FCloudPtr qcloud, float radius, int max_nn,
									device_vector<int> &starting_voxel_id,
									device_vector<int> &nn_voxel_id)
{
	starting_voxel_id.clear();
	nn_voxel_id.clear();

	int point_num = qcloud.size();

	if (point_num <= 0 || voxel_num_ <= 0) {
		return;
	}

	/**
	 * If the query cloud has more than 1,000,000 points,
	 * search for batch of 100,000 points.
	 */
	int batch_size = BATCH_SIZE_;
	std::vector<std::shared_ptr<device_vector<int>>> starting_nn_voxel_list;
	std::vector<std::shared_ptr<device_vector<int>>> nn_voxel_list;
	int acc_nn_num = 0;

	// Run radiusSearchSmall on each chunk of sub-cloud of the input cloud.
	for (int pos = 0; pos < point_num; pos += batch_size) {
		std::shared_ptr<device_vector<int>> sub_starting_voxel_id(new device_vector<int>(__allocator));
		std::shared_ptr<device_vector<int>> sub_nn_voxel_id(new device_vector<int>(__allocator));

		radiusSearchSmall(qcloud.subCloud(pos, batch_size), radius, max_nn,
							*sub_starting_voxel_id, *sub_nn_voxel_id);

		add(*sub_starting_voxel_id, acc_nn_num);
		starting_nn_voxel_list.push_back(sub_starting_voxel_id);
		nn_voxel_list.push_back(sub_nn_voxel_id);

		acc_nn_num += sub_nn_voxel_id->size();
	}

	// Fuse all results
	starting_voxel_id.resize(point_num + 1);
	nn_voxel_id.resize(acc_nn_num);

	for (size_t i = 0, snn_pos = 0, nn_pos = 0; i < starting_nn_voxel_list.size(); ++i) {
		// Copy all of the last vector
		int snn_cp_size = (i != starting_nn_voxel_list.size() - 1) ?
							starting_nn_voxel_list[i]->size() - 1 :
							starting_nn_voxel_list[i]->size();
		int nn_cp_size = nn_voxel_list[i]->size();

		gcopyDtoD(starting_voxel_id.data() + snn_pos, starting_nn_voxel_list[i]->data(),
					snn_cp_size * sizeof(int));
		gcopyDtoD(nn_voxel_id.data() + nn_pos, nn_voxel_list[i]->data(),
					nn_cp_size * sizeof(int));

		snn_pos += snn_cp_size;
		nn_pos += nn_cp_size;
	}
}

/* Find the indices range (min_bound, max_bound) of voxels
 * which are close to a coordinate value.
 *
 * Input:
 * 		@coordinate: the input coordinate value
 * 		@radius: the range to determine the vicinity
 * 		@resolution: size of a voxel dimension
 * 		@min_bound, max_bound: the lowest and highest boundary values
 * Output:
 * 		@min_vid, max_vid: the lower and upper boundaries of voxels
 * 							in the coordinate's vicinity
 */
__device__ void findVoxelBoundary(float coordinate, float radius, float resolution,
									int min_bound, int max_bound,
									int &min_vid, int &max_vid)
{
	max_vid = __float2int_rd((coordinate + radius) / resolution);
	min_vid = __float2int_rd((coordinate - radius) / resolution);

	max_vid = (max_vid > max_bound) ? max_bound : max_vid;
	min_vid = (min_vid < min_bound) ? min_bound : min_vid;
}

__global__ void findCandidateVoxels(FCloudPtr qcloud, int cvoxel_num, int cvoxel_per_point,
									Vector3i bb_dim, float radius, Vector3f res,
									Vector3i rmax_b, Vector3i rmin_b, Vector3i max_b,
									Vector3i min_b, Vector3i vgdim, int *voxel_id,
									int *points_per_voxel, int voxel_num, int min_points_per_voxel,
									double *centroid, int *candidate_voxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < cvoxel_num; i += stride) {
		int pid = i / cvoxel_per_point;
		/**
		 * local_id is the offset of the voxel that the thread
		 * handles among all candidates of one point.
		 */
		int local_id = i % cvoxel_per_point;
		auto q = qcloud[pid];

		Vector3i max_id, min_id;

		findVoxelBoundary(q.x, radius, res.x, rmin_b.x, rmax_b.x, min_id.x, max_id.x);
		findVoxelBoundary(q.y, radius, res.y, rmin_b.y, rmax_b.y, min_id.y, max_id.y);
		findVoxelBoundary(q.z, radius, res.z, rmin_b.z, rmax_b.z, min_id.z, max_id.z);

		auto vid3 = min_id + id1ToId3(local_id, bb_dim);

		if (vid3.x <= max_id.x && vid3.y <= max_id.y && vid3.z <= max_id.z) {
			int vid = id3ToId1(vid3, min_b, vgdim);
			int real_vid = voxel_id[vid];

			if (real_vid < voxel_id[vid + 1] && points_per_voxel[real_vid] >= min_points_per_voxel) {
				MatrixDD centr(3, 1, voxel_num, centroid + real_vid);
	    		double dx = q.x - centr(0);
	    		double dy = q.y - centr(1);
	    		double dz = q.z - centr(2);

	    		if (sqrt(dx * dx + dy * dy + dz * dz) <= radius) {
	    			candidate_voxel_id[i] = real_vid;
	    		}
			}
		}
	}
}

struct isValid {
	inline __device__ bool operator()(int val) {
		return (val >= 0);
	}
};

__global__ void computeStartingVid(int *psum, int point_num, int cvoxel_per_point,
									int *starting_voxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	// The last element of starting_voxel_id is the total number of neighbor voxels
	for (int i = index; i <= point_num; i += stride) {
		starting_voxel_id[i] = psum[i * cvoxel_per_point];
	}
}

void VoxelGrid2::radiusSearchSmall(FCloudPtr qcloud, float radius, int max_nn,
									device_vector<int> &starting_voxel_id,
									device_vector<int> &nn_voxel_id)
{
	// Reset output
	starting_voxel_id.clear();
	nn_voxel_id.clear();

	int point_num = qcloud.size();

	if (point_num <= 0 || voxel_num_ <= 0) {
		return;
	}

	// Dimensions of the bounding box that cover
	Vector3i bb_dim;

	bb_dim.x = static_cast<int>(floor((radius + radius) / res_.x)) + 1;
	bb_dim.y = static_cast<int>(floor((radius + radius) / res_.y)) + 1;
	bb_dim.z = static_cast<int>(floor((radius + radius) / res_.z)) + 1;

	int cvoxel_per_point = bb_dim.x * bb_dim.y * bb_dim.z;
	int cvoxel_num = cvoxel_per_point * point_num;

	if (cvoxel_num <= 0) {
		return;
	}

	device_vector<int> cvoxel_id(cvoxel_num, __allocator);

	fill(cvoxel_id, -1);

	// Testing input query points
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(cvoxel_num, findCandidateVoxels,
					qcloud, cvoxel_num, cvoxel_per_point, bb_dim, radius,
					res_, rmax_b_, rmin_b_, max_b_, min_b_, vgdim_,
					voxel_id_.data(), points_per_voxel_.data(), voxel_num_,
					min_points_per_voxel_, centroid_.data(), cvoxel_id.data()));

	device_vector<int> psum(__allocator);

	streamCompaction<int, isValid>(cvoxel_id, nn_voxel_id, psum);

	// Compute starting_voxel_id from psum
	starting_voxel_id.resize(point_num + 1);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, computeStartingVid,
					psum.data(), point_num, cvoxel_per_point, starting_voxel_id.data()));
}


/* Compute the number of points per voxel using atomicAdd.
 *
 * Params:
 * 		@cloud[in]: coordinates of input points
 * 		@point_num[in]: number of input points
 * 		@vgdim[in]: dimensions of the voxel grid
 * 		@res[in]: resolution of the voxel grid
 * 		@min_b[in]: the lower boundaries of the voxel grid
 * 		@voxel_id[in]: real indices of voxels
 * 		@points_per_voxel[out]: number of points per voxel
 */
__global__ void countPointsPerVoxel(FCloudPtr cloud, int point_num, Vector3i vgdim,
										Vector3f res, Vector3i min_b, int *voxel_id,
										int* points_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		// Determine the 1D index of the voxel
		int vid = voxelId(cloud[i], res, min_b, vgdim);

		// Actual index of the voxel
		int real_id = voxel_id[vid];

		// Update the number of points per voxel by atomicAdd
		atomicAdd(points_per_voxel + real_id, 1);
	}
}

/**
 * Rearrange points to locations corresponding to voxels
 *
 * Params:
 * 		@cloud[in]: coordinates of the point cloud
 * 		@point_num[in]: number of points
 * 		@res[in]: resolution of the voxel grid
 * 		@min_b[in]: lower bound of the voxel grid
 * 		@vgdim[in]: dimensions of the voxel grid
 * 		@voxel_id[in]: real indices of voxels in the grid
 * 		@writing_location[in]: specify where to write the indices of points to
 * 		@point_ids[out]: sorted indices of the input points
 */
__global__ void scatterPointsToVoxels(FCloudPtr cloud, int point_num, Vector3f res,
										Vector3i min_b, Vector3i vgdim, int *voxel_id,
										int* writing_locations, int* point_ids)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < point_num; i += stride) {
		// Determine the 1D index of the voxel again
		int vid = voxelId(cloud[i], res, min_b, vgdim);

		// Actual index of the voxel
		int real_id = voxel_id[vid];

		/* Update the writing location by atomicAdd. The return value
		 * is where the index of the current point is written to. */
		int loc = atomicAdd(writing_locations + real_id, 1);

		// Write the index of the current point to the output
		point_ids[loc] = i;
	}
}

template <typename PointType>
void VoxelGrid2::update(typename PointCloud<PointType>::Ptr &new_cloud)
{
	// null pointers or empty cloud should be ignored
	if (!new_cloud || new_cloud->size() <= 0) {
		return;
	}

	// New boundaries and new size of the newly added point cloud
	Vector3i new_min_b, new_max_b;
	Vector3i new_rmin_b, new_rmax_b;
	Vector3i new_vgdim;

	// Step 1: Find boundaries of the voxel grid that covers the new points ONLY
	FCloudPtr cptr = new_cloud->data();

	findBoundaries(cptr, new_rmin_b, new_rmax_b, new_min_b, new_max_b, new_vgdim);

	// Update rmax and rmin
	if (rmin_b_.x > new_rmin_b.x) {
		rmin_b_.x = new_rmin_b.x;
	}

	if (rmin_b_.y > new_rmin_b.y) {
		rmin_b_.y = new_rmin_b.y;
	}

	if (rmin_b_.z > new_rmin_b.z) {
		rmin_b_.z = new_rmin_b.z;
	}

	if (rmax_b_.x < new_rmax_b.x) {
		rmax_b_.x = new_rmax_b.x;
	}

	if (rmax_b_.y < new_rmax_b.y) {
		rmax_b_.y = new_rmax_b.y;
	}

	if (rmax_b_.z < new_rmax_b.z) {
		rmax_b_.z = new_rmax_b.z;
	}

	// Step 2: Extend buffers so the voxel grid can cover the new points
	extendMemory(cptr, new_min_b, new_max_b);

	// Step 3: Build a small voxel grid that cover the new points
	device_vector<int> new_points_per_voxel(__allocator);
	device_vector<int> new_starting_point_ids(__allocator);
	device_vector<int> new_point_ids(__allocator);
	device_vector<int> new_voxel_id(__allocator);

	int new_non_empty_voxel_num = computeVoxelId(cptr, new_min_b, new_vgdim, new_voxel_id);

	// new_voxel_id is a full-representation of the new voxel grid, not a sparse one
	scatterPointsToVoxelGrid(cptr, new_min_b, new_vgdim, new_non_empty_voxel_num,
								new_voxel_id, new_points_per_voxel, new_starting_point_ids,
								new_point_ids);

	// Step 4: Update the current grid's (centroid_, inverse_cov_) with new points
	updateCentroidAndCovariance(cptr, new_voxel_id, new_points_per_voxel,
								new_starting_point_ids, new_point_ids,
								new_min_b, new_vgdim);
}

void VoxelGrid2::findBoundaries(FCloudPtr cloud, Vector3i &rmin_b, Vector3i &rmax_b,
									Vector3i &min_b, Vector3i &max_b, Vector3i &vgdim)
{
	int point_num = cloud.size();

	if (point_num <= 0) {
		return;
	}

	float min_x, min_y, min_z, max_x, max_y, max_z;

	// Find the bounding box of the input point cloud
	minMax(cloud.X(), min_x, max_x, point_num, __allocator);
	minMax(cloud.Y(), min_y, max_y, point_num, __allocator);
	minMax(cloud.Z(), min_z, max_z, point_num, __allocator);

	// Convert the boundaries from meters to number of voxels
	rmax_b.x = static_cast<int>(floor(max_x / res_.x));
	rmax_b.y = static_cast<int>(floor(max_y / res_.y));
	rmax_b.z = static_cast<int>(floor(max_z / res_.z));

	rmin_b.x = static_cast<int>(floor(min_x / res_.x));
	rmin_b.y = static_cast<int>(floor(min_y / res_.y));
	rmin_b.z = static_cast<int>(floor(min_z / res_.z));

	/**
	 * When the new points are added to the voxel grid, the grid
	 * may expand. In such case, we have to allocate a new voxel
	 * grid, and copies data from the old buffers to the new one.
	 *
	 * To avoid frequent copy, voxel grids are over-allocated,
	 * means their boundaries are larger than the boundaries of
	 * the cloud's bounding box. As new points enter, if they are
	 * still in the voxel grid, we just need to update the measur-
	 * ements in voxels, instead of allocating a new larger voxel
	 * grid.
	 *
	 * The @max_b and @min_b is the boundaries of the voxel grid
	 * to be allocated. All of conversion between 1D and 3D indices
	 * in the voxel grid will be computed based on these values
	 * instead of @rmax_b and @rmin_b.
	 *
	 * TODO: This over-allocation method is meh. It may
	 * reduce the number of times of allocating and copying
	 * a new voxel grid. However I wonder if this is really
	 * boost the performance much. The execution time of
	 * process of updating the voxel grid should be measured
	 * again. If it is not so slow, may be we do not have
	 * to do this.
	 */
	max_b.x = roundUp(rmax_b.x, BLOCK_X_);
	max_b.y = roundUp(rmax_b.y, BLOCK_Y_);
	max_b.z = roundUp(rmax_b.z, BLOCK_Z_);

	min_b.x = roundDown(rmin_b.x, BLOCK_X_);
	min_b.y = roundDown(rmin_b.y, BLOCK_Y_);
	min_b.z = roundDown(rmin_b.z, BLOCK_Z_);

	// Size of the voxel grid to be created
	vgdim.x = max_b.x - min_b.x + 1;
	vgdim.y = max_b.y - min_b.y + 1;
	vgdim.z = max_b.z - min_b.z + 1;
}


/**
 * Actually this is not really a copy.
 * When the voxel grid expands, its non-empty voxels are still
 * non-empty in the new voxel grid. Hence, we have to mark the
 * non-empty old voxels in the new voxel grid.
 *
 * This function examine all voxels in the old voxel grid, find
 * the non-empty one, convert their indices in the old grid to
 * the new indices in the new grid, and mark them in the new grid.
 *
 *
 * Params:
 * 		@src_voxel_id[in]: the voxel_id vector of the old voxel grid
 * 		@old_voxel_num[in]: total number of ALL voxels (including empty ones)
 * 							in the old voxel grid
 * 		@src_min_b[in]: the lower bound of the old voxel grid
 * 		@src_vgdim[in]: the dimension of the old voxel grid
 * 		@dst_voxel_id[out]: the voxel_id vector of the new voxel grid
 * 		@dst_min_b[in]: the lower bound of the new voxel grid
 * 		@dst_vgdim[in]: the dimension of the new voxel grid
 */
__global__ void copyVoxelId(int *src_voxel_id, int old_voxel_num, Vector3i src_min_b, Vector3i src_vgdim,
							int *dst_voxel_id, Vector3i dst_min_b, Vector3i dst_vgdim)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < old_voxel_num; i += stride) {
		// Only copy non-empty voxels
		if (src_voxel_id[i] < src_voxel_id[i + 1]) {
			// i is the 1D index of the voxel in the source grid
			// Convert 1D array index to 3D global index
			Vector3i src_id3 = id1ToId3(i, src_min_b, src_vgdim);

			// The 3D global index is the same between the src and the dst grids

			// Now convert the 3D global index to 1D index in the dst grid
			int dst_id = id3ToId1(src_id3, dst_min_b, dst_vgdim);

			// Mark the voxel as non-empty in the dst voxel_id buffer
			dst_voxel_id[dst_id] = 1;
		}
	}
}

/**
 * Copy a matrix vectors from the src voxel grid to the dst voxel grid.
 * The matrix vectors include centroid_, inverse_covariance_, points_per_voxel_,
 * tmp_centroid_, and tmp_cov_.
 *
 * The process is as follow: we examine the src voxel grid to find the
 * non-empty voxels, then find their indices in the dst voxel grid.
 * Finally, we retrieve the pointer to their matrix using thos indices
 * and perform a simple assignment.
 *
 * Params:
 * 		@src_voxel_id[in]: the voxel_id of the old voxel grid
 * 		@old_toal_voxel_num[in]: number of ALL voxels (including empty ones)
 * 									in the old voxel grid
 * 		@src[in]: the vector to be copied
 * 		@src_voxel_num[in]: the number of non-empty voxels in the old grid
 * 		@src_min_b[in]: the lower bounds of the old voxel grid
 * 		@src_vgdim[in]: the dimensions of the old voxel grid
 * 		@dst_voxel_id[in]: the voxel_id of the new voxel grid
 * 		@total_dst_voxel_num[in]: number of ALL voxels (including empty ones)
 * 									int the new voxel grid
 * 		@dst[out]: the destination of the copy
 * 		@dst_voxel_num[in]: the number of non-empty voxels in the new grid
 * 		@dst_min_b[in]: the lower bounds of the new voxel grid
 * 		@dst_vgdim[in]: the dimensions of the new voxel grid
 */
template <int row_num, int col_num, typename Scalar = double>
__global__ void copyMatrix(int *src_voxel_id, int old_total_voxel_num, Scalar *src,
							int src_voxel_num, Vector3i src_min_b, Vector3i src_vgdim,
							int *dst_voxel_id, int total_dst_voxel_num, Scalar *dst,
							int dst_voxel_num, Vector3i dst_min_b, Vector3i dst_vgdim)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < old_total_voxel_num; i += stride) {
		/**
		 * Remember: i is the index of a src voxel in the voxel_id_
		 * vector. It is NOT the index of matrices (centroid, icov, etc)
		 * of the voxel in the matrix vectors. The actual index
		 * of them in those vectors is src_voxel_id.
		 */
		int src_real_id = src_voxel_id[i];

		// Skip empty voxels
		if (src_real_id >= src_voxel_id[i + 1]) {
			continue;
		}

		// Convert the 1D index in the src grid to a 3D global index
		Vector3i src_id3 = id1ToId3(i, src_min_b, src_vgdim);

		// The 3D global index is the same between src and dst voxel grids

		// Convert the 3D global index to the 1D index in the dst grid
		int dst_id1 = id3ToId1(src_id3, dst_min_b, dst_vgdim);

		// The actual index of the matrix in the dst voxel grid
		int dst_real_id = dst_voxel_id[dst_id1];

		// Retrieve pointers to the source and destination matrix
		MatrixDevice<Scalar> src_mat(row_num, col_num, src_voxel_num, src + src_real_id);
		MatrixDevice<Scalar> dst_mat(row_num, col_num, dst_voxel_num, dst + dst_real_id);

		// Copy src to dst
		src_mat.copy(dst_mat);
	}
}

/**
 * Extend the GPU memory to hold both the old and new points.
 * Basically allocate new buffers and copy data from old buffers
 * to the new one.
 *
 * Params:
 * 		@new_cloud[in]: the new points to be added to the voxel grid
 * 		@new_min_b, new_max_b[in]: the lower and upper boundaries (in
 * 						number of voxels) of the bounding that cover
 * 						the new points ONLY.
 */
void VoxelGrid2::extendMemory(FCloudPtr new_cloud, Vector3i new_min_b, Vector3i new_max_b)
{
	int point_num = new_cloud.size();

	if (point_num <= 0) {
		return;
	}

	// Compute the boundaries of the new voxel grid
	if (new_min_b.x > min_b_.x) {
		new_min_b.x = min_b_.x;
	}

	if (new_min_b.y > min_b_.y) {
		new_min_b.y = min_b_.y;
	}

	if (new_min_b.z > min_b_.z) {
		new_min_b.z = min_b_.z;
	}

	if (new_max_b.x < max_b_.x) {
		new_max_b.x = max_b_.x;
	}

	if (new_max_b.y < max_b_.y) {
		new_max_b.y = max_b_.y;
	}

	if (new_max_b.z < max_b_.z) {
		new_max_b.z = max_b_.z;
	}

	// Extend the voxel_id_ if the old grid cannot cover the new points
	// Dimensions of the new voxel grid
	Vector3i new_vgdim = new_max_b - new_min_b + 1;

	// Number of ALL voxels (including both empty and non-empty ones) in the old and new grid
	int new_total_voxel_num = new_vgdim.x * new_vgdim.y * new_vgdim.z;
	int old_total_voxel_num = vgdim_.x * vgdim_.y * vgdim_.z;

	// Allocate new voxel_id vector, zero-init
	device_vector<int> new_voxel_id(new_total_voxel_num + 1, __allocator, true);

	if (old_total_voxel_num > 0) {
		// Check the old voxel id and mark non-empty voxels in the new_voxel_id
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyVoxelId,
						voxel_id_.data(), old_total_voxel_num, min_b_, vgdim_,
						new_voxel_id.data(), new_min_b, new_vgdim));
	}

	// The new points may occupy new voxels. Check them and mark non-empty voxels
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, markNonEmptyVoxels,
					new_cloud, point_num, res_, new_min_b, new_vgdim,
					new_voxel_id.data()));

	// Re-compute the number of non-empty voxels
	int new_voxel_num = 0;

	ExclusiveScan(new_voxel_id, new_voxel_num);

	// If the number of non-empty voxels changed, extend other buffers
	if (new_voxel_num > voxel_num_) {
		/* If new_voxel_num > voxel_num, some empty voxels becomes non-empty.
		 * All vectors centroid_, inverse_covariance, points_per_voxel,
		 * tmp_centroid_, tmp_cov_ must be rebuilt.
		 */
		matrix_vector<double> new_tmp_cov(new_voxel_num, 3, 3, __allocator, true);

		// Initialize tmp covariance to identity
		// Initialize tmp_centroid and tmp_cov
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_voxel_num, initTmpCovariance,
						new_tmp_cov.data(), new_voxel_num));

		// Copy tmp_cov
		if (old_total_voxel_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyMatrix<3, 3, double>,
							voxel_id_.data(), old_total_voxel_num, tmp_cov_.data(),
							voxel_num_, min_b_, vgdim_, new_voxel_id.data(), new_total_voxel_num,
							new_tmp_cov.data(), new_voxel_num, new_min_b, new_vgdim));
		}

		tmp_cov_ = std::move(new_tmp_cov);

		// Copy tmp_centroid
		matrix_vector<double> new_tmp_centroid(new_voxel_num, 3, 1, __allocator, true);

		if (old_total_voxel_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyMatrix<3, 1, double>,
							voxel_id_.data(), old_total_voxel_num, tmp_centroid_.data(),
							voxel_num_, min_b_, vgdim_, new_voxel_id.data(), new_total_voxel_num,
							new_tmp_centroid.data(), new_voxel_num, new_min_b, new_vgdim));
		}

		tmp_centroid_ = std::move(new_tmp_centroid);

		// Copy centroid
		// TODO: is zero init neccesary?
		device_vector<double> new_centroid(new_voxel_num * 3, __allocator, true);

		if (old_total_voxel_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyMatrix<3, 1, double>,
							voxel_id_.data(), old_total_voxel_num, centroid_.data(),
							voxel_num_, min_b_, vgdim_, new_voxel_id.data(), new_total_voxel_num,
							new_centroid.data(), new_voxel_num, new_min_b, new_vgdim));
		}

		centroid_ = std::move(new_centroid);

		// Copy inverse covariance
		// TODO: is zero init neccesary?
		matrix_vector<double> new_inverse_cov(new_voxel_num, 3, 3, __allocator, true);

		if (old_total_voxel_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyMatrix<3, 3, double>,
							voxel_id_.data(), old_total_voxel_num, inverse_covariance_.data(),
							voxel_num_, min_b_, vgdim_, new_voxel_id.data(), new_total_voxel_num,
							new_inverse_cov.data(), new_voxel_num, new_min_b, new_vgdim));
		}

		inverse_covariance_ = std::move(new_inverse_cov);

		// Copy points_per_voxel
		device_vector<int> new_points_per_voxel(new_voxel_num, __allocator, true);

		if (old_total_voxel_num > 0) {
			checkCudaErrors(launchSync<BLOCK_SIZE_X>(old_total_voxel_num, copyMatrix<1, 1, int>,
							voxel_id_.data(), old_total_voxel_num, points_per_voxel_.data(),
							voxel_num_, min_b_, vgdim_, new_voxel_id.data(), new_total_voxel_num,
							new_points_per_voxel.data(), new_voxel_num, new_min_b, new_vgdim));
		}

		points_per_voxel_ = std::move(new_points_per_voxel);
	}

	// Update boundaries
	min_b_ = new_min_b;
	max_b_ = new_max_b;
	vgdim_ = new_vgdim;

	voxel_num_ = new_voxel_num;

	// Replace the voxel_id_ by the new one
	voxel_id_ = std::move(new_voxel_id);
}


/* Distribute points to appropriate voxels.
 * Count the number of points per each voxel, and re-arrange indices of
 * input points to fit the voxel indices. The output is a sparse matrix
 * representation of the points, and a vector that contains the number
 * of points belonging to each voxels.
 *
 * Params:
 * 		@cloud[in]: coordinates of input points
 * 		@min_b[in]: the lower boundaries of the voxel grid
 * 		@vgdim[in]: the dimensions of the voxel grid
 * 		@non_empty_voxel_num[in]: number of non-empty voxels
 * 		@voxel_id[in]: the voxel_id vector of the voxel grid
 * 		@points_per_voxel[out]: number of points per each voxel
 * 		@starting_point_ids[out]: the starting indices of the list of points of each voxel
 * 		@point_ids[out]: the indices of the input points
 */
void VoxelGrid2::scatterPointsToVoxelGrid(FCloudPtr cloud, Vector3i min_b, Vector3i vgdim,
											int non_empty_voxel_num, device_vector<int> &voxel_id,
											device_vector<int> &points_per_voxel,
											device_vector<int> &starting_point_ids,
											device_vector<int> &point_ids)
{
	int point_num = cloud.size();

	points_per_voxel.resize(non_empty_voxel_num);
	fill(points_per_voxel, 0);

	/* Count the number of points that belong to each voxel */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, countPointsPerVoxel,
					cloud, point_num, vgdim, res_, min_b, voxel_id.data(),
					points_per_voxel.data()));

	/* Allocate the starting point indices and the writing location */
	starting_point_ids.resize(non_empty_voxel_num + 1);

	device_vector<int> wlocation(non_empty_voxel_num, __allocator);

	gcopyDtoD(starting_point_ids.data(), points_per_voxel.data(), sizeof(int) * non_empty_voxel_num);

	ExclusiveScan(starting_point_ids);

	gcopyDtoD(wlocation.data(), starting_point_ids.data(), sizeof(int) * non_empty_voxel_num);

	point_ids.resize(point_num);

	/* Re-arrange point indices to match the indices of voxels */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(point_num, scatterPointsToVoxels,
					cloud, point_num, res_, min_b, vgdim, voxel_id.data(),
					wlocation.data(), point_ids.data()));
}

/* Convert the indices of source voxels to the
 * indices in the destination voxel grid.
 *
 * Params:
 * 		@src_voxel_id[in]: the voxel_id vector of the old voxel grid
 * 		@src_voxel_num[in]: number of input voxels (including empty ones)
 * 							in the old voxel grid
 * 		@src_min_b[in]: lower boundaries of the old voxel grid
 * 		@src_vgdim[in]: dimensions of the old voxel grid
 * 		@dst_min_b[in]: lower boundaries of the new voxel grid
 * 		@dst_vgdim[in]: dimensions of the new voxel grid
 *		@voxel_id[in]: the real indices of output voxels
 * 		@uvoxel_id[out]: the real indices of the voxels to be updated
 */
__global__ void convertVoxelId(int *src_voxel_id, int src_voxel_num,
								Vector3i src_min_b, Vector3i src_vgdim,
								Vector3i dst_min_b, Vector3i dst_vgdim,
								int *dst_voxel_id, int *uvoxel_id)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < src_voxel_num; i += stride) {
		/* The index of the current source voxel is i.
		 * Because we use the sparse representation to represent
		 * voxel's information, the information of voxel i in
		 * data vectors (centroid, inverse_covariance, points_per_voxel,
		 * starting_point_ids) are stored at index src_voxel_id[i] of
		 * those vectors. Here, src_voxel_id plays the role of a
		 * list of starting indices of voxels.
		 *
		 * Here we get the index where the information of voxel i
		 * are stored in real_id.
		 */
		int real_id = src_voxel_id[i];

		// Check if the voxel is non-empty
		if (real_id < src_voxel_id[i + 1]) {
			// Convert the 1D index i of the voxel to a 3D global coordinate
			Vector3i id3 = id1ToId3(i, src_min_b, src_vgdim);

			/* Convert the 3D global coordinate to the 1D index in
			 * the DESTINATION voxel grid.
			 */
			int dst_id = id3ToId1(id3, dst_min_b, dst_vgdim);

			/* Similar to the source voxel grid, the destination voxel grid
			 * is also represented by a sparse representation. Hence, the
			 * actual index of a dst voxel information is stored at index
			 * dst_voxel_id[dst_id] of information vectors.
			 *
			 * We output that index for later step of updating.
			 */
			uvoxel_id[real_id] = dst_voxel_id[dst_id];
		}
	}
}


/* Update the number of points per voxel in the destination grid,
 * given the number of points from the source grid.
 *
 * Params:
 * 		@uvoxel_id[in]: the indices of source voxels to be updated in the destination grid
 * 		@new_voxel_num[in]: the number of source voxels
 * 		@src_point_per_voxel[in]: the number of points per source voxel
 * 		@dst_point_per_voxel[out]: the number of points per destination voxel
 */
__global__ void updatePointsPerVoxel(int *uvoxel_id, int new_voxel_num, int *src_point_per_voxel,
										int *dst_point_per_voxel)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < new_voxel_num; i += stride) {
		int dst_id = uvoxel_id[i];	// Only care about voxels that contains newly added points

		dst_point_per_voxel[dst_id] += src_point_per_voxel[i];
	}
}

/**
 * Add new points' measurement to the current voxel grid.
 * Update centroids, covariances, inverse covariances, tmp centroids, and
 * tmp cov of voxels that contains new points.
 *
 * In the previous steps, a new voxel grid that cover the new points only
 * was created. This function fuses the new voxel grid to the current
 * expanded voxel grid.
 *
 * Params:
 * 		@new_cloud[in]: coordinates of new points
 * 		@new_voxel_id[in]: the voxel_id vector of the grid that cover ONLY the new cloud
 * 		@new_points_per_voxel[in]: number of new points per new voxel
 * 		@new_starting_point_ids[in]: starting indices of new points in new voxels
 * 		@new_point_ids[in]: sorted indices of new points
 * 		@min_b[in]: the lower bound of the new voxel grid
 * 		@vgdim[in]: the dimensions of the new voxel grid
 */
void VoxelGrid2::updateCentroidAndCovariance(FCloudPtr new_cloud, device_vector<int> &new_voxel_id,
												device_vector<int> &new_points_per_voxel,
												device_vector<int> &new_starting_point_ids,
												device_vector<int> &new_point_ids,
												Vector3i min_b, Vector3i vgdim)
{
	/**
	 * Source grid: the voxel gird that covers the new points
	 * Source voxels: the voxels that belong to the source grid
	 * Destination grid: the current expanded voxel grid of the class
	 * Destination voxels: the voxels that belong to the destination grid
	 */
	/**
	 * Remember the size of voxel_id is number_of_voxel + 1.
	 * The last element is the number of non-empty voxels in the grid.
	 * This size is the number of ALL voxels, including the empty ones.
	 */
	int new_voxel_num = new_voxel_id.size() - 1;

	if (new_voxel_num <= 0) {
		return;
	}

	// Compute the 1D indices of source voxels in the destination grid
	/* We do not want to examine all voxels. Only ones that contain new
	 * points will be updated. Their indices are recorded in an individual
	 * vector.
	 */
	device_vector<int> uvoxel_id(new_points_per_voxel.size(), __allocator);	// Voxels to be updated

	/* The input vector src_voxel_id contains the indices of voxels to
	 * be updated, but in the input grid. They must be converted to
	 * the real indices in the destination grid (the current expanded grid).
	 */
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_voxel_num, convertVoxelId,
					new_voxel_id.data(), new_voxel_num, min_b, vgdim, min_b_, vgdim_,
					voxel_id_.data(), uvoxel_id.data()));

	// Now uvoxel_id contains the real id of the non-empty new voxels
	initialize(new_cloud, new_starting_point_ids, new_point_ids, uvoxel_id);

	// This is the number of non-empty voxels in the input grid
	int new_non_empty_voxel_num = uvoxel_id.size();

	// Update number of points per voxel
	checkCudaErrors(launchSync<BLOCK_SIZE_X>(new_non_empty_voxel_num, updatePointsPerVoxel,
					uvoxel_id.data(), new_non_empty_voxel_num, new_points_per_voxel.data(),
					points_per_voxel_.data()));

	// Update centroids
	updateCentroids(uvoxel_id);

	// Extracts voxels which have at least min_points_per_voxel_ from the input voxels
	device_vector<int> updated_vid(__allocator);

	// Extract valid voxels from new voxels only
	extractValidVoxels(uvoxel_id, updated_vid);

	// Update the inverse covariances of new voxels
	updateInverseCovariances(updated_vid);
}

void VoxelGrid2::initialize(FCloudPtr cloud, device_vector<int> &starting_point_ids,
							device_vector<int> &point_ids, device_vector<int> &updated_vid)
{
	// If the updated_vid is empty, update all voxels in the grid now
	// Otherwise, only update voxels indicated by updated_vid
	int voxel_num = (updated_vid.size() <= 0) ? voxel_num_ : updated_vid.size();

	if (voxel_num <= 0) {
		return;
	}

	// Update tmp centroid and tmp covariance
	int thread_num = voxel_num * 32;

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(thread_num, computeTmpCentroidAndCovariance,
					cloud, starting_point_ids.data(), point_ids.data(), voxel_num,
					tmp_centroid_(0), tmp_centroid_(1), tmp_centroid_(2),
					tmp_cov_(0, 0), tmp_cov_(0, 1), tmp_cov_(0, 2),
					tmp_cov_(1, 1), tmp_cov_(1, 2), tmp_cov_(2, 2),
					updated_vid.data()));
}

void VoxelGrid2::updateCentroids(device_vector<int> &updated_vid)
{
	int voxel_num = (updated_vid.size() <= 0) ? voxel_num_ : updated_vid.size();

	if (voxel_num <= 0) {
		return;
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(voxel_num, computeVoxelCentroid,
					tmp_centroid_.data(), points_per_voxel_.data(), voxel_num,
					centroid_.data(), voxel_num_, updated_vid.data()));
}

void VoxelGrid2::testVoxel(int vid)
{
	// Print centroid
	MatrixHost<double> mat31(3, 1, __allocator), mat33(3, 3, __allocator);

	mat31 = MatrixDD(3, 1, voxel_num_, centroid_.data() + vid);

	std::cout << std::endl << "GPU At voxel " << vid << std::endl;
	std::cout << "Number of points = " << points_per_voxel_[vid] << std::endl;
	std::cout << "Centroid = " << std::endl << mat31 << std::endl;

	mat33 = MatrixDD(3, 3, voxel_num_, inverse_covariance_.data() + vid);

	std::cout << "InverseCovariance = " << std::endl << mat33 << std::endl;

	mat31 = MatrixDD(3, 1, voxel_num_, tmp_centroid_.data() + vid);

	std::cout << "TmpCentroid = " << std::endl << mat31 << std::endl;

	mat33 = MatrixDD(3, 3, voxel_num_, tmp_cov_.data() + vid);

	std::cout << "TmpCov = " << std::endl << mat33 << std::endl << std::endl;
}

template void VoxelGrid2::setInputCloud<pcl::PointXYZ>(PointCloud<pcl::PointXYZ>::Ptr&);
template void VoxelGrid2::setInputCloud<pcl::PointXYZI>(PointCloud<pcl::PointXYZI>::Ptr&);

template void VoxelGrid2::radiusSearch<pcl::PointXYZ>(PointCloud<pcl::PointXYZ>::Ptr&, float, int,
														device_vector<int>&, device_vector<int>&);
template void VoxelGrid2::radiusSearch<pcl::PointXYZI>(PointCloud<pcl::PointXYZI>::Ptr&, float, int,
														device_vector<int>&, device_vector<int>&);

template void VoxelGrid2::update<pcl::PointXYZ>(PointCloud<pcl::PointXYZ>::Ptr&);
template void VoxelGrid2::update<pcl::PointXYZI>(PointCloud<pcl::PointXYZI>::Ptr&);

}

}
