#ifndef MATRIX_HOST_H_
#define MATRIX_HOST_H_

#include <iostream>
#include <vector>
#include <string>

#include "gmemory.h"
#include "device_vector.h"

namespace gpu {
template<typename Scalar> class MatrixDevice;

template <typename Scalar = double>
class MatrixHost : public Base {
	friend class MatrixDevice<Scalar>;
public:
	MatrixHost(GMemoryAllocator *allocator) :
		Base(allocator),
		device_buffer_(allocator)
	{
		rows_ = cols_ = 0;
		__allocator = allocator;
	}

	MatrixHost(int rows, int cols, GMemoryAllocator *allocator) :
		Base(allocator),
		device_buffer_(rows * cols, allocator, true)
	{
		rows_ = rows;
		cols_ = cols;

		host_buffer_ = std::vector<Scalar>(rows_ * cols_);
	}

	MatrixHost(const MatrixHost<Scalar>& other) :
		Base(other),
		device_buffer_(other.device_buffer_)
	{
		rows_ = other.rows_;
		cols_ = other.cols_;

		host_buffer_ = other.host_buffer_;
	}

	MatrixHost(MatrixHost<Scalar> &&other) :
		Base(other),
		device_buffer_(std::move(other.device_buffer_))
	{
		rows_ = other.rows_;
		cols_ = other.cols_;

		host_buffer_ = other.host_buffer_;
	}

	MatrixHost<Scalar>& operator=(const MatrixHost<Scalar> &other) {
		rows_ = other.rows_;
		cols_ = other.cols_;

		host_buffer_ = other.host_buffer_;
		device_buffer_ = other.device_buffer_;

		return *this;
	}

	MatrixHost<Scalar>& operator=(MatrixHost<Scalar> &&other) {
		rows_ = other.rows_;
		cols_ = other.cols_;

		host_buffer_ = std::move(other.host_buffer_);
		device_buffer_ = std::move(other.device_buffer_);

		return *this;
	}

	MatrixHost<Scalar>& operator=(const MatrixDevice<Scalar> &input);

	void debug() {
		std::cout << *this;
	}

	Scalar at(int r, int c) const {

		return host_buffer_[r * cols_ + c];
	}

	// Copy current host data to the device buffer
	void syncGpu() {
		if (rows_ > 0 && cols_ > 0) {
			gcopyHtoD(device_buffer_.data(), host_buffer_.data(), sizeof(Scalar) * rows_ * cols_);
		}
	}

	// Copy GPU data to the host buffer
	void syncHost() {
		if (rows_ > 0 && cols_ > 0) {
			gcopyDtoH(host_buffer_.data(), device_buffer_.data(), sizeof(Scalar) * rows_ * cols_);
		}
	}

	// Return element (r, c)
	Scalar &operator()(int r, int c) {
		assert(r >= 0 && r < rows_ && c >= 0 && c < cols_);

		return host_buffer_[r * cols_ + c];
	}

	// Used in the case the matrix is a vector
	Scalar &operator()(int idx) {
		assert((rows_ == 1 && idx >= 0 && idx < cols_) ||
				(cols_ == 1 && idx >= 0 && idx < rows_));

		return host_buffer_[idx];
	}

	void reset() {
		gzero(device_buffer_.data(), device_buffer_.size() * sizeof(Scalar));
	}

	/* Print out the content of the matrix */
	friend std::ostream &operator<<(std::ostream &os, const MatrixHost<Scalar> &m) {
		for (int i = 0; i < m.rows_; i++) {
			for (int j = 0; j < m.cols_; j++) {
				os << m.at(i, j) << " ";
			}

			os << std::endl;
		}

		os << std::endl;

		return os;
	}

	// Print refcount of device_buffer_
	int refCount() {
		return device_buffer_.refCount();
	}

	int rows() {
		return rows_;
	}

	int cols() {
		return cols_;
	}

	// Create an identity matrix of row rows and col columns
	static MatrixHost<Scalar> Identity(int row, int col, GMemoryAllocator *allocator) {
		assert(row == col && row > 0);

		MatrixHost<Scalar> output(row, col, allocator);

		for (int r = 0; r < row; ++r) {
			for (int c = 0; c < col; ++c) {
				output(r, c) = (r == c) ? 1 : 0;
			}
		}

		output.syncGpu();

		return output;
	}

private:
	int rows_, cols_;
	std::vector<Scalar> host_buffer_;
	device_vector<Scalar> device_buffer_;
};


using MatrixHF = class MatrixHost<float>;
using MatrixHD = class MatrixHost<double>;

}

#endif
