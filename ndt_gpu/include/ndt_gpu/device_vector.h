#ifndef DEVICE_MEM_H_
#define DEVICE_MEM_H_

#include <cuda.h>
#include <cassert>
#include <string>
#include <exception>
#include <memory>
#include <vector>

#include "debug.h"
#include "common.h"

namespace gpu {

template <typename ValType> class matrix_vector;

template <typename ValType>
class device_vector : public Base {
	friend class matrix_vector<ValType>;
public:
	device_vector(GMemoryAllocator *allocator) :
		Base(allocator)
	{
		data_ = nullptr;
		ele_num_ = 0;
		ref_count_ = std::make_shared<unsigned int>(1);
	}

	device_vector(int ele_num, GMemoryAllocator *allocator, bool zero_init = false) :
		Base(allocator)
	{
		data_ = nullptr;
		ele_num_ = 0;

		if (ele_num > 0) {
			ele_num_ = ele_num;

			alloc(&data_, sizeof(ValType) * ele_num_);

			if (zero_init) {
				gzero(data_, sizeof(ValType) * ele_num_);
			}

		} else {
			data_ = nullptr;
			ele_num_ = 0;
		}

		ref_count_ = std::make_shared<unsigned int>(1);
	}

	// Copy constructor
	device_vector(const device_vector<ValType> &other) :
		Base(other)
	{
		data_ = nullptr;
		ele_num_ = other.ele_num_;

		if (ele_num_ > 0) {
			alloc(&data_, sizeof(ValType) * ele_num_);
			gcopyDtoD(data_, other.data_, sizeof(ValType) * ele_num_);
		}

		ref_count_ = std::make_shared<unsigned int>(1);
	}

	device_vector(device_vector<ValType> &&other) :
		Base(other)
	{
		data_ = other.data_;
		ele_num_ = other.ele_num_;

		ref_count_ = other.ref_count_;

		++(*ref_count_);
	}

	// Copy assignment, deep copy
	device_vector<ValType> &operator=(const device_vector<ValType> &other) {
		if (ele_num_ != other.ele_num_) {
			free(data_);

			ele_num_ = other.ele_num_;

			if (ele_num_ > 0) {
				alloc(&data_, sizeof(ValType) * ele_num_);
			}
		}

		if (ele_num_ > 0) {
			gcopyDtoD(data_, other.data_, sizeof(ValType) * ele_num_);
		}

		return *this;
	}

	device_vector<ValType> &operator=(device_vector<ValType> &&other) {
		release();
		data_ = other.data_;
		ele_num_ = other.ele_num_;

		ref_count_ = other.ref_count_;

		++(*ref_count_);

		return *this;
	}

	// Copy data from a host-side vector
	device_vector<ValType> &operator=(const std::vector<ValType> &other) {
		int host_ele_num = other.size();

		if (ele_num_ != host_ele_num) {
			resize(host_ele_num);
		}

		// Copy from the host to the device
		if (host_ele_num > 0) {
			gcopyHtoD(data_, other.data(), sizeof(ValType) * host_ele_num);
		}

		return *this;
	}

	void clear() {
		if (ele_num_ > 0) {
			free(data_);
		}

		data_ = nullptr;
		ele_num_ = 0;
	}

	/**
	 * If copy == true, copy the data from the old buffer
	 * to the new one. Otherwise, just resize the buffer.
	 */
	void resize(int new_ele_num, bool copy = false) {
		if (new_ele_num == ele_num_) {
			return;
		}

		if (new_ele_num == 0) {
			if (ele_num_ > 0) {
				free(data_);
			}

			ele_num_ = 0;

		} else {
			if (ele_num_ == 0) {
				ele_num_ = new_ele_num;

				alloc(&data_, sizeof(ValType) * ele_num_);

			} else {
				ValType *new_data = nullptr;

				alloc(&new_data, sizeof(ValType) * new_ele_num);

				if (copy) {
					int copy_size = (ele_num_ < new_ele_num) ? ele_num_ : new_ele_num;

					gcopyDtoD(new_data, data_, sizeof(ValType) * copy_size);
				}

				free(data_);

				data_ = new_data;
				ele_num_ = new_ele_num;
			}
		}
	}

	int size() {
		return ele_num_;
	}

	int size() const {
		return ele_num_;
	}

	ValType *data() {
		return data_;
	}

	const ValType *data() const {
		return data_;
	}

	ValType operator[](int idx) {
		verify(idx >= 0 && idx <= ele_num_);

		ValType ret_val;

		gcopyDtoH(&ret_val, data_ + idx, sizeof(ValType));

		return ret_val;
	}

	/* Return the size of occupied GPU memory in bytes */
	size_t sizeInBytes() {
		return (sizeof(ValType) * static_cast<size_t>(ele_num_));
	}

	int refCount() {
		return *ref_count_;
	}

	std::vector<ValType> host_vector() const {
		std::vector<ValType> output(ele_num_);

		gcopyDtoH(output.data(), data_, sizeof(ValType) * ele_num_);

		return output;
	}

	~device_vector() {
		release();
	}

private:

	void release() {

		--(*ref_count_);

		if (0 == *ref_count_) {
			if (ele_num_ > 0) {
				free(data_);
				data_ = nullptr;
			}

			ref_count_.reset();
		}

		ele_num_ = 0;
	}

	ValType *data_;
	int ele_num_;
	std::shared_ptr<unsigned int> ref_count_;
};

template <typename Scalar> class MatrixHost;
template <typename Scalar> class MatrixDevice;

template <typename ValType>
class matrix_vector : public Base {
public:
	matrix_vector(GMemoryAllocator *allocator) :
		Base(allocator),
		matrix_list_(__allocator)
	{
		ele_num_ = 0;
		row_ = col_ = 0;
	}

	matrix_vector(int ele_num, int row, int col, GMemoryAllocator *allocator,
					bool zero_init = false) :
		Base(allocator),
		matrix_list_(ele_num * row * col, __allocator, zero_init)
	{
		ele_num_ = ele_num;
		row_ = row;
		col_ = col;
	}

	matrix_vector(device_vector<ValType> &vec, int row, int col) :
		Base(vec),
		matrix_list_(std::move(vec))
	{
		assert(vec.size() % (row * col) == 0);

		ele_num_ = vec.size() / (row * col);
		row_ = row;
		col_ = col;
	}

	matrix_vector(const matrix_vector<ValType> &other) :
		Base(other),
		matrix_list_(other.matrix_list_)
	{

		ele_num_ = other.ele_num_;
		row_ = other.row_;
		col_ = other.col_;
	}

	matrix_vector(matrix_vector<ValType> &&other) :
		Base(other),
		matrix_list_(std::move(other.matrix_list_))
	{
		ele_num_ = other.ele_num_;
		row_ = other.row_;
		col_ = other.col_;

		other.ele_num_ = 0;
		other.row_ = other.col_ = 0;
	}

	matrix_vector<ValType> &operator=(const matrix_vector<ValType> &other)
	{
		matrix_list_ = other.matrix_list_;
		ele_num_ = other.ele_num_;
		row_ = other.row_;
		col_ = other.col_;

		return *this;
	}

	matrix_vector<ValType> &operator=(matrix_vector<ValType> &&other)
	{
		matrix_list_ = std::move(other.matrix_list_);
		ele_num_ = other.ele_num_;
		row_ = other.row_;
		col_ = other.col_;

		other.ele_num_ = 0;
		other.row_ = other.col_ = 0;

		return *this;
	}

	int size() {
		return ele_num_;
	}

	int size() const {
		return ele_num_;
	}

	ValType *data() {
		return matrix_list_.data();
	}

	const ValType *data() const {
		return matrix_list_.data();
	}

	MatrixHost<ValType> operator[](int idx) {
		MatrixDevice<ValType> tmp(row_, col_, ele_num_, matrix_list_.data() + idx);
		MatrixHost<ValType> res(row_, col_, matrix_list_.__allocator);

		res = tmp;

		return res;
	}

	// TODO: no boundary check?
	// Get the list of cells at (row, col)
	ValType *operator()(int row, int col) {
		return matrix_list_.data() + (row * col_ + col) * ele_num_;
	}

	// In the case of vector
	ValType *operator()(int idx) {
		return matrix_list_.data() + idx * ele_num_;
	}

	int rowNum() {
		return row_;
	}

	int rowNum() const {
		return row_;
	}

	int colNum() {
		return col_;
	}

	int colNum() const {
		return col_;
	}

	/* Return the size of occupied GPU memory in bytes */
	size_t sizeInBytes() {
		return (matrix_list_.sizeInBytes());
	}

	// Change the shape of the matrix list
	// Delete the current data
	void resize(int ele_num, int row, int col) {
		verify(ele_num > 0 && row > 0 && col > 0);

		row_ = row;
		col_ = col;
		ele_num_ = ele_num;

		matrix_list_.resize(ele_num * row * col);
	}

	~matrix_vector() {}

private:
	device_vector<ValType> matrix_list_;
	int row_, col_;
	int ele_num_;
};
}

#endif
