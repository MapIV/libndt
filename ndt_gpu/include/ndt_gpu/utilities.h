#ifndef GPU_UTIL_H_
#define GPU_UTIL_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/scan.h>
#include <thrust/fill.h>
#include <thrust/sort.h>
#include <cassert>

#include "common.h"
#include "device_vector.h"
#include "MatrixDevice.h"
#include "vector3x.h"

namespace gpu {

extern __shared__ double _util_block_buffer[];

/* A wrapper for exclusive scan using thrust.
 * Output sum of all elements is stored at sum. */
template <typename T = int>
void ExclusiveScan(T *input, int ele_num, T *sum)
{
	  thrust::device_ptr<T> dev_ptr(input);

	  thrust::exclusive_scan(dev_ptr, dev_ptr + ele_num, dev_ptr);
	  checkCudaErrors(cudaDeviceSynchronize());

	  *sum = *(dev_ptr + ele_num - 1);
}

/* A wrapper for exclusive scan using thrust.
 * Output sum is not required. */
template <typename T = int>
void ExclusiveScan(T *input, int ele_num)
{
	  thrust::device_ptr<T> dev_ptr(input);

	  thrust::exclusive_scan(dev_ptr, dev_ptr + ele_num, dev_ptr);
	  checkCudaErrors(cudaDeviceSynchronize());
}

template <typename T = int>
void ExclusiveScan(device_vector<T> &input, T *sum)
{
	ExclusiveScan<T>(input.data(), input.size(), sum);
}

template <typename T = int>
void ExclusiveScan(device_vector<T> &input, T &sum)
{
	ExclusiveScan<T>(input.data(), input.size(), &sum);
}

template <typename T = int>
void ExclusiveScan(device_vector<T> &input)
{
	ExclusiveScan<T>(input.data(), input.size());
}

template <typename T = int>
void InclusiveScan(T *input, int ele_num, T *sum)
{
	  thrust::device_ptr<T> dev_ptr(input);

	  thrust::inclusive_scan(dev_ptr, dev_ptr + ele_num, dev_ptr);
	  checkCudaErrors(cudaDeviceSynchronize());

	  *sum = *(dev_ptr + ele_num - 1);
}

template <typename T = int>
void InclusiveScan(T *input, int ele_num)
{
	thrust::device_ptr<T> dev_ptr(input);

	thrust::inclusive_scan(dev_ptr, dev_ptr + ele_num, dev_ptr);
	checkCudaErrors(cudaDeviceSynchronize());
}

template <typename keyType, typename valType>
void SortByKey(keyType *key, valType *val, int ele_num)
{
	thrust::device_ptr<keyType> dev_key(key);
	thrust::device_ptr<valType> dev_val(val);

	thrust::sort_by_key(dev_key, dev_key + ele_num, dev_val);
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());
}

template <typename T>
__global__ void reductionSumGpu(T *input, int ele_num, T *output)
{
	T *block_sum = (T*)_util_block_buffer;
	T local_sum = 0;
	int tid = threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = tid + blockIdx.x * blockDim.x; i < ele_num; i += stride) {
		local_sum += input[i];
	}

	block_sum[tid] = local_sum;
	__syncthreads();

	int floor_pow2 = blockDim.x;

	while (floor_pow2 & (floor_pow2 - 1)) {
		floor_pow2 &= (floor_pow2 - 1);
	}

	if (tid >= floor_pow2) {
		block_sum[tid - floor_pow2] += block_sum[tid];
	}

	__syncthreads();

	for (int active_threads = floor_pow2 >> 1; active_threads > 0; active_threads >>= 1) {
		if (tid < active_threads) {
			block_sum[tid] += block_sum[tid + active_threads];
		}

		__syncthreads();
	}

	if (tid == 0) {
		output[blockIdx.x] = block_sum[0];
	}
}

template <typename T>
void reductionSum(T *input, int ele_num, T *sum, GMemoryAllocator *allocator)
{
	assert(sum != nullptr);

	if (ele_num == 0) {
		*sum = 0;
	}

	int block_x = (ele_num > BLOCK_SIZE_X) ? BLOCK_SIZE_X : ele_num;
	int grid_x = (ele_num + block_x - 1) / block_x;

	if (grid_x > GRID_SIZE_DEFAULT_) {
		grid_x = GRID_SIZE_DEFAULT_;
	}

	device_vector<T> tmp_sum(grid_x, allocator);

	reductionSumGpu<<<grid_x, block_x, block_x * sizeof(T)>>>(input, ele_num, tmp_sum.data());
	checkCudaErrors(cudaGetLastError());

	reductionSumGpu<<<1, grid_x, grid_x * sizeof(T)>>>(tmp_sum.data(), grid_x, tmp_sum.data());
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	gcopyDtoH(sum, tmp_sum.data(), sizeof(T));
}

template <typename T>
void reductionSum(device_vector<T> &input, T *sum)
{
	reductionSum(input.data(), input.size(), sum, input.__allocator);
}

template <typename T>
void reductionSum(device_vector<T> &input, T &sum)
{
	reductionSum(input.data(), input.size(), &sum, input.__allocator);
}

template <typename Scalar>
__global__ void reductionMatrixSum(Scalar *matrix_list, int mat_num, int row_num, int col_num, Scalar *mat_sum, int mat_sum_offset)
{
	int tid = threadIdx.x;
	int row = blockIdx.y;
	int col = blockIdx.z;

	if (row < row_num && col < col_num) {
		Scalar *in = matrix_list + (row * col_num + col) * mat_num;
		Scalar *out = mat_sum + (row * col_num + col) * mat_sum_offset;
		Scalar local_sum = 0;

		for (int i = tid + blockIdx.x * blockDim.x; i < mat_num; i += blockDim.x * gridDim.x) {
			local_sum += in[i];
		}

		Scalar *block_sum = (Scalar*)_util_block_buffer;

		block_sum[tid] = local_sum;
		__syncthreads();

		int floor_pow2 = blockDim.x;

		if (floor_pow2 & (floor_pow2 - 1)) {
			while (floor_pow2 & (floor_pow2 - 1)) {
				floor_pow2 &= (floor_pow2 - 1);
			}

			if (tid >= floor_pow2) {
				block_sum[tid - floor_pow2] += block_sum[tid];
			}

			__syncthreads();
		}

		for (int active_threads = floor_pow2 >> 1; active_threads > 0; active_threads >>= 1) {
			if (tid < active_threads) {
				block_sum[tid] += block_sum[tid + active_threads];
			}
			__syncthreads();
		}

		if (tid == 0) {
			out[blockIdx.x] = block_sum[0];
		}
	}
}


template <typename T>
void reductionSum(matrix_vector<T> &input, MatrixHost<T> &matrix_sum)
{
	assert(input.size() > 0 && input.rowNum() > 0 && input.colNum() > 0);
	assert(input.rowNum() == matrix_sum.rows() && input.colNum() == matrix_sum.cols());

	GMemoryAllocator *allocator = input.__allocator;

    int mat_num = input.size();
    int row = input.rowNum();
    int col = input.colNum();

	int block_x = (mat_num > BLOCK_SIZE_X) ? BLOCK_SIZE_X : mat_num;
	int grid_x = (mat_num + block_x - 1) / block_x;

	if (grid_x > GRID_SIZE_DEFAULT_) {
		grid_x = GRID_SIZE_DEFAULT_;
	}

	matrix_vector<T> tmp_mat_sum(grid_x, row, col, allocator);

	dim3 grid, block;

	grid.x = grid_x;
	grid.y = row;
	grid.z = col;

	block.x = block_x;
	block.y = block.z = 1;

	reductionMatrixSum<<<grid, block, block.x * sizeof(T)>>>(input.data(), mat_num, row, col, tmp_mat_sum.data(), tmp_mat_sum.size());
	checkCudaErrors(cudaGetLastError());

	grid.x = 1;
	block.x = grid_x;
	reductionMatrixSum<<<grid, block, block.x * sizeof(T)>>>(tmp_mat_sum.data(), grid_x, row, col, tmp_mat_sum.data(), tmp_mat_sum.size());
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	matrix_sum = tmp_mat_sum[0];
}

template <typename Scalar>
struct CmpLess {
	inline __device__ bool operator()(Scalar x, Scalar y) {
		return (x < y);
	}
};

template <typename Scalar>
struct CmpGreater {
	inline __device__ bool operator()(Scalar x, Scalar y) {
		return (x > y);
	}
};

template <typename Scalar, typename Compare>
__global__ void reductionFind(Scalar *input, int ele_num, Scalar initVal, Scalar *output, Compare cmp)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	__shared__ Scalar sval[BLOCK_SIZE_X];

	Scalar tval = initVal;

	for (int i = index; i < ele_num; i += stride) {
		Scalar tmp = input[i];

		tval = (cmp(tmp, tval)) ? tmp : tval;
	}

	sval[threadIdx.x] = tval;

	__syncthreads();

	int floor_pow2 = blockDim.x;

	if (floor_pow2 & (floor_pow2 - 1)) {
		while (floor_pow2 & (floor_pow2 - 1)) {
			floor_pow2 &= floor_pow2 - 1;
		}

		if (threadIdx.x >= floor_pow2) {
			float l_val = sval[threadIdx.x - floor_pow2];
			float h_val = sval[threadIdx.x];

			sval[threadIdx.x - floor_pow2] = (cmp(l_val, h_val)) ? l_val : h_val;
		}

		__syncthreads();
	}

	// Now find the max values in the block
	for (int active_threads = floor_pow2 >> 1; active_threads > 0; active_threads >>= 1) {
		if (threadIdx.x < active_threads) {
			Scalar lval = sval[threadIdx.x];
			Scalar hval = sval[threadIdx.x + active_threads];

			sval[threadIdx.x] = (cmp(lval, hval)) ? lval : hval;
		}

		__syncthreads();
	}

	if (threadIdx.x == 0) {
		output[blockIdx.x] = sval[0];
	}
}


template <typename Scalar, typename Compare>
Scalar reductionFind(Scalar *input, int ele_num, Scalar initVal, GMemoryAllocator *allocator)
{
	Scalar retval = initVal;

	if (ele_num > 0) {
		int block_x = (ele_num > BLOCK_SIZE_X) ? BLOCK_SIZE_X : ele_num;
		int grid_x = (ele_num + block_x - 1) / block_x;

		if (grid_x > GRID_SIZE_DEFAULT_) {
			grid_x = GRID_SIZE_DEFAULT_;
		}

		device_vector<Scalar> tmp_val(grid_x, allocator);

		reductionFind<<<grid_x, block_x>>>(input, ele_num, initVal, tmp_val.data(), Compare());
		checkCudaErrors(cudaGetLastError());

		if (grid_x > 1) {
			reductionFind<<<1, block_x>>>(tmp_val.data(), grid_x, initVal, tmp_val.data(), Compare());
			checkCudaErrors(cudaGetLastError());
		}
		checkCudaErrors(cudaDeviceSynchronize());

		gcopyDtoH(&retval, tmp_val.data(), sizeof(Scalar));
	}

	return retval;
}

template <typename Scalar, typename Compare>
Scalar reductionFind(device_vector<Scalar> &input, Scalar initVal)
{
	return reductionFind(input.data(), input.size(), initVal, input.__allocator);
}

template <typename Scalar>
Scalar max(device_vector<Scalar> &input)
{
	return reductionFind<Scalar, CmpGreater<Scalar>>(input, std::numeric_limits<Scalar>::lowest());
}

template <typename Scalar>
Scalar min(device_vector<Scalar> &input)
{
	return reductionFind<Scalar, CmpLess<Scalar>>(input, std::numeric_limits<Scalar>::max());
}


/* Similar to reductionFind but this is used when we need to
 * look for both min and max. By using this, we only have to
 * access the global memory once, instead of twice (one for
 * max and one for min).
 */
template <typename Scalar>
__global__ void reductionMinMax(Scalar *input, int ele_num,
								Scalar initMin, Scalar initMax,
								Scalar *out_min, Scalar *out_max)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	__shared__ Scalar smin[BLOCK_SIZE_X];
	__shared__ Scalar smax[BLOCK_SIZE_X];

	Scalar tmin = initMin;
	Scalar tmax = initMax;

	for (int i = index; i < ele_num; i += stride) {
		Scalar tmp = input[i];

		tmin = (tmp < tmin) ? tmp : tmin;
		tmax = (tmp > tmax) ? tmp : tmax;
	}

	smin[threadIdx.x] = tmin;
	smax[threadIdx.x] = tmax;

	__syncthreads();

	// For arbitrary size of blockDim (not power of 2)
	int floor_pow2 = blockDim.x;

	if (floor_pow2 & (floor_pow2 - 1)) {
		while (floor_pow2 & (floor_pow2 - 1)) {
			floor_pow2 &= floor_pow2 - 1;
		}

		if (threadIdx.x >= floor_pow2) {
			int l = threadIdx.x - floor_pow2, h = threadIdx.x;

			Scalar l_min = smin[l];
			Scalar h_min = smin[h];

			Scalar l_max = smax[l];
			Scalar h_max = smax[h];

			smin[l] = (l_min < h_min) ? l_min : h_min;
			smax[l] = (l_max > h_max) ? l_max : h_max;
		}

		__syncthreads();
	}

	// Now find the max values in the block
	for (int active_threads = floor_pow2 >> 1; active_threads > 0; active_threads >>= 1) {
		if (threadIdx.x < active_threads) {
			int l = threadIdx.x, h = threadIdx.x + active_threads;
			Scalar l_min = smin[l], h_min = smin[h];
			Scalar l_max = smax[l], h_max = smax[h];

			smin[l] = (l_min < h_min) ? l_min : h_min;
			smax[l] = (l_max > h_max) ? l_max : h_max;
		}

		__syncthreads();
	}

	if (threadIdx.x == 0) {
		out_min[blockIdx.x] = smin[0];
		out_max[blockIdx.x] = smax[0];
	}
}

template <typename Scalar>
void minMax(Scalar *input, Scalar &min, Scalar &max, int ele_num, GMemoryAllocator *allocator)
{
	// Initial min max values
	min = std::numeric_limits<Scalar>::max();
	max = std::numeric_limits<Scalar>::lowest();

	if (ele_num <= 0) {
		return;
	}

	int block_x = (ele_num > BLOCK_SIZE_X) ? BLOCK_SIZE_X : ele_num;
	int grid_x = (ele_num + block_x - 1) / block_x;

	if (grid_x > GRID_SIZE_DEFAULT_) {
		grid_x = GRID_SIZE_DEFAULT_;
	}

	device_vector<Scalar> tmp_min(grid_x, allocator);
	device_vector<Scalar> tmp_max(grid_x, allocator);

	reductionMinMax<<<grid_x, block_x>>>(input, ele_num, min, max,
											tmp_min.data(), tmp_max.data());
	checkCudaErrors(cudaGetLastError());

	if (grid_x > 1) {
		// 1, block_x or 1, grid_x?
		reductionFind<<<1, block_x>>>(tmp_min.data(), grid_x, min, tmp_min.data(),
										CmpLess<Scalar>());
		checkCudaErrors(cudaGetLastError());

		reductionFind<<<1, block_x>>>(tmp_max.data(), grid_x, max, tmp_max.data(),
										CmpGreater<Scalar>());
		checkCudaErrors(cudaGetLastError());
	}
	checkCudaErrors(cudaDeviceSynchronize());

	gcopyDtoH(&min, tmp_min.data(), sizeof(Scalar));
	gcopyDtoH(&max, tmp_max.data(), sizeof(Scalar));
}

template <typename Scalar>
void minMax(device_vector<Scalar> &input, Scalar &min, Scalar &max)
{
	minMax(input.data(), min, max, input.size(), input.__allocator);
}

// Compare elements of two device vectors
template <typename T>
int compare(device_vector<T> &a, device_vector<T> &b)
{
	if (a.size() != b.size())
		return -1;

	if (a.size() == 0) {
		std::cout << "Empty vectors " << std::endl;
		return 0;
	}

	// Copy a and b to the host memory
	std::vector<T> ha(a.size());
	std::vector<T> hb(b.size());

	gcopyDtoH(ha.data(), a.data(), sizeof(T) * a.size());
	gcopyDtoH(hb.data(), b.data(), sizeof(T) * b.size());

	int miss_count = 0;

	for (int i = 0; i < ha.size(); ++i) {
		if (std::abs(ha[i] - hb[i]) > 0.00001) {
			std::cout << "At " << i << " A = " << ha[i] << " while B = " << hb[i] << " diff = " << std::abs(ha[i] - hb[i]) << std::endl;
			++miss_count;
		}
	}

	return miss_count;
}

template <int block_size, typename... Arguments>
inline cudaError_t launchSync(int thread_num, void(*f)(Arguments...), Arguments... args)
{
	int block_x = (thread_num > block_size) ? block_size : thread_num;
	int grid_x = (thread_num + block_x - 1) / block_x;

	f<<<grid_x, block_x>>>(args...);

	cudaError_t err = cudaGetLastError();

	if (err != cudaSuccess) {
		return err;
	}

	return cudaDeviceSynchronize();
}

template <int block_size, typename... Arguments>
inline cudaError_t launchASync(int thread_num, void(*f)(Arguments...), Arguments... args)
{
	int block_x = (thread_num > block_size) ? block_size : thread_num;
	int grid_x = (thread_num + block_x - 1) / block_x;

	f<<<grid_x, block_x>>>(args...);

	return cudaGetLastError();
}

template <int block_size, typename... Arguments>
inline cudaError_t launchASync(int thread_num, cudaStream_t &stream,
                                void(*f)(Arguments...), Arguments... args)
{
	int block_x = (thread_num > block_size) ? block_size : thread_num;
	int grid_x = (thread_num + block_x - 1) / block_x;

	f<<<grid_x, block_x, 0, stream>>>(args...);

	return cudaGetLastError();
}

template <int block_size, int grid_y, int grid_z, typename... Arguments>
inline cudaError_t launchSync(int thread_num, void(*f)(Arguments...), Arguments... args)
{
	int block_x = (thread_num > block_size) ? block_size : thread_num;
	dim3 grid;

	grid.x = (thread_num + block_x - 1) / block_x;
	grid.y = grid_y;
	grid.z = grid_z;

	f<<<grid, block_x>>>(args...);

	cudaError_t err = cudaGetLastError();

	if (err != cudaSuccess) {
		return err;
	}

	return cudaDeviceSynchronize();
}

template <int block_size, int grid_y, int grid_z, typename... Arguments>
inline cudaError_t launchASync(int thread_num, void(*f)(Arguments...), Arguments... args)
{
	int block_x = (thread_num > block_size) ? block_size : thread_num;
	dim3 grid;

	grid.x = (thread_num + block_x - 1) / block_x;
	grid.y = grid_y;
	grid.z = grid_z;

	f<<<grid, block_x>>>(args...);

	return cudaGetLastError();
}

// Functions to conversions between 3d indices and 1D indices
inline __host__ __device__ int id3ToId1(Vector3i id, Vector3i lb, Vector3i dim)
{
	return ((id.x - lb.x) + (id.y - lb.y) * dim.x + (id.z - lb.z) * dim.x * dim.y);
}

// Similar to above but the lower bounds of the grid are all zero
inline __host__ __device__ int id3ToId1(Vector3i id, Vector3i dim)
{
	return (id.x + id.y * dim.x + id.z * dim.x * dim.y);
}

inline __host__ __device__ int id3ToId1(int x, int y, int z, Vector3i lb, Vector3i dim)
{
	return ((x - lb.x) + (y - lb.y) * dim.x + (z - lb.z) * dim.x * dim.y);
}

inline __host__ __device__ int id3ToId1(int x, int y, int z, Vector3i dim)
{
	return (x + y * dim.x + z * dim.x * dim.y);
}

inline __host__ __device__ Vector3i id1ToId3(int id, Vector3i lb, Vector3i dim)
{
	Vector3i res;

	int tmp = dim.x * dim.y;

	res.z = id / tmp;
	res.x = id % dim.x;
	res.y = (id - res.x - res.z * tmp) / dim.x;

	return (res + lb);
}

inline __host__ __device__ Vector3i id1ToId3(int id, Vector3i dim)
{
	Vector3i res;

	int tmp = dim.x * dim.y;

	res.z = id / tmp;
	res.x = id % dim.x;
	res.y = (id - res.x - res.z * tmp) / dim.x;

	return res;
}

inline __host__ __device__ void id1ToId3(int id, Vector3i lb, Vector3i dim,
											int &x, int &y, int &z)
{
	int tmp = dim.x * dim.y;

	z = id / tmp;
	x = id % dim.x;
	y = (id - x - z * tmp) / dim.x;

	x += lb.x;
	y += lb.y;
	z += lb.z;
}

inline __host__ __device__ void id1ToId3(int id, Vector3i dim, int &x, int &y, int &z)
{
	int tmp = dim.x * dim.y;

	z = id / tmp;
	x = id % dim.x;
	y = (id - x - z * tmp) / dim.x;
}

inline __host__ __device__ void rangeCheck(int index, size_t range, const char *file_name,
											int line_no, const char *func_name)
{
	if (index < 0 || index >= range) {
		printf("[%s, %d] %s::Out-of-bound access: index = %d, range = %lu\n",
				file_name, line_no, func_name, index, range);
	}
}

#ifndef rangegassert
#define rangegassert(index, range, file_name)	(rangeCheck(index, range, file_name, __LINE__, __func__))
#endif




template <typename DevPtr>
__global__ void gpuCopy(DevPtr dst, DevPtr src, int ele_num)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		dst.emplace(i, src[i]);
	}
}

template <typename DevPtr>
void copy(DevPtr dst, DevPtr src, int ele_num)
{
	if (ele_num > 0) {
		checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, gpuCopy, dst, src, ele_num));
	}
}

template <typename T0, typename T1>
__global__ void fillGpu(T0 *data, int ele_num, T1 val)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		data[i] = val;
	}
}

template<typename T0, typename T1>
void fill(device_vector<T0> &data, T1 val)
{
	int ele_num = data.size();

	if (ele_num <= 0) {
		return;
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, fillGpu,
					data.data(), ele_num, val));
}

template<typename T0, typename T1>
void fill(matrix_vector<T0> &data, T1 val)
{
	int ele_num = data.size() * data.rowNum() * data.colNum();

	if (ele_num <= 0) {
		return;
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, fillGpu,
					data.data(), ele_num, val));
}



template <typename T0, typename T1>
__global__ void addGpu(T0 *data, int ele_num, T1 val)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		data[i] += val;
	}
}

template <typename T0, typename T1>
void add(device_vector<T0> &data, T1 val)
{
	int ele_num = data.size();

	if (ele_num <= 0) {
		return;
	}

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, addGpu,
					data.data(), ele_num, val));
}


template <typename T, typename CondType>
__global__ void markValidElement(T *input, int ele_num, CondType cond, int *mark)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		mark[i] = (cond(input[i])) ? 1 : 0;
	}
}

template <typename T>
__global__ void getValidElement(T *input, int ele_num, int *wloc, T *output)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		int loc = wloc[i];

		if (loc < wloc[i + 1]) {
			output[loc] = input[i];
		}
	}
}

/**
 * Stream compaction.
 * Evaluate every element of @input and extract ones
 * where the evaluation returns true. Also output
 * the starting pid of the input, which indicates
 * where the input elements located in the output.
 *
 * Params:
 * 		@input[in]: the input elements
 * 		@output[out]: the output elements where the
 * 					evaluations return true
 * 		@psum[out]: the starting pid vector of the output
 *
 * Returns: number of elements in the output
 */
template <typename T, typename CondType>
int streamCompaction(device_vector<T> &input, device_vector<T> &output,
						device_vector<int> &psum)
{
	output.clear();
	psum.clear();

	int ele_num = input.size();

	if (ele_num <= 0) {
		return 0;
	}

	psum.resize(ele_num + 1);

	fill(psum, 0);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, markValidElement,
					input.data(), ele_num, CondType(), psum.data()));

	int out_size = 0;

	ExclusiveScan(psum, out_size);

	if (out_size <= 0) {
		return 0;
	}

	output.resize(out_size);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, getValidElement,
					input.data(), ele_num, psum.data(), output.data()));

	return out_size;
}

/**
 * Stream compaction.
 * Evaluate elements in the @input vector using a condition
 * function and get the elements evaluated true under the
 * specified condition.
 *
 * Params:
 * 		@input[in]: the input vector to be evaluated
 * 		@output[out]: elements which is evaluated as true
 *
 * Returns: the number of elements in @output
 */
template <typename T, typename CondType>
int streamCompaction(device_vector<T> &input, device_vector<T> &output)
{
	GMemoryAllocator *allocator = input.__allocator;

	int ele_num = input.size();

	if (ele_num <= 0) {
		return 0;
	}

	device_vector<int> mark(allocator);

	return streamCompaction(input, output, mark);
}



template <typename DevPtr>
__global__ void getValidElement(DevPtr input, int *wloc, int ele_num, DevPtr output)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;

	for (int i = index; i < ele_num; i += stride) {
		int loc = wloc[i];

		if (loc < wloc[i + 1]) {
			output[loc] = input[i];
		}
	}
}

/**
 * Evaluate @stat of elements in @input using a condition.
 * Record the
 * The class Container must have the following member functions:
 *	- ptr(void): return a device-side pointer
 *	- resize(int size): reallocate the data in the container
 *
 * Params:
 */
template <typename Container, typename T, typename CondType>
int streamCompaction(Container &input, device_vector<T> &stat, Container &output)
{
	GMemoryAllocator *allocator = stat.__allocator;

	int ele_num = input.size();

	if (ele_num <= 0) {
		return 0;
	}

	device_vector<int> mark(ele_num + 1, allocator);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, markValidElement,
					stat.data(), ele_num, CondType(), mark.data()));

	int out_size = 0;

	ExclusiveScan(mark, out_size);

	if (out_size <= 0) {
		return 0;
	}

	output.resize(out_size);

	checkCudaErrors(launchSync<BLOCK_SIZE_X>(ele_num, getValidElement,
					input.data(), mark.data(), ele_num, output.data()));

	return out_size;
}
}


#endif
