#ifndef GDEBUG_H_
#define GDEBUG_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <iostream>

inline void gassert(cudaError_t err_code, const char *file, int line)
{
	if (err_code != cudaSuccess) {
		fprintf(stderr, "Error: %s %s %d\n", cudaGetErrorString(err_code), file, line);
		cudaDeviceReset();
		exit(EXIT_FAILURE);
	}
}

#define checkCudaErrors(err_code) gassert(err_code, __FILE__, __LINE__)

#ifndef timeDiff
#define timeDiff(start, end) ((end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec)
#endif

#define gfree(ptr)	{ if (ptr != nullptr) { checkCudaErrors(cudaFree(ptr)); ptr = nullptr; } }
#define galloc(ptr, size)	{ gfree(ptr); checkCudaErrors(cudaMalloc(&ptr, size)); }
#define gcopyHtoD(dst, src, size)	{ checkCudaErrors(cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice)); }
#define gcopyDtoH(dst, src, size)	{ checkCudaErrors(cudaMemcpy(dst, src, size, cudaMemcpyDeviceToHost)); }
#define gcopyDtoD(dst, src, size)	{ checkCudaErrors(cudaMemcpy(dst, src, size, cudaMemcpyDeviceToDevice)); }
#define gzero(ptr, size)	{ checkCudaErrors(cudaMemset(ptr, 0, size)); checkCudaErrors(cudaDeviceSynchronize()); }
#define gtime(val)	{ gettimeofday(&val, NULL); }
#define gcopyHtoDAsync(dst, src, size, stream)	{ checkCudaErrors(cudaMemcpyAsync(dst, src, size, cudaMemcpyHostToDevice, stream)); }
#define gcopyDtoHAsync(dst, src, size, stream)	{ checkCudaErrors(cudaMemcpyAsync(dst, src, size, cudaMemcpyDeviceToHost, stream)); }
#define gcopyDtoDAsync(dst, src, size, stream)	{ checkCudaErrors(cudaMemcpyAsync(dst, src, size, cudaMemcpyDeviceToDevice, stream)); }

inline void host_verify(bool res, const char *file, int line)
{
	if (!res) {
		fprintf(stderr, "Error: condition failed. %s %d\n", file, line);
		exit(EXIT_FAILURE);
	}
}

#ifndef verify
#define verify(cond)	host_verify(cond, __FILE__, __LINE__)
#endif

#endif
