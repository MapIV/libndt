#ifndef GPU_VECTOR3X_H_
#define GPU_VECTOR3X_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <float.h>

namespace gpu {

template <typename Scalar>
class Vector3x {
public:
	inline __host__ __device__ Vector3x() {
		x = y = z = 0;
	}

	inline __host__ __device__ Vector3x(Scalar ix, Scalar iy, Scalar iz) :
			x(ix), y(iy), z(iz) {}

	inline __host__ __device__ Vector3x(const Vector3x &other) {
		x = other.x;
		y = other.y;
		z = other.z;
	}

	inline __host__ __device__ Vector3x(Vector3x &&other) {
		x = other.x;
		y = other.y;
		z = other.z;
	}

	inline __host__ __device__ Vector3x &operator=(const Vector3x &other) {
		x = other.x;
		y = other.y;
		z = other.z;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator=(Vector3x &&other) {
		x = other.x;
		y = other.y;
		z = other.z;

		return *this;
	}

	// Access element i
	inline __host__ __device__ Scalar &operator[](const int i) {
		return (i == 0) ? x : ((i == 1) ? y : z);
	}

	inline __host__ __device__ const Scalar &operator[](const int i) const {
		return (i == 0) ? x : ((i == 1) ? y : z);
	}

	inline __host__ __device__ Scalar &X() { return x; }
	inline __host__ __device__ Scalar &Y() { return y; }
	inline __host__ __device__ Scalar &Z() { return z; }
	inline __host__ __device__ const Scalar &X() const { return x; }
	inline __host__ __device__ const Scalar &Y() const { return y; }
	inline __host__ __device__ const Scalar &Z() const { return z; }

	inline __host__ __device__ Vector3x operator+(const Vector3x &other) const {
		return Vector3x(x + other.x, y + other.y, z + other.z);
	}

	inline __host__ __device__ Vector3x operator-(const Vector3x &other) const {
		return Vector3x(x - other.x, y - other.y, z - other.z);
	}

	// Element-wise multiplication
	inline __host__ __device__ Vector3x operator*(const Vector3x &other) const {
		return Vector3x<Scalar>(x * other.x, y * other.y, z * other.z);
	}

	// Element-wise division. Note that this does not check 0-division so be careful!
	inline __host__ __device__ Vector3x operator/(const Vector3x &other) const {
		return Vector3x(x / other.x, y / other.y, z / other.z);
	}

	inline __host__ __device__ Vector3x operator%(const Vector3x &other) const;

	inline __host__ __device__ Vector3x operator*(Scalar k) const {
		return Vector3x(x * k, y * k, z * k);
	}

	inline __host__ __device__ Vector3x operator/(Scalar k) const {
		return Vector3x(x / k, y / k, z / k);
	}

	inline __host__ __device__ Vector3x operator%(Scalar k) const;

	// dot/inner product
	inline __host__ __device__ Scalar dot(const Vector3x &other) {
		return (x * other.x + y * other.y + z * other.z);
	}

	// cross/outer product
	inline __host__ __device__ Vector3x operator^(const Vector3x &other) {
		return Vector3x(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
	}

	inline __host__ __device__ Vector3x &operator+=(const Vector3x &other) {
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator-=(const Vector3x &other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return *this;
	}

	// Element-wise multiplication
	inline __host__ __device__ Vector3x &operator*=(const Vector3x &other) {
		x *= other.x;
		y *= other.y;
		z *= other.z;

		return *this;
	}

	// Element-wise division
	inline __host__ __device__ Vector3x &operator/=(const Vector3x &other) {
		x /= other.x;
		y /= other.y;
		z /= other.z;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator%=(const Vector3x &other);

	// Element-wise operators with Scalar operands
	inline __host__ __device__ Vector3x &operator*=(Scalar k) {
		x *= k;
		y *= k;
		z *= k;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator/=(Scalar k) {
		x /= k;
		y /= k;
		z /= k;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator%=(Scalar k);

	inline __host__ __device__ Vector3x &operator+=(Scalar k) {
		x += k;
		y += k;
		z += k;

		return *this;
	}

	inline __host__ __device__ Vector3x &operator-=(Scalar k) {
		x -= k;
		y -= k;
		z -= k;

		return *this;
	}

	inline __host__ __device__ Vector3x operator+(Scalar k) const {
		Vector3x res;

		res.x = x + k;
		res.y = y + k;
		res.z = z + k;

		return res;
	}

	inline __host__ __device__ Vector3x operator-(Scalar k) const {
		Vector3x res;

		res.x = x - k;
		res.y = y - k;
		res.z = z - k;

		return res;
	}



	// L2 norm - length
	inline __host__ __device__ Scalar norm() {
		return sqrt(x * x + y * y + z * z);
	}

	inline __host__ __device__ Scalar squaredNorm() {
		return (x * x + y * y + z * z);
	}

	// Normalize
	inline __host__ __device__ Vector3x &normalize() {
		Scalar l2_norm = norm();

		if (l2_norm > 0) {
			x /= l2_norm;
			y /= l2_norm;
			z /= l2_norm;
		}

		return *this;
	}

	inline __host__ __device__ bool operator==(const Vector3x &other) const {
		return (x == other.x && y == other.y && z == other.z);
	}

	inline __host__ __device__ bool operator!=(const Vector3x &other) const {
		return (x != other.x || y != other.y || z != other.z);
	}

	inline __host__ __device__ bool operator<(const Vector3x &other) const {
		return (x != other.x) ? (x < other.x) : ((y != other.y) ? y < other.y : (z < other.z));
	}

	inline __host__ __device__ bool operator>(const Vector3x &other) const {
		return (x != other.x) ? (x > other.x) : ((y != other.y) ? y > other.y : (z > other.z));
	}

	inline __host__ __device__ bool operator<=(const Vector3x &other) const {
		return (x != other.x) ? (x < other.x) : ((y != other.y) ? y < other.y : (z <= other.z));
	}

	inline __host__ __device__ bool operator>=(const Vector3x &other) const {
		return (x != other.x) ? (x > other.x) : ((y != other.y) ? y > other.y : (z >= other.z));
	}

	inline __host__ __device__ Vector3x operator-() const {
		return Vector3x(-x, -y, -z);
	}

	// type-cast
	template <typename Scalar2>
	explicit inline __host__ __device__ operator Vector3x<Scalar2>() const {
		return Vector3x<Scalar2>(static_cast<Scalar2>(x), static_cast<Scalar2>(y),
									static_cast<Scalar2>(z));
	}

	friend std::ostream &operator<<(std::ostream &os, const Vector3x& val) {
		os << val.x << "," << val.y << "," << val.z;

		return os;
	}

	Scalar x, y, z;
};

/**
 * Extract the greater components of both inputs.
 * 
 * Params:
 *		@a, b[in]: the two input vectors
 *
 * Returns: the vector that contains the greater components of both inputs.
 */
template <typename Scalar>
inline __host__ __device__ Vector3x<Scalar> max(const Vector3x<Scalar> &a, const Vector3x<Scalar> &b)
{
	Vector3x<Scalar> res;

	res.x = (a.x > b.x) ? a.x : b.x;
	res.y = (a.y > b.y) ? a.y : b.y;
	res.z = (a.z > b.z) ? a.z : b.z;

	return res;
}

/**
 * Extract the lesser components of both inputs.
 * 
 * Params:
 *		@a, b[in]: the two input vectors
 *
 * Returns: the vector that contains the lesser components of both inputs.
 */
template <typename Scalar>
inline __host__ __device__ Vector3x<Scalar> min(const Vector3x<Scalar> &a, const Vector3x<Scalar> &b)
{
	Vector3x<Scalar> res;

	res.x = (a.x < b.x) ? a.x : b.x;
	res.y = (a.y < b.y) ? a.y : b.y;
	res.z = (a.z < b.z) ? a.z : b.z;

	return res;
}

using Vector3f = class Vector3x<float>;
using Vector3i = class Vector3x<int>;
using Vector3u = class Vector3x<uint32_t>;
using Vector3d = class Vector3x<double>;

inline __host__ __device__ float distance(const Vector3f &a, const Vector3f &b)
{
	float x = a.x - b.x;
	float y = a.y - b.y;
	float z = a.z - b.z;

	return sqrt(x * x + y * y + z * z);
}

/**
 * Compute distance between a 3D point and a box.
 * The box is identified by a lower boundary @l
 * and upper boundary @u.
 * 
 * Params:
 * 		@q[in]: coordinate of the point
 * 		@l, u[in]: lower and upper boundaries of the box
 * Returns: distance between the point and the box
 */
inline __host__ __device__ float distance(const Vector3f &q, const Vector3f &l, const Vector3f &u)
{
	float dx, dy, dz;

	dx = (q.x < l.x) ? (l.x - q.x) : ((q.x > u.x) ? (q.x - u.x) : 0);
	dy = (q.y < l.y) ? (l.y - q.y) : ((q.y > u.y) ? (q.y - u.y) : 0);
	dz = (q.z < l.z) ? (l.z - q.z) : ((q.z > u.z) ? (q.z - u.z) : 0);

	return sqrtf(dx * dx + dy * dy + dz * dz);
}

template <>
inline __host__ __device__ Vector3i Vector3i::operator%(const Vector3i &other) const
{
	return Vector3i(x % other.x, y % other.y, z % other.z);
}

template <>
inline __host__ __device__ Vector3i Vector3i::operator%(int k) const
{
		return Vector3x(x % k, y % k, z % k);
}

template <>
inline __host__ __device__ Vector3i &Vector3i::operator%=(const Vector3i &other)
{
	x %= other.x;
	y %= other.y;
	z %= other.z;

	return *this;
}

template <>
inline __host__ __device__ Vector3i &Vector3i::operator%=(int k)
{
	x %= k;
	y %= k;
	z %= k;

	return *this;
}

}


#endif
