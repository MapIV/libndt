/* VoxelGrid2
 * The VoxelGrid2 uses a dense format to represent a voxel grid.
 * It requires too much GPU memory and causes the out-of-memory
 * error as the map becomes too big. The VoxelGrid2 uses a sparse-
 * matrix representation for the voxel grid. Hence it should be
 * more memory-saving than VoxelGrid2.
 *
 */
#ifndef GPU_VGRID2_H_
#define GPU_VGRID2_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <memory>

#include <iostream>
#include <fstream>
#include <float.h>
#include <vector>

#include <pcl/point_types.h>

#include "common.h"
#include "MatrixDevice.h"
#include "MatrixHost.h"
#include "Octree.h"
#include "device_vector.h"
#include "vector3x.h"
#include "point_cloud.h"

namespace gpu {

namespace vgrid2 {

class VoxelGrid2 : public Base {
public:
	typedef std::shared_ptr<VoxelGrid2> Ptr;
	typedef std::shared_ptr<const VoxelGrid2> ConstPtr;

	VoxelGrid2(GMemoryAllocator *allocator);

	VoxelGrid2(const VoxelGrid2 &other);

	VoxelGrid2(VoxelGrid2 &&other);

	VoxelGrid2& operator=(const VoxelGrid2 &other);

	VoxelGrid2& operator=(VoxelGrid2&& other);

	/* Set input points */
	template <typename PointType>
	void setInputCloud(typename PointCloud<PointType>::Ptr &cloud);

	void setMinVoxelSize(int size);

	/* For each input point, search for voxels whose distance between their centroids and
	 * the input point are less than radius.
	 */
	template <typename PointType>
	void radiusSearch(typename PointCloud<PointType>::Ptr &qcloud,
						float radius, int max_nn,
						device_vector<int> &starting_voxel_id,
						device_vector<int> &nn_voxel_id);

	void radiusSearch(FCloudPtr qcloud, float radius, int max_nn,
						device_vector<int> &starting_voxel_id,
						device_vector<int> &voxel_id);



	int getVoxelNum() const { return voxel_num_; }

	Vector3f getResolution() const { return res_; }
	Vector3i getUpperBound() const { return max_b_; }
	Vector3i getLowerBound() const { return min_b_; }
	Vector3i getGridDim() const { return vgdim_; }

	void setResolution(float res_x, float res_y, float res_z) {
		res_.x = res_x;
		res_.y = res_y;
		res_.z = res_z;
	}

	/* Get the centroid list. */
	device_vector<double> &getCentroidList() { return centroid_; }

	/* Get the pointer to the inverse covariances list. */
	matrix_vector<double> &getInverseCovarianceList() { return inverse_covariance_; }

	device_vector<int> &getPointsPerVoxelList() { return points_per_voxel_; }

	template <typename PointType>
	void update(typename PointCloud<PointType>::Ptr &new_cloud);

	// Print voxel's information
	void testVoxel(int vid);

	void memInfo() {
		size_t size_in_bytes = sizeInBytes();

		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) / 100.0 : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("***** VoxelGrid2:: Memory details *****\n");
		printf("\tmax_b_xyz = %d, %d, %d\n", max_b_.x, max_b_.y, max_b_.z);
		printf("\tmin_b_xyz = %d, %d, %d\n", min_b_.x, min_b_.y, min_b_.z);
		printf("\treal_max_bxyz = %d, %d, %d\n", rmax_b_.x, rmax_b_.y, rmax_b_.z);
		printf("\treal_min_bxyz = %d, %d, %d\n", rmin_b_.x, rmin_b_.y, rmin_b_.z);

		printf("VoxelGrid2::centroid_: %d elements, %.2f MB, %.2f%%\n", centroid_.size(),
				static_cast<float>(centroid_.sizeInBytes()) / to_mb,
				static_cast<float>(centroid_.sizeInBytes()) / to_percent);

		printf("VoxelGrid2::inverse_covariance_: %d elements, %.2f MB, %.2f%%\n", inverse_covariance_.size(),
				static_cast<float>(inverse_covariance_.sizeInBytes()) / to_mb,
				static_cast<float>(inverse_covariance_.sizeInBytes()) / to_percent);

		printf("VoxelGrid2::points_per_voxel_: %d elements, %.2f MB, %.2f%%\n", points_per_voxel_.size(),
				static_cast<float>(points_per_voxel_.sizeInBytes()) / to_mb,
				static_cast<float>(points_per_voxel_.sizeInBytes()) / to_percent);

		printf("VoxelGrid2::tmp_centroid_: %d elements, %.2f MB, %.2f%%\n", tmp_centroid_.size(),
				static_cast<float>(tmp_centroid_.sizeInBytes()) / to_mb,
				static_cast<float>(tmp_centroid_.sizeInBytes()) / to_percent);

		printf("VoxelGrid2::tmp_cov_: %d elements, %.2f MB, %.2f%%\n", tmp_cov_.size(),
				static_cast<float>(tmp_cov_.sizeInBytes()) / to_mb,
				static_cast<float>(tmp_cov_.sizeInBytes()) / to_percent);

	}

	/* Return the amount of occupied GPU memory in bytes */
	size_t sizeInBytes() {
		return (centroid_.sizeInBytes() + inverse_covariance_.sizeInBytes() +
				points_per_voxel_.sizeInBytes() + tmp_centroid_.sizeInBytes() + tmp_cov_.sizeInBytes());
	}

private:
	// Use when searching for a large query cloud (point number > BATCH_SIZE_)
	void radiusSearchLarge(FCloudPtr qcloud, float radius, int max_nn,
							device_vector<int> &starting_voxel_id,
							device_vector<int> &nn_voxel_id);

	// Use when searching for a small query cloud (point number <= BATCH_SIZE_)
	void radiusSearchSmall(FCloudPtr qcloud, float radius, int max_nn,
							device_vector<int> &starting_voxel_id,
							device_vector<int> &voxel_id);

	/* Allocate buffers */
	void allocBuffers(int voxel_num);

	int computeVoxelId(FCloudPtr cloud, Vector3i min_b, Vector3i vgdim,
						device_vector<int> &voxel_id);

	/* Compute centroids and covariances of voxels. */
	void computeCentroidAndCovariance(FCloudPtr cloud, device_vector<int> &starting_point_ids,
										device_vector<int> &point_ids);

	/**
	 * Initialize tmp_centroids_ and tmp_cov_.
	 * If the @updated_vid is empty, initialize all voxels
	 * in the grid. Otherwise, only updated the one indicated
	 * by @updated_vid.
	 *
	 * Params:
	 * 		@cloud[in]: the new input cloud
	 *		@starting_point_ids[in]: the starting indices of the
	 *				points that belong to each voxel to be updated.
	 * 		@point_ids[in]: the sorted indices of points to match
	 * 				the order of voxels.
	 * 		@updated_vid[in]: indices of voxels to be updated
	 */
	void initialize(FCloudPtr cloud, device_vector<int> &starting_point_ids,
					device_vector<int> &point_ids, device_vector<int> &updated_vid);

	/***
	 * Compute centroids_ of new voxels. The indices of
	 * new voxels are indicated by @updated_vid. If @updated_vid
	 * is empty, compute centroids_ of all voxels in the grid.
	 *
	 * Params:
	 * 		@updated_vid[in]: indices of voxels to be updated
	 */
	void updateCentroids(device_vector<int> &updated_vid);

	/**
	 * Find boundaries of the input point cloud and compute
	 * the number of necessary voxels as well as boundaries
	 * measured in number of leaf size
	 */
	void findBoundaries(FCloudPtr cloud, Vector3i &rmin_b, Vector3i &rmax_b,
						Vector3i &min_b, Vector3i &max_b, Vector3i &vgdim);

	void extendMemory(FCloudPtr new_cloud, Vector3i new_min_b, Vector3i new_max_b);

	void updateCentroidAndCovariance(FCloudPtr new_cloud, device_vector<int> &voxel_id,
										device_vector<int> &points_per_voxel,
										device_vector<int> &starting_point_ids,
										device_vector<int> &point_ids,
										Vector3i min_b, Vector3i vgdim);

	void scatterPointsToVoxelGrid(FCloudPtr cloud, Vector3i min_b, Vector3i vgdim,
									int non_empty_voxel_num, device_vector<int> &voxel_id,
									device_vector<int> &points_per_voxel,
									device_vector<int> &starting_point_ids,
									device_vector<int> &point_ids);

	void filterOutEmptyVoxels(device_vector<int> &points_per_voxel,
								device_vector<int> &starting_point_ids,
								device_vector<int> &voxel_id);

	/**
	 * Update the inverse covariances of new voxels. Indices
	 * of voxels to be updated are specified by updated_vid.
	 * If update_all == true, update all voxels in the grid.
	 *
	 * The update_vid is supposed to contains indices of voxels
	 * that have at least min_points_per_voxel_ points.
	 */
	void updateInverseCovariances(device_vector<int> &updated_vid);


	/**
	 * Among the voxels in input_vid, extract the ones which
	 * have at least min_points_per_voxel_ points. In the case
	 * input_vid is empty, examine all voxels in the grid.
	 *
	 * Params:
	 * 		@input_vid[in]: indices of input voxels
	 * 		@valid_vid[out]: indices of valid voxels
	 */
	void extractValidVoxels(device_vector<int> &input_vid, device_vector<int> &valid_vid);


	/**
	 * voxel_id_[i] is the index of data structures of voxel i
	 * in the followed vectors (centroid_, inverse_cov, etc).
	 */
	device_vector<int> voxel_id_;

	/* Centroids of non-empty voxels.
	 * List of 3x1 double vectors.
	 */
	device_vector<double> centroid_;

	/* Inverse covariance of non-empty voxels.
	 * List of 3x3 double matrices.
	 */
	matrix_vector<double> inverse_covariance_;	// List of 3x3 double matrix

	/* Number of points per non-empty voxels */
	device_vector<int> points_per_voxel_;

	int voxel_num_;		// Number of NON-EMPTY voxels
	Vector3f res_;		// Resolution, a.k.a, size of each voxel

	Vector3i max_b_;	// Upper bounds of the grid, measured in number of voxels
	Vector3i min_b_;	// Lower bounds of the grid, measured in number of voxels
	Vector3i vgdim_;	// Size of the voxel grid, measured in number of voxels
	int min_points_per_voxel_;

	Vector3i rmax_b_, rmin_b_;	// Real upper and lower bounds of the grid

	// Added for incremental ndt
	/* Tmp centroid and covariance of non-empty voxels */
	matrix_vector<double> tmp_centroid_;	// List of 3x1 double vectors
	matrix_vector<double> tmp_cov_;			// List of 3x3 double matrix

	static const int BLOCK_X_ = 16;
	static const int BLOCK_Y_ = 16;
	static const int BLOCK_Z_ = 8;
	static const int BATCH_SIZE_ = 131072;
};

}

}

#endif
