#ifndef HOST_POINT_CLOUD_H_
#define HOST_POINT_CLOUD_H_

#include <cuda.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <vector>
#include <memory>
#include <list>

#include "common.h"
#include "device_vector.h"
#include "vector3x.h"

namespace gpu {

template <typename Scalar = float> class CloudPtr;

template <typename PointType, typename Scalar = float>
class PointCloud : public Base {
	friend class CloudPtr<Scalar>;

public:
	typedef std::shared_ptr<PointCloud<PointType>> Ptr;
	typedef std::shared_ptr<const PointCloud<PointType>> ConstPtr;

	PointCloud(GMemoryAllocator *allocator) :
		Base(allocator),
		x_(__allocator),
		y_(__allocator),
		z_(__allocator)
	{
		point_num_ = 0;
	}

	PointCloud(const PointCloud &other) :
		Base(other),
		x_(other.x_),
		y_(other.y_),
		z_(other.z_)
	{
		point_num_ = other.point_num_;
	}

	PointCloud(PointCloud &&other) :
		Base(other),
		x_(std::move(other.x_)),
		y_(std::move(other.y_)),
		z_(std::move(other.z_))
	{
		point_num_ = other.point_num_;
	}

	/**
	 * Assignment does not change the reserved size.
	 * In the case the reserve size of the cloud is
	 * less than the input's size, reallocate the
	 * vectors.
	 */
	PointCloud &operator=(const PointCloud &other) {
		int reserved_size = x_.size();
		int other_size = other.x_.size();

		if (reserved_size < other_size) {
			x_ = other.x_;
			y_ = other.y_;
			z_ = other.z_;
		} else {
			gcopyDtoD(x_.data(), other.x_.data(), sizeof(Scalar) * other.point_num_);
			gcopyDtoD(y_.data(), other.y_.data(), sizeof(Scalar) * other.point_num_);
			gcopyDtoD(z_.data(), other.z_.data(), sizeof(Scalar) * other.point_num_);
		}

		point_num_ = other.point_num_;

		return *this;
	}

	PointCloud &operator=(PointCloud &&other) {
		x_ = std::move(other.x_);
		y_ = std::move(other.y_);
		z_ = std::move(other.z_);

		point_num_ = other.point_num_;

		return *this;
	}

	void setInputCloud(typename pcl::PointCloud<PointType>::Ptr &input_cloud);

	void setInputCloud();

	void setInputCloud(typename std::list<typename pcl::PointCloud<PointType>::Ptr> &input_clouds);

	const device_vector<Scalar> &X() const { return x_; }
	const device_vector<Scalar> &Y() const { return y_; }
	const device_vector<Scalar> &Z() const { return z_; }

	device_vector<Scalar> &X() { return x_; }
	device_vector<Scalar> &Y() { return y_; }
	device_vector<Scalar> &Z() { return z_; }

	int size() const { return point_num_; }

	void clear();
	void resize(int new_point_num);
	void reserve(int new_size);

	CloudPtr<Scalar> data();

	typename pcl::PointCloud<PointType>::Ptr host_cloud() {
		typename pcl::PointCloud<PointType>::Ptr output(new pcl::PointCloud<PointType>);

		output->resize(point_num_);

		std::vector<Scalar> x(point_num_), y(point_num_), z(point_num_);

		gcopyDtoH(x.data(), x_.data(), sizeof(Scalar) * point_num_);
		gcopyDtoH(y.data(), y_.data(), sizeof(Scalar) * point_num_);
		gcopyDtoH(z.data(), z_.data(), sizeof(Scalar) * point_num_);

		for (size_t i = 0; i < point_num_; ++i) {
			PointType p;

			p.x = x[i];
			p.y = y[i];
			p.z = z[i];

			(*output)[i] = p;
		}

		return output;
	}

private:
	device_vector<Scalar> x_, y_, z_;	// points' coordinates

	/**
	 * The number of points may be different from the
	 * length of xyz vector. This is because xyz are
	 * over-allocated so when the cloud is resize, the
	 * vectors do not have to be re-allocated frequently.
	 */
	int point_num_;
};

/**
 * Device-side pointer to a point cloud
 */
template <typename Scalar>
class CloudPtr {
public:
	inline __host__ __device__ CloudPtr() {
		x_ = y_ = z_ = nullptr;
		point_num_ = 0;
	}

	inline __host__ __device__ CloudPtr(Scalar *x, Scalar *y, Scalar *z, int point_num) {
		x_ = x;
		y_ = y;
		z_ = z;
		point_num_ = point_num;
	}

	inline __host__ __device__ CloudPtr(const CloudPtr<Scalar> &other) {
		x_ = other.x_;
		y_ = other.y_;
		z_ = other.z_;

		point_num_ = other.point_num_;
	}

	inline __host__ __device__ CloudPtr(CloudPtr<Scalar> &&other) {
		x_ = other.x_;
		y_ = other.y_;
		z_ = other.z_;

		point_num_ = other.point_num_;
	}

	inline __host__ __device__ CloudPtr<Scalar> &operator=(const CloudPtr<Scalar> &other) {
		x_ = other.x_;
		y_ = other.y_;
		z_ = other.z_;

		point_num_ = other.point_num_;

		return *this;
	}

	inline __host__ __device__ CloudPtr<Scalar> &operator=(CloudPtr<Scalar> &&other) {
		x_ = other.x_;
		y_ = other.y_;
		z_ = other.z_;

		point_num_ = other.point_num_;

		return *this;
	}

	inline __host__ __device__ CloudPtr<Scalar> &operator+=(int offset) {
		if (point_num_ > offset) {
			x_ += offset;
			y_ += offset;
			z_ += offset;

			point_num_ = point_num_ - offset;
		} else {
			x_ = y_ = z_ = nullptr;
			point_num_ = 0;
		}

		return *this;
	}

	inline __device__ Vector3x<Scalar> operator[](int pid) {
		return Vector3x<Scalar>(x_[pid], y_[pid], z_[pid]);
	}

	// Insert a point to a location
	template <typename OtherScalar>
	inline __device__ void emplace(int loc, Vector3x<OtherScalar> val) {
		x_[loc] = val.X();
		y_[loc] = val.Y();
		z_[loc] = val.Z();
	}

	inline __host__ __device__ int size() const {
		return point_num_;
	}

	inline __host__ __device__ Scalar *X() { return x_; }
	inline __host__ __device__ Scalar *Y() { return y_; }
	inline __host__ __device__ Scalar *Z() { return z_; }

	inline __host__ __device__ CloudPtr<Scalar> subCloud(int pos, int length = -1)
	{
		length = (length < 0 || length >= point_num_) ? point_num_ - pos : length;

		return CloudPtr<Scalar>(x_ + pos, y_ + pos, z_ + pos, length);
	}

private:
	Scalar *x_, *y_, *z_;
	int point_num_;
};

template <typename PointType, typename Scalar>
CloudPtr<Scalar> PointCloud<PointType, Scalar>::data()
{
	return CloudPtr<Scalar>(x_.data(), y_.data(), z_.data(), point_num_);
}

template <typename Scalar>
inline __host__ __device__ CloudPtr<Scalar> operator+(const CloudPtr<Scalar> &input, int offset)
{
	CloudPtr<Scalar> res(input);

	res += offset;

	return res;
}

using FCloudPtr = CloudPtr<float>;
using DCloudPtr = CloudPtr<double>;

}

#endif
