#ifndef GPU_MEM_ALLOCATOR_H_
#define GPU_MEM_ALLOCATOR_H_

#include <iostream>
#include <list>
#include <utility>
#include <cuda.h>
#include <string>
#include <iterator>
#include <tuple>
#include <memory>
#include <map>

#include "debug.h"

#define MAX_MEMORY_BLOCK_SIZE_	(1073741824)

namespace gpu {

enum gmemErr_t
{
  gmemSuccess = 0,
  gmemErrOutOfMemory,
  gmemErrInvalidPointer,


  gmemErrUnknown
};

inline std::string gmemGetErrorString(gmemErr_t code)
{
	std::string retval;

	switch (code) {
    case gmemSuccess:
    	retval = "Succeeded!";

    	break;
    case gmemErrOutOfMemory:
    	retval = "Out of GPU memory";

    	break;
    case gmemErrInvalidPointer:
    	retval = "Invalid pointer";

    	break;
    case gmemErrUnknown:
    	retval = "Unknown error";

    	break;
	}

	return retval;
}

inline void gmemAssert(gmemErr_t err_code, const char* file, int line)
{
	if (err_code != gmemSuccess) {
		fprintf(stderr, "Error: %s %s %d\n", gmemGetErrorString(err_code).c_str(), file, line);
		cudaDeviceReset();
		exit(EXIT_FAILURE);
	}
}

#define checkGmemErrors(err_code) gmemAssert(err_code, __FILE__, __LINE__)

class GMemoryAllocator {
private:

	/**
	 * Each Block is a large-size pre-allocated GPU buffer from which
	 * allocation/free requests are served. By using this, we can void
	 * cudaMalloc/Free, which are much slower.
	 */
	class GMemoryBlock {
	public:
		// Each chunk is a tuple of (address, occupied/free, length) of a memory chunk
		typedef std::tuple<double*, bool, uint64_t> ChunkType;

		// A memory map is a list of chunk
		typedef std::list<std::tuple<double*, bool, uint64_t>> MemoryMap;
		typedef MemoryMap::iterator MemoryMapItr;

		/**
		 * The FreeMap holds the free memory chunks in the MemoryMap
		 * FreeMap is used when allocating a new buffer with a given size.
		 * From FreeMap, the system looks for a smallest chunk which is
		 * greater than the given size. A buffer is 'cut' from the chunk
		 * and is assigned to the new buffer.
		 *
		 * Key of a FreeMap element is the length of a chunk and the value
		 * is the iterator to the chunk in the MemoryMap.
		 *
		 * Because chunks may have the same length, std::multimap is used.
		 */
		typedef std::multimap<uint64_t, MemoryMapItr> FreeMap;
		typedef FreeMap::iterator FreeMapItr;

		/**
		 * The BusyMap holds the occupied memory chunks in the MemoryMap.
		 * It is used when freeing a buffer. From BusyMap, the system searches
		 * for the given pointer, marks it as free and remove it from the
		 * BusyMap.
		 *
		 * Key is the address of a chunk, value is the iterator to the chunk
		 * in the MemoryMap.
		 */
		typedef std::map<double*, MemoryMapItr> BusyMap;
		typedef BusyMap::iterator BusyMapItr;

		GMemoryBlock() {
			memory_pool_ = nullptr;

			galloc(memory_pool_, MAX_MEMORY_BLOCK_SIZE_);
			mem_map_ = new MemoryMap;
			free_map_ = new FreeMap;
			busy_map_ = new BusyMap;

			block_remaining_size_ = new uint64_t(MAX_MEMORY_BLOCK_SIZE_);
			mem_map_->push_back(std::make_tuple(memory_pool_, true, MAX_MEMORY_BLOCK_SIZE_ / sizeof(double)));
			free_map_->insert(std::pair<uint64_t, MemoryMapItr>(MAX_MEMORY_BLOCK_SIZE_ / sizeof(double), mem_map_->begin()));
			ref_count_ = new unsigned int;

			*ref_count_ = 1;
		}

		GMemoryBlock(const GMemoryBlock &other) {
			memory_pool_ = other.memory_pool_;

			/***
			 * Each chunk contains a head address, availability(true - available/
			 * false - occupied), and size (in number of double elements)
			 */
			mem_map_ = other.mem_map_;
			free_map_ = other.free_map_;
			busy_map_ = other.busy_map_;

			block_remaining_size_ = other.block_remaining_size_;
			ref_count_ = other.ref_count_;

			++(*ref_count_);
		}

		GMemoryBlock(GMemoryBlock &&other) {
			memory_pool_ = other.memory_pool_;

			mem_map_ = other.mem_map_;
			free_map_ = other.free_map_;
			busy_map_ = other.busy_map_;

			block_remaining_size_ = other.block_remaining_size_;
			ref_count_ = other.ref_count_;

			other.mem_map_ = nullptr;
			other.memory_pool_ = nullptr;
			other.block_remaining_size_ = nullptr;
			other.free_map_ = nullptr;
			other.busy_map_ = nullptr;

			++(*ref_count_);
		}

		GMemoryBlock& operator=(const GMemoryBlock &other) {
			release();

			memory_pool_ = other.memory_pool_;

			mem_map_ = other.mem_map_;
			free_map_ = other.free_map_;
			busy_map_ = other.busy_map_;

			block_remaining_size_ = other.block_remaining_size_;
			ref_count_ = other.ref_count_;

			++(*ref_count_);

			return *this;
		}

		GMemoryBlock& operator=(GMemoryBlock &&other) {
			release();

			memory_pool_ = other.memory_pool_;
			mem_map_ = other.mem_map_;
			free_map_ = other.free_map_;
			busy_map_ = other.busy_map_;

			block_remaining_size_ = other.block_remaining_size_;
			ref_count_ = other.ref_count_;

			other.memory_pool_ = nullptr;
			other.mem_map_ = nullptr;
			other.free_map_ = nullptr;
			other.busy_map_ = nullptr;
			other.block_remaining_size_ = nullptr;

			++(*ref_count_);

			return *this;
		}


		/**
		 * Allocate a GPU buffer.
		 *
		 * Params:
		 * 		@ptr[out]: pointer of a pointer pointing to the allocated buffer
		 * 		@size[in]: the requested size
		 *
		 * Returns: gmemSuccess if the size is 0 or a buffer is allocated. If
		 * the allocation failed, an error code is returned.
		 */
		gmemErr_t gmemAlloc(void **ptr, uint64_t size) {
			if (size == 0) {
				*ptr = nullptr;

				return gmemSuccess;
			}

			// Compute the length of the buffer to be allocated (as number of doubles)
			uint64_t alloc_size = computeAlignedSize(size);

			// Find the smallest chunks that is greater than the length needed
			FreeMapItr free_chunk = free_map_->lower_bound(alloc_size);

			// If a chunk was found
			if (free_chunk != free_map_->end()) {
				// Retrieve its information from the chunk list
				MemoryMapItr it = free_chunk->second;

				// Assign the head address of the chunk to the output pointer
				*ptr = std::get<0>(*it);

				// Mark the chunk as not available
				std::get<1>(*it) = false;

				// Remove the chunk from free_map_
				free_map_->erase(free_chunk);

				// Add free_chunk to the busy_map_
				busy_map_->insert(std::pair<double*, MemoryMapItr>(std::get<0>(*it), it));

				/**
				 * If size of the chunk is larger than alloc_size, 'cut' the chunk
				 * into two pieces. One piece contains the head address and has
				 * length as alloc_size. The other piece is the remain of the chunk.
				 * The first piece is already given the @ptr and was already inserted
				 * to the busy_map_. The other piece is inserted to the free_map_.
				 */
				if (std::get<2>(*it) > alloc_size) {
					// Create a new piece from the free part of the chunk
					ChunkType new_chunk(std::get<0>(*it) + alloc_size, true, std::get<2>(*it) - alloc_size);

					// Insert the new chunk to next to the old chunk
					MemoryMapItr new_it = mem_map_->insert(std::next(it), new_chunk);

					// Add the new chunk to the free_map_
					free_map_->insert(std::pair<uint64_t, MemoryMapItr>(std::get<2>(*new_it), new_it));

					// Since the old chunk was cut, its size must be shrunk
					std::get<2>(*it) = alloc_size;
				}

				// Reduce the remaining size of the block
				// The size of chunks are measured in the number of doubles
				// so they must be converted to bytes.
				*block_remaining_size_ -= std::get<2>(*it) * sizeof(double);

				return gmemSuccess;
			}

			// If no appropriate chunk was found, return out-of-memory error
			return gmemErrOutOfMemory;
		}

		/**
		 * Free a buffer.
		 * The chunk containing that buffer is marked as free
		 * and could be merged to its adjacent chunks if those
		 * chunks are free too.
		 *
		 * Params:
		 * 		@ptr[in]: pointer to the buffer to be freed
		 *
		 * Returns: gmemSuccess if the free is done or ptr is nullptr,
		 * or an error if the ptr does not exist.
		 */
		gmemErr_t gmemFree(void *ptr) {
			if (ptr == nullptr) {
				return gmemSuccess;
			}

			// Find @ptr in the busy_map_
			BusyMapItr busy_it = busy_map_->find((double*)ptr);

			// If @ptr was found
			if (busy_it != busy_map_->end()) {
				// Get the iterator of the chunk of @ptr in the mem_map_
				MemoryMapItr it = busy_it->second;

				// If the chunk's status is true, meaning it is free, then the request was wrong
				if (std::get<1>(*it)) {
					return gmemErrInvalidPointer;
				}

				// Since the chunk is free then, increase the size of available memory of the block
				*block_remaining_size_ += std::get<2>(*it) * sizeof(double);

				// Remove the busy iterator of @ptr (busy_it) from busy_map_
				busy_map_->erase(busy_it);

				// Mark the chunk as free in the chunk list
				std::get<1>(*it) = true;

				/**
				 * If we just stop here, the memory pool will likely become
				 * fragmented since it contains a lots of small chunks. It may
				 * not be possible to find space for a large request later. To
				 * avoid such situation, adjacent free chunks are merged to a
				 * single large free chunk.
				 */
				// Attempt to merge the chunk with its preceded chunk
				if (it != mem_map_->begin()) {
					MemoryMapItr prev_it = std::prev(it);

					// If prev_it is free, merge it with the current chunk
					if (std::get<1>(*prev_it)) {
						it = mergeFreeChunks(prev_it, it);
					}
				}

				// Attempt to merge the chunk with the following chunk std::next(it)
				if (it != mem_map_->end()) {
					MemoryMapItr next_it = std::next(it);

					/***
					 * If the following chunk is not the end of the mem_map_, and
					 * it is free too, then we merge it with the current chunk.
					 */
					if (next_it != mem_map_->end() && std::get<1>(*next_it)) {
						it = mergeFreeChunks(it, next_it);
					}
				}

				// Insert it to free_map_
				free_map_->insert(std::pair<uint64_t, MemoryMapItr>(std::get<2>(*it), it));

				return gmemSuccess;
			}

			return gmemErrInvalidPointer;
		}

		/**
		 * Find the smallest chunk that is larger than or
		 * equal to @size from the BLock.
		 *
		 * Params:
		 * 		@size[in]: a requested size
		 *
		 * Returns: size of the smallest chunk (in bytes) that
		 * is larger or equal to @size, or 0 if no such chunk
		 * exists.
		 */
		uint64_t getAvailableSize(uint64_t size) {
			uint64_t alloc_size = computeAlignedSize(size);

			FreeMapItr it = free_map_->lower_bound(alloc_size);

			if (it != free_map_->end()) {
				return it->first * sizeof(double);
			} else {
				return 0;
			}
		}

		/**
		 * Returns: the total remaining free size of the Block.
		 */
		uint64_t getRemainingSize() {
			return *block_remaining_size_;
		}

		/**
		 * Return true if the block has no occupied chunk and false otherwise.
		 */
		bool isEmpty() {
			return ((*block_remaining_size_) == MAX_MEMORY_BLOCK_SIZE_);
		}

		/**
		 * Search for a pointer busy_map_ and return the size of the
		 * allocated buffer. Return 0 if the pointer was not found.
		 */
		uint64_t query(void *ptr) {
			BusyMapItr busy_it = busy_map_->find((double*)ptr);

			if (busy_it != busy_map_->end()) {
				MemoryMapItr it = busy_it->second;

				if (!std::get<1>(*it)) {
					return std::get<2>(*it);
				}
			}

			return 0;
		}

		/**
		 * Delete everything of the Block.
		 */
		~GMemoryBlock() {
			release();
		}

		/**
		 * Convert an input size to number of double elements.
		 * Because the pool is a buffer of double elements, this
		 * conversion make the later allocation steps more simple.
		 *
		 * Params:
		 * 		@size[in]: the size (requested by an application)
		 *
		 * Return: the number of double elements
		 */
		static inline uint64_t computeAlignedSize(uint64_t size) {
			return (((size - 1) / (sizeof(double) * 2) + 1) * 2);
		}

	private:

		/**
		 * Merge @next to @prev to create a single chunk having
		 * the size equal to the size of @prev plus @next. Only
		 * free continuous chunks can be merged.
		 *
		 * The merge process is simple:
		 * - First, we remove @prev and @next from @free_map_ because
		 *   their sizes will change later.
		 * - Then, we update @prev size += @next size in the chunk list
		 * - Finally, we remove @next from the chunk list because it
		 *   does not exist anymore.
		 *
		 * Params:
		 * 		@prev[in/out]: the iterator (on the chunk list) of a
		 * 						chunk to which @next will be merged to.
		 * 		@next[in]: the iterator (on the chunk list) of a chunk
		 * 						to be merged to @prev
		 * Returns: iterator to @prev
		 */
		MemoryMapItr mergeFreeChunks(MemoryMapItr &prev, MemoryMapItr &next) {
			// Remove prev from free_map since its size will change after merging with @next
			// Because the free_map_ is a multimap, we get the list of chunks having same size as @prev
			std::pair<FreeMapItr, FreeMapItr> range = free_map_->equal_range(std::get<2>(*prev));

			// Then check them one by one to find the iterator of @prev in free_map_ and remove it
			for (FreeMapItr it = range.first; it != range.second; ++it) {
				if (it->second == prev) {
					free_map_->erase(it);
					break;
				}
			}

			// Because next will not exist after being merged, it shoud be removed from free_map_ too
			// Remove next from free_map by the same way
			range = free_map_->equal_range(std::get<2>(*next));

			for (FreeMapItr it = range.first; it != range.second; ++it) {
				if (it->second == next) {
					free_map_->erase(it);
					break;
				}
			}

			// Update size of @prev
			std::get<2>(*prev) += std::get<2>(*next);

			// Remove next from the chunk list
			mem_map_->erase(next);

			return prev;
		}

		// Delete everything of the block
		void release() {
			--(*ref_count_);

			if (*ref_count_ == 0) {
				gfree(memory_pool_);
				delete mem_map_;
				delete free_map_;
				delete busy_map_;
				delete block_remaining_size_;

				delete ref_count_;
			}

			memory_pool_ = nullptr;
			mem_map_ = nullptr;
			free_map_ = nullptr;
			busy_map_ = nullptr;
			block_remaining_size_ = nullptr;
			ref_count_ = nullptr;
		}

		/* Actual pre-allocated GPU memory pool */
		double *memory_pool_;

		/* List of chunks. Each chunk contains a head address, availability(true/false),
		 * and size (in number of double elements).
		 */
		MemoryMap *mem_map_;

		/* Multimap of free chunks. Key is size of chunk. Value is the MemoryMapItr
		 * that points to the chunk in mem_map_
		 */
		FreeMap	*free_map_;

		/* Map of busy chunks. Key is the address of the chunk. Value is the
		 * MemoryMapItr that points to the chunk in mem_map_
		 */
		BusyMap *busy_map_;

		uint64_t *block_remaining_size_;
		unsigned int *ref_count_;
	};

public:
	typedef std::shared_ptr<GMemoryAllocator> Ptr;

	GMemoryAllocator();

	GMemoryAllocator(const GMemoryAllocator &other);

	GMemoryAllocator(GMemoryAllocator &&other);

	GMemoryAllocator &operator=(const GMemoryAllocator &other);

	GMemoryAllocator &operator=(GMemoryAllocator &&other);

	/**
	 * Allocate a buffer. From the list of blocks, find a block
	 * that have smallest available size that is larger than
	 * or equal to size and allocate memory from that block.
	 *
	 * Params:
	 * 		@ptr[out]: pointer of pointer to the allocated memory
	 * 		@size[in]: the requested size of the buffer
	 * Returns: gmemSuccess if successfully allocate memory, or
	 * an error if failed.
	 */
	gmemErr_t gmemAlloc(void **ptr, uint64_t size);

	/**
	 * Search for @ptr from all blocks. If @ptr was
	 * found in a block, free @ptr from that block. Also,
	 * if after freeing the block is empty then free the
	 * block too.
	 *
	 * Params:
	 * 		@ptr[in]: pointer to a buffer to be freed
	 * Return: gmemSuccess if succeeded, or an gmemErrrInvalidPointer
	 * if @ptr was not found.
	 */
	gmemErr_t gmemFree(void *ptr);

	template <typename T>
	gmemErr_t gmemAlloc(T **ptr, uint64_t size) {
		return gmemAlloc((void **)ptr, size);
	}

	/**
	 * Check if a pointer exists in the current allocated buffers.
	 *
	 * Params:
	 * 		@ptr[in]: the pointer to a buffer to be queried
	 * Return: true if @ptr exists, or false otherwise
	 */
	bool query(void *ptr) {
		if (ptr == nullptr || ptr == NULL) {
			return false;
		}

		for (auto &b : *blocks_) {
			uint64_t val = b.query(ptr);

			if (val > 0) {
				return true;
			}
		}

		return false;
	}

	~GMemoryAllocator();
private:

	/**
	 * Reset the blocks_ pointer.
	 */
	void release();

	std::shared_ptr<std::list<GMemoryBlock>> blocks_;
};

}

#endif
