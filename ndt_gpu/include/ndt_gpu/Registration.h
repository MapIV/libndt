#ifndef GNDT_H_
#define GNDT_H_

#include <cuda.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include "common.h"
#include "MatrixHost.h"
#include "device_vector.h"
#include "point_cloud.h"
#include "vector3x.h"


namespace gpu {
template <typename PointSourceType, typename PointTargetType, typename Scalar = float>
class Registration : public Base {
public:
	using Base::__allocator;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Registration(GMemoryAllocator *allocator);
	Registration(const Registration<PointSourceType, PointTargetType, Scalar> &other);

	void align(const Eigen::Matrix<float, 4, 4> &guess);

	void align(pcl::PointCloud<PointTargetType> &output, const Eigen::Matrix<float, 4, 4> &guess)
	{
		align(guess);
	}

	void setTransformationEpsilon(double trans_eps) { transformation_epsilon_ = trans_eps; }

	double getTransformationEpsilon() const { return transformation_epsilon_; }

	void setMaximumIterations(int max_itr) { max_iterations_ = max_itr; }

	int getMaximumIterations() const { return max_iterations_; }

	Eigen::Matrix<float, 4, 4> getFinalTransformation() const;

	/**
	 * Set input Scanned point cloud.
	 * Copy input points from the main memory to the GPU memory
	 */
	void setInputSource(typename pcl::PointCloud<PointSourceType>::Ptr &input);

	void setInputSource(typename PointCloud<PointSourceType>::Ptr &input);


	/* Set input reference map point cloud.
	 * Copy input points from the main memory to the GPU memory */
	void setInputTarget(typename pcl::PointCloud<PointTargetType>::Ptr &input);

	void setInputTarget(PointCloud<PointTargetType> &input);

	int getFinalNumIteration() const { return nr_iterations_; }

	bool hasConverged() const { return converged_; }

	size_t sizeInBytes() {
		return (source_cloud_->size() * sizeof(Vector3f) +
				trans_cloud_->size() * sizeof(Vector3f));
	}

	void memInfo() {
		size_t size_in_bytes = sizeInBytes();
		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) / 100.0 : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("\nRegistration's Memory\n");
		printf("Registration::xyz_: %d elements, %.2f MB, %.2f%%\n", source_cloud_->size(),
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_percent);

		printf("Registration::trans_xyz_: %d elements, %.2f MB, %.2f%%\n\n", trans_cloud_->size(),
				static_cast<float>(trans_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(trans_cloud_->size() * sizeof(Vector3f)) / to_percent);
	}

	// Functions for copying MatrixHost 4x4 to Eigen::Matrix<float, 4, 4>
	void copyToEigen(Eigen::Matrix<float, 4, 4> &output, MatrixHost<float> &input) {
		for (int r = 0; r < 4; ++r) {
			for (int c = 0; c < 4; ++c) {
				output(r, c) = input(r, c);
			}
		}
	}

	void copyFromEigen(MatrixHost<float> &output, Eigen::Matrix<float, 4, 4> &input) {
		for (int r = 0; r < 4; ++r) {
			for (int c = 0; c < 4; ++c) {
				output(r, c) = input(r, c);
			}
		}

		output.syncGpu();
	}

	virtual ~Registration() {}

	virtual void computeTransformation(const Eigen::Matrix<float, 4, 4> &guess);

	double transformation_epsilon_;
	int max_iterations_;

	//Source clouds
	typename PointCloud<PointSourceType>::Ptr source_cloud_;

	//Transformed source clouds
	typename PointCloud<PointSourceType>::Ptr trans_cloud_;

	bool converged_;
	int nr_iterations_;

	MatrixHost<float> final_transformation_, transformation_, previous_transformation_;	// 4x4 matrices
};


template class Registration<pcl::PointXYZ, pcl::PointXYZ>;
template class Registration<pcl::PointXYZI, pcl::PointXYZI>;



}

#endif
