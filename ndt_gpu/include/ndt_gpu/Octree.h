#ifndef GPU_OCTREE_H_
#define GPU_OCTREE_H_

#include <cuda.h>
#include <vector>

#include "common.h"
#include "debug.h"
#include "device_vector.h"
#include "point_cloud.h"

namespace gpu {
namespace octree {

template <typename PointType> class HostOctree;

template <typename PointType>
class Octree : public Base {
	friend class HostOctree<PointType>;
public:
	typedef std::shared_ptr<Octree<PointType>> Ptr;
	typedef std::shared_ptr<const Octree<PointType>> ConstPtr;

	Octree(GMemoryAllocator *allocator);

	Octree(const Octree<PointType> &other);
	Octree(Octree<PointType> &&other);

	Octree<PointType> &operator=(const Octree<PointType> &other);
	Octree<PointType> &operator=(Octree<PointType> &&other);

	/* Set input cloud.
	 *
	 * Input:
	 * 		@input[in]: the pointer to the input GPU-side point cloud
	 */
	void setInputCloud(typename PointCloud<PointType>::Ptr &input);

	/* Search for the points (qx, qy, qz), return the index of
	 * the result point in nn_pid and the corresponding distances
	 * in nn_dist */
	void NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
					device_vector<int> &nn_pid, device_vector<float> &nn_dist);

	void NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
					device_vector<float> &nn_dist);

	void kNNSearch(typename PointCloud<PointType>::Ptr &qcloud, int k, device_vector<int> &result);

	/**
	 * Update the tree content when new points are added to the cloud.
	 * 3 major steps:
	 * - Compute new boundaries
	 * - Extend the buffers used for the tree if boundaries changed
	 * - Update the tree's content with new points
	 */
	void update(typename PointCloud<PointType>::Ptr &new_cloud, int point_num);

	size_t sizeInBytes() {
		return (cloud_->size() * sizeof(Vector3f) + status_.sizeInBytes() + level_id_.sizeInBytes()
				+ level_dim_.sizeInBytes() + level_lb_.sizeInBytes() + level_ub_.sizeInBytes()
				+ node_size_.sizeInBytes() + starting_pid_.sizeInBytes() + point_id_.sizeInBytes()
				);
	}

	float sizeInMBytes() {
		return (static_cast<float>(sizeInBytes()) / static_cast<float>(1024 * 1024));
	}

	void memInfo() {
		size_t size_in_bytes = sizeInBytes();
		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("***** Octree:: memory details *****\n");
		printf("\tHeight = %d\n", height_);
		printf("\tNumber of nodes = %d\n", node_num_);
		printf("\tNumber of leaves = %d; leaf_x*y*z = %d\n", leaf_num_,
				leaf_size_.x * leaf_size_.y * leaf_size_.z);
		printf("\tSize of the bottom xyz = %d, %d, %d\n",
				leaf_size_.x, leaf_size_.y, leaf_size_.z);
		printf("\tNumber of points = %d, number of allocated points = %d\n",
				point_num_, alloc_point_num_);
		printf("\tmin_b_xyz = %d, %d, %d\n", min_b_.x, min_b_.y, min_b_.z);
		printf("\tmax_b_xyz = %d, %d, %d\n", max_b_.x, max_b_.y, max_b_.z);
		printf("Octree::cloud_: %d points, %.2f MB, %.2f%%\n", cloud_->size(),
				static_cast<float>(cloud_->size() * sizeof(Vector3f) * 3) / to_mb,
				static_cast<float>(cloud_->size() * sizeof(Vector3f) * 3) / to_percent);

		printf("Octree::pnum_: %d elements, %.2f MB, %.2f%%\n", status_.size(),
				static_cast<float>(status_.sizeInBytes()) / to_mb,
				static_cast<float>(status_.sizeInBytes()) / to_percent);

		printf("Octree::level_id_: %d elements, %.2f MB, %.2f%%\n", level_id_.size(),
				static_cast<float>(level_id_.sizeInBytes()) / to_mb,
				static_cast<float>(level_id_.sizeInBytes()) / to_percent);

		printf("Octree::level_dim_: %d elements, %.2f MB, %.2f%%\n", level_dim_.size(),
				static_cast<float>(level_dim_.sizeInBytes()) / to_mb,
				static_cast<float>(level_dim_.sizeInBytes()) / to_percent);

		printf("Octree::level_lb_: %d elements, %.2f MB, %.2f%%\n", level_lb_.size(),
				static_cast<float>(level_lb_.sizeInBytes()) / to_mb,
				static_cast<float>(level_lb_.sizeInBytes()) / to_percent);

		printf("Octree::level_ub_: %d elements, %.2f MB, %.2f%%\n", level_ub_.size(),
				static_cast<float>(level_ub_.sizeInBytes()) / to_mb,
				static_cast<float>(level_ub_.sizeInBytes()) / to_percent);

		printf("Octree::node_size_: %d elements, %.2f MB, %.2f%%\n", node_size_.size(),
				static_cast<float>(node_size_.sizeInBytes()) / to_mb,
				static_cast<float>(node_size_.sizeInBytes()) / to_percent);

		printf("Octree::starting_pid_: %d elements, %.2f MB, %.2f%%\n", starting_pid_.size(),
				static_cast<float>(starting_pid_.sizeInBytes()) / to_mb,
				static_cast<float>(starting_pid_.sizeInBytes()) / to_percent);

		printf("Octree::point_id_: %d elements, %.2f MB, %.2f%%\n", point_id_.size(),
				static_cast<float>(point_id_.sizeInBytes()) / to_mb,
				static_cast<float>(point_id_.sizeInBytes()) / to_percent);
	}

	~Octree() {}

private:
	void buildOctree();

	void findBoundaries();

	void scatterPointsToLeaves();

	void buildLeaves(device_vector<uint32_t> &status);

	void allocateOctree();

	void allocateOctree(Vector3i leaf_size, Vector3i min_b, Vector3i max_b,
						device_vector<uint32_t> &status, device_vector<Vector3i> &level_dim,
						device_vector<Vector3i> &level_lb, device_vector<Vector3i> &level_ub,
						device_vector<Vector3f> &node_size, device_vector<int> &level_id,
						int &node_num, int &height);

	void buildLevelMultiPass();

	void buildUpper(uint32_t *ppnum, Vector3i pdim, Vector3i plb, Vector3i pub,
					uint32_t *cpnum, Vector3i cdim, Vector3i clb, Vector3i cub);

	// Functions for updating the tree when new points coming
	// Compute new boundaries
	void computeBoundaries(FCloudPtr cloud, Vector3i &new_max_b, Vector3i &new_min_b,
							Vector3i &new_leaf_size, int &new_leaf_num);

	void extendMemory(int new_point_num, Vector3i new_max_b, Vector3i new_min_b,
						Vector3i new_leaf_size, int new_leaf_num);

	void updateTreeContent(typename PointCloud<PointType>::Ptr &new_cloud, int point_num);


	// Test functions

	/* The CPU-based goDown. Used to validate the result of the
	 * GPU-based goDown. Print out the nodes on the path from
	 * top to bottom when querying qid in the Octree. */
	void testGoDown(typename PointCloud<PointType>::Ptr &qcloud,
					const device_vector<int> &nearest_node,
					const device_vector<float> &gpu_min_dist);

	void testGoUp(typename PointCloud<PointType>::Ptr &qcloud, const device_vector<int> &nn_id,
					const device_vector<int> &nn_pid, const device_vector<float> &nn_dist);

	void testLevel(int level, int line_no);

	/* Coordinates of points, basically a copy of the input point cloud.
	 * As new points are added, the buffers are extended and new points
	 * are appended to the end of original vectors.
	 */
	typename PointCloud<PointType>::Ptr cloud_;

	/* Each tree node stores the status of its children.
	 * If the a child node is not empty, its corresponding bit
	 * in the status node is set. Otherwise, it is clear.
	 * At the bottom level, the status_ is the number of points
	 * each leaf is holding
	 */
	device_vector<uint32_t> status_;

	/* The starting index of each level.
	 * Use sparse representation to store tree's levels.
	 * level_id_[i] is the index of level ith of the tree.
	 *
	 * However, in each level, nodes are organized in a
	 * vector-style grid structure, in which a node (x, y, z)
	 * is store at index x + y * level_dim_x + z * level_dim_x * level_dim_y.
	 * Accessing tree nodes in each level is fast, but
	 * this is waste of memory because lots of nodes are empty.
	 */
	device_vector<int> level_id_;

	/* level_dim and hlevel_dim are the same, they are all
	 * specify the dimensions of each octree level.
	 * However, the first one is in the GPU memory thus is
	 * accessible by GPU threads. The second one is in the
	 * host memory and is used to manage the size of the GPU
	 * buffers dedicated to the octree.
	 */
	device_vector<Vector3i> level_dim_;	// Dimensions of octree levels at GPU side

	/* Used when extending the tree.
	 *
	 * To copy tree nodes, we need to compute the index of the
	 * old node in the new tree. This is done via 4 steps:
	 * old local 1D index -> old local 3D index -> 3D global
	 * index -> newe local 3D index -> new local 1D index
	 *
	 * By adding/subtracting the 3D index to the boundary values,
	 * we can convert local 3D index and global 3D index back and
	 * forth.
	 *
	 * The boundaries are first created on the host, then is copied
	 * to the GPU memory.
	 */
	device_vector<Vector3i> level_lb_;	// GPU-side level lower boundary
	device_vector<Vector3i> level_ub_;	// GPU-side level upper boundary

	/* Size of nodes in each level */
	device_vector<Vector3f> node_size_;

	int height_;			// Height of the tree, not included bottom level
	int node_num_;			// Total number of nodes

	/* starting_pid_ and point_id_ are used in the bottom level
	 * of the tree, where points are stored. To access indices
	 * of points belong to a leaf i, we go to point_id_ from
	 * index starting_pid_[i] to starting_pid_[i + 1] - 1.
	 */
	device_vector<int> starting_pid_;	// Starting index of leafs
	device_vector<int> point_id_;		// Indices of points sorted by leaf id

	int leaf_num_;
	Vector3i leaf_size_;	// Size of bottom level
	int point_num_;			// The number of point in the current point cloud
	Vector3f res_;			// Resolution

	/* The boundaries of the ALLOCATED bottom grid.
	 * As a new point cloud enters, the system computes new
	 * boundaries. If new boundaries exceed the current boundaries,
	 * the octree is moved to bigger space.
	 */
	Vector3i min_b_, max_b_;

	/* To avoid copying the point cloud whenever new points are added,
	 * the buffers for points are also over-allocated. This variable
	 * indicates the size of the allocated buffer for the point cloud
	 * (in number of points, not bytes).
	 */
	int alloc_point_num_;


	/* The following parameters indicate the rounding factor
	 * in each dimensions of the leaf. To avoid frequent allocation
	 * and copy when new data are integrated, the system over-allocate
	 * a memory pool that is actually larger than the required memory
	 * of the data (point cloud in this case).
	 *
	 * To that end, when computing the max and min boundaries, the results
	 * are rounded toward a multiplier of these parameters. Max value is
	 * rounded up toward plus infinity, and min value is toward minus infinity.
	 */
	static const int MAX_BX_ = 8;
	static const int MAX_BY_ = 8;
	static const int MAX_BZ_ = 4;

	static const int POINT_BLOCK_ = 131072;
};

/**
 * A class for debugging the GPU Octree
 */
template <typename PointType>
class HostOctree {
public:
	HostOctree(const Octree<PointType> &gpu_tree);

	void goDown(PointType p, int &nn_id, float &nn_dist);

	void goUp(PointType p, int &nn_id, float &nn_dist);

	void traceToTop(int leaf_id, std::vector<Vector3i> &path);

	/* Brute-Force NN.
	 * Find the nearest point, the index of the leaf node that
	 * contains that point, the nearest distance of (qx,qy,qz)
	 * Used for testing the result of the nearest neighbor search.
	 * Use the sequential search to compute and compare the
	 * distance between the query point qid and the reference
	 * points x_, y_, z_ to find the nearest neighbor of qid.
	 * Its index is stored in nn_id and their distance is put int nn_dist.
	 */
	void BFNNSearch(PointType p, int &nn_id, int &nn_pid, float &nn_dist);

private:
	/**
	 * From a parent node, go down and find the nearest child
	 *
	 * Params:
	 * 		@p[in]: the query point
	 * 		@level[in]: the level of the parent node
	 * 		@node_id[in/out]: the index of the input parent node
	 * 							and the output nearest child node
	 */
	void goDown(PointType p, int level, Vector3i &node_id);

	/**
	 * Find the nearest neighbor of the query point in a leaf node
	 *
	 * Params:
	 * 		@p[in]: the query point
	 * 		@lid[in]: the index of the leaf node
	 * 		@nn_id[out]: the index of the nearest neighbor point
	 * 		@nn_dist[out]: the distance to the nearest neighbor point
	 */
	void goDown(PointType p, int lid, int &nn_id, float &nn_dist);

	float dist(PointType p, PointType q) {
		float x = p.x - q.x;
		float y = p.y - q.y;
		float z = p.z - q.z;

		return sqrt(x * x + y * y + z * z);
	}

	float dist(Vector3f p, int idx, int idy, int idz, Vector3f nsize) {
		float lx, ly, lz, ux, uy, uz;

		lx = idx * nsize.x;
		ly = idy * nsize.y;
		lz = idz * nsize.z;

		ux = lx + nsize.x;
		uy = ly + nsize.y;
		uz = lz + nsize.z;

		float tx = ((p.x > ux) ? p.x - ux : ((p.x < lx) ? lx - p.x : 0));
		float ty = ((p.y > uy) ? p.y - uy : ((p.y < ly) ? ly - p.y : 0));
		float tz = ((p.z > uz) ? p.z - uz : ((p.z < lz) ? lz - p.z : 0));

		return sqrt(tx * tx + ty * ty + tz * tz);
	}

	int leafId(PointType p) {
		int idx = static_cast<int>(floorf(p.x / res_.x)) - min_b_.x;
		int idy = static_cast<int>(floorf(p.y / res_.y)) - min_b_.y;
		int idz = static_cast<int>(floorf(p.z / res_.z)) - min_b_.z;

		return (idx + idy * leaf_size_.x + idz * leaf_size_.x * leaf_size_.y);
	}

	typename pcl::PointCloud<PointType>::Ptr cloud_;
	std::vector<uint32_t> status_;
	std::vector<int> level_id_;
	std::vector<Vector3i> level_dim_;
	std::vector<Vector3i> level_lb_;
	std::vector<Vector3i> level_ub_;
	std::vector<Vector3f> node_size_;
	std::vector<int> starting_pid_;	// Starting index of leafs
	std::vector<int> point_id_;		// Indices of points sorted by leaf id

	Vector3i leaf_size_;
	Vector3f res_;
	Vector3i min_b_, max_b_;

	int height_;
};


}

}

#endif
