#ifndef GPU_VGRID_H_
#define GPU_VGRID_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <memory>

#include <float.h>
#include <vector>

#include "common.h"
#include "MatrixDevice.h"
#include "MatrixHost.h"
#include "device_vector.h"
#include "point_cloud.h"
#include "vector3x.h"

namespace gpu {

namespace vgrid {

class VoxelGrid : public Base {
public:
	typedef std::shared_ptr<VoxelGrid> Ptr;
	typedef std::shared_ptr<const VoxelGrid> ConstPtr;

	VoxelGrid(GMemoryAllocator *allocator);

	VoxelGrid(const VoxelGrid &other);
	VoxelGrid(VoxelGrid &&other);

	/* Set input points */
	template <typename PointType>
	void setInputCloud(typename PointCloud<PointType>::Ptr &input) {
		setInputCloud(input);
	}

	void setMinVoxelSize(int size);

	/* For each input point, search for voxels whose distance between their centroids and
	 * the input point are less than radius.
	 * Results of the search are stored into valid_points, starting_voxel_id, and voxel_id.
	 * Valid points: the function return one or more voxels for these points. Other points
	 * are considered as invalid.
	 * Valid voxels: voxels returned from the search.
	 * The number of valid points is stored in valid_point_num.
	 * The total number of valid voxels. If the query of both point A and point B return
	 * voxel X, then the number of valid voxels is 2, not 1. */
	template <typename PointType>
	void radiusSearch(typename PointCloud<PointType>::Ptr &qcloud, float radius, int max_nn,
						device_vector<int> &starting_voxel_id, device_vector<int> &nn_voxel_id) {
		radiusSearch(qcloud->data(), radius, starting_voxel_id, nn_voxel_id);
	}

	int getVoxelNum() const { return voxel_num_; }

	Vector3f getResolution() const { return res_; }
	Vector3i getMaxBoundary() const { return max_b_; }
	Vector3i getMinBoundary() const { return min_b_; }
	Vector3i getGridDim() const { return vgdim_; }

	void setResolution(float res) {
		res_.x = res_.y = res_.z = res;
	}

	/* Get the centroid list. */
	device_vector<double> &getCentroidList() { return centroid_; }

	/* Get the pointer to the inverse covariances list. */
	device_vector<double> &getInverseCovarianceList() { return icov_; }

	device_vector<int> &getPointsPerVoxelList() { return points_per_voxel_; }

	VoxelGrid& operator=(const VoxelGrid &other);
	VoxelGrid& operator=(VoxelGrid &&other);

	template <typename PointType>
	void update(typename PointCloud<PointType>::Ptr &new_cloud);

	// Print voxel's information
	void testVoxel(int vid);

	void memInfo() {
		size_t size_in_bytes = sizeInBytes();

		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) / 100.0 : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("***** VoxelGrid:: Memory details *****\n");
		printf("\tmax_b_xyz = %d, %d, %d\n", max_b_.x, max_b_.y, max_b_.z);
		printf("\tmin_b_xyz = %d, %d, %d\n", min_b_.x, min_b_.y, min_b_.z);
		printf("\treal_max_bxyz = %d, %d, %d\n", real_max_b_.x, real_max_b_.y, real_max_b_.z);
		printf("\treal_min_bxyz = %d, %d, %d\n", real_min_b_.x, real_min_b_.y, real_min_b_.z);

		printf("VoxelGrid::centroid_: %d elements, %.2f MB, %.2f%%\n", centroid_.size(),
				static_cast<float>(centroid_.sizeInBytes()) / to_mb,
				static_cast<float>(centroid_.sizeInBytes()) / to_percent);

		printf("VoxelGrid::inverse_covariance_: %d elements, %.2f MB, %.2f%%\n", icov_.size(),
				static_cast<float>(icov_.sizeInBytes()) / to_mb,
				static_cast<float>(icov_.sizeInBytes()) / to_percent);

		printf("VoxelGrid::points_per_voxel_: %d elements, %.2f MB, %.2f%%\n", points_per_voxel_.size(),
				static_cast<float>(points_per_voxel_.sizeInBytes()) / to_mb,
				static_cast<float>(points_per_voxel_.sizeInBytes()) / to_percent);

		printf("VoxelGrid::tmp_centroid_: %d elements, %.2f MB, %.2f%%\n", tmp_centroid_.size(),
				static_cast<float>(tmp_centroid_.sizeInBytes()) / to_mb,
				static_cast<float>(tmp_centroid_.sizeInBytes()) / to_percent);

		printf("VoxelGrid::tmp_cov_: %d elements, %.2f MB, %.2f%%\n", tmp_cov_.size(),
				static_cast<float>(tmp_cov_.sizeInBytes()) / to_mb,
				static_cast<float>(tmp_cov_.sizeInBytes()) / to_percent);

	}

	/* Return the amount of occupied GPU memory in bytes */
	size_t sizeInBytes() {
		return (centroid_.sizeInBytes() + icov_.sizeInBytes() +
				points_per_voxel_.sizeInBytes() + tmp_centroid_.sizeInBytes() + tmp_cov_.sizeInBytes());
	}

private:
	void radiusSearch(FCloudPtr qcloud, float radius, int max_nn,
						device_vector<int> &starting_voxel_id,
						device_vector<int> &nn_voxel_id);


	void setInputCloud(FCloudPtr input);

	/* Allocate buffers */
	void allocBuffers();

	/* Compute centroids and covariances of voxels. */
	void computeCentroidAndCovariance(FCloudPtr input, device_vector<int> &starting_point_ids,
										device_vector<int> &point_ids);

	/* Put points into voxels */
	void scatterPointsToVoxelGrid(FCloudPtr input, device_vector<int> &starting_point_ids,
									device_vector<int> &point_ids);

	/* Find boundaries of input point cloud and compute
	 * the numbere of necessary voxels as well as boundaries
	 * measured in number of leaf size
	 */
	void findBoundaries(float *x, float *y, float *z, int point_num,
						Vector3i &min_b, Vector3i &max_b,
						Vector3i &real_min_b, Vector3i &real_max_b,
						Vector3i &vgdim, int &voxel_num);

	void extendMemory(Vector3i min_b, Vector3i max_b);

	void updateCentroidAndCovariance(FCloudPtr input,
										device_vector<int> &voxel_id,
										device_vector<int> &points_per_voxel,
										device_vector<int> &starting_point_ids,
										device_vector<int> &point_ids,
										Vector3i min_b, Vector3i vgdim);

	void scatterPointsToVoxelGrid(FCloudPtr input, Vector3i min_b, Vector3i vgdim,
									device_vector<int> &points_per_voxel,
									device_vector<int> &starting_point_ids,
									device_vector<int> &point_ids);

	void filterOutEmptyVoxels(device_vector<int> &points_per_voxel,
								device_vector<int> &starting_point_ids,
								device_vector<int> &voxel_id);

	//Coordinate of input points
	device_vector<double> centroid_;			// List of 3x1 double vector
	device_vector<double> icov_;	// List of 3x3 double inverse covariance matrix
	device_vector<int> points_per_voxel_;

	int voxel_num_;						// Number of voxels
	Vector3f  res_;		// Resolution, a.k.a, size of each voxel

	// Upper, lower boundaries, and dimension in number of voxels of the grid
	Vector3i max_b_, min_b_, vgdim_;
	int min_points_per_voxel_;

	// Added for incremental ndt
	device_vector<double> tmp_centroid_;	// List of 3x1 double vectors
	device_vector<double> tmp_cov_;			// List of 3x3 double matrix
	// The actual boundaries of the voxel grid, including empty voxels.
	Vector3i real_max_b_, real_min_b_;

	static const int BLOCK_X_ = 16;
	static const int BLOCK_Y_ = 16;
	static const int BLOCK_Z_ = 8;
};

}

}

#endif
