#ifndef GPU_PLTREE_H_
#define GPU_PLTREE_H_

#include <cuda.h>
#include <vector>
#include <memory>

#include "common.h"
#include "debug.h"
#include "device_vector.h"
#include "point_cloud.h"
#include "vector3x.h"

namespace gpu {

namespace pltree {

template <typename PointType> class HostPLTree;

template <typename PointType>
class PointlessTree : public Base {
	friend class HostPLTree<PointType>;

public:
	typedef std::shared_ptr<PointlessTree<PointType>> Ptr;
	typedef std::shared_ptr<const PointlessTree<PointType>> ConstPtr;

	PointlessTree(GMemoryAllocator *allocator);

	PointlessTree(const PointlessTree<PointType> &other);
	PointlessTree(PointlessTree<PointType> &&other);

	PointlessTree<PointType> &operator=(const PointlessTree<PointType> &other);
	PointlessTree<PointType> &operator=(PointlessTree<PointType> &&other);

	/* Set input cloud.
	 *
	 * Input:
	 * 		@x, y, z: the GPU buffers contains the input coordinates
	 * 		@point_num: the actual number of input points, may differ from size
	 * 					of x, y, z because x, y, z may be over-allocated.
	 */
	void setInputCloud(typename PointCloud<PointType>::Ptr &input);

	/* Search for the points (qx, qy, qz), return the index of
	 * the result point in nn_pid and the corresponding distances
	 * in nn_dist */
	void NNSearch(typename PointCloud<PointType>::Ptr &qcloud,
					device_vector<int> &nn_pid, device_vector<float> &nn_dist);

	void NNSearch(typename PointCloud<PointType>::Ptr &qcloud, device_vector<float> &nn_dist);

	void kNNSearch(typename PointCloud<PointType>::Ptr &qcloud, int k, device_vector<int> &nn_id);

	/**
	 * Update the tree content when new points are added to the cloud.
	 * 3 major steps:
	 * - Compute new boundaries
	 * - Extend the buffers used for the tree if boundaries changed
	 * - Update the tree's content with new points
	 */
	void update(typename PointCloud<PointType>::Ptr &new_cloud);

	size_t sizeInBytes() {
		return (status_.sizeInBytes() + level_id_.sizeInBytes()
				+ level_dim_.sizeInBytes() + level_lb_.sizeInBytes() + level_ub_.sizeInBytes()
				+ node_size_.sizeInBytes() + centroids_->size() * sizeof(Vector3f));
	}

	float sizeInMBytes() {
		return (static_cast<float>(sizeInBytes()) / static_cast<float>(1024 * 1024));
	}

	void memInfo() {
		printf("***** PointlessTree:: memory details *****\n");

		size_t size_in_bytes = sizeInBytes();
		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("***** PointlessTree:: memory details *****\n");
		printf("\tHeight = %d\n", level_dim_.size());
		printf("\tNumber of nodes = %d\n", status_.size());
		printf("\tNumber of leaves = %d\n", centroids_->size());
		printf("\tmin_b_xyz = %d, %d, %d\n", bot_lb_.x, bot_lb_.y, bot_lb_.z);
		printf("\tmax_b_xyz = %d, %d, %d\n", bot_ub_.x, bot_ub_.y, bot_ub_.z);

		printf("PointlessTree::status_: %d elements, %.2f MB, %.2f%%\n", status_.size(),
				static_cast<float>(status_.sizeInBytes()) / to_mb,
				static_cast<float>(status_.sizeInBytes()) / to_percent);

		printf("PointlessTree::level_id_: %d elements, %.2f MB, %.2f%%\n", level_id_.size(),
				static_cast<float>(level_id_.sizeInBytes()) / to_mb,
				static_cast<float>(level_id_.sizeInBytes()) / to_percent);

		printf("PointlessTree::level_dim_: %d elements, %.2f MB, %.2f%%\n", level_dim_.size(),
				static_cast<float>(level_dim_.sizeInBytes()) / to_mb,
				static_cast<float>(level_dim_.sizeInBytes()) / to_percent);

		printf("PointlessTree::level_lb_: %d elements, %.2f MB, %.2f%%\n", level_lb_.size(),
				static_cast<float>(level_lb_.sizeInBytes()) / to_mb,
				static_cast<float>(level_lb_.sizeInBytes()) / to_percent);

		printf("PointlessTree::level_ub_: %d elements, %.2f MB, %.2f%%\n", level_ub_.size(),
				static_cast<float>(level_ub_.sizeInBytes()) / to_mb,
				static_cast<float>(level_ub_.sizeInBytes()) / to_percent);

		printf("PointlessTree::node_size_: %d elements, %.2f MB, %.2f%%\n", node_size_.size(),
				static_cast<float>(node_size_.sizeInBytes()) / to_mb,
				static_cast<float>(node_size_.sizeInBytes()) / to_percent);

		printf("PointlessTree::cxyz_: %d elements (x3), %.2f MB, %.2f%%\n", centroids_->size(),
				static_cast<float>(centroids_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(centroids_->size() * sizeof(Vector3f)) / to_percent);

	}

	~PointlessTree() {}

private:
	void buildTree(typename PointCloud<PointType>::Ptr &cloud);

	/**
	 * Compute boundaries of the bottom level of the tree.
	 * Higher levels are built recursively later.
	 *
	 * Params:
	 * 		@cloud[in]: the input point cloud
	 * 		@bot_ub, bot_lb[out]: the upper and lower boundaries
	 * 							(in number of tree nodes) of the
	 * 							bottom level of the tree
	 */
	void computeBoundaries(FCloudPtr cloud, Vector3i &bot_ub, Vector3i &bot_lb);

	/**
	 * Distribute points to leaf nodes (bottom level). Count the
	 * number of points per leaf and the centroids of leaf nodes.
	 *
	 * Params:
	 * 		@cloud[in]: the pointer to the input cloud
	 * 		@bot_ub, bot_lb[in]: the upper and lower dimensions of
	 * 					the bounding box that cover the input cloud
	 * 					(in the case of update, they are NOT the bot_ub_
	 * 					and bot_lb_ of the class)
	 */
	void scatterPointsToLeaves(typename PointCloud<PointType>::Ptr &cloud,
								Vector3i bot_ub, Vector3i bot_lb);

	void scatterPointsToLeavesSmall(typename PointCloud<PointType>::Ptr &cloud,
									Vector3i bot_ub, Vector3i bot_lb);

	/**
	 * Compute the boundaries, dimensions, and the size (in meters)
	 * of tree nodes of each tree level.
	 * Also allocate memory for the centroids of the bottom level.
	 *
	 * Params:
	 * 		@bot_lb, bot_ub[in]: the lower and upper boundaries of the
	 * 					bottom level of the tree
	 * 		@status[out]: the buffer to hold the status of each tree node
	 * 		@level_dim[out]: dimensions of each level (in number of nodes)
	 * 		@level_lb, level_ub[out]: the lower and upper boundaries of
	 * 					each tree level
	 * 		@node_size[out]: the size in meters of tree nodes in each level
	 * 		@level_id[out]: the starting indices of each level in the @status vector
	 * 		@centroids[out]: the point cloud holding the centroids of leaves
	 */
	void allocateTree(Vector3i bot_lb, Vector3i bot_ub,
						device_vector<uint32_t> &status, device_vector<Vector3i> &level_dim,
						device_vector<Vector3i> &level_lb, device_vector<Vector3i> &level_ub,
						device_vector<Vector3f> &node_size, device_vector<int> &level_id,
						typename PointCloud<PointType>::Ptr &centroids);

	void buildLevelMultiPass();

	void buildUpper(uint32_t *pstatus, Vector3i pdim, Vector3i plb, Vector3i pub,
					uint32_t *cstatus, Vector3i cdim, Vector3i clb, Vector3i cub);

	void extendMemory(Vector3i new_bot_ub, Vector3i new_bot_lb);

	void updateTreeContent(typename PointCloud<PointType>::Ptr &new_cloud,
							Vector3i new_bot_ub, Vector3i new_bot_lb);


	// Test functions

	/* The CPU-based goDown. Used to validate the result of the
	 * GPU-based goDown. Print out the nodes on the path from
	 * top to bottom when querying qid in the PointlessTree. */
	void testGoDown(typename PointCloud<PointType>::Ptr &qcloud,
					const device_vector<int> &nearest_node,
					const device_vector<float> &gpu_min_dist);

	void testGoUp(typename PointCloud<PointType>::Ptr &qcloud,
					device_vector<int> &nn_id, device_vector<float> &nn_dist);

	/* Each tree node stores the number of points in the node. */
	device_vector<uint32_t> status_;

	/* The starting index of each level.
	 * Use sparse representation to store tree's levels.
	 * level_id_[i] is the index of level ith of the tree.
	 *
	 * However, in each level, nodes are organized in a
	 * vector-style grid structure, in which a node (x, y, z)
	 * is store at index x + y * level_dim_x + z * level_dim_x * level_dim_y.
	 * Accessing tree nodes in each level is fast, but
	 * this is waste of memory because lots of nodes are empty.
	 */
	device_vector<int> level_id_;

	/* level_dim and hlevel_dim are the same, they are all
	 * specify the dimensions of each PointlessTree level.
	 * However, the first one is in the GPU memory thus is
	 * accessible by GPU threads. The second one is in the
	 * host memory and is used to manage the size of the GPU
	 * buffers dedicated to the PointlessTree.
	 */
	device_vector<Vector3i> level_dim_;	// Dimensions of PointlessTree levels at GPU side

	/* Used when extending the tree.
	 *
	 * To copy tree nodes, we need to compute the index of the
	 * old node in the new tree. This is done via 4 steps:
	 * old local 1D index -> old local 3D index -> 3D global
	 * index -> newe local 3D index -> new local 1D index
	 *
	 * By adding/subtracting the 3D index to the boundary values,
	 * we can convert local 3D index and global 3D index back and
	 * forth.
	 *
	 * The boundaries are first created on the host, then is copied
	 * to the GPU memory.
	 */
	device_vector<Vector3i> level_lb_;	// GPU-side level lower boundary
	device_vector<Vector3i> level_ub_;	// GPU-side level upper boundary

	/* Size of nodes in each level */
	device_vector<Vector3f> node_size_;

	Vector3f res_;	// Resolution of the leaves

	/* The lower and upper boundaries of the ALLOCATED bottom grid.
	 * As a new point cloud enters, the system computes new
	 * boundaries. If new boundaries exceed the current boundaries,
	 * the PointlessTree is moved to a bigger buffer.
	 */
	Vector3i bot_lb_, bot_ub_;

	/* Centroids of leaves */
	typename PointCloud<PointType>::Ptr centroids_;

	/* The following parameters indicate the rounding factor
	 * in each dimensions of the leaf. To avoid frequent allocation
	 * and copy when new data are integrated, the system over-allocate
	 * a memory pool that is actually larger than the required memory
	 * of the data (point cloud in this case).
	 *
	 * To that end, when computing the max and min boundaries, the results
	 * are rounded toward a multiplier of these parameters. Max value is
	 * rounded up toward plus infinity, and min value is toward minus infinity.
	 */
	static const int MAX_BX_ = 8;
	static const int MAX_BY_ = 8;
	static const int MAX_BZ_ = 4;
};

// Pointer to a tree node. Used in GPU kernels.
class DevTreeNode {
public:
	void goUp();

	void goDown();
private:
	Vector3i idx_;
	uint32_t status_;
};

template <typename PointType>
class HostPLTree {
public:
	HostPLTree(const PointlessTree<PointType> &gpu_tree);

	void goDown(PointType p, int &nn_id, float &nn_dist);

	void goUp(PointType p, int &nn_id, float &nn_dist);

	void traceToTop(int leaf_id, std::vector<Vector3i> &path);

	void BFNNSearch(PointType p, int &nn_id, float &nn_dist);

private:

	void goDown(PointType p, int level, Vector3i &node_id);

	void goDown(PointType p, int lid, int &nn_id, float &nn_dist);

	float dist(PointType p, PointType q);

	float dist(PointType p, int i, int j, int k, Vector3f nsize) {
		float lx, ly, lz, ux, uy, uz;

		lx = i * nsize.x;
		ly = j * nsize.y;
		lz = k * nsize.z;

		ux = lx + nsize.x;
		uy = ly + nsize.y;
		uz = lz + nsize.z;

		float tx = ((p.x > ux) ? p.x - ux : ((p.x < lx) ? lx - p.x : 0));
		float ty = ((p.y > uy) ? p.y - uy : ((p.y < ly) ? ly - p.y : 0));
		float tz = ((p.z > uz) ? p.z - uz : ((p.z < lz) ? lz - p.z : 0));

		return sqrt(tx * tx + ty * ty + tz * tz);
	}

	int leafId(PointType p) {
		int idx = static_cast<int>(floorf(p.x / res_.x)) - bot_lb_.x;
		int idy = static_cast<int>(floorf(p.y / res_.y)) - bot_lb_.y;
		int idz = static_cast<int>(floorf(p.z / res_.z)) - bot_lb_.z;

		return (idx + idy * bot_dim_.x + idz * bot_dim_.x * bot_dim_.y);
	}

	std::vector<uint32_t> status_;
	std::vector<int> level_id_;
	std::vector<Vector3i> level_dim_;	// Dimensions of PointlessTree levels
	std::vector<Vector3i> level_lb_;	// GPU-side level lower boundary
	std::vector<Vector3i> level_ub_;	// GPU-side level upper boundary

	/* Size of nodes in each level */
	std::vector<Vector3f> node_size_;

	Vector3f res_;	// Resolution of the leaves

	/* The lower and upper boundaries of the ALLOCATED bottom grid.
	 * As a new point cloud enters, the system computes new
	 * boundaries. If new boundaries exceed the current boundaries,
	 * the PointlessTree is moved to a bigger buffer.
	 */
	Vector3i bot_lb_, bot_ub_, bot_dim_;

	/* Centroids of leaves */
	std::vector<float> cx_, cy_, cz_;
};

}

}

#endif
