#ifndef GPU_BASE_H_
#define GPU_BASE_H_

#include <cuda.h>
#include <cuda_runtime.h>

#include <string>

#include "gmemory.h"
#include "debug.h"

namespace gpu {

// Use memory pool to reduce the allocation time
#ifndef GMEM_
#define GMEM_
#endif

// All classes in the namespace gpu must inherit from this class
class Base {
public:
	Base(GMemoryAllocator *allocator) {
		verify(allocator != nullptr);

		__allocator = allocator;
	}

	Base(const Base &other) {
		__allocator = other.__allocator;
	}

	Base(Base &&other) {
		__allocator = other.__allocator;
	}

	Base &operator=(const Base &other) {
		__allocator = other.__allocator;

		return *this;
	}

	Base &operator=(Base &&other) {
		__allocator = other.__allocator;

		return *this;
	}

	template <typename T>
	void alloc(T **data, uint64_t size) {
		if (*data != nullptr) {
			free(*data);
		}

#ifdef GMEM_
		checkGmemErrors(__allocator->gmemAlloc(data, size));
#else
		checkCudaErrors(cudaMalloc(data, size));
#endif
	}

	template <typename T>
	void free(T *data) {
		if (data != nullptr) {
#ifdef GMEM_
		checkGmemErrors(__allocator->gmemFree(data));
#else
		checkCudaErrors(cudaFree(data));
#endif
		}
	}

	GMemoryAllocator *__allocator;
};


}


#endif
