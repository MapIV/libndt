#ifndef GPU_NDT_H_
#define GPU_NDT_H_

#include <cuda.h>
#include <cuda_runtime.h>
#include <memory>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#include <string>

#include "common.h"
#include "point_cloud.h"
#include "Registration.h"
#include "VoxelGrid.h"

#define USE_PLTREE_

#ifdef USE_PLTREE_
#include "PointlessTree.h"
#else
#include "Octree.h"
#endif

#define USE_VGRID2_

#ifdef USE_VGRID2_
#include "VoxelGrid2.h"
#endif

namespace gpu {
template <typename PointSourceType, typename PointTargetType, typename Scalar = float>
class NormalDistributionsTransform: public Registration<PointSourceType, PointTargetType, Scalar> {
	typedef Registration<PointSourceType, PointTargetType> BaseRegType;
	typedef typename pcl::PointCloud<PointSourceType>::Ptr SourceHCPtr;	// host-side Source cloud pointer
	typedef typename pcl::PointCloud<PointTargetType>::Ptr TargetHCPtr;	// host-side Target cloud pointer

	typedef typename PointCloud<PointSourceType>::Ptr SourceDCPtr;	// device-side source cloud pointer
	typedef typename PointCloud<PointTargetType>::Ptr TargetDCPtr;	// device-side target cloud pointer

#ifdef USE_PLTREE_
	typedef pltree::PointlessTree<PointTargetType> TreeType;
#else
	typedef Octree<PointTargetType> TreeType;
#endif
	typedef typename TreeType::Ptr TreePtr;

#ifdef USE_VGRID2_
	typedef vgrid2::VoxelGrid2 GridType;
#else
	typedef vgrid::VoxelGrid GridType;
#endif
	typedef GridType::Ptr GridPtr;

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	NormalDistributionsTransform(GMemoryAllocator *allocator);

	NormalDistributionsTransform(const NormalDistributionsTransform<PointSourceType, PointTargetType, Scalar> &other);

	void setStepSize(double step_size) { step_size_ = step_size; }

	void setResolution(Scalar resolution) { resolution_ = resolution; }

	void setOutlierRatio(double olr) { outlier_ratio_ = olr; }

	double getStepSize() const { return step_size_; }

	float getResolution() const { return resolution_; }

	double getOutlierRatio() const { return outlier_ratio_; }

	double getTransformationProbability() const { return trans_probability_; }

	double getTransformationProbability(SourceHCPtr &source_cloud,
										TargetHCPtr &trans_cloud) {
		return getTransformationProbability2(source_cloud, trans_cloud);
	}

	double getTransformationProbability2(SourceHCPtr &source_cloud, TargetHCPtr &trans_cloud);

	// Added on 2020.08.15, incremental update
	// TODO: need to test
	void updateVoxelGrid(SourceHCPtr &new_cloud);

	void updateVoxelGrid(SourceDCPtr &new_cloud);

	// Similar to updateVoxelGrid but not update tree
	void updateVoxelGridRaw(SourceHCPtr &new_cloud);

	void updateVoxelGridRaw(SourceDCPtr &new_cloud);
	// End of adding

	int getRealIterations() const { return real_iterations_; }

	/* Set the input reference map points */
	void setInputTarget(TargetHCPtr &input);

	void setInputTarget(TargetDCPtr &input);

	/* Set the input reference map points, but not build the tree structure.
	 * This is used when the tree structure is already available (maybe built
	 * in advanced), so we do not have to build the same tree twice.
	 */
	void setInputTargetRaw(TargetHCPtr &input);

	void setInputTargetRaw(TargetDCPtr &input);

	/* Compute and get fitness score */
	double getFitnessScore(double max_range = DBL_MAX);

	// Turn on/off line search
	// true: turn on, false: turn off
	void setLineSearchMode(bool mode)
	{
		line_search_ = mode;
	}

	size_t sizeInBytes() {
		return (voxel_grid_->sizeInBytes() + BaseRegType::sizeInBytes()
				+ tree_->sizeInBytes());
	}

	float sizeInMBytes() {
		return (static_cast<float>(sizeInBytes()) / static_cast<float>(1024 * 1024));
	}

	void memInfo() {
		size_t size_in_bytes = sizeInBytes();
		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) / 100.0 : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("\n***** NormalDistributionsTransform::Memory details *****\n");
		printf("NormalDistributionsTransform:: total_mem = %.2f MB\n", static_cast<float>(size_in_bytes) / to_mb);
		printf("NormalDistributionsTransform::voxel_grid_: %.2f MB, %.2f%%\n",
				static_cast<float>(voxel_grid_->sizeInBytes()) / to_mb,
				static_cast<float>(voxel_grid_->sizeInBytes()) / to_percent);
		printf("NormalDistributionsTransform::source_xyz: %d elements (x3), total %.2f MB, %.2f%%\n",
				source_cloud_->size(),
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_percent);
		printf("NormalDistributionsTransform::trans_xyz: %d elements (x3), %.2f MB, %.2f%%\n",
				trans_cloud_->size(),
				static_cast<float>(trans_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(trans_cloud_->size() * 3) / to_percent);

		voxel_grid_->memInfo();

		printf("NormalDistributionsTransform::tree_: %.2f MB, %.2f%%\n",
				static_cast<float>(tree_->sizeInBytes()) / to_mb,
				static_cast<float>(tree_->sizeInBytes()) / to_percent);

		tree_->memInfo();
		printf("\n");
	}

	// Not count tree
	void memInfoRaw() {
		size_t size_in_bytes = sizeInBytes() - tree_->sizeInBytes();
		float to_percent = (size_in_bytes > 0) ? static_cast<float>(size_in_bytes) / 100.0 : 1;
		float to_mb = static_cast<float>(1024 * 1024);

		printf("\n***** NormalDistributionsTransform::Memory details *****\n");
		printf("NormalDistributionsTransform:: total_mem = %.2f MB\n", static_cast<float>(size_in_bytes) / to_mb);
		printf("NormalDistributionsTransform::voxel_grid_: %.2f MB, %.2f%%\n",
				static_cast<float>(voxel_grid_->sizeInBytes()) / to_mb,
				static_cast<float>(voxel_grid_->sizeInBytes()) / to_percent);
		printf("NormalDistributionsTransform::source_cloud: %d elements (x3), total %.2f MB, %.2f%%\n",
				source_cloud_->size(),
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(source_cloud_->size() * sizeof(Vector3f)) / to_percent);
		printf("NormalDistributionsTransform::trans_xyz: %d elements (x3), %.2f MB, %.2f%%\n",
				trans_cloud_->size(),
				static_cast<float>(trans_cloud_->size() * sizeof(Vector3f)) / to_mb,
				static_cast<float>(trans_cloud_->size() * sizeof(Vector3f)) / to_percent);
		voxel_grid_->memInfo();

		printf("\n");
	}

	/* Testing, get and set the octree pointer.
	 * These functions allow multiple ndt objects
	 * to share the same tree (ndt objects must be built
	 * on top of the same point cloud).
	 */
	void setSearchTree(TreePtr &other_tree) { tree_ = other_tree; }
	TreePtr &getSearchTree() { return tree_; }

	~NormalDistributionsTransform() {}

protected:
	void computeTransformation(const Eigen::Matrix<float, 4, 4> &guess);

	using BaseRegType::transformation_epsilon_;
	using BaseRegType::max_iterations_;

	using BaseRegType::source_cloud_;
	using BaseRegType::trans_cloud_;
	using BaseRegType::converged_;
	using BaseRegType::nr_iterations_;

	// 4x4 matrices
	using BaseRegType::final_transformation_;
	using BaseRegType::transformation_;
	using BaseRegType::previous_transformation_;

	using BaseRegType::__allocator;

private:
	double computeDerivatives(Eigen::Matrix<double, 6, 1> &score_gradient,
								Eigen::Matrix<double, 6, 6> &hessian,
								SourceDCPtr &trans_cloud,
								Eigen::Matrix<double, 6, 1> &pose,
								bool compute_hessian = true);

	void computePointDerivatives(PointCloud<PointSourceType, Scalar> &scattered_source_cloud,
									MatrixDD &dj_ang, MatrixDD &dh_ang,
									matrix_vector<double> &point_gradients,
									matrix_vector<double> &point_hessians,
									bool compute_hessian = true);

	double updateDerivatives(PointCloud<PointSourceType, double> &scattered_trans_cloud,
							matrix_vector<double> &point_gradients,
							matrix_vector<double> &point_hessians,
							matrix_vector<double> &icov,
							Eigen::Matrix<double, 6, 1> &score_gradient,
							Eigen::Matrix<double, 6, 6> &hessian,
							bool compute_hessian = true);

	void updateHessians(PointCloud<PointSourceType, double> &scattered_trans_cloud,
						matrix_vector<double> &point_gradients,
						matrix_vector<double> &point_hessians,
						matrix_vector<double> &icov,
						Eigen::Matrix<double, 6, 6> &hessian);

	//Copied from PCL's ndt.h
    double auxilaryFunction_PsiMT (double a, double f_a, double f_0,
    								double g_0, double mu = 1.e-4) {
    	return (f_a - f_0 - mu * g_0 * a);
    }

    //Copied from PCL's ndt.h
    double auxilaryFunction_dPsiMT (double g_a, double g_0, double mu = 1.e-4) {
    	return (g_a - mu * g_0);
    }

    double updateIntervalMT (double &a_l, double &f_l, double &g_l,
								double &a_u, double &f_u, double &g_u,
								double a_t, double f_t, double g_t);

    double trialValueSelectionMT (double a_l, double f_l, double g_l,
									double a_u, double f_u, double g_u,
									double a_t, double f_t, double g_t);

    // Both types should be cloud of Scalar
    template <typename PointType>
	void transformPointCloud(PointCloud<PointType> &in_cloud, PointCloud<PointType> &out_cloud,
								const Eigen::Matrix<float, 4, 4> &transform);

	void computeAngleDerivatives(Eigen::Matrix<double, 6, 1>& pose,
								bool compute_hessian = true);

	double computeStepLengthMT(const Eigen::Matrix<double, 6, 1> &x,
								Eigen::Matrix<double, 6, 1> &step_dir,
								double step_init, double step_max,
								double step_min, double &score,
								Eigen::Matrix<double, 6, 1> &score_gradient,
								Eigen::Matrix<double, 6, 6> &hessian,
								SourceDCPtr &trans_cloud,
								Eigen::Matrix<float, 4, 4> &final_transformations);

	void computeHessian(Eigen::Matrix<double, 6, 6> &hessian, SourceDCPtr &trans_cloud);

	int preprocessing(FCloudPtr trans_cloud,
						PointCloud<PointSourceType, double> &scattered_trans_cloud,
						PointCloud<PointSourceType, Scalar> &scattered_source_cloud,
						matrix_vector<double> &icov);

	int preprocessing(GridPtr voxel_grid, FCloudPtr trans_cloud,
						PointCloud<PointSourceType, double> &scattered_trans_cloud,
						matrix_vector<double> &icov, device_vector<int> &starting_voxel_id);

	double gauss_d1_, gauss_d2_;
	double outlier_ratio_;

	MatrixHost<double> j_ang_;
	MatrixHost<double> h_ang_;

	double step_size_;
	Scalar resolution_;
	double trans_probability_;

	int real_iterations_;

	GridPtr voxel_grid_;

	// Search tree, used to compute fitness score
	TreePtr tree_;
	bool line_search_;
};

template class NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>;
template class NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI>;
}

#endif
