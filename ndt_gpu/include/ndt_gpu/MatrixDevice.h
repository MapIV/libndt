#ifndef MATRIX_DEVICE_H_
#define MATRIX_DEVICE_H_

#include <cuda.h>
#include <float.h>

#include "common.h"
#include "MatrixHost.h"


namespace gpu {

template <typename Scalar = double>
class MatrixDevice {
public:
	CUDAH MatrixDevice() {
		buffer_ = nullptr;
		rows_ = cols_ = stride_ = 0;
	}

	CUDAH MatrixDevice(int rows, int cols, int stride, Scalar *buffer) {
		rows_ = rows;
		cols_ = cols;
		stride_ = stride;
		buffer_ = buffer;
	}

	CUDAH MatrixDevice(const MatrixDevice &other) {
		rows_ = other.rows_;
		cols_ = other.cols_;
		stride_ = other.stride_;
		buffer_ = other.buffer_;
	}

	MatrixDevice(MatrixHost<Scalar> &m) {
		rows_ = m.rows_;
		cols_ = m.cols_;
		stride_ = 1;
		buffer_ = m.device_buffer_.data();
	}

	CUDAH int rows() const { return rows_; }
	CUDAH int cols() const { return cols_; }
	CUDAH int stride() const { return stride_; }
	CUDAH Scalar *buffer() const { return buffer_; }

	CUDAH void copy(MatrixDevice<Scalar> &output) {
		for (int i = 0; i < rows_; i++) {
			for (int j = 0; j < cols_; j++) {
				output(i, j) = buffer_[(i * cols_ + j) * stride_];
			}
		}
	}

	CUDAH Scalar *cellAddr(int row, int col) {
		if (row >= rows_ || col >= cols_ || row < 0 || col < 0)
			return NULL;

		return buffer_ + (row * cols_ + col) * stride_;
	}

	CUDAH Scalar *cellAddr(int index) {
		if (rows_ == 1 && index >= 0 && index < cols_) {
				return buffer_ + index * stride_;
		} else if (cols_ == 1 && index >= 0 && index < rows_) {
				return buffer_ + index * stride_;
		}

		return NULL;
	}

	//Assignment operator
	CUDAH MatrixDevice<Scalar>& operator=(const MatrixDevice<Scalar> &other) {
		rows_ = other.rows_;
		cols_ = other.cols_;
		stride_ = other.stride_;
		buffer_ = other.buffer_;

		return *this;
	}

	CUDAH MatrixDevice<Scalar>& operator=(MatrixDevice<Scalar> &&other) {
		rows_ = other.rows_;
		cols_ = other.cols_;
		stride_ = other.stride_;
		buffer_ = other.buffer_;

		return *this;
	}

	CUDAH Scalar& operator()(int row, int col) {
		return buffer_[(row * cols_ + col) * stride_];
	}

	CUDAH void set(int row, int col, Scalar val) {
		buffer_[(row * cols_ + col) * stride_] = val;
	}

	CUDAH Scalar& operator()(int index) {
		return buffer_[index * stride_];
	}

	CUDAH Scalar at(int row, int col) const {
		return buffer_[(row * cols_ + col) * stride_];
	}

	CUDAH bool operator*=(Scalar val) {
		for (int i = 0; i < rows_; i++) {
			for (int j = 0; j < cols_; j++) {
				buffer_[(i * cols_ + j) * stride_] *= val;
			}
		}

		return true;
	}

	CUDAH bool operator/=(Scalar val) {
		if (val == 0)
			return false;

		for (int i = 0; i < rows_ * cols_; i++) {
				buffer_[i * stride_] /= val;
		}

		return true;
	}

	CUDAH bool transpose(MatrixDevice<Scalar> &output) {
		if (rows_ != output.cols_ || cols_ != output.rows_)
			return false;

		for (int i = 0; i < rows_; i++) {
			for (int j = 0; j < cols_; j++) {
				output(j, i) = buffer_[(i * cols_ + j) * stride_];
			}
		}

		return true;
	}

	//Only applicable for 3x3 MatrixDevice or below
	CUDAH bool inverse(MatrixDevice<Scalar> &output) {
		if (rows_ != cols_ || rows_ == 0 || cols_ == 0)
			return false;

		if (rows_ == 1) {
			if (buffer_[0] != 0)
				output(0, 0) = 1 / buffer_[0];
			else
				return false;
		}

		if (rows_ == 2) {
			Scalar det = at(0, 0) * at(1, 1) - at(0, 1) * at(1, 0);

			if (det != 0) {
				output(0, 0) = at(1, 1) / det;
				output(0, 1) = - at(0, 1) / det;

				output(1, 0) = - at(1, 0) / det;
				output(1, 1) = at(0, 0) / det;
			} else
				return false;
		}

		if (rows_ == 3) {
			// Testing, replace at(i, j) by eij
			Scalar e00 = at(0, 0);
			Scalar e01 = at(0, 1);
			Scalar e02 = at(0, 2);
			Scalar e10 = at(1, 0);
			Scalar e11 = at(1, 1);
			Scalar e12 = at(1, 2);
			Scalar e20 = at(2, 0);
			Scalar e21 = at(2, 1);
			Scalar e22 = at(2, 2);
			// End

			Scalar det = e00 * e11 * e22 + e01 * e12 * e20 + e10 * e21 * e02
							- e02 * e11 * e20 - e01 * e10 * e22 - e00 * e12 * e21;

			if (det != 0) {
				Scalar idet = 1.0 / det;

				output(0, 0) = (e11 * e22 - e12 * e21) * idet;
				output(0, 1) = - (e01 * e22 - e02 * e21) * idet;
				output(0, 2) = (e01 * e12 - e02 * e11) * idet;

				output(1, 0) = - (e10 * e22 - e12 * e20) * idet;
				output(1, 1) = (e00 * e22 - e02 * e20) * idet;
				output(1, 2) = - (e00 * e12 - e02 * e10) * idet;

				output(2, 0) = (e10 * e21 - e11 * e20) * idet;
				output(2, 1) = - (e00 * e21 - e01 * e20) * idet;
				output(2, 2) = (e00 * e11 - e01 * e10) * idet;
			} else
				return false;
		}

		return true;
	}

	CUDAH MatrixDevice<Scalar> col(int index) {
		return MatrixDevice<Scalar>(rows_, 1, stride_ * cols_, buffer_ + index * stride_);
	}

	CUDAH MatrixDevice<Scalar> row(int index) {
		return MatrixDevice<Scalar>(1, cols_, stride_, buffer_ + index * cols_ * stride_);
	}

protected:
	Scalar *buffer_;
	int rows_, cols_, stride_;
};


using MatrixDF = class MatrixDevice<float>;
using MatrixDD = class MatrixDevice<double>;
}

#endif
